#ifdef VERTEX_SHADER
layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec3 vertexNormal;
layout(location = 2) in vec2 vertexTexCoord;

uniform mat4 mvp;
uniform mat4 modelMatrix;

out vec3 fragNormal;
#ifdef USE_TEXTURE
out vec2 fragTexCoords;
#endif
out vec3 fragWorldPosition;
void main()
{
  fragNormal = vertexNormal;
#ifdef USE_TEXTURE
  fragTexCoords = vertexTexCoord;
#endif
  fragWorldPosition = vec3(modelMatrix * vec4(vertexPosition, 1.0));

  gl_Position = mvp * vec4(vertexPosition, 1.0);
}
#endif

#ifdef FRAGMENT_SHADER

in vec3 fragNormal;
#ifdef USE_TEXTURE
uniform sampler2D albedoTexture;
in vec2 fragTexCoords;
#else
uniform vec4 color;
#endif
in vec3 fragWorldPosition;

out vec4 outputColor;

void main()
{
#ifdef USE_TEXTURE
    vec3 albedoColor = texture(albedoTexture, fragTexCoords).rgb;
    albedoColor = pow(albedoColor, vec3(2.2));
#else
    vec3 albedoColor = color.rgb;
#endif
    vec3 N = normalize(fragNormal);

    outputColor = vec4(1.0, 0.0, 0.0, 1.0);
}
#endif
