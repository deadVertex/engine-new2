#ifdef VERTEX_SHADER
layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec3 vertexNormal;
layout(location = 2) in vec3 vertexTangent;
layout(location = 3) in vec2 vertexTexCoord;
#ifdef ENABLE_SKINNING
layout(location = 4) in uint vertexBoneId;
#endif
#ifdef USE_MESH_INSTANCING
layout(location = 4) in mat4 modelMatrix;
#endif

#if defined(USE_MESH_INSTANCING) || defined(ENABLE_SKINNING)
uniform mat4 viewProjection;
#else
uniform mat4 mvp;
uniform mat4 modelMatrix;
#endif

#ifdef ENABLE_SKINNING
#define MAX_BONES 255
layout(std140) uniform BoneTransformsUniformBuffer
{
    mat4 transforms[MAX_BONES];
    uint length;
} boneTransformsBuffer;
#endif

out vec3 fragNormal;
out vec2 fragTexCoords;
#ifdef USE_NORMAL_MAP
out mat3 tbnMatrix;
#endif
out vec3 fragWorldPosition;



#ifdef WORLD_TEXTURE_COORDINATES
int CalculateMostSignificantAxis(vec3 v)
{
    int result = 0;
    result = (abs(v[1]) > abs(v[result])) ? 1 : result;
    result = (abs(v[2]) > abs(v[result])) ? 2 : result;
    return result;
}

vec2 CalculateWorldTextureCoordinates(vec3 worldPosition, vec3 normal)
{
    vec3 p = worldPosition;

    int axis = CalculateMostSignificantAxis(normal);
    if (axis == 0)
    {
        return vec2(-p.z * sign(normal.x), p.y);
    }
    else if (axis == 1)
    {
        return vec2(-p.x, p.z * sign(normal.y));
    }
    else if (axis == 2)
    {
        return vec2(p.x * sign(normal.z), p.y);
    }
    return vec2(0);
}
#endif
#
void main()
{
#ifdef ENABLE_SKINNING
  mat4 transform = boneTransformsBuffer.transforms[vertexBoneId];
#else
  mat4 transform = modelMatrix;
#endif
#
  vec3 normal = normalize(vec3(transform * vec4(vertexNormal, 0.0)));
  fragNormal = normal;
#ifdef USE_NORMAL_MAP
  vec3 tangent = normalize(vec3(transform * vec4(vertexTangent, 0.0)));
  tangent = normalize(tangent - dot(normal, tangent) * normal);

  vec3 bitangent = cross(normal, tangent);

  tbnMatrix = mat3(tangent, bitangent, normal);
#endif

  fragWorldPosition = vec3(transform * vec4(vertexPosition, 1.0));
#ifdef WORLD_TEXTURE_COORDINATES
  fragTexCoords = CalculateWorldTextureCoordinates(fragWorldPosition, normal);
#else
  fragTexCoords = vertexTexCoord;
#endif

#if defined(USE_MESH_INSTANCING) || defined(ENABLE_SKINNING)
  gl_Position = viewProjection * transform * vec4(vertexPosition, 1.0);
#else
  gl_Position = mvp * vec4(vertexPosition, 1.0);
#endif
}
#endif

#ifdef FRAGMENT_SHADER

in vec3 fragNormal;
in vec2 fragTexCoords;
uniform sampler2D albedoTexture;
uniform vec4 color;
uniform sampler2D normalMap;
in mat3 tbnMatrix;

in vec3 fragWorldPosition;

out vec4 outputColor;

void main()
{
#ifdef USE_TEXTURE
    vec3 albedoColor = texture(albedoTexture, fragTexCoords).rgb;
    albedoColor = pow(albedoColor, vec3(2.2));
#else
    vec3 albedoColor = color.rgb;
#endif

#ifdef USE_NORMAL_MAP
    vec3 s = texture(normalMap, fragTexCoords).rgb;
    s = pow(s, vec3(2.2)); // FIXME: WHY IS THE NORMALMAP SRGB ENCODED!!!!!!
    vec3 n = 255.0/128.0 * s - vec3(1.0);
    vec3 N = normalize(tbnMatrix * n);
#else
    vec3 N = normalize(fragNormal);
#endif

#ifdef VISUALIZE_NORMALS
    vec3 albedoColor = 0.5 * N + vec3(0.5);
    outputColor = vec4(albedoColor, 1.0);
#else
    vec3 totalLight =
        CalculateDiffuseLighting(albedoColor, fragWorldPosition, N, 0.0);

    outputColor = vec4(totalLight, 1.0);
#endif
}
#endif
