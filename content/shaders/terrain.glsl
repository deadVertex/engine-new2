#ifdef VERTEX_SHADER
layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec3 vertexNormal;
layout(location = 2) in vec3 vertexTangent;
layout(location = 3) in vec2 vertexTexCoord;

uniform mat4 mvp;
uniform mat4 modelMatrix;
uniform sampler2D heightmap;
uniform vec4 uvTransform = vec4(0, 0, 1, 1);

out vec3 fragNormal;
out vec2 fragTexCoords;
out vec3 fragWorldPosition;
out vec2 fragWorldTexCoords;
out mat3 tbnMatrix;
void main()
{
    vec2 worldTexCoords = vertexTexCoord * uvTransform.zw + uvTransform.xy;
    float heightSample = texture(heightmap, worldTexCoords).x;

    vec3 position = vertexPosition;
    position.y = heightSample;

    vec3 normal = normalize(vec3(modelMatrix * vec4(vertexNormal, 0.0)));
    vec3 tangent = normalize(vec3(modelMatrix * vec4(vertexTangent, 0.0)));
    tangent = tangent - dot(normal, tangent) * normal;

    vec3 bitangent = normalize(cross(normal, tangent));

    tbnMatrix = mat3(tangent, bitangent, normal);

    fragNormal = normal;
    fragTexCoords = vertexTexCoord;
    fragWorldTexCoords = worldTexCoords;
    fragWorldPosition = vec3(modelMatrix * vec4(position, 1.0));

    gl_Position = mvp * vec4(position, 1.0);
}
#endif

#ifdef FRAGMENT_SHADER

in vec3 fragNormal;
uniform sampler2D grassTexture;
uniform sampler2D rockTexture;
uniform sampler2D sandTexture;
uniform sampler2D splatMap;
uniform sampler2D heightmap;
uniform sampler2D normalMap;
in vec2 fragTexCoords;
in vec3 fragWorldPosition;
in vec2 fragWorldTexCoords;
in mat3 tbnMatrix;

out vec4 outputColor;

vec3 ApplyFog(vec3 surfaceColor, float distance)
{
    float b = 0.0004;
    float fogAmount = 1.0 - exp(-distance * b);
    vec3 fogColor = vec3(0.48, 0.57, 0.64);
    return mix(surfaceColor, fogColor, fogAmount);
}

void main()
{
#ifdef DEBUG_WIREFRAME
    outputColor = vec4(0.5, 1.0, 0.4, 1.0);
#else
    ivec2 textureDim = textureSize(heightmap, 0);
    vec2 inc = 1.0 / textureDim;
    float t0 = texture(heightmap, fragWorldTexCoords).x;
    float t1 = texture(heightmap, fragWorldTexCoords + vec2(inc.x, 0)).x;
    float t2 = texture(heightmap, fragWorldTexCoords + vec2(0, inc.y)).x;

    // Can probably still use fragTexCoords here
    vec3 p0 = vec3(fragWorldTexCoords.x, fragWorldTexCoords.y, t0);
    vec3 p1 = vec3(fragWorldTexCoords.x + inc.x, fragWorldTexCoords.y, t1);
    vec3 p2 = vec3(fragWorldTexCoords.x, fragWorldTexCoords.y + inc.y, t2);

    vec3 tangent = p1 - p0;
    vec3 bitangent = p2 - p0;
    vec3 normal = normalize(cross(tangent, bitangent));

#define TANGENT_SPACE_TO_WORLD_SPACE
#ifdef TANGENT_SPACE_TO_WORLD_SPACE
    normal = normalize(tbnMatrix * normal);
#endif

    vec3 N = normalize(mix(fragNormal, normal, 0.5));

//#define VISUALIZE_NORMALS
#ifdef VISUALIZE_NORMALS
    vec3 albedoColor = 0.5 * N + vec3(0.5);
    outputColor = vec4(albedoColor, 1.0);
#else

    float r = texture(splatMap, fragWorldTexCoords * 1.0).r;
    float g = texture(splatMap, fragWorldTexCoords * 1.0).g;

    // TODO: Use regular fragTexCoords here
    vec3 sand = texture(sandTexture, fragWorldTexCoords * 500.0f).rgb;
    vec3 grass = texture(grassTexture, fragWorldTexCoords * 500.0f).rgb;
    vec3 rock = texture(rockTexture, fragWorldTexCoords * 500.0f).rgb;
    sand = pow(sand, vec3(2.2));
    grass = pow(grass, vec3(2.2));
    rock = pow(rock, vec3(2.2));
    vec3 albedoColor = grass;
    //albedoColor = mix(albedoColor, grass, r);
    albedoColor = mix(albedoColor, rock, g);

    vec3 totalLight =
        CalculateDiffuseLighting(albedoColor, fragWorldPosition, N, 0.0);

    float distance = length(fragWorldPosition - cameraPosition);
    totalLight = ApplyFog(totalLight, distance);
    outputColor = vec4(totalLight, 1.0);
#endif
#endif
}
#endif
