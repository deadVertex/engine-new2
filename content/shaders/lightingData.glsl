#ifdef FRAGMENT_SHADER
// NOTE: Must match values defined in game.h
#define MAX_DIRECTIONAL_LIGHTS 4
#define MAX_POINT_LIGHTS 32
#define MAX_SPOT_LIGHTS 32

struct DirectionalLight
{
    vec4 direction;
    vec4 color;
};

struct PointLight
{
    vec4 position;
    vec4 color;
    vec4 attenuation;
};

struct SpotLight
{
    vec4 position;
    vec4 direction;
    vec4 color;
    vec4 attenuation[2];
};

layout(std140) uniform LightingDataUniformBuffer
{
    DirectionalLight directionalLights[MAX_DIRECTIONAL_LIGHTS];
    PointLight pointLights[MAX_POINT_LIGHTS];
    SpotLight spotLights[MAX_SPOT_LIGHTS];
    vec4 ambientLightColor;
    uint directionalLightCount;
    uint pointLightCount;
    uint spotLightCount;
} lightingData;

uniform mat4 inLightViewProjection;
uniform sampler2DShadow inShadowMap;
uniform vec3 cameraPosition;

// TODO: Look at using w coordinate to fade out shadow
float CalculateShadowAmount(
    vec3 position, mat4 lightViewProjection, sampler2DShadow shadowMap)
{
    vec4 p = lightViewProjection * vec4(position, 1);
    p /= p.w;

    vec3 coords = (p.xyz) * 0.5 + vec3(0.5);
    coords.z -= 0.005;

    return texture(shadowMap, coords, 0.0);
}

float PointLightWindowingFunction(float r, float rmax)
{
    float a = max(1 - pow(r / rmax, 4), 0);
    float result = a * a;
    return result;
}

float PointLightDistanceFunction(float r, float r0, float rmin, float rmax)
{
    float t = r0 / max(r, rmin);
    float t2 = t * t;
    float result = t2 * PointLightWindowingFunction(r, rmax);
    return result;
}

float SpotLightDirectionFunction(vec3 D, vec3 L, float cosThetaU, float cosThetaP)
{
    float cosThetaS = dot(D, -L);
    float spotLightT = clamp((cosThetaS - cosThetaU) / (cosThetaP - cosThetaU), 0.0, 1.0);
    float result = spotLightT * spotLightT;
    return result;
}

vec3 CalculateDiffuseLighting(vec3 surfaceColor, vec3 surfacePosition, vec3 surfaceNormal, float shininess)
{
    vec3 totalLight = lightingData.ambientLightColor.rgb * surfaceColor;

    for (uint i = 0u; i < lightingData.directionalLightCount; ++i)
    {
        DirectionalLight directionalLight = lightingData.directionalLights[i];
        vec3 L = -directionalLight.direction.xyz;
        vec3 C = directionalLight.color.rgb;

        float shadow = CalculateShadowAmount(
            surfacePosition, inLightViewProjection, inShadowMap);
        totalLight += shadow * max(dot(surfaceNormal,L), 0.0) * C * surfaceColor;

        vec3 V = normalize(cameraPosition - surfacePosition);
        vec3 R = reflect(L, surfaceNormal);
        vec3 H = normalize(L + V);

        float specular = shininess > 0
                             ? pow(max(dot(H, surfaceNormal), 0.0), shininess)
                             : 0.0;
        vec3 specularColor = vec3(1);

        totalLight += shadow * specular * specularColor * C;
    }

    for (uint i = 0u; i < lightingData.pointLightCount; ++i)
    {
        PointLight pointLight = lightingData.pointLights[i];
        vec3 P = pointLight.position.xyz;
        vec3 C = pointLight.color.rgb;
        vec3 L = normalize(P - surfacePosition);

        float r = length(P - surfacePosition);
        float r0 = pointLight.attenuation.x;
        float rmin = pointLight.attenuation.y;
        float rmax = pointLight.attenuation.z;

        totalLight += max(dot(surfaceNormal, L), 0.0) * C *
            PointLightDistanceFunction(r, r0, rmin, rmax) * surfaceColor;
    }

    for (uint i = 0u; i < lightingData.spotLightCount; ++i)
    {
        SpotLight spotLight = lightingData.spotLights[i];
        vec3 P = spotLight.position.xyz;
        vec3 C = spotLight.color.rgb;
        vec3 D = spotLight.direction.xyz;
        vec3 L = normalize(P - surfacePosition);

        float r = length(P - surfacePosition);
        float r0 = spotLight.attenuation[0].x;
        float rmin = spotLight.attenuation[0].y;
        float rmax = spotLight.attenuation[0].z;

        float cosThetaU = spotLight.attenuation[1].x;
        float cosThetaP = spotLight.attenuation[1].y;

        totalLight += max(dot(surfaceNormal, L), 0.0) * C * 
            PointLightDistanceFunction(r, r0, rmin, rmax) *
            SpotLightDirectionFunction(D, L, cosThetaU, cosThetaP) * surfaceColor;
    }

    return totalLight;
}

#endif
