#pragma once

struct Win32_SoundOutput
{
    u32 samplesPerSecond;
    u32 bytesPerSample;
    u32 bufferSize;
    u32 runningSampleIndex;
    u32 safetyBytes;
    b32 isValid;
    i16 *samples;
};

struct Win32_ReplayBuffer
{
    HANDLE fileHandle;
    HANDLE memoryMap;
    void *memoryBlock;
};

#define WIN32_STATE_FILE_NAME_LENGTH MAX_PATH
struct Win32_State
{
    HANDLE recordingFile;
    HANDLE playbackFile;

    b32 isRecordingInput;
    b32 isPlayingBackInput;

    void *gameMemory;
    u64 gameMemorySize;

    Win32_ReplayBuffer replayBuffer;

    char exeFilePath[WIN32_STATE_FILE_NAME_LENGTH];
    char *lastExeFilePathSlash;

    HMODULE gameCodeLibrary;
    FILETIME gameCodeLastWriteTime;

    char gameCodeDllFullPath[MAX_PATH];
    char tempGameDllFullPath[MAX_PATH];
};

#define WIN32_WINDOW_CLASS_NAME "EngineWindowClass"
#define WIN32_WINDOW_NAME "Engine New 2"
#define WIN32_TARGET_TIMER_RESOLUTION 1

#define WIN32_AUDIO_SAMPLES_PER_SECOND 48000
#define WIN32_AUDIO_BYTES_PER_SAMPLE (2 * sizeof(i16)) // 16-bit stereo

// 1 Second long audio buffer
#define WIN32_AUDIO_BUFFER_SIZE                                                \
    (WIN32_AUDIO_SAMPLES_PER_SECOND * WIN32_AUDIO_BYTES_PER_SAMPLE)

#define WIN32_TARGET_UPDATE_RATE 60

#define WIN32_SOURCE_DLL_NAME "game.dll"
#define WIN32_TEMP_DLL_NAME "game_temp.dll"
#define WIN32_GAME_CODE_LOCK "lock.tmp"

#define WIN32_REPLAY_STATE_FILENAME "replay.state"
#define WIN32_REPLAY_INPUT_FILENAME "replay.input"

struct Win32_GameCode
{
    HMODULE library;
    FILETIME lastWriteTime;
    GameUpdateFunction *update;
    GameServerUpdateFunction *serverUpdate;
    GameGetSoundSamplesFunction *getSoundSamples;

    b32 isValid;
};

