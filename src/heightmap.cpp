inline f32 SampleHeightMap(HeightMap heightMap, u32 x, u32 y)
{
    x = MinU(x, heightMap.width);
    y = MinU(y, heightMap.height);
    //Assert(x < heightMap.width);
    //Assert(y < heightMap.height);
    return heightMap.values[y * heightMap.width + x];
}

inline f32 HeightMapSampleNearest(HeightMap heightMap, f32 u, f32 v)
{
    Assert(u >= 0.0f && u <= 1.0f);
    Assert(v >= 0.0f && v <= 1.0f);
    f32 sampleU = u * (heightMap.width - 1);
    f32 sampleV = v * (heightMap.height - 1);
    i32 x = (i32)Ceil(sampleU);
    i32 y = (i32)Ceil(sampleV);

    return SampleHeightMap(heightMap, x, y);
}

inline f32 HeightMapSampleBilinear(HeightMap heightMap, f32 u, f32 v)
{
    f32 s = u * heightMap.width - 0.5f;
    f32 t = v * heightMap.height - 0.5f;

    i32 x0 = (i32)Floor(s);
    i32 y0 = (i32)Floor(t);
    i32 x1 = x0 + 1;
    i32 y1 = y0 + 1;

    f32 fx = s - x0;
    f32 fy = t - y0;

    f32 samples[4];
    samples[0] = SampleHeightMap(heightMap, x0, y0);
    samples[1] = SampleHeightMap(heightMap, x1, y0);
    samples[2] = SampleHeightMap(heightMap, x0, y1);
    samples[3] = SampleHeightMap(heightMap, x1, y1);

    f32 p = Lerp(samples[0], samples[1], fx);
    f32 q = Lerp(samples[2], samples[3], fx);

    f32 result = Lerp(p, q, fy);
    return result;
}
