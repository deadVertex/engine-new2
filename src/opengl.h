#pragma once

struct OpenGL_Info
{
    const char *vendor;
    const char *renderer;
    const char *version;
    const char *shadingLanguageVersion;
    const char *extensions;
};

struct OpenGL_Mesh
{
    u32 vertexBuffer;
    u32 indexBuffer;
    u32 vertexArray;
    u32 indexCount;
    u32 modelMatrixBuffer;
    u32 maxInstanceCount;
};

struct OpenGL_VertexBuffer
{
    u32 vbo;
    u32 vao;
    u32 layout;
    u32 capacity;
};

struct OpenGL_Shader
{
    GLuint program;
    i32 uniformLocations[MAX_UNIFORMS];
    b32 depthTestEnabled;
    b32 alphaBlendingEnabled;
    b32 lineModeEnabled;
    u32 faceCullingMode;
};

struct OpenGL_UniformBuffer
{
    u32 ubo;
    u32 capacity;
};

struct OpenGL_RenderTarget
{
    u32 fbo;
    u32 texture;
    u32 depthTexture;
    u32 width;
    u32 height;
};

struct OpenGL_Texture
{
    u32 handle;
    u32 type;
};
