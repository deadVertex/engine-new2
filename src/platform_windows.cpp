global Win32_State g_PlatformState;

inline void Win32_LogMessage(const char *fmt, ...)
{
    char buffer[256];
    va_list args;
    va_start(args, fmt);
    vsnprintf(buffer, sizeof(buffer), fmt, args);
    va_end(args);
    OutputDebugString(buffer);
    OutputDebugString("\n");
}

internal void Win32_BuildExeFilePath(
    Win32_State *state, char *fileName, char *dest, u32 destLength)
{
    ConcatStrings(state->exeFilePath,
        SafeTruncateU64ToU32(
            state->lastExeFilePathSlash - state->exeFilePath),
        fileName, StringLength(fileName), dest, destLength);
}

internal void Win32_GetExeFilePath(Win32_State *state)
{
    GetModuleFileName(
        0, state->exeFilePath, sizeof(state->exeFilePath));

    state->lastExeFilePathSlash = state->exeFilePath;
    for (char *cursor = state->exeFilePath; *cursor; ++cursor)
    {
        if (*cursor == '\\')
        {
            state->lastExeFilePathSlash = cursor + 1;
        }
    }
}

inline FILETIME Win32_GetLastWriteTime(const char *filename)
{
    FILETIME lastWriteTime = {};

    WIN32_FILE_ATTRIBUTE_DATA data;
    if (GetFileAttributesEx(filename, GetFileExInfoStandard, &data))
    {
        lastWriteTime = data.ftLastWriteTime;
    }
    return lastWriteTime;
}

internal b32 IsLockFileActive(const char *lockFileName)
{
    WIN32_FILE_ATTRIBUTE_DATA ignored;
    if (!GetFileAttributesEx(lockFileName, GetFileExInfoStandard, &ignored))
        return false;

    return true;
}

internal GameCode Win32_LoadGameCode(Win32_State *state,
    const char *sourceDllName, const char *tempDllName)
{
    GameCode result = {};

    state->gameCodeLastWriteTime = Win32_GetLastWriteTime(sourceDllName);
    CopyFile(sourceDllName, tempDllName, FALSE);

    state->gameCodeLibrary = LoadLibraryA(tempDllName);
    if (state->gameCodeLibrary)
    {
        result.update = (GameUpdateFunction *)GetProcAddress(
            state->gameCodeLibrary, "GameUpdate");
        result.serverUpdate = (GameServerUpdateFunction *)GetProcAddress(
            state->gameCodeLibrary, "GameServerUpdate");
        result.getSoundSamples = (GameGetSoundSamplesFunction *)GetProcAddress(
            state->gameCodeLibrary, "GameGetSoundSamples");
    }

    return result;
}

internal void Win32_UnloadGameCode(Win32_State *state)
{
    if (state->gameCodeLibrary)
    {
        FreeLibrary(state->gameCodeLibrary);
    }
}

DebugReadEntireFile(Win32_ReadEntireFile)
{
    DebugReadFileResult result = {};
    HANDLE file = CreateFileA(path, GENERIC_READ, FILE_SHARE_READ, 0,
                              OPEN_EXISTING, 0, 0);
    if (file != INVALID_HANDLE_VALUE)
    {
        LARGE_INTEGER tempSize;
        if (GetFileSizeEx(file, &tempSize))
        {
            result.size = SafeTruncateU64ToU32(tempSize.QuadPart);
            result.contents = VirtualAlloc(
                NULL, result.size, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);

            if (result.contents)
            {
                DWORD bytesRead;
                if (!ReadFile(file, result.contents, result.size, &bytesRead,
                              0) ||
                    (result.size != bytesRead))
                {
                    LogMessage("Failed to read file %s", path);
                    VirtualFree(result.contents, 0, MEM_RELEASE);
                    result.contents = NULL;
                    result.size = 0;
                }
            }
            else
            {
                LogMessage("Failed to allocate %d bytes for file %s",
                          result.size, path);
            }
        }
        else
        {
            LogMessage("Failed to read file size for file %s", path);
        }
        CloseHandle(file);
    }
    else
    {
        LogMessage("Failed to open file %s", path);
    }
    return result;
}

DebugWriteEntireFile(Win32_WriteEntireFile)
{
    b32 result = false;
    HANDLE file = CreateFileA(path, GENERIC_WRITE, FILE_SHARE_WRITE, NULL,
                              CREATE_ALWAYS, 0, 0);
    if (file != INVALID_HANDLE_VALUE)
    {
        DWORD bytesWritten = 0;
        if (WriteFile(file, data, length, &bytesWritten, NULL))
        {
            result = true;
        }
        else
        {
            LogMessage("Failed to write %u bytes to file %s", length, path);
        }

        CloseHandle(file);
    }
    else
    {
        LogMessage("Failed to open file %s", path);
    }

    return result;
}

DebugFreeFileMemory(Win32_FreeFileMemory)
{
    VirtualFree(fileResult.contents, 0, MEM_RELEASE);
}

internal void Win32_InitializeGameMemory(GameMemory *memory,
    u64 persistentStorageSize, u64 temporaryStorageSize,
    size_t storageLocation = 0)
{
    memory->persistentStorageSize = persistentStorageSize;
    memory->temporaryStorageSize = temporaryStorageSize;

    u64 totalStorageSize =
        memory->persistentStorageSize + memory->temporaryStorageSize;

    void *gameMemory = VirtualAlloc((void *)storageLocation, totalStorageSize,
        MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
    Assert(gameMemory);

    memory->persistentStorage = gameMemory;
    memory->temporaryStorage =
        (u8 *)memory->persistentStorage + memory->persistentStorageSize;
    memory->readEntireFile = &Win32_ReadEntireFile;
    memory->writeEntireFile = &Win32_WriteEntireFile;
    memory->freeFileMemory = &Win32_FreeFileMemory;
    memory->logMessage = &Win32_LogMessage;
    memory->setCursorMode = &SetCursorMode;
}

internal b32 Win32_Init(GameMemory *memory, GameCode *gameCode)
{
    LogMessage = &Win32_LogMessage;

    TIMECAPS timeCaps = {};
    if (timeGetDevCaps(&timeCaps, sizeof(timeCaps)) == MMSYSERR_NOERROR)
    {
        if (WIN32_TARGET_TIMER_RESOLUTION > timeCaps.wPeriodMin)
        {
            LogMessage("Device does not support target timer resolution of %u "
                       "ms, only supports min period of %u ms",
                WIN32_TARGET_TIMER_RESOLUTION, timeCaps.wPeriodMin);
        }
    }
    else
    {
        LogMessage(
            "Failed to determine device timer capabilities. ErrorCode: %u",
            GetLastError());
    }

    b32 isSleepGranular = (timeBeginPeriod(WIN32_TARGET_TIMER_RESOLUTION) == TIMERR_NOERROR);
    if (!isSleepGranular)
    {
        LogMessage("Insufficent Win32 timer resolution, unable to run game reliably.");
        return false;
    }

    // TODO: Not sure about this version
    WSADATA wsaData;
    if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
    {
        LogMessage("Failed to initialize WinSock");
        return false;
    }

    Win32_GetExeFilePath(&g_PlatformState);

    Win32_BuildExeFilePath(&g_PlatformState, WIN32_SOURCE_DLL_NAME,
        g_PlatformState.gameCodeDllFullPath,
        sizeof(g_PlatformState.gameCodeDllFullPath));

    Win32_BuildExeFilePath(&g_PlatformState, WIN32_TEMP_DLL_NAME,
        g_PlatformState.tempGameDllFullPath,
        sizeof(g_PlatformState.tempGameDllFullPath));

    *gameCode = Win32_LoadGameCode(&g_PlatformState,
        g_PlatformState.gameCodeDllFullPath,
        g_PlatformState.tempGameDllFullPath);
    Assert(gameCode->update && gameCode->serverUpdate && gameCode->getSoundSamples);

    size_t storageLocation = 0;
#ifdef DEBUG_FIXED_ADDRESSES
    storageLocation = Terabytes(2);
#endif

    Win32_InitializeGameMemory(memory, MAX_GAME_PERSISTENT_MEMORY,
        MAX_GAME_TEMPORARY_MEMORY, storageLocation);

    return true;
}

internal b32 Win32_HandleGameCodeReload(GameCode *gameCode)
{
    b32 result = false;
    FILETIME newDllWriteTime = Win32_GetLastWriteTime(WIN32_SOURCE_DLL_NAME);
    if (CompareFileTime(
            &newDllWriteTime, &g_PlatformState.gameCodeLastWriteTime) != 0)
    {
        if (!IsLockFileActive(WIN32_GAME_CODE_LOCK))
        {
            Win32_UnloadGameCode(&g_PlatformState);
            ClearToZero(gameCode, sizeof(GameCode));
            *gameCode = Win32_LoadGameCode(&g_PlatformState,
                g_PlatformState.gameCodeDllFullPath,
                g_PlatformState.tempGameDllFullPath);

            result = (gameCode->update && gameCode->serverUpdate &&
                      gameCode->getSoundSamples);
            LogMessage("Game code reloaded");
        }
    }

    return result;
}
