#include "game.h"

#include <stdio.h>

#ifdef PLATFORM_WINDOWS
#define WIN32_LEAN_AND_MEAN
#include <Ws2tcpip.h>
#include <Windows.h>
#include <TimeAPI.h>
#endif

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <SDL2/SDL.h>

internal DebugSetCursorMode(SetCursorMode);
#ifdef PLATFORM_WINDOWS
#include "windows_main.h"
#include "platform_windows.cpp"
#endif

#ifdef PLATFORM_LINUX
#include "platform_linux.cpp"
#endif

internal u64 Platform_GetMilliseconds();
#include "socket_layer.cpp"
#include "opengl.cpp"

global b32 g_IsRunning = true;
global b32 g_IsClientRunning;
global b32 g_IsServerRunning;
global u32 g_WindowMode = WindowMode_Windowed;
global b32 g_EnableVSync = false;
global u32 g_FrameRateCap = 145;
global u32 g_FrameBufferWidth = 1024;
global u32 g_FrameBufferHeight = 768;
global u32 g_MonitorHeight;
global u32 g_MonitorWidth;
global u32 g_MonitorRefreshRate;
global GameMemory g_ServerMemory;

GLFWwindow *g_Window = NULL;
global u32 g_CursorMode = CursorMode_Unlocked;
global double g_PrevMousePosX;
global double g_PrevMousePosY;

internal u64 Platform_GetMilliseconds()
{
    u64 milliseconds = (u64)(glfwGetTime() * 1000.0);
    return milliseconds;
}

internal void Platform_ErrorCallback(int error, const char *description)
{
    LogMessage("GLFW ERROR: %s", description);
}

internal DebugSetCursorMode(SetCursorMode)
{
    if (mode == CursorMode_Unlocked)
    {
        glfwSetInputMode(g_Window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
    }
    else if (mode == CursorMode_Locked)
    {
        glfwSetInputMode(g_Window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
        // Not supported in our current version of GLFW on 18.04
        //if (glfwRawMouseMotionSupported())
            //glfwSetInputMode(g_Window, GLFW_RAW_MOUSE_MOTION, GLFW_TRUE);
    }
	else
	{
		InvalidCodePath();
	}

    g_CursorMode = mode;
}

internal void WindowCloseCallback(GLFWwindow *window) { g_IsRunning = false; }

internal void MouseButtonCallback(
    GLFWwindow *window, int button, int action, int mods)
{
    GameInput *input =
            (GameInput *)glfwGetWindowUserPointer(window);

    if (button == GLFW_MOUSE_BUTTON_LEFT)
    {
        input->buttonStates[KEY_MOUSE_BUTTON_LEFT].isDown = (action == GLFW_PRESS);
    }
    if (button == GLFW_MOUSE_BUTTON_MIDDLE)
    {
        input->buttonStates[KEY_MOUSE_BUTTON_MIDDLE].isDown = (action == GLFW_PRESS);
    }
    if (button == GLFW_MOUSE_BUTTON_RIGHT)
    {
        input->buttonStates[KEY_MOUSE_BUTTON_RIGHT].isDown = (action == GLFW_PRESS);
    }
}
#define KEY_HELPER(NAME)                                                       \
    case GLFW_KEY_##NAME:                                                      \
        return KEY_##NAME;
internal u8 ConvertKey(int key)
{
    if (key >= GLFW_KEY_SPACE && key <= GLFW_KEY_GRAVE_ACCENT)
    {
        return (u8)key;
    }
    switch (key)
    {
        KEY_HELPER(BACKSPACE);
        KEY_HELPER(TAB);
        KEY_HELPER(INSERT);
        KEY_HELPER(HOME);
        KEY_HELPER(PAGE_UP);
    // Can't use KEY_HELPER( DELETE ) as windows has a #define for DELETE
    case GLFW_KEY_DELETE:
        return KEY_DELETE;
        KEY_HELPER(END);
        KEY_HELPER(PAGE_DOWN);
        KEY_HELPER(ENTER);

        KEY_HELPER(LEFT_SHIFT);
    case GLFW_KEY_LEFT_CONTROL:
        return KEY_LEFT_CTRL;
        KEY_HELPER(LEFT_ALT);
        KEY_HELPER(RIGHT_SHIFT);
    case GLFW_KEY_RIGHT_CONTROL:
        return KEY_RIGHT_CTRL;
        KEY_HELPER(RIGHT_ALT);

        KEY_HELPER(LEFT);
        KEY_HELPER(RIGHT);
        KEY_HELPER(UP);
        KEY_HELPER(DOWN);

        KEY_HELPER(ESCAPE);

        KEY_HELPER(F1);
        KEY_HELPER(F2);
        KEY_HELPER(F3);
        KEY_HELPER(F4);
        KEY_HELPER(F5);
        KEY_HELPER(F6);
        KEY_HELPER(F7);
        KEY_HELPER(F8);
        KEY_HELPER(F9);
        KEY_HELPER(F10);
        KEY_HELPER(F11);
        KEY_HELPER(F12);
    case GLFW_KEY_KP_0:
        return KEY_NUM0;
    case GLFW_KEY_KP_1:
        return KEY_NUM1;
    case GLFW_KEY_KP_2:
        return KEY_NUM2;
    case GLFW_KEY_KP_3:
        return KEY_NUM3;
    case GLFW_KEY_KP_4:
        return KEY_NUM4;
    case GLFW_KEY_KP_5:
        return KEY_NUM5;
    case GLFW_KEY_KP_6:
        return KEY_NUM6;
    case GLFW_KEY_KP_7:
        return KEY_NUM7;
    case GLFW_KEY_KP_8:
        return KEY_NUM8;
    case GLFW_KEY_KP_9:
        return KEY_NUM9;
    case GLFW_KEY_KP_DECIMAL:
        return KEY_NUM_DECIMAL;
    case GLFW_KEY_KP_DIVIDE:
        return KEY_NUM_DIVIDE;
    case GLFW_KEY_KP_MULTIPLY:
        return KEY_NUM_MULTIPLY;
    case GLFW_KEY_KP_SUBTRACT:
        return KEY_NUM_MINUS;
    case GLFW_KEY_KP_ADD:
        return KEY_NUM_PLUS;
    case GLFW_KEY_KP_ENTER:
        return KEY_NUM_ENTER;
    }
    return KEY_UNKNOWN;
}

internal void KeyCallback(GLFWwindow *window, int glfwKey, int scancode,
                          int action, int mods)
{
    GameInput *input =
            (GameInput *)glfwGetWindowUserPointer(window);

    u8 key = ConvertKey(glfwKey);
    if (key < MAX_KEYS)
    {
        if (action == GLFW_PRESS)
        {
            input->buttonStates[key].isDown = true;
        }
        else if (action == GLFW_RELEASE)
        {
            input->buttonStates[key].isDown = false;
        }
        // else: ignore key repeat messages
    }
}

internal void CharacterCallback(GLFWwindow *window, u32 codepoint)
{
    GameInput *input =
            (GameInput *)glfwGetWindowUserPointer(window);

    if (input->characterCount < ArrayCount(input->characters))
    {
        input->characters[input->characterCount++] = codepoint;
    }
}

internal bool Platform_InitOpenGL()
{
    // Print out information about the retrieved OpenGL context
    OpenGL_Info info = OpenGL_GetInfo();
    LogMessage("OpenGL Renderer\n\tVendor: %s\n\tGPU: %s\n\tVersion: "
            "%s\n\tShading Language Version: %s\n",
            info.vendor, info.renderer, info.version,
            info.shadingLanguageVersion);

    GLenum result = glewInit();
    if (result != GLEW_OK)
    {
        LogMessage("%s\n", glewGetErrorString(result));
		return false;
    }

    if (!OpenGL_Init())
    {
        LogMessage("OpenGL_Init failed");
        return false;
    }

	return true;
}

internal void InputBeginFrame(GameInput *input)
{
    for (u32 buttonIndex = 0; buttonIndex < ArrayCount(input->buttonStates);
         ++buttonIndex)
    {
        input->buttonStates[buttonIndex].wasDown =
            input->buttonStates[buttonIndex].isDown;
    }

    input->characterCount = 0;
}

internal void ProcessMouseInput(GameInput *input)
{
    double mousePosX, mousePosY;
    glfwGetCursorPos(g_Window, &mousePosX, &mousePosY);
    if (g_CursorMode == CursorMode_Unlocked)
    {
        input->mousePosX = (i32)mousePosX;
        input->mousePosY = (i32)mousePosY;
    }
    else
    {
        // Generate relative mouse motion values, ideally we would use raw
        // input for this as these values are still scaled by the window
        // manager sensitivity
        input->mouseRelPosX = (i32)(mousePosX - g_PrevMousePosX);
        input->mouseRelPosY = (i32)(mousePosY - g_PrevMousePosY);
    }
    g_PrevMousePosX = mousePosX;
    g_PrevMousePosY = mousePosY;
}


internal void StartServer()
{
    if (!g_IsServerRunning)
    {
        if (Sock_InitServer())
        {

            size_t storageLocation = 0;
#ifdef DEBUG_FIXED_ADDRESSES
            storageLocation = Terabytes(4);
#endif

#ifdef PLATFORM_WINDOWS
            Win32_InitializeGameMemory(&g_ServerMemory,
                MAX_GAME_PERSISTENT_MEMORY, MAX_GAME_TEMPORARY_MEMORY,
                storageLocation);
#endif

#ifdef PLATFORM_LINUX
            Linux_InitializeGameMemory(&g_ServerMemory,
                MAX_GAME_PERSISTENT_MEMORY, MAX_GAME_TEMPORARY_MEMORY,
                storageLocation);
#endif
            g_IsServerRunning = true;
        }
        else
        {
            LogMessage("Failed to start server");
        }
    }
}

internal b32 StartClient(const char *address)
{
    if (!g_IsClientRunning)
    {
        if (Sock_InitClient(address))
        {
            g_IsClientRunning = true;
        }
        else
        {
            LogMessage("Failed to start client");
        }
    }

    return g_IsClientRunning;
}

internal DebugGetPlatformData(GetPlatformData)
{
    data->isRunning = g_IsRunning;
    data->isClientRunning = g_IsClientRunning;
    data->isServerRunning = g_IsServerRunning;
    data->windowMode = g_WindowMode;
    data->vSyncEnabled = g_EnableVSync;
    data->frameRateCap = g_FrameRateCap;
    data->frameBufferWidth = g_FrameBufferWidth;
    data->frameBufferHeight = g_FrameBufferHeight;
}

internal void UpdateWindowMode(u32 windowMode)
{
    switch (windowMode)
    {
    case WindowMode_Fullscreen:
    {
        glfwSetWindowMonitor(g_Window, glfwGetPrimaryMonitor(), 0, 0,
            g_FrameBufferWidth, g_FrameBufferHeight, GLFW_DONT_CARE);
        break;
    }
    case WindowMode_Windowed:
    {
        glfwSetWindowMonitor(g_Window, NULL, 5, 50, g_FrameBufferWidth,
            g_FrameBufferHeight, GLFW_DONT_CARE);
        // FIXME: Update to GLFW 3.3 on Linux
        //glfwSetWindowAttrib(g_Window, GLFW_DECORATED, GLFW_TRUE);
        //glfwSetWindowAttrib(g_Window, GLFW_RESIZABLE, GLFW_TRUE);
        break;
    }
    case WindowMode_WindowedBorderless:
    {
        g_FrameBufferWidth = g_MonitorWidth;
        g_FrameBufferHeight = g_MonitorHeight;
        glfwSetWindowMonitor(g_Window, glfwGetPrimaryMonitor(), 0, 0,
            g_FrameBufferWidth, g_FrameBufferHeight, g_MonitorRefreshRate);
        break;
    }
    default:
        InvalidCodePath();
        break;
    }
    Assert(g_Window);
}

internal DebugSetPlatformData(SetPlatformData)
{
    g_IsRunning = data->isRunning;

    if (data->vSyncEnabled != g_EnableVSync)
    {
        i32 interval = data->vSyncEnabled ? 1 : 0;
        glfwSwapInterval(interval);
        g_EnableVSync = data->vSyncEnabled;
    }

    g_FrameRateCap = data->frameRateCap;

    if ((data->frameBufferWidth != g_FrameBufferWidth) ||
        (data->frameBufferHeight != g_FrameBufferHeight))
    {
        glfwSetWindowSize(
            g_Window, data->frameBufferWidth, data->frameBufferHeight);
        g_FrameBufferWidth = data->frameBufferWidth;
        g_FrameBufferHeight = data->frameBufferHeight;
    }

    if (data->windowMode != g_WindowMode)
    {
        UpdateWindowMode(data->windowMode);
        g_WindowMode = data->windowMode;
    }
}

struct SoundOutput
{
    SDL_AudioDeviceID audioDevice;
    u32 samplesPerSecond;
    u32 bytesPerSample;
    u32 runningSampleIndex;
    u32 sampleCount; // For single channel
};

internal void OutputSound(
    SoundOutput *soundOutput, GameSoundOutputBuffer *soundBuffer)
{
    SDL_QueueAudio(soundOutput->audioDevice, soundBuffer->samples,
        soundBuffer->sampleCount * soundOutput->bytesPerSample);
}

internal b32 InitAudio(SoundOutput *soundOutput, u32 samplesPerSecond, u32 sampleCount)
{
    SDL_Init(SDL_INIT_AUDIO);

    SDL_AudioSpec requested = {};
    requested.freq = samplesPerSecond;
    requested.format = AUDIO_S16LSB;
    requested.channels = 2;
    requested.samples = SafeTruncateU32ToU16(sampleCount);

    SDL_AudioSpec available = {};
    soundOutput->audioDevice = SDL_OpenAudioDevice(NULL, false, &requested, &available, 0);
    if (!soundOutput->audioDevice)
    {
        LogMessage("Failed to open audio device: %s", SDL_GetError());
        return false;
    }
    else if (available.format != requested.format)
    {
        LogMessage("Signed 16-bit audio is not available on this machine.");
        return false;
    }

    soundOutput->samplesPerSecond = samplesPerSecond;
    soundOutput->bytesPerSample = 4;
    soundOutput->sampleCount = sampleCount;

    SDL_PauseAudioDevice(soundOutput->audioDevice, 0); // Start playing audio
    return true;
}

#ifdef PLATFORM_WINDOWS
int WinMain(HINSTANCE instance, HINSTANCE prevInstance, LPSTR cmdLine,
            int cmdShow)
#else
int main(int argc, char **argv)
#endif
{
    GameMemory memory = {};
    GameCode gameCode = {};

    g_DebugEventTable =
        (DebugEventTable *)Linux_AllocateMemory(sizeof(DebugEventTable));

#ifdef PLATFORM_WINDOWS
    if (!Win32_Init(&memory, &gameCode))
    {
        return -1;
    }
#endif

#ifdef PLATFORM_LINUX
    if (!Linux_Init(&memory, &gameCode))
    {
        return -1;
    }
#endif

    glfwSetErrorCallback(Platform_ErrorCallback);
    if (glfwInit() != GLFW_TRUE)
    {
        return -1;
    }

    const GLFWvidmode *mode = glfwGetVideoMode(glfwGetPrimaryMonitor());
    g_MonitorWidth = mode->width;
    g_MonitorHeight = mode->height;
    g_MonitorRefreshRate = mode->refreshRate;

    glfwWindowHint(GLFW_DECORATED, GLFW_TRUE);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);
    glfwWindowHint(GLFW_DOUBLEBUFFER, GLFW_TRUE);
    glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_API);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GLFW_TRUE);
// TODO: Core context, profile, compatible

    GLFWwindow *window = glfwCreateWindow(
        g_FrameBufferWidth, g_FrameBufferHeight, "Engine New 2", NULL, NULL);
    if (window == NULL)
    {
        glfwTerminate();
        return -1;
    }
    g_Window = window;

    glfwSetWindowCloseCallback(window, WindowCloseCallback);
    glfwSetMouseButtonCallback(window, MouseButtonCallback);
    glfwSetKeyCallback(window, KeyCallback);
    glfwSetCharCallback(window, CharacterCallback);

    glfwMakeContextCurrent(window);
    glfwSwapInterval(0);

    if (!Platform_InitOpenGL())
    {
        glfwTerminate();
        return -1;
	}

    SoundOutput soundOutput = {};
    if (!InitAudio(&soundOutput, 48000, 1600))
    {
        if (soundOutput.audioDevice != 0)
        {
            SDL_CloseAudioDevice(soundOutput.audioDevice);
        }

        glfwTerminate();
        return -1;
    }

    GameInput input = {};
    glfwSetWindowUserPointer(g_Window, &input);

    RenderCommand renderCommands[0x1000];

    memory.startServer = &StartServer;
    memory.startClient = &StartClient;
    memory.getPlatformData = &GetPlatformData;
    memory.setPlatformData = &SetPlatformData;
    memory.debugEventTable = g_DebugEventTable;

    memory.renderCommands = renderCommands;
    memory.maxRenderCommands = ArrayCount(renderCommands);

    g_ServerMemory.debugEventTable = g_DebugEventTable;

    u64 lastReceivedTimestamp = 0;

    double maxFrameTime = 0.25;
    double currentTime = glfwGetTime();
    double sleepMaxError = 0.0;
    while (g_IsRunning)
    {
        BEGIN_TIMED_BLOCK(TotalFrameTime);
        double minFrameTime = 1.0 / (double)g_FrameRateCap;

        double loopFrameTime = glfwGetTime() - currentTime;

        double newTime = glfwGetTime();
        double dt = newTime - currentTime;
        if (dt > maxFrameTime)
            dt = maxFrameTime;

        currentTime = newTime;

        BEGIN_TIMED_BLOCK(HandleGameCodeReload);
#ifdef PLATFORM_WINDOWS
        memory.wasCodeReloaded = Win32_HandleGameCodeReload(&gameCode);
#elif PLATFORM_LINUX
        memory.wasCodeReloaded = Linux_HandleGameCodeReload(&gameCode);
#endif
        END_TIMED_BLOCK(HandleGameCodeReload);

        if (g_IsServerRunning)
        {
            // TODO: Support running server at different rate
            g_ServerMemory.incomingPacketCount = 0;
            g_ServerMemory.outgoingPacketCount = 0;
            BEGIN_TIMED_BLOCK(ServerRecievePackets);
            u32 packetCount =
                Sock_ServerReceivePackets(g_ServerMemory.incomingPackets,
                    ArrayCount(g_ServerMemory.incomingPackets));
            END_TIMED_BLOCK(ServerRecievePackets);
            //LogMessage("Received %u packets", packetCount);

            g_ServerMemory.incomingPacketCount = packetCount;

            if (memory.reliableOutgoingDataLength > 0)
            {
                Assert(memory.reliableOutgoingDataLength <
                       sizeof(g_ServerMemory.reliableIncomingData));
                g_ServerMemory.reliableIncomingDataLength =
                    memory.reliableOutgoingDataLength;

                CopyMemory(g_ServerMemory.reliableIncomingData,
                    memory.reliableOutgoingData,
                    g_ServerMemory.reliableIncomingDataLength);
            }

            // FIXME: Should not be using dt here, we should provide a fixed update rate
            gameCode.serverUpdate(&g_ServerMemory, (f32)dt);
            g_ServerMemory.reliableIncomingDataLength = 0;

            BEGIN_TIMED_BLOCK(ServerSendPackets);
            Sock_SendPacketsToClients(g_ServerMemory.outgoingPackets,
                g_ServerMemory.outgoingPacketCount);
            END_TIMED_BLOCK(ServerSendPackets);
        }

        memory.incomingPacketCount = 0;
        memory.outgoingPacketCount = 0;
        if (g_IsClientRunning)
        {
            u32 packetCount = Sock_ClientReceivePackets(
                memory.incomingPackets, ArrayCount(memory.incomingPackets));

            //LogMessage("Client received %u packets", packetCount);
            for (u32 i = 0; i < packetCount; ++i)
            {
                // TODO: Compare timestamps, drop older sequence numbers
                lastReceivedTimestamp =
                    memory.incomingPackets[i].header.timestamp;
                u64 networkLatency =
                    Platform_GetMilliseconds() -
                    memory.incomingPackets[0].header.lastReceivedTimestamp;
                memory.networkLatency = (f32)networkLatency;
            }
            memory.incomingPacketCount = packetCount;
        }

        BEGIN_TIMED_BLOCK(HandleInput);
        InputBeginFrame(&input);

        glfwPollEvents();

        ProcessMouseInput(&input);

        if (g_WindowMode == WindowMode_Windowed)
        {
            int width, height;
            glfwGetFramebufferSize(g_Window, &width, &height);
            memory.frameBufferWidth = width;
            memory.frameBufferHeight = height;
        }
        else
        {
            memory.frameBufferWidth = g_FrameBufferWidth;
            memory.frameBufferHeight = g_FrameBufferHeight;
        }
        END_TIMED_BLOCK(HandleInput);

        BEGIN_TIMED_BLOCK(ZeroRenderCommandsArray);
        ClearToZero(renderCommands, sizeof(renderCommands[0]) * memory.renderCommandsCount);
        END_TIMED_BLOCK(ZeroRenderCommandsArray);
        memory.renderCommandsCount = 0;

        memory.reliableOutgoingDataLength = 0;
        gameCode.update(&memory, &input, (f32)dt);


        if (g_IsClientRunning)
        {
            if (memory.outgoingPacketCount > 0)
            {
                Sock_SendPacketToServer(&memory.outgoingPackets[0], 0);
            }
        }
        else
        {
            Assert(memory.outgoingPacketCount == 0);
        }

        OpenGL_DrawFrame(memory.renderCommands, memory.renderCommandsCount,
            memory.frameBufferWidth, memory.frameBufferHeight);

        BEGIN_TIMED_BLOCK(HandleAudio);
        u32 queueSize = SDL_GetQueuedAudioSize(soundOutput.audioDevice);

        // FIXME: Two frames of audio at 60Hz seems to work okay although it is
        // completely wrong. Should update this to use a circular buffer system
        // rather than SDL_QueueAudio.
#define TARGET_QUEUE_BYTES (1600 * 4)
        Assert(queueSize <= TARGET_QUEUE_BYTES);
        i32 bytesToWrite = TARGET_QUEUE_BYTES - queueSize;
        i16 samples[TARGET_QUEUE_BYTES / 2];
        ClearToZero(samples, sizeof(samples));

        //LogMessage("QueueLength: %u, bytesToWrite: %u, sampleCount: %u",
            //queueSize, bytesToWrite, bytesToWrite / soundOutput.bytesPerSample);

        GameSoundOutputBuffer soundBuffer = {};
        soundBuffer.samplesPerSecond = soundOutput.samplesPerSecond;
        soundBuffer.sampleCount = bytesToWrite / soundOutput.bytesPerSample;
        soundBuffer.samples = samples;
        gameCode.getSoundSamples(&memory, &soundBuffer);

        OutputSound(&soundOutput, &soundBuffer);
        END_TIMED_BLOCK(HandleAudio);

        BEGIN_TIMED_BLOCK(SwapBuffers);
        glFinish();
        glfwSwapBuffers(g_Window);
        END_TIMED_BLOCK(SwapBuffers);

        BEGIN_TIMED_BLOCK(WaitForFrameEnd);
        double totalFrameTime = glfwGetTime() - currentTime;
        if (totalFrameTime < minFrameTime)
        {
            double remainder = minFrameTime - totalFrameTime;
            u32 millisecondsRemaining = (u32)(remainder * 1000.0);
            if (millisecondsRemaining > 2)
            {
                millisecondsRemaining -= 1;
                double sleepStart = glfwGetTime();
#ifdef PLATFORM_WINDOWS
                Sleep(millisecondsRemaining);
#elif defined(PLATFORM_LINUX)
		usleep(millisecondsRemaining * 1000);
#endif
                double sleepTime = glfwGetTime() - sleepStart;
                double sleepError = sleepTime - ((double)millisecondsRemaining / (1000.0));
                if (sleepError > sleepMaxError)
                    sleepMaxError = sleepError;
            }

            // Busy wait the remainder
            while (glfwGetTime() - currentTime < minFrameTime)
            {
            }
        }
        END_TIMED_BLOCK(WaitForFrameEnd);

        memory.frameTime = (f32)(glfwGetTime() - currentTime);
        END_TIMED_BLOCK(TotalFrameTime);

        g_DebugEventTable->readCursor = g_DebugEventTable->writeCursor;
        g_DebugEventTable->writeCursor = (g_DebugEventTable->writeCursor + 1) % 2;
        g_DebugEventTable->count[g_DebugEventTable->writeCursor] = 0;
    }

	glfwDestroyWindow(g_Window);
    glfwTerminate();
    if (soundOutput.audioDevice != 0)
    {
        SDL_CloseAudioDevice(soundOutput.audioDevice);
    }

    return 0;
}
