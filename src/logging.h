#pragma once

#define DebugLogMessage(NAME) void NAME(const char *fmt, ...)
typedef DebugLogMessage(DebugLogMessageFunction);

global DebugLogMessageFunction *LogMessage;

