#include "game.h"
#include "debug_drawing.cpp"
#include "collision_detection.cpp"

#include <stdio.h>

#undef Assert

#define Assert(COND) \
    if (!(COND)) \
{ \
    printf("%s:%u: Assertion Failed!\n\t%s\n", __FILE__, __LINE__, #COND); \
    return false; \
}

#define AssertEqualF32(EXPECTED, ACTUAL)                                       \
    if (!Equal(EXPECTED, ACTUAL))                                              \
    {                                                                          \
        printf("%s:%u: Assertion Failed!\n\tEqual(" #EXPECTED ", " #ACTUAL     \
               ")\n\t%g != %g\n",                                              \
            __FILE__, __LINE__, EXPECTED, ACTUAL);                             \
        return false;                                                          \
    }

#define AssertEqualV3(EXPECTED, ACTUAL)                                        \
    if (!Equal(EXPECTED, ACTUAL))                                              \
    {                                                                          \
        printf("%s:%u: Assertion Failed!\n\t(%g, %g, %g) != (%g, %g, %g)\n",   \
            __FILE__, __LINE__, EXPECTED.x, EXPECTED.y, EXPECTED.z, ACTUAL.x,  \
            ACTUAL.y, ACTUAL.z);                                               \
        return false;                                                          \
    }

internal b32 Test_RayIntersectAabb()
{
    vec3 aabbCenter = Vec3(0.0f, 0.0f, -5.0f);
    vec3 halfDims = Vec3(0.5f);
    vec3 aabbMin = aabbCenter - halfDims;
    vec3 aabbMax = aabbCenter + halfDims;
    {
        // No collision if ray misses aabb
        vec3 start = Vec3(5.0f, 0.0f, 0.0f);
        vec3 end = Vec3(5.0f, 0.0f, -10.0f);

        RaycastResult raycastResult =
            RayIntersectAabb_(aabbMin, aabbMax, start, end);

        Assert(raycastResult.isValid == false);
        AssertEqualF32(raycastResult.tmin, 0.0f);
        Assert(Equal(raycastResult.hitPoint, Vec3(0.0f)));
        Assert(Equal(raycastResult.hitNormal, Vec3(0.0f)));
    }

    {
        vec3 start = Vec3(0.0f, 0.0f, 5.0f);
        vec3 end = Vec3(0.0f, 0.0f, -5.0f);

        RaycastResult raycastResult =
            RayIntersectAabb_(aabbMin, aabbMax, start, end);

        // Assumes that end is set to the aabbCenter
        f32 tmin = 1.0f - (halfDims.z / Length(end - start));
        Assert(raycastResult.isValid == true);
        AssertEqualF32(raycastResult.tmin, tmin);
        AssertEqualV3(raycastResult.hitPoint, Vec3(0.0f, 0.0f, aabbMax.z)); // Assumes looking down -z axis
        AssertEqualV3(raycastResult.hitNormal, Vec3(0.0f, 0.0f, 1.0f));
    }

    {
        // Ray origin is inside aabb
        vec3 start = Vec3(0.0f, 0.0f, -5.0f);
        vec3 end = Vec3(0.0f, 0.0f, 5.0f);

        RaycastResult raycastResult =
            RayIntersectAabb_(aabbMin, aabbMax, start, end);
        Assert(raycastResult.isValid == true);
        AssertEqualF32(raycastResult.tmin, 0.0f);
        AssertEqualV3(raycastResult.hitPoint, start);
        // FIXME: Should be the normal of the closest face
        //AssertEqualV3(raycastResult.hitNormal, SafeNormalize(start - end));
    }
    return true;
}

internal b32 Test_RayIntersectSphere()
{
    vec3 sphereCenter = Vec3(0.0f, 0.0f, -5.0f);
    f32 radius = 0.5f;

    {
        // No collision if ray misses sphere
        vec3 start = Vec3(5.0f, 0.0f, 0.0f);
        vec3 end = Vec3(5.0f, 0.0f, -10.0f);

        RaycastResult raycastResult =
            RayIntersectSphere_(sphereCenter, radius, start, end);

        Assert(raycastResult.isValid == false);
        AssertEqualF32(raycastResult.tmin, 0.0f);
        Assert(Equal(raycastResult.hitPoint, Vec3(0.0f)));
        Assert(Equal(raycastResult.hitNormal, Vec3(0.0f)));
    }

    {
        vec3 start = Vec3(0.0f, 0.0f, 5.0f);
        vec3 end = Vec3(0.0f, 0.0f, -5.0f);

        RaycastResult raycastResult =
            RayIntersectSphere_(sphereCenter, radius, start, end);

        // Assumes that end is set to the sphereCenter
        f32 tmin = 1.0f - (radius / Length(end - start));
        Assert(raycastResult.isValid == true);
        AssertEqualF32(raycastResult.tmin, tmin);
        Assert(Equal(raycastResult.hitPoint, Vec3(0.0f, 0.0f, -4.5f)));
        Assert(Equal(raycastResult.hitNormal, Vec3(0.0f, 0.0f, 1.0f)));
    }

    // FIXME: Need to decide if we want to consider ray origin inside sphere a
    // valid ray intersection
#if 0
    {
        // Ray origin is inside sphere
        vec3 start = Vec3(0.0f, 0.0f, -5.0f);
        vec3 end = Vec3(0.0f, 0.0f, 5.0f);

        RaycastResult raycastResult =
            RayIntersectSphere(sphereCenter, radius, start, end);
        Assert(raycastResult.isValid == false);
        AssertEqualF32(raycastResult.tmin, 0.0f);
        AssertEqualV3(raycastResult.hitPoint, Vec3(0.0f, 0.0f, 0.0f));
        AssertEqualV3(raycastResult.hitNormal, Vec3(0.0f, 0.0f, 0.0f));
    }
#endif

    return true;
}

internal b32 Test_RayIntersectCapsule()
{
    vec3 endpointA = Vec3(0, -0.75, -5);
    vec3 endpointB = Vec3(0, 0.75, -5);
    f32 radius = 0.5f;

    {
        // No collision
        vec3 start = Vec3(5.0f, 0.0f, 0.0f);
        vec3 end = Vec3(5.0f, 0.0f, -10.0f);

        RaycastResult raycastResult =
            RayIntersectCapsule_(endpointA, endpointB, radius, start, end);

        Assert(raycastResult.isValid == false);
        AssertEqualF32(raycastResult.tmin, 0.0f);
        Assert(Equal(raycastResult.hitPoint, Vec3(0.0f)));
        Assert(Equal(raycastResult.hitNormal, Vec3(0.0f)));
    }

    {
        // -Z Axis
        vec3 start = Vec3(0.0f, 0.0f, 5.0f);
        vec3 end = Vec3(0.0f, 0.0f, -5.0f);

        RaycastResult raycastResult =
            RayIntersectCapsule_(endpointA, endpointB, radius, start, end);

        // Assumes that end is set to the capsule center
        f32 tmin = 1.0f - (radius / Length(end - start));
        Assert(raycastResult.isValid == true);
        AssertEqualF32(raycastResult.tmin, tmin);
        Assert(Equal(raycastResult.hitPoint, Vec3(0.0f, 0.0f, -4.5f)));
        Assert(Equal(raycastResult.hitNormal, Vec3(0.0f, 0.0f, 1.0f)));
    }

    {
        // -Y Axis
        vec3 start = Vec3(0.0f, 5.0f, -5.0f);
        vec3 end = Vec3(0.0f, 0.0f, -5.0f);

        RaycastResult raycastResult =
            RayIntersectCapsule_(endpointA, endpointB, radius, start, end);

        // Assumes that end is set to the capsule center
        f32 maxY = endpointB.y + radius;
        f32 tmin = 1.0f - (maxY / Length(end - start));
        Assert(raycastResult.isValid == true);
        AssertEqualF32(raycastResult.tmin, tmin);
        Assert(Equal(raycastResult.hitPoint, Vec3(0.0f, maxY, -5.0f)));
        Assert(Equal(raycastResult.hitNormal, Vec3(0.0f, 1.0f, 0.0f)));
    }

    return true;
}

internal b32 Test_RayIntersectTriangle()
{
    vec3 vertices[3] = {
        Vec3(-0.5f, -0.5f, -5.0f),
        Vec3(0.0f, 0.5f, -5.0f),
        Vec3(0.5f, -0.5f, -5.0f)
    };

    {
        // No collision if ray misses triangle
        vec3 start = Vec3(5.0f, 0.0f, 0.0f);
        vec3 end = Vec3(5.0f, 0.0f, -10.0f);

        RaycastResult raycastResult = RayIntersectTriangle_(
            vertices[0], vertices[1], vertices[2], start, end);

        Assert(raycastResult.isValid == false);
        AssertEqualF32(raycastResult.tmin, 0.0f);
        Assert(Equal(raycastResult.hitPoint, Vec3(0.0f)));
        Assert(Equal(raycastResult.hitNormal, Vec3(0.0f)));
    }

    {
        // -Z Axis
        vec3 start = Vec3(0.0f, 0.0f, 5.0f);
        vec3 end = Vec3(0.0f, 0.0f, -10.0f);

        RaycastResult raycastResult = RayIntersectTriangle_(
            vertices[0], vertices[1], vertices[2], start, end);

        f32 tmin = Abs((vertices[0].z - start.z) / (start.z - end.z));
        Assert(raycastResult.isValid == true);
        AssertEqualF32(raycastResult.tmin, tmin);
        Assert(Equal(raycastResult.hitPoint, Vec3(0.0f, 0.0f, -5.0f)));
        Assert(Equal(raycastResult.hitNormal, Vec3(0.0f, 0.0f, 1.0f)));
    }

    {
        // Z Axis - Backface
        vec3 start = Vec3(0.0f, 0.0f, -10.0f);
        vec3 end = Vec3(0.0f, 0.0f, 5.0f);

        RaycastResult raycastResult = RayIntersectTriangle_(
            vertices[0], vertices[1], vertices[2], start, end);

        Assert(raycastResult.isValid == false);
        AssertEqualF32(raycastResult.tmin, 0.0f);
        Assert(Equal(raycastResult.hitPoint, Vec3(0.0f)));
        Assert(Equal(raycastResult.hitNormal, Vec3(0.0f)));
    }
    return true;
}

internal b32 Test_RayIntersectTriangleMesh()
{
    // Clockwise winding order
    vec3 meshVertices[] = {
        Vec3(0.5f, -0.5f, 0.5f),
        Vec3(0.0f, -0.5f, -0.5f),
        Vec3(-0.5f, -0.5f, 0.5f),

        Vec3(-0.5f, -0.5f, 0.5f),
        Vec3(0.0f, 0.5f, 0.0f),
        Vec3(0.5f, -0.5f, 0.5f),

        Vec3(0.5f, -0.5f, 0.5f),
        Vec3(0.0f, 0.5f, 0.0f),
        Vec3(0.0f, -0.5f, -0.5f),

        Vec3(0.0f, -0.5f, -0.5f),
        Vec3(0.0f, 0.5f, 0.0f),
        Vec3(-0.5f, -0.5f, 0.5f)
    };
    for (u32 i = 0; i < ArrayCount(meshVertices); ++i)
    {
        meshVertices[i] += Vec3(0, 0, -5);
    }

    u32 triangleCount = ArrayCount(meshVertices) / 3;

    {
        // No collision if ray misses triangle mesh
        vec3 start = Vec3(5.0f, 0.0f, 0.0f);
        vec3 end = Vec3(5.0f, 0.0f, -10.0f);

        RaycastResult meshRaycastResults[256];
        RayIntersectTriangle(
            meshVertices, triangleCount, start, end, meshRaycastResults, NULL);

        RaycastResult *raycastResult =
            FindClosestRaycastResult(meshRaycastResults, triangleCount);
        Assert(raycastResult == NULL);
    }

    {
        // -Z Axis
        vec3 start = Vec3(0.0f, 0.0f, 5.0f);
        vec3 end = Vec3(0.0f, 0.0f, -5.0f);

        RaycastResult meshRaycastResults[256];
        RayIntersectTriangle(
            meshVertices, triangleCount, start, end, meshRaycastResults, NULL);

        u32 count = 0;
        for (u32 i = 0; i < triangleCount; ++i)
        {
            if (meshRaycastResults[i].isValid)
                count++;
        }

        Assert(count == 1);

        RaycastResult *raycastResult =
            FindClosestRaycastResult(meshRaycastResults, triangleCount);
        Assert(raycastResult != NULL);

        f32 zStart = -5.0f + 0.5f;
        f32 zEnd = -5.0f + 0.0f;
        f32 zMid = (zStart + zEnd) * 0.5f;

        f32 tmin = (zMid - start.z) / (end.z - start.z);
        AssertEqualF32(raycastResult->tmin, tmin);
        Assert(Equal(raycastResult->hitPoint, start + (end - start) * tmin));
        // TODO: Figure out normal
        //Assert(Equal(raycastResult.hitNormal, Vec3(0.0f)));
    }

    return true;
}

int main(int argc, char **argv)
{
    int result = 1;
    if (Test_RayIntersectAabb())
    {
        result = 0;
    }

    if (Test_RayIntersectSphere())
    {
        result = 0;
    }

    if (Test_RayIntersectCapsule())
    {
        result = 0;
    }

    if (Test_RayIntersectTriangle())
    {
        result = 0;
    }

    if (Test_RayIntersectTriangleMesh())
    {
        result = 0;
    }

    if (!result)
    {
        printf("1/1 tests passed\n");
    }

    return result;
}
