#pragma once

#define MAX_CVARS 0x1000
#define CVAR_NAME_LENGTH 40
struct CVarTable
{
    u32 keys[MAX_CVARS];
    char names[MAX_CVARS][CVAR_NAME_LENGTH];
    CVarValue values[MAX_CVARS];
    u32 count;
};

inline b32 FindCVarByKey(CVarTable *table, u32 key, u32 *index)
{
    b32 result = false;
    for (u32 i = 0; i < table->count; ++i)
    {
        if (table->keys[i] == key)
        {
            *index = i;
            result = true;
            break;
        }
    }

    return result;
}

inline b32 FindCVarByName(CVarTable *table, const char *name, u32 *index)
{
    b32 result = false;
    for (u32 i = 0; i < table->count; ++i)
    {
        if (StringCompare(table->names[i], name))
        {
            *index = i;
            result = true;
            break;
        }
    }

    return result;
}

inline void CVarAllocate(CVarTable *table, u32 key, const char *name, CVarValue value)
{
    Assert(table->count < MAX_CVARS);

    u32 index = table->count++;

    table->keys[index] = key;
    table->values[index] = value;
    StringCopy(table->names[index], CVAR_NAME_LENGTH, name);
}

inline void CVarRegister(CVarTable *table, u32 key, const char *name, CVarValue value)
{
    u32 index = 0;
    if (!FindCVarByKey(table, key, &index))
    {
        CVarAllocate(table, key, name, value);
    }
}

inline CVarValue* CVar_GetValue(CVarTable *table, u32 key)
{
    CVarValue *result = NULL;
    u32 index = 0;
    if (FindCVarByKey(table, key, &index))
    {
        result = table->values + index;
    }
    return result;
}

inline f32 CVar_GetValueF32(CVarTable *table, u32 key)
{
    CVarValue *value = CVar_GetValue(table, key);
    Assert(value);
    Assert(value->dataType == DataType_F32);
    return value->f;
}

inline u32 CVar_GetValueU32(CVarTable *table, u32 key)
{
    CVarValue *value = CVar_GetValue(table, key);
    Assert(value);
    Assert(value->dataType == DataType_U32);
    return value->u;
}

inline void CVar_SetValueU32(CVarTable *table, u32 key, u32 u)
{
    CVarValue *value = CVar_GetValue(table, key);
    Assert(value);
    Assert(value->dataType == DataType_U32);
    value->u = u;
}

inline void CVar_SetValueF32(CVarTable *table, u32 key, f32 f)
{
    CVarValue *value = CVar_GetValue(table, key);
    Assert(value);
    Assert(value->dataType == DataType_F32);
    value->f = f;
}

inline void RegisterCVars(
    CVarTable *table, CVarRegistrationData *entries, u32 count)
{
    for (u32 i = 0; i < count; ++i)
    {
        CVarRegister(table, entries[i].key, entries[i].name, entries[i].value);
    }
}

