/****************************************************************
 *                      TODO LIST
 ****************************************************************
 Defects
 - Binary try search for entity component tables does not rebalance itself,
    adding and removing components will not work correctly at runtime.

 - Query OpenGL GPU limits for uniform buffers
 - Broadphase is too slow
 - Heightmap quad tree creation needs to be brough into entity system
 - Better system for determining active camera/player
 - Collision debug visualizations don't apply to GameRender
 - API for accessing server state from client, instead of reading random global variables
 - SORT OUT UI SYSTEMS (IMGUI and IMGUI2)
 - Depth aware debug line drawing is broken
 - RPC API which can be used to serialize functions calls, console commands would then map on top of it
 - Implement dirty bit optimization in component tables

 Animation
 - ECS parent child relationships
 - Decide how we what are doing about root node animation
 - GPU skinning
 - FBX model importing
 - Animation playback
 - Animation blending

 Game
 - Dropping items on death
 - Minimum time between attacks
 - Foot step sounds
 - Automatic weapons
 - Shotgun
 - Bow
 - Healing items
 - AI enemies
 - Impact effects
 - Player damage effects
 - Breaking building parts
 - Add stairs building parts
 - Add doors
 - Render view model in separate pass with its own FOV

 Terrain
 - Sphere sweep heightmap test
 - Calculate max distance for quad tree based on render depth rather than table
 - Test alternative triangle mesh layouts, e.g. triangle vs diamond
 - Summed Area table to accelerate quad tree construction
 - Build up quad tree in unit space and multiple to world space at runtime
 - Implement CDLOD vertex morphing function
 - Multiple collision quad trees


 Collision Detection
 - Want to support local origin for collision shapes
 - Compound shapes!
 - Convex hull shape
 - Tighter broadphase bounding volumes
 - Collision detection debugging tools
 - Closest point computations which we can use to prevent players from getting stuck

 Networking
 - Start generalizing some of the entity networking code
 - Bidirectional reliable data stream between client and server
 - Better system for debugging network events
 - Fixed server update rate independent of client framerate
 - Clientside prediction
 - Clientside Lerp

 Console
 - Console text cursor
 - Cvar completion for console

 Rendering
 - Cascaded shadow maps
 - Mesh skinning
 - Particle systems
 - Grass rendering




 Misc
 - Asset system!
 - Support code reload on server
 - Stack based UI system for positioning elements






 */
#include "game.h"
#include "cvar.h"
#include "ecs.h"

#include "asset.cpp"
#include "perlin.cpp"
#include "ui.cpp"
#include "ecs.cpp"
#include "debug_drawing.cpp"
global DebugDrawingSystem *g_ServerDebugDrawingSystem;
global DebugDrawingSystem *g_DebugDrawingSystem;

#include "heightmap.cpp"
#include "random.cpp"
#include "collision_detection.cpp"
#include "camera.cpp"
#include "collision_detection_testbench.cpp"
#include "entity_prefabs.cpp"
#include "terrain.cpp"
#include "entity_collision.cpp"
#include "entity_rendering.cpp"
#include "entity_networking.cpp"
#include "inventory_system.cpp"
#include "cmd.cpp"
#include "console.cpp"
#include "fps_graph.cpp"
#include "particle.cpp"

#include "player.cpp"
#include "entity_misc.cpp"

#include "game_networking.cpp"
#include "game_cmds.cpp"
#include "game_assets.cpp"

//#include "model_importing.cpp"

global CollisionDebugState *g_ServerCollisionDebugState;
global vec3 g_CameraPosition;

#define SKIP_MAIN_MENU

struct ClientData
{
    u32 clientId;
    NetEntityClient entityClient;
    EntityId entityToControl;
    PlayerCommand prevPlayerCommand;
    f32 secondsUntilRespawn;
};

struct ServerGameState
{
    b32 isInitialized;
    MemoryArena tempArena;
    MemoryArena persistentArena;
    MemoryArena entityArena;
    MemoryArena collisionWorldArena;
    ecs_EntityWorld entityWorld;

    NetEntityServer server;

    ClientData clients[64];
    u32 clientCount;

    DebugDrawingSystem debugDrawingSystem;

    GameEventQueue eventQueue;

    CollisionDebugState collisionDebugState;

    Cmd_System commandSystem;
    CVarTable cvarTable;

    GameAssets assets;

    RandomNumberGenerator rng;
};

struct PlayingSound
{
    u32 audioClipId;
    f32 volume;
    u32 runningSampleIndex;
};

struct GameState
{
    b32 isInitialized;
    MemoryArena tempArena;
    u32 prevWindowWidth;
    u32 prevWindowHeight;
    u32 state;

    // TODO: Move fonts into GameAssets
    Font debugFont;
    Font testFont;
    Font mainMenuTitleFont;
    b32 gameContentLoaded;

    MemoryArena persistentArena;
    MemoryArena entityArena;
    MemoryArena collisionWorldArena;
    ecs_EntityWorld entityWorld;

    // TODO: Move as much of this into entity system as we can
    b32 showInventory;
    PlayerCommand prevPlayerCommand;
    InventoryState inventoryState;
    EntityId prevPlayerOpenContainer;

    b32 showGameUI;

    DebugDrawingSystem debugDrawingSystem;

    // TODO: Move this into game assets
    Material materials[MAX_MATERIALS];

    VertexBuffer textBuffer;
    Console console;
    Cmd_System commandSystem;
    CVarTable cvarTable;

    CollisionDebugState collisionDebugState;
    CollisionDetectionTestbench collisionDetectionTestbench;

    FpsGraph fpsGraph;

    // TODO: Move this into an AudioMixer struct
    PlayingSound playingSounds[MAX_PLAYING_SOUNDS];
    u32 playingSoundsCount;

    GameAssets assets;

    // TODO: Make this a component
    ParticleSystemInstance particleSystems[MAX_PARTICLE_SYSTEMS];
    ParticleSystemInstance *particleSystemsFreeList;

    EntityId particleSystemKeys[MAX_PARTICLE_SYSTEMS];
    ParticleSystemInstance *particleSystemValues[MAX_PARTICLE_SYSTEMS];
    u32 particleSystemCount;

    RandomNumberGenerator rng;

    f32 currentTime;
};

#define E(NAME, DATATYPE, CAPACITY, NOTIFY_ON_REMOVE) \
{NAME##Id, #NAME, DATATYPE, CAPACITY, NOTIFY_ON_REMOVE}

// clang-format off
global ComponentInitializationData g_ComponentInitData[MaxComponentsIds] = {
    E(PositionComponent            , DataType_Vec3 , MAX_ENTITIES           , false) ,
    E(RotationComponent            , DataType_Quat , MAX_ENTITIES           , false) ,
    E(ScaleComponent               , DataType_Vec3 , MAX_ENTITIES           , false) ,
    E(RenderedMeshComponent        , DataType_U32  , MAX_ENTITIES           , false) ,
    E(ShadowCasterComponent        , DataType_None , MAX_ENTITIES           , false) ,
    E(RenderedMaterialComponent    , DataType_U32  , MAX_ENTITIES           , false) ,
    E(ActiveCameraComponent        , DataType_None , 1                      , false) ,
    E(LightEmitterRadiusComponent  , DataType_F32  , MAX_ENTITIES           , false) ,
    E(LightMaximumRangeComponent   , DataType_F32  , MAX_ENTITIES           , false) ,
    E(LightColorComponent          , DataType_Vec3 , MAX_ENTITIES           , false) ,
    E(SpotLightInnerAngleComponent , DataType_F32  , MAX_SPOT_LIGHTS        , false) ,
    E(SpotLightOuterAngleComponent , DataType_F32  , MAX_SPOT_LIGHTS        , false) ,
    E(DirectionalLightComponent    , DataType_None , MAX_DIRECTIONAL_LIGHTS , false) ,
    E(AmbientLightComponent        , DataType_None , 1                      , false) ,
    E(PointLightComponent          , DataType_None , MAX_POINT_LIGHTS       , false) ,
    E(SpotLightComponent           , DataType_None , MAX_SPOT_LIGHTS        , false) ,
    E(NetEntityIdComponent         , DataType_U32  , MAX_ENTITIES           , false) ,
    E(InventoryComponent           , DataType_None , MAX_ENTITIES           , false) ,
    E(InventorySlotOwnerComponent  , DataType_U32  , MAX_ITEMS              , false) ,
    E(InventorySlotIndexComponent  , DataType_U32  , MAX_ITEMS              , false) ,
    E(ItemIdComponent              , DataType_U32  , MAX_ITEMS              , false) ,
    E(EntityPrefabIdComponent      , DataType_U32  , MAX_ENTITIES           , false) ,
    E(InteractableComponent        , DataType_None , MAX_ENTITIES           , false) ,
    E(OpenContainerComponent       , DataType_U32  , MAX_PLAYERS            , false) ,
    E(BoxHalfDimensionsComponent   , DataType_Vec3 , MAX_ENTITIES           , false) ,
    E(BuildablePreviewComponent    , DataType_None , 2                      , false) ,
    E(ActivePlayerComponent        , DataType_None , 1                      , false) ,
    E(DebugCameraComponent         , DataType_None , MAX_ENTITIES           , false) ,
    E(HealthComponent              , DataType_F32  , MAX_ENTITIES           , false) ,
    E(VelocityComponent            , DataType_Vec3 , MAX_ENTITIES           , false) ,
    E(EulerAnglesComponent         , DataType_Vec3 , MAX_ENTITIES           , false) ,
    E(PlayerSpawnPointComponent    , DataType_None , MAX_ENTITIES           , false) ,
    E(OnGroundComponent            , DataType_U32  , MAX_ENTITIES           , false) ,
    E(SkyboxComponent              , DataType_None , 1                      , false) ,
    E(AiBrainComponent             , DataType_None , MAX_ENTITIES           , false) ,
    E(EnemySpawnPointComponent     , DataType_None , MAX_ENTITIES           , false) ,
    E(RadiusComponent              , DataType_F32  , MAX_ENTITIES           , false) ,
    E(CapsuleEndPointAComponent    , DataType_Vec3 , MAX_ENTITIES           , false) ,
    E(CapsuleEndPointBComponent    , DataType_Vec3 , MAX_ENTITIES           , false) ,
    E(TriangleMeshComponent        , DataType_U32  , MAX_ENTITIES           , false) ,
    E(CollisionShapeComponent      , DataType_U32  , MAX_ENTITIES           , true) ,
    E(ActionBarActiveSlotComponent , DataType_U32  , MAX_ENTITIES           , false) ,
    E(BulletComponent              , DataType_None , MAX_ENTITIES           , false) ,
    E(ProjectileOwnerComponent     , DataType_U32  , MAX_ENTITIES           , false) ,
    E(ItemQuantityComponent        , DataType_U32  , MAX_ITEMS              , false) ,
    E(DeployableIdComponent        , DataType_U32  , MAX_ENTITIES           , false) ,
    E(TerrainComponent             , DataType_None , 1                      , false) ,
    E(IsVisibleComponent           , DataType_U32  , MAX_ENTITIES           , false) ,
    E(ParticleSystemComponent      , DataType_U32  , MAX_PARTICLE_SYSTEMS   , true)  ,
    E(TimeToLiveComponent          , DataType_F32  , MAX_ENTITIES           , false) ,
    E(LootTableComponent           , DataType_U32  , MAX_ENTITIES           , false) ,
    E(StaticComponent              , DataType_None , MAX_ENTITIES           , true)  ,
    E(BroadPhaseAabbComponent      , DataType_Aabb , MAX_ENTITIES           , false) ,
    E(AiTargetComponent            , DataType_U32  , MAX_ENTITIES           , false) ,
    E(TimeUntilNextAttackComponent , DataType_F32  , MAX_ENTITIES           , false) ,
    E(WorldTransformComponent      , DataType_Mat4 , MAX_ENTITIES           , false) ,
    E(ParentComponent              , DataType_U32  , MAX_ENTITIES           , false) ,
    E(LocalTransformComponent      , DataType_Mat4 , MAX_ENTITIES           , false) ,
    E(IsBoneComponent              , DataType_None , MAX_ENTITIES           , false) ,
    E(IsWorldTransformDirtyComponent , DataType_U32, MAX_ENTITIES           , false) ,
    E(IsLocalTransformDirtyComponent , DataType_U32, MAX_ENTITIES           , false) ,
    E(BoneIdComponent              , DataType_U32  , MAX_ENTITIES           , false) ,
    E(DrawWorldTransformComponent  , DataType_None , MAX_ENTITIES           , false) ,
};
// clang-format on

#undef E

global Rpc g_ServerRpcQueue[8];
global u32 g_ServerRpcQueueLength;

inline void SendRpcToServer(Rpc *rpc)
{
    Assert(g_ServerRpcQueueLength < ArrayCount(g_ServerRpcQueue));
    g_ServerRpcQueue[g_ServerRpcQueueLength++] = *rpc;
}

internal void DisplayDebugCounters(GameState *gameState, GameMemory *memory);

internal void PopulateLootTables(GameAssets *assets)
{
    {
        LootTable *testTable = assets->lootTables + LootTable_Test;
        testTable->entries[0] = { ItemID_Pistol, 1, 0.2f };
        testTable->entries[1] = { ItemID_PistolAmmo, 12, 0.8f };
        testTable->entries[2] = { ItemID_Hatchet, 1, 0.5f };
        testTable->count = 3;
    }
}

internal void PlaySound(GameState *gameState, u32 audioClipId, f32 volume = 1.0f)
{
    Assert(gameState->playingSoundsCount < ArrayCount(gameState->playingSounds));
    PlayingSound *sound = gameState->playingSounds + gameState->playingSoundsCount++;
    sound->audioClipId = audioClipId;
    sound->volume = volume;
    sound->runningSampleIndex = 0;
}

internal i32 Cmd_PlaySound(GameState *gameState, u32 argc, const char **argv)
{
    if (argc < 2)
    {
        LogConsole("Must provide an integer for the audio clip ID and "
                   "optionally a number between 0 and 1 for the volume");
        return -1;
    }

    u32 audioClipId = atol(argv[1]);
    GameAssets *assets = &gameState->assets;
    if (audioClipId < ArrayCount(assets->audioClips))
    {
        f32 volume = 1.0f;
        if (argc > 2)
        {
            volume = (f32)atof(argv[2]);
            if (volume < 0.0f || volume > 1.0f)
            {
                LogConsole("Volume must be between 0 and 1.");
                return -1;
            }
        }

        PlaySound(gameState, audioClipId, volume);
        return 0;
    }
    else
    {
        LogConsole("Invalid audio clip id");
        return -1;
    }
}

internal void DrawLoadingScreen(GameState *gameState, GameMemory *memory);
internal i32 Cmd_Connect(
    GameState *gameState, GameMemory *memory, u32 argc, const char **argv)
{
    if (argc < 2)
    {
        LogConsole("Must provide an ip address");
        return -1;
    }

    if (gameState->state != GameState_MainMenu)
    {
        LogConsole("Connect command only works from main menu for now.");
        return -1;
    }

    const char *address = argv[1];

    if (memory->startClient(address))
    {
        gameState->state = GameState_Experimental;
        DrawLoadingScreen(gameState, memory);
        return 0;
    }

    return -1;
}

internal i32 Cmd_MemInfo(GameState *gameState)
{
    MemoryArena *persistentArena = &gameState->persistentArena;
    MemoryArena *entityArena = &gameState->entityArena;

    LogConsole("persistentArena: %.2f%%",
        ((f32)persistentArena->size /
            (f32)persistentArena->capacity) * 100.0f);
    LogConsole("entityArena: %.2f%%",
        ((f32)entityArena->size /
            (f32)entityArena->capacity) * 100.0f);

    return 0;
}

internal i32 Cmd_CollisionTestbench(GameState *gameState)
{
    gameState->state = GameState_CollisionDetectionTestBench;

    return 0;
}

// NOTE: Replace contents of entity world
internal void LoadMap(ecs_EntityWorld *world, GameAssets *assets)
{
    ecs_DeleteAllEntities(world);

    //CreateEntityFromPrefab(world, EntityPrefab_Skybox);

//#define USE_TERRAIN_MAP
#ifdef USE_TERRAIN_MAP
    SpawnEntityAtPosition(world, EntityPrefab_DirectionalLight, Vec3(10, 40, 0));

    SpawnEntityAtPosition(world, EntityPrefab_AmbientLight, Vec3(1, 5, 0));

    SpawnEntityAtPositionWithScale(world, EntityPrefab_Terrain, Vec3(0.0f),
        Vec3(TERRAIN_PATCH_SCALE, TERRAIN_HEIGHT_SCALE, TERRAIN_PATCH_SCALE));

    SpawnEntityAtPosition(world, EntityPrefab_Camera, Vec3(0, 200, 10));

    SpawnEntityOnTerrain(
        world, assets, EntityPrefab_PlayerSpawnPoint, Vec3(-10, 4, -10));

#if 0
    SpawnEntityOnTerrain(world, assets, EntityPrefab_EnemySpawnPoint, Vec3(-50, 2, 10));
    SpawnEntityOnTerrain(world, assets, EntityPrefab_EnemySpawnPoint, Vec3(50, 2, 10));
    SpawnEntityOnTerrain(world, assets, EntityPrefab_EnemySpawnPoint, Vec3(20, 2, -40));
    SpawnEntityOnTerrain(world, assets, EntityPrefab_EnemySpawnPoint, Vec3(5, 2, 55));
#endif



#else
    SpawnEntityAtPosition(world, EntityPrefab_Camera, Vec3(0, 4, 0));

    SpawnEntityAtPosition(
        world, EntityPrefab_PlayerSpawnPoint, Vec3(-10, 4, -10));

    //SpawnEntityAtPosition(world, EntityPrefab_Pistol, Vec3(0, 1, 0));

#if 0
    SpawnEntityAtPosition(world, EntityPrefab_EnemySpawnPoint, Vec3(-5, 4, 10));

    SpawnEntityAtPosition(world, EntityPrefab_EnemySpawnPoint, Vec3(0, 4, 10));

    SpawnEntityAtPosition(world, EntityPrefab_EnemySpawnPoint, Vec3(5, 4, 15));

    SpawnEntityAtPosition(world, EntityPrefab_EnemySpawnPoint, Vec3(8, 4, 8));
#endif

    SpawnEntityAtPosition(world, EntityPrefab_DirectionalLight, Vec3(0, 5, 0));

    SpawnEntityAtPosition(world, EntityPrefab_AmbientLight, Vec3(1, 5, 0));

    SpawnEntityAtPositionWithScale(
        world, EntityPrefab_BoxGeometry, Vec3(0, -1, 0), Vec3(250, 0.5, 250));
#endif
}

internal i32 Cmd_LoadMap(ecs_EntityWorld *world, GameAssets *assets, GameMemory *memory)
{
    LoadMap(world, assets);
#if 0
    DebugReadFileResult readResult = memory->readEntireFile("./test_map");
    Assert(readResult.size > 0);
    ecs_DeleteAllEntities(world);

    ecs_DeserializeWorld(world, readResult.contents, readResult.size);
    LogConsole("Map loaded from ./test_map");
#endif

    return 0;
}

internal i32 Cmd_DumpClientData(ServerGameState *gameState)
{
    LogConsole("INDEX\tCLIENT ID\tENTITY ID\tSECONDS UNTIL RESPAWN");
    for (u32 i = 0; i < gameState->clientCount; ++i)
    {
        ClientData *clientData = gameState->clients + i;
        LogConsole("%u\t%u\t%u\t%g", i, clientData->clientId,
            clientData->entityToControl, clientData->secondsUntilRespawn);
    }

    return 0;
}

internal void cl_HandleCommand(
    CommandExecutionEvent *event, GameState *gameState, GameMemory *memory)
{
    switch (event->commandId)
    {
        case CommandID_MemInfo:
            Cmd_MemInfo(gameState);
            break;
        case CommandID_CollisionTestbench:
            Cmd_CollisionTestbench(gameState);
            break;
        //case CommandID_RenderTest:
            //Cmd_RenderTest(gameState);
            //break;
        case CommandID_LoadMap:
            Cmd_LoadMap(&gameState->entityWorld, &gameState->assets, memory);
            break;
        case CommandID_ServerExec:
            Cmd_ServerExec(memory, event->argc, event->argv);
            break;
        case CommandID_PlaySound:
            Cmd_PlaySound(gameState, event->argc, event->argv);
            break;
        case CommandID_Connect:
            Cmd_Connect(gameState, memory, event->argc, event->argv);
            break;
        default:
            com_HandleCommand(event, &gameState->commandSystem,
                &gameState->cvarTable, memory, &gameState->entityWorld,
                &gameState->tempArena, &gameState->collisionDebugState);
            break;
    }
}

internal void sv_HandleCommand(CommandExecutionEvent *event,
    ServerGameState *gameState, GameMemory *memory)
{
    switch (event->commandId)
    {
        case CommandID_AddItem:
            Cmd_AddItem(&gameState->entityWorld, event->argc, event->argv);
            break;
        case CommandID_DumpClientData:
            Cmd_DumpClientData(gameState);
            break;
        default:
            com_HandleCommand(event, &gameState->commandSystem,
                &gameState->cvarTable, memory, &gameState->entityWorld,
                &gameState->tempArena, &gameState->collisionDebugState);
            break;
    }
}

// Must be called on code reload as we use static strings who's address will
// change on code reload
internal void RegisterCommands(
    Cmd_System *cmdSystem, CommandRegistrationData *commands, u32 count)
{
    cmdSystem->count = 0;
    for (u32 i = 0 ; i < count; ++i)
    {
        Cmd_Register(cmdSystem, commands[i].string, commands[i].id);
    }
}

internal void LoadFonts(GameState *gameState, GameMemory *memory)
{
    DebugReadFileResult fontFile =
        memory->readEntireFile("../content/Inconsolata-Regular.ttf");
    if (fontFile.contents != NULL)
    {
        CreateFont(&gameState->debugFont, fontFile.contents, fontFile.size, memory,
                &gameState->tempArena, 256, 256, 18.0f, Texture_DebugFont);
        CreateFont(&gameState->testFont, fontFile.contents, fontFile.size, memory,
                &gameState->tempArena, 512, 512, 32.0f, Texture_TestFont);
        CreateFont(&gameState->mainMenuTitleFont, fontFile.contents,
                fontFile.size, memory, &gameState->tempArena, 512, 512, 48.0f,
                Texture_MainMenuTitleFont);
    }
}

internal void LoadMinimalContent(GameState *gameState, GameMemory *memory)
{
    LoadInternalMeshes(memory, &gameState->tempArena);
    LoadInternalShaders(memory, &gameState->tempArena);
    LoadFonts(gameState, memory);
    CreateVertexBuffers(memory);


    RenderCommand_CreateRenderTarget *createShadowBuffer =
        AllocateRenderCommand(memory, RenderCommand_CreateRenderTarget);
    createShadowBuffer->id = RenderTarget_ShadowBuffer;
    createShadowBuffer->width = 4096;
    createShadowBuffer->height = 4096;
    createShadowBuffer->isShadowBuffer = true;

    RenderCommand_CreateShader *createVignetteShader =
        AllocateRenderCommand(memory, RenderCommand_CreateShader);
    createVignetteShader->id = Shader_FullscreenVignette;
    createVignetteShader->shaderSource = fullscreenVignetteSource;
    createVignetteShader->depthTestEnabled = false;
    createVignetteShader->alphaBlendingEnabled = true;
    AddUniformDefinition(createVignetteShader, UniformValue_MvpMatrix,
            UniformType_Mat4, "mvp");
}

internal void DrawLoadingScreen(GameState *gameState, GameMemory *memory)
{
    mat4 orthographic = Orthographic(0.0f, (f32)memory->frameBufferWidth, 0.0f,
        (f32)memory->frameBufferHeight);

    // Draw loading screen
    {
        RenderCommand_Clear *clear =
            AllocateRenderCommand(memory, RenderCommand_Clear);
        clear->color = Vec4(0.05, 0.05, 0.05, 1.0);
    }

    ui_DrawStringArguments loadingText = {};
    loadingText.memory = memory;
    loadingText.vertexBuffer = &gameState->textBuffer;
    loadingText.text = "Loading ...";
    loadingText.position = Vec2((f32)memory->frameBufferWidth * 0.5f, 50.0f);
    loadingText.color = Vec4(0.4, 0.4, 0.4, 1.0);
    loadingText.font = &gameState->mainMenuTitleFont;
    loadingText.orthographicProjection = orthographic;
    loadingText.horizontalAlignment = HorizontalAlign_Center;
    loadingText.verticalAlignment = VerticalAlign_Center;
    ui_DrawString(&loadingText);

    // Draw vignette effect
    {
        RenderCommand_DrawMesh *drawVignette =
            AllocateRenderCommand(memory, RenderCommand_DrawMesh);
        drawVignette->mesh = Mesh_Quad;
        drawVignette->shader = Shader_FullscreenVignette;
        AddShaderUniformValueMat4(drawVignette, UniformValue_MvpMatrix,
                orthographic *
                WorldTransform(Vec3(memory->frameBufferWidth * 0.5f,
                        memory->frameBufferHeight * 0.5f, 0.0f),
                    Vec3((f32)memory->frameBufferWidth,
                        -(f32)memory->frameBufferHeight, 1.0f)));
    }
}

internal void DrawMainMenu(GameState *gameState, GameMemory *memory, GameInput *input)
{
    mat4 orthographic = Orthographic(0.0f, (f32)memory->frameBufferWidth, 0.0f,
        (f32)memory->frameBufferHeight);

    {
        RenderCommand_Clear *clear = AllocateRenderCommand(memory, RenderCommand_Clear);
        clear->color = Vec4(0.35, 0.58, 0.9, 1.0);
    }

    LayoutState layoutState = {};
    InitLayout(&layoutState, (f32)memory->frameBufferWidth,
        (f32)memory->frameBufferHeight);

    ui_DrawQuadArguments drawPanel = {};
    drawPanel.memory = memory;
    drawPanel.orthographicProjection = orthographic;
    drawPanel.position = GetTopLeft(&layoutState) - Vec2(0, 50);
    drawPanel.width = GetWidth(&layoutState);
    drawPanel.height = 60.0f;
    drawPanel.color = Vec4(0, 0, 0, 0.2);
    drawPanel.horizontalAlignment = HorizontalAlign_Left;
    drawPanel.verticalAlignment = VerticalAlign_Top;
    ui_DrawQuad(&drawPanel);
    PushRect(&layoutState, Rect(drawPanel.position, drawPanel.width, drawPanel.height));

    ui_DrawStringArguments drawMainMenu = {};
    drawMainMenu.memory = memory;
    drawMainMenu.vertexBuffer = &gameState->textBuffer;
    drawMainMenu.text = "<Main Menu Title>";
    drawMainMenu.position = GetCenterCenter(&layoutState);
    drawMainMenu.color = Vec4(0.1, 0.1, 0.1, 1.0);
    drawMainMenu.font = &gameState->mainMenuTitleFont;
    drawMainMenu.orthographicProjection = orthographic;
    drawMainMenu.horizontalAlignment = HorizontalAlign_Center;
    drawMainMenu.verticalAlignment = VerticalAlign_Center;
    ui_DrawString(&drawMainMenu);
    PopRect(&layoutState);

    ui_DrawButtonArguments startServerButton = {};
    startServerButton.memory = memory;
    startServerButton.orthographicProjection = orthographic;
    startServerButton.vertexBuffer= &gameState->textBuffer;
    startServerButton.font = &gameState->testFont;
    startServerButton.color = Vec4(0.6, 0.6, 0.6, 1.0);
    startServerButton.hoverColor = Vec4(0.7, 0.7, 0.7, 1.0);
    startServerButton.clickColor = Vec4(0.7, 0.7, 0.9, 1.0);
    startServerButton.textColor = Vec4(0.05, 0.05, 0.05, 1.0);
    startServerButton.input = input;
    startServerButton.text = "Start Server";
    startServerButton.position = GetBottomLeft(&layoutState) + Vec2(50.0f, 150.0f);

    ui_DrawButtonArguments playButton = {};
    playButton.memory = memory;
    playButton.orthographicProjection = orthographic;
    playButton.vertexBuffer= &gameState->textBuffer;
    playButton.font = &gameState->testFont;
    playButton.color = Vec4(0.6, 0.6, 0.6, 1.0);
    playButton.hoverColor = Vec4(0.7, 0.7, 0.7, 1.0);
    playButton.clickColor = Vec4(0.7, 0.7, 0.9, 1.0);
    playButton.textColor = Vec4(0.05, 0.05, 0.05, 1.0);
    playButton.input = input;
    playButton.text = "Play Game";
    playButton.position = GetBottomLeft(&layoutState) + Vec2(50.0f, 100.0f);

    if (ui_DrawButton(&startServerButton))
    {
        memory->startServer();
        LogMessage("Server started!");
    }

    if (ui_DrawButton(&playButton))
    {
        gameState->state = GameState_Experimental;
        memory->startClient("127.0.0.1");

        DrawLoadingScreen(gameState, memory);
    }
}

internal void InitializeMaterials(GameState *gameState)
{
    ClearToZero(gameState->materials, sizeof(gameState->materials));

    gameState->materials[Material_GreenDiffuse].shader = Shader_DiffuseColor;
    gameState->materials[Material_GreenDiffuse].useLightingData = true;
    gameState->materials[Material_GreenDiffuse].useTexture = false;
    gameState->materials[Material_GreenDiffuse].color = Vec4(0.3, 0.8, 0.2, 1.0);

    gameState->materials[Material_DevGrid512].shader = Shader_DiffuseTexture;
    gameState->materials[Material_DevGrid512].useLightingData = true;
    gameState->materials[Material_DevGrid512].useTexture = true;
    gameState->materials[Material_DevGrid512].texture = Texture_DevGrid512;

    gameState->materials[Material_StorageBox].shader = Shader_DiffuseColor;
    gameState->materials[Material_StorageBox].useLightingData = true;
    gameState->materials[Material_StorageBox].useTexture = false;
    gameState->materials[Material_StorageBox].color = Vec4(0.1f, 0.2f, 0.4f, 1.0f);

    gameState->materials[Material_BuildingPreview].shader = Shader_DiffuseColor;
    gameState->materials[Material_BuildingPreview].useLightingData = true;
    gameState->materials[Material_BuildingPreview].useTexture = false;
    gameState->materials[Material_BuildingPreview].color = Vec4(0.0f, 1.0f, 0.0f, 1.0f);

    gameState->materials[Material_Red].shader = Shader_DiffuseColor;
    gameState->materials[Material_Red].useLightingData = true;
    gameState->materials[Material_Red].useTexture = false;
    gameState->materials[Material_Red].color = Vec4(1.0f, 0.0f, 0.0f, 1.0f);

    gameState->materials[Material_WorldDevGrid512].shader = Shader_WorldDiffuseTexture;
    gameState->materials[Material_WorldDevGrid512].useLightingData = true;
    gameState->materials[Material_WorldDevGrid512].useTexture = true;
    gameState->materials[Material_WorldDevGrid512].texture = Texture_DevGrid512;

    gameState->materials[Material_Hatchet].shader = Shader_DiffuseTexture;
    gameState->materials[Material_Hatchet].useLightingData = true;
    gameState->materials[Material_Hatchet].useTexture = true;
    gameState->materials[Material_Hatchet].texture = Texture_HatchetBaseColor;

    gameState->materials[Material_Skybox].shader = Shader_CubeMap;
    gameState->materials[Material_Skybox].useLightingData = false;
    gameState->materials[Material_Skybox].useTexture = true;
    gameState->materials[Material_Skybox].texture = Texture_Skybox;

    gameState->materials[Material_VisualizeNormals].shader = Shader_VisualizeNormals;
    gameState->materials[Material_VisualizeNormals].useLightingData = false;
    gameState->materials[Material_VisualizeNormals].ignoreColor = true;

    gameState->materials[Material_NormalMapTest].shader = Shader_DiffuseNormalMapping;
    gameState->materials[Material_NormalMapTest].useLightingData = true;
    gameState->materials[Material_NormalMapTest].useNormalMap = true;
    gameState->materials[Material_NormalMapTest].color = Vec4(0.3, 0.8, 0.2, 1.0);
    gameState->materials[Material_NormalMapTest].texture = Texture_TestNormalMap;

    gameState->materials[Material_UVTest].shader = Shader_DiffuseTexture;
    gameState->materials[Material_UVTest].useLightingData = true;
    gameState->materials[Material_UVTest].useTexture = true;
    gameState->materials[Material_UVTest].texture = Texture_UVGrid;

    gameState->materials[Material_Skinning].shader = Shader_Skinning;
    gameState->materials[Material_Skinning].useLightingData = true;
    gameState->materials[Material_Skinning].useTexture = false;
    gameState->materials[Material_Skinning].color = Vec4(0.3, 0.8, 0.2, 1.0);
    gameState->materials[Material_Skinning].useBoneTransformsBuffer = true;
}

internal void RenderParticleSystems(GameState *gameState, GameMemory *memory,
    f32 dt, CameraProperties *cameraProperties)
{
    for (u32 i = 0; i < gameState->particleSystemCount; ++i)
    {
        RenderParticleSystems(gameState->particleSystemValues[i], dt,
            &gameState->debugDrawingSystem, memory,
            cameraProperties->projection, cameraProperties->view,
            cameraProperties->position, &gameState->rng);
    }
}

internal void DrawWorldGrid(DebugDrawingSystem *debugDrawingSystem, vec3 origin)
{
    f32 cellSize = 1.0f;
    f32 size = 128.0f;
    f32 halfSize = size * 0.5f;
    vec3 lineColor = Vec3(0.3, 0.3, 0.3);
    f32 lineLength = 1024.0f;
    for (f32 x = -halfSize; x < halfSize; x+= cellSize)
    {
        vec3 start = origin + Vec3(x, 0, -halfSize);
        vec3 end = origin + Vec3(x, 0, halfSize);
        DrawLine(debugDrawingSystem, start, end, lineColor, 0.0f, true);
    }

    for (f32 z = -halfSize; z < halfSize; z+= cellSize)
    {
        vec3 start = origin + Vec3(-halfSize, 0, z);
        vec3 end = origin + Vec3(halfSize, 0, z);
        DrawLine(debugDrawingSystem, start, end, lineColor, 0.0f, true);
    }
}

internal void GameRender(GameState *gameState, GameMemory *memory,
    GameInput *input, f32 dt)
{
    TIMED_BLOCK();

    ecs_EntityWorld *world = &gameState->entityWorld;

    DebugDrawingSystem *debugDrawingSystem = &gameState->debugDrawingSystem;

    mat4 orthographic = Orthographic(0.0f, (f32)memory->frameBufferWidth, 0.0f,
        (f32)memory->frameBufferHeight);

    float shadowRadius = 100.0f;
    mat4 shadowProjection = Orthographic(-shadowRadius, shadowRadius,
        -shadowRadius, shadowRadius, -shadowRadius, shadowRadius);

    CameraProperties cameraProperties =
        GetActiveCameraProperties(&gameState->entityWorld,
            (f32)memory->frameBufferWidth, (f32)memory->frameBufferHeight);
    g_CameraPosition = cameraProperties.position;

    EntityId sun = NULL_ENTITY;
    GetFirstEntity(&gameState->entityWorld, DirectionalLightComponentId, &sun);
    Assert(sun != NULL_ENTITY);

    quat sunRotation;
    ecs_GetComponent(&gameState->entityWorld, RotationComponentId, &sun, 1,
        &sunRotation, sizeof(sunRotation));

    vec3 sunVector = Rotate(sunRotation, Vec3(0, -1, 0));
    vec3 sunDirection = Normalize(sunVector);

    // Rotate camera to look in X+ direction as ChangeOfBasis uses input
    // vector as X-axis
    mat4 shadowView = ChangeOfBasis(sunDirection) * RotateY(PI * -0.5f);
    shadowView = Transpose(shadowView) * Translate(-cameraProperties.position);

//#define DEBUG_SUN_SHADOWS
#ifdef DEBUG_SUN_SHADOWS
    perspective = shadowProjection;
    view = shadowView;
#endif

#define DRAW_SHADOWS 1
#if DRAW_SHADOWS
    // Draw shadows
    {
        RenderCommand_BindRenderTarget *bindShadowBuffer =
            AllocateRenderCommand(memory, RenderCommand_BindRenderTarget);
        bindShadowBuffer->id = RenderTarget_ShadowBuffer;

        RenderCommand_Clear *clearShaderBuffer =
            AllocateRenderCommand(memory, RenderCommand_Clear);
        clearShaderBuffer->color = Vec4(0, 0, 0, 1);

        if (CVar_GetValueU32(&gameState->cvarTable, CVAR(r_draw_shadows)))
        {
            DrawShadowsSystem(&gameState->entityWorld, &gameState->tempArena,
                memory, shadowProjection * shadowView);
        }
    }
#endif
    // Populate lighting data buffer
    {
        LightingDataUniformBuffer *lightingData =
            AllocateStruct(&gameState->tempArena, LightingDataUniformBuffer);
        ClearToZero(lightingData, sizeof(*lightingData));

        RenderAmbientLightSystem(&gameState->entityWorld, lightingData);
        RenderDirectionalLightsSystem(&gameState->entityWorld, lightingData);
        RenderPointLightsSystem(&gameState->entityWorld, lightingData);
        RenderSpotLightsSystem(&gameState->entityWorld, lightingData);

        RenderCommand_UpdateUniformBuffer *updateLightingData =
            AllocateRenderCommand(memory, RenderCommand_UpdateUniformBuffer);
        updateLightingData->id = UniformBuffer_LightingData;
        updateLightingData->data = lightingData;
        updateLightingData->length = sizeof(*lightingData);
        updateLightingData->offset = 0;
    }

    // Render to HDR buffer for tone mapping
    {
        RenderCommand_BindRenderTarget *bindHdrBuffer =
            AllocateRenderCommand(memory, RenderCommand_BindRenderTarget);
        bindHdrBuffer->id = RenderTarget_HdrBuffer;

        RenderCommand_Clear *clearHdrBuffer =
            AllocateRenderCommand(memory, RenderCommand_Clear);
        clearHdrBuffer->color = Vec4(0.35, 0.58, 0.9, 1.0);
    }

    if (CVar_GetValueU32(&gameState->cvarTable, CVAR(r_draw_origin)))
    {
        DrawAxis(debugDrawingSystem, Identity(), 1.0f);
    }

    if (CVar_GetValueU32(&gameState->cvarTable, CVAR(r_draw_grid)))
    {
        vec3 gridOrigin = cameraProperties.position;
        gridOrigin.x = Floor(gridOrigin.x);
        gridOrigin.y = 0.0f;
        gridOrigin.z = Floor(gridOrigin.z);
        DrawWorldGrid(debugDrawingSystem, gridOrigin);
    }

    if (CVar_GetValueU32(&gameState->cvarTable, CVAR(r_draw_entity_positions)))
    {
        RenderPositionSystem(&gameState->entityWorld, debugDrawingSystem);
    }

    // Draw scene
    if (CVar_GetValueU32(&gameState->cvarTable, CVAR(r_draw_skybox)))
    {
        RenderSkyboxSystem(&gameState->entityWorld, memory,
            cameraProperties.rotation, cameraProperties.projection,
            gameState->materials, ArrayCount(gameState->materials));
    }

    if (CVar_GetValueU32(&gameState->cvarTable, CVAR(r_draw_terrain)))
    {
        RenderQuadtreeTerrainSystem(&gameState->entityWorld, memory,
            cameraProperties.position, cameraProperties.view,
            cameraProperties.projection, shadowProjection * shadowView,
            gameState->materials, ArrayCount(gameState->materials), false,
            debugDrawingSystem, gameState->assets.heightMap,
            gameState->assets.renderQuadTree);
    }

    UpdateLocalTransformSystem(world);
    DirtyWorldTransformSystem(&gameState->entityWorld);
    DirtyChildWorldTransformSystem(&gameState->entityWorld);
    UpdateWorldTransformSystem(&gameState->entityWorld);
    DrawWorldTransformsSystem(world, debugDrawingSystem);
    DrawBonesSystem(world, debugDrawingSystem);
    UpdateBoneTransformsSystem(
        world, &gameState->tempArena, memory, &gameState->assets);
    //DrawBoneNamesSystem(world, &gameState->animPlaybackState, debugDrawingSystem);

    if (CVar_GetValueU32(&gameState->cvarTable, CVAR(r_draw_meshes)))
    {
        RenderMeshesSystem(&gameState->entityWorld, &gameState->tempArena,
            memory, cameraProperties.view, cameraProperties.projection,
            cameraProperties.position, shadowProjection * shadowView,
            gameState->materials, ArrayCount(gameState->materials));
    }

    RenderInstancedMeshesSystem(&gameState->entityWorld, &gameState->tempArena,
        memory, cameraProperties.view, cameraProperties.projection,
        cameraProperties.position, shadowProjection * shadowView,
        gameState->materials, ArrayCount(gameState->materials));

    if (CVar_GetValueU32(&gameState->cvarTable, CVAR(r_draw_particles)))
    {
        RenderParticleSystems(gameState, memory, dt, &cameraProperties);
    }

    // FIXME: Make this an entity instead!
    DrawViewModel(&gameState->entityWorld, memory,
        cameraProperties.projection * cameraProperties.view,
        shadowProjection * shadowView, cameraProperties.position,
        gameState->materials, ArrayCount(gameState->materials));

    DrawLightIconsSystem(world, memory,
        cameraProperties.projection * cameraProperties.view,
        cameraProperties.position);

    {
        mat4 mvp = cameraProperties.projection * cameraProperties.view;
        for (u32 i = 0; i < debugDrawingSystem->worldTextCount; ++i)
        {
            DebugWorldText *worldText = debugDrawingSystem->worldText + i;
            vec4 v = mvp * Vec4(worldText->position, 1.0f);
            if (v.w > 0.0f)
            {
                vec3 p = v.xyz * (1.0f / v.w);
                f32 x = p.x * 0.5f + 0.5f;
                f32 y = p.y * 0.5f + 0.5f;

                ui_DrawStringArguments args;
                args.memory = memory;
                args.vertexBuffer = &gameState->textBuffer;
                args.text = worldText->text;
                args.length = worldText->textLength;
                args.position = Vec2(x * memory->frameBufferWidth,
                        y * memory->frameBufferHeight);
                args.color = Vec4(1.0f);
                args.font = &gameState->debugFont;
                args.orthographicProjection = orthographic;
                args.horizontalAlignment = HorizontalAlign_Center;
                args.verticalAlignment = VerticalAlign_Center;
                ui_DrawString(&args);
            }
        }
    }
    {
        // TODO: Investigate how this is working and update it
        // Draw collision debug state
        if (CVar_GetValueU32(
                &gameState->cvarTable, CVAR(collision_debug_enabled)))
        {
            CollisionDebugState *state = &gameState->collisionDebugState;

            u32 selectedCall = CVar_GetValueU32(
                &gameState->cvarTable, CVAR(collision_debug_selected_call));
            selectedCall = Clamp((i32)selectedCall, 0, state->count);

            if (state->count > selectedCall)
            {
                CollisionDebugCall *call = state->calls + selectedCall;
                CollisionDebug_ReplayCall(
                    call, debugDrawingSystem, &gameState->tempArena);
            }
        }

        // Allocate vertices for both client and server debug drawing
        VertexPC *debugLineVertices =
            AllocateArray(&gameState->tempArena, VertexPC, MAX_DEBUG_LINES * 4);
        u32 debugLineVertexCount = Debug_GetVertices(
            debugDrawingSystem, debugLineVertices, MAX_DEBUG_LINES * 2, true);

        // TODO: API for reading server state
        // FIXME: Must also check that the server is running because if it is
        // stopped g_ServerDebugDrawingSystem will not be cleared.
        if (g_ServerDebugDrawingSystem != NULL)
        {
            debugLineVertexCount +=
                Debug_GetVertices(g_ServerDebugDrawingSystem,
                    debugLineVertices + debugLineVertexCount,
                    MAX_DEBUG_LINES * 2, true);
        }

        {
            RenderCommand_UpdateVertexBuffer *updateLinesBuffer =
                AllocateRenderCommand(memory, RenderCommand_UpdateVertexBuffer);
            updateLinesBuffer->id = VertexBuffer_DebugLines;
            updateLinesBuffer->vertexLayout = VertexLayout_PositionAndColor;
            updateLinesBuffer->vertices = debugLineVertices;
            updateLinesBuffer->vertexCount = debugLineVertexCount;
        }

        {
            RenderCommand_DrawVertexBuffer *drawLinesBuffer =
                AllocateRenderCommand(memory, RenderCommand_DrawVertexBuffer);
            drawLinesBuffer->id = VertexBuffer_DebugLines;
            drawLinesBuffer->shader = Shader_VertexColor;
            drawLinesBuffer->primitive = Primitive_Lines;
            drawLinesBuffer->firstVertex = 0;
            drawLinesBuffer->vertexCount = debugLineVertexCount;
            AddShaderUniformValueMat4(drawLinesBuffer, UniformValue_MvpMatrix,
                cameraProperties.projection * cameraProperties.view);
        }
    }

    // Bind default frame buffer
    RenderCommand_BindRenderTarget *bindBackBuffer =
        AllocateRenderCommand(memory, RenderCommand_BindRenderTarget);
    bindBackBuffer->id = RenderTarget_BackBuffer;

    RenderCommand_Clear *clearFramebuffer =
        AllocateRenderCommand(memory, RenderCommand_Clear);
    clearFramebuffer->color = Vec4(0, 0, 0, 1);

    // Draw tone mapped HDR buffer
    {
        RenderCommand_DrawMesh *drawHdrBuffer =
            AllocateRenderCommand(memory, RenderCommand_DrawMesh);
        drawHdrBuffer->mesh = Mesh_Quad;
        drawHdrBuffer->shader = Shader_Tonemapping;
        AddShaderUniformValueRenderTarget(
            drawHdrBuffer, UniformValue_HdrBuffer, RenderTarget_HdrBuffer);
        AddShaderUniformValueMat4(drawHdrBuffer, UniformValue_MvpMatrix,
            orthographic *
                WorldTransform(Vec3(memory->frameBufferWidth * 0.5f,
                                   memory->frameBufferHeight * 0.5f, 0.0f),
                    Vec3((f32)memory->frameBufferWidth,
                        (f32)memory->frameBufferHeight, 1.0f)));
    }

    // Render UI elements on top
    if (gameState->showGameUI)
    {
        EntityId player;
        if (GetFirstEntity(world, ActivePlayerComponentId, &player))
        {
            if (gameState->showInventory)
            {
                // Draw inventory system
                EntityId inventory[INVENTORY_SLOT_COUNT];
                GetInventorySlots(
                    world, player, inventory, ArrayCount(inventory));

                EntityId subjectInventory[INVENTORY_SLOT_COUNT];
                EntityId subject = NULL_ENTITY;
                ecs_GetComponent(world, OpenContainerComponentId, &player, 1,
                    &subject, sizeof(subject));
                if (subject != NULL_ENTITY)
                {
                    GetInventorySlots(world, subject, subjectInventory,
                        ArrayCount(subjectInventory));
                }

                DrawInventory(world, inventory, ArrayCount(inventory), memory,
                    input, orthographic, &gameState->inventoryState, player,
                    subject, subjectInventory, ArrayCount(subjectInventory),
                    &gameState->textBuffer, &gameState->debugFont);
            }

            b32 drawActionBar = true;
            if (drawActionBar)
            {
                // Draw actionbar system
                EntityId actionBar[ACTION_BAR_SLOT_COUNT];
                GetActionBarSlots(
                    world, player, actionBar, ArrayCount(actionBar));

                u32 activeSlot = 0;
                ecs_GetComponent(world, ActionBarActiveSlotComponentId, &player,
                    1, &activeSlot, sizeof(activeSlot));

                DrawActionBar(world, actionBar, ArrayCount(actionBar), memory,
                    input, orthographic, &gameState->inventoryState, player,
                    activeSlot, gameState->showInventory,
                    &gameState->textBuffer, &gameState->debugFont);
            }

            b32 drawStatusBars = true;
            if (drawStatusBars)
            {
                // Health display system
                f32 health;
                ecs_GetComponent(&gameState->entityWorld, HealthComponentId,
                    &player, 1, &health, sizeof(health));
                {
                    ui_DrawProgressBar drawHealthBar = {};
                    drawHealthBar.memory = memory;
                    drawHealthBar.vertexBuffer = &gameState->textBuffer;
                    drawHealthBar.orthographic = orthographic;
                    drawHealthBar.position =
                        Vec2((f32)memory->frameBufferWidth - 270.0f, 150.0f);
                    drawHealthBar.width = 260.0f;
                    drawHealthBar.height = 30.0f;
                    drawHealthBar.color = Vec4(1.0f, 0.2f, 0.2f, 1.0f);
                    drawHealthBar.bgColor = Vec4(0.1, 0.1, 0.1, 0.6);
                    drawHealthBar.percentage = health / MAX_PLAYER_HEALTH;

                    char healthString[32];
                    snprintf(healthString, sizeof(healthString), "HEALTH: %d",
                        (i32)health);
                    drawHealthBar.text = healthString;
                    drawHealthBar.textColor = Vec4(1, 1, 1, 1);
                    drawHealthBar.font = &gameState->testFont;
                    DrawProgressBar(&drawHealthBar);
                }

                // Food display
                {
                    // TODO: Game logic for this!
                    ui_DrawProgressBar drawHealthBar = {};
                    drawHealthBar.memory = memory;
                    drawHealthBar.vertexBuffer = &gameState->textBuffer;
                    drawHealthBar.orthographic = orthographic;
                    drawHealthBar.position =
                        Vec2((f32)memory->frameBufferWidth - 270.0f, 110.0f);
                    drawHealthBar.width = 260.0f;
                    drawHealthBar.height = 30.0f;
                    drawHealthBar.color = Vec4(1.0f, 0.7f, 0.2f, 1.0f);
                    drawHealthBar.bgColor = Vec4(0.1, 0.1, 0.1, 0.6);
                    drawHealthBar.percentage = 0.5f;

                    char healthString[32];
                    snprintf(healthString, sizeof(healthString), "FOOD: %d",
                        (i32)50);
                    drawHealthBar.text = healthString;
                    drawHealthBar.textColor = Vec4(1, 1, 1, 1);
                    drawHealthBar.font = &gameState->testFont;
                    DrawProgressBar(&drawHealthBar);
                }

                // Radiation display
                {
                    // TODO: Game logic for this!
                    ui_DrawProgressBar drawHealthBar = {};
                    drawHealthBar.memory = memory;
                    drawHealthBar.vertexBuffer = &gameState->textBuffer;
                    drawHealthBar.orthographic = orthographic;
                    drawHealthBar.position =
                        Vec2((f32)memory->frameBufferWidth - 270.0f, 70.0f);
                    drawHealthBar.width = 260.0f;
                    drawHealthBar.height = 30.0f;
                    drawHealthBar.color = Vec4(0.6f, 1.0f, 0.2f, 1.0f);
                    drawHealthBar.bgColor = Vec4(0.1, 0.1, 0.1, 0.6);
                    drawHealthBar.percentage = 0.1f;

                    char healthString[32];
                    snprintf(healthString, sizeof(healthString),
                        "RADIATION: %d", (i32)10);
                    drawHealthBar.text = healthString;
                    drawHealthBar.textColor = Vec4(1, 1, 1, 1);
                    drawHealthBar.font = &gameState->testFont;
                    DrawProgressBar(&drawHealthBar);
                }
            }

            b32 showCrosshair = true;
            if (showCrosshair)
            {
                // Debug crosshair
                ui_DrawQuadArguments drawCrosshair = {};
                drawCrosshair.memory = memory;
                drawCrosshair.orthographicProjection = orthographic;
                drawCrosshair.position = Vec2(memory->frameBufferWidth * 0.5f,
                    memory->frameBufferHeight * 0.5f);
                drawCrosshair.width = 4.0f;
                drawCrosshair.height = 4.0f;
                drawCrosshair.color = Vec4(1, 0, 1, 1);
                drawCrosshair.horizontalAlignment = HorizontalAlign_Center;
                drawCrosshair.verticalAlignment = VerticalAlign_Center;

                ui_DrawQuadArguments drawCrosshairShadow = drawCrosshair;
                drawCrosshairShadow.position += Vec2(1, -1);
                drawCrosshairShadow.color = Vec4(0, 0, 0, 1);

                ui_DrawQuad(&drawCrosshairShadow);
                ui_DrawQuad(&drawCrosshair);
            }

            b32 showPlayerPosition = true;
            if (showPlayerPosition)
            {
                // Draw player position and velocity
                vec3 position;
                vec3 velocity;
                vec3 eulerAngles;
                ecs_GetComponent(world, PositionComponentId, &player, 1,
                    &position, sizeof(position));
                ecs_GetComponent(world, VelocityComponentId, &player, 1,
                    &velocity, sizeof(velocity));
                ecs_GetComponent(world, EulerAnglesComponentId, &player, 1,
                    &eulerAngles, sizeof(eulerAngles));

                char positionString[64];
                snprintf(positionString, sizeof(positionString),
                    "%.04f %0.4f, %0.4f", position.x, position.y, position.z);

                ui_DrawStringArguments drawPosition = {};
                drawPosition.memory = memory;
                drawPosition.vertexBuffer = &gameState->textBuffer;
                drawPosition.text = positionString;
                drawPosition.position =
                    Vec2(10.0f, memory->frameBufferHeight - 20.0f);
                drawPosition.color = Vec4(1, 1, 0, 1);
                drawPosition.font = &gameState->debugFont;
                drawPosition.orthographicProjection = orthographic;
                drawPosition.horizontalAlignment = HorizontalAlign_Left;
                drawPosition.verticalAlignment = VerticalAlign_Center;
                ui_DrawString(&drawPosition);

                char rotationString[64];
                snprintf(rotationString, sizeof(rotationString),
                    "%.04f %0.4f, %0.4f", eulerAngles.x, eulerAngles.y,
                    eulerAngles.z);

                ui_DrawStringArguments drawRotation = {};
                drawRotation.memory = memory;
                drawRotation.vertexBuffer = &gameState->textBuffer;
                drawRotation.text = rotationString;
                drawRotation.position =
                    Vec2(10.0f, memory->frameBufferHeight - 40.0f);
                drawRotation.color = Vec4(1, 0.4, 0.2, 1);
                drawRotation.font = &gameState->debugFont;
                drawRotation.orthographicProjection = orthographic;
                drawRotation.horizontalAlignment = HorizontalAlign_Left;
                drawRotation.verticalAlignment = VerticalAlign_Center;
                ui_DrawString(&drawRotation);

                char velocityString[64];
                snprintf(velocityString, sizeof(velocityString),
                    "%.04f %0.4f, %0.4f", velocity.x, velocity.y, velocity.z);

                ui_DrawStringArguments drawVelocity = {};
                drawVelocity.memory = memory;
                drawVelocity.vertexBuffer = &gameState->textBuffer;
                drawVelocity.text = velocityString;
                drawVelocity.position =
                    Vec2(10.0f, memory->frameBufferHeight - 60.0f);
                drawVelocity.color = Vec4(0, 1, 1, 1);
                drawVelocity.font = &gameState->debugFont;
                drawVelocity.orthographicProjection = orthographic;
                drawVelocity.horizontalAlignment = HorizontalAlign_Left;
                drawVelocity.verticalAlignment = VerticalAlign_Center;
                ui_DrawString(&drawVelocity);
            }
        }

        b32 enableNetworking =
            CVar_GetValueU32(&gameState->cvarTable, CVAR(enable_networking));
        b32 showPing = CVar_GetValueU32(&gameState->cvarTable, CVAR(show_ping));
        if (enableNetworking && showPing)
        {
            char pingString[32];
            snprintf(pingString, sizeof(pingString), "PING: %g ms",
                memory->networkLatency);

            ui_DrawStringArguments drawPing = {};
            drawPing.memory = memory;
            drawPing.vertexBuffer = &gameState->textBuffer;
            drawPing.text = pingString;
            drawPing.position = Vec2(memory->frameBufferWidth - 10.0f,
                memory->frameBufferHeight - 40.0f);
            drawPing.color = Vec4(1, 1, 1, 1);
            drawPing.font = &gameState->debugFont;
            drawPing.orthographicProjection = orthographic;
            drawPing.horizontalAlignment = HorizontalAlign_Right;
            drawPing.verticalAlignment = VerticalAlign_Center;
            ui_DrawString(&drawPing);
        }
    }
}

inline b32 FindParticleSystemEntityMapping(GameState *gameState, u32 entityId)
{
    b32 found = false;
    for (u32 particleIdx = 0; particleIdx < gameState->particleSystemCount; ++particleIdx)
    {
        if (gameState->particleSystemKeys[particleIdx] == entityId)
        {
            found = true;
            break;
        }
    }

    return found;
}

inline ParticleSystemInstance *AllocateParticleSystemInstance(GameState *gameState)
{
    ParticleSystemInstance *instance = gameState->particleSystemsFreeList;
    if (instance != NULL)
    {
        Assert(gameState->particleSystemCount <
               ArrayCount(gameState->particleSystems));

        gameState->particleSystemsFreeList = instance->next;
        instance->next = NULL;
    }
    else
    {
        LogMessage("No free particle system is available");
    }

    return instance;
}

internal void AllocateParticleEmittersSystem(GameState *gameState)
{
    ecs_EntityWorld *world = &gameState->entityWorld;
    // Find all entities which have a ParticleSystemComponent but not a
    // ParticleSystemOwnerComponent
    EntityId entities[MAX_ENTITIES];
    u32 entityCount = ecs_GetEntitiesWithComponent(
        world, ParticleSystemComponentId, entities, ArrayCount(entities));

    u32 particleSystemIds[MAX_ENTITIES];
    ecs_GetComponent(world, ParticleSystemComponentId, entities, entityCount,
            particleSystemIds, sizeof(particleSystemIds[0]));

    for (u32 entityIdx = 0; entityIdx < entityCount; ++entityIdx)
    {
        EntityId entityId = entities[entityIdx];
        if (!FindParticleSystemEntityMapping(gameState, entityId))
        {
            // Allocate particle system instance
            ParticleSystemInstance *instance =
                AllocateParticleSystemInstance(gameState);

            if (instance != NULL)
            {
                vec3 position;
                ecs_GetComponent(&gameState->entityWorld, PositionComponentId,
                    entities + entityIdx, 1, &position, sizeof(position));

                instance->origin = position;

                u32 particleSystemId = particleSystemIds[entityIdx];
                InitializeParticleSystem(
                    instance, &gameState->rng, particleSystemId);

                Assert(gameState->particleSystemCount <
                       ArrayCount(gameState->particleSystemKeys));
                u32 instanceIdx = gameState->particleSystemCount++;
                gameState->particleSystemKeys[instanceIdx] = entityId;
                gameState->particleSystemValues[instanceIdx] = instance;
            }
        }
    }
}

internal void RemoveParticleSystem(GameState *gameState, EntityId entity)
{
    for (u32 entityIdx = 0; entityIdx < gameState->particleSystemCount; ++entityIdx)
    {
        if (gameState->particleSystemKeys[entityIdx] == entity)
        {
            // Free particle system instance
            ParticleSystemInstance *instance =
                gameState->particleSystemValues[entityIdx];
            Assert(instance != NULL);
            instance->next = gameState->particleSystemsFreeList;
            gameState->particleSystemsFreeList = instance;

            // Swap and reduce
            u32 last = gameState->particleSystemCount - 1;
            gameState->particleSystemKeys[entityIdx] = gameState->particleSystemKeys[last];
            gameState->particleSystemValues[entityIdx] = gameState->particleSystemValues[last];
            gameState->particleSystemCount--;
            break;
        }
    }
}

internal void UpdateGameStateExperimental(
    GameState *gameState, GameMemory *memory, GameInput *input, f32 dt)
{
    ecs_EntityWorld *world = &gameState->entityWorld;

    if (!gameState->gameContentLoaded)
    {
        LoadShaders(memory, &gameState->tempArena);
        LoadSlowContent(
            &gameState->assets, memory, true, &gameState->tempArena);
        InitializeMaterials(gameState);

        // Asset code
        InitializeQuadTree(&gameState->assets.heightMapQuadTree,
            TERRAIN_COLLISION_MAX_NODES, &gameState->assets.terrainCollisionArena);
        InitializeQuadTree(&gameState->assets.renderQuadTree,
            TERRAIN_RENDERING_MAX_NODES,
            &gameState->assets.terrainCollisionArena);

        // Entity code
        LoadMap(world, &gameState->assets);

        {
            VertexSkinning vertices[12] = {};
            vertices[0].position = Vec3(-0.5, 0, -0.5);
            vertices[0].normal = Normalize(Vec3(-1, 0, -1));
            vertices[0].boneId = 0;
            vertices[1].position = Vec3(-0.5, 0, 0.5);
            vertices[1].normal = Normalize(Vec3(-1, 0, 1));
            vertices[1].boneId = 0;
            vertices[2].position = Vec3(0.5, 0, 0.5);
            vertices[2].normal = Normalize(Vec3(1, 0, 1));
            vertices[2].boneId = 0;
            vertices[3].position = Vec3(0.5, 0, -0.5);
            vertices[3].normal = Normalize(Vec3(1, 0, -1));
            vertices[3].boneId = 0;

            vertices[4].position = Vec3(-0.5, 0.5, -0.5);
            vertices[4].normal = Normalize(Vec3(-1, 0, -1));
            vertices[4].boneId = 1;
            vertices[5].position = Vec3(-0.5, 0.5, 0.5);
            vertices[5].normal = Normalize(Vec3(-1, 0, 1));
            vertices[5].boneId = 1;
            vertices[6].position = Vec3(0.5, 0.5, 0.5);
            vertices[6].normal = Normalize(Vec3(1, 0, 1));
            vertices[6].boneId = 1;
            vertices[7].position = Vec3(0.5, 0.5, -0.5);
            vertices[7].normal = Normalize(Vec3(1, 0, -1));
            vertices[7].boneId = 1;

            vertices[8].position = Vec3(-0.5, 1, -0.5);
            vertices[8].normal = Normalize(Vec3(-1, 0, -1));
            vertices[8].boneId = 2;
            vertices[9].position = Vec3(-0.5, 1, 0.5);
            vertices[9].normal = Normalize(Vec3(-1, 0, 1));
            vertices[9].boneId = 2;
            vertices[10].position = Vec3(0.5, 1, 0.5);
            vertices[10].normal = Normalize(Vec3(1, 0, 1));
            vertices[10].boneId = 2;
            vertices[11].position = Vec3(0.5, 1, -0.5);
            vertices[11].normal = Normalize(Vec3(1, 0, -1));
            vertices[11].boneId = 2;

            u32 indices[] = {
                0, 3, 2, 2, 1, 0, // -Y
                0, 1, 5, 5, 4, 0, // -X
                2, 3, 7, 7, 6, 2, //  X
                3, 0, 4, 4, 7, 3, // -Z
                1, 2, 6, 6, 5, 1, //  Z

                4, 5, 9, 9, 8, 4, // -X
                6, 7, 11, 11, 10, 6, //  X
                7, 4, 8, 8, 11, 7, // -Z
                5, 6, 10, 10, 9, 5, //  Z

                8, 9, 10, 10, 11, 8, // +Y
            };

            VertexSkinning *vertexBuffer = AllocateArray(
                &gameState->tempArena, VertexSkinning, ArrayCount(vertices));
            CopyMemory(vertexBuffer, vertices, sizeof(vertices));

            u32 *indexBuffer =
                AllocateArray(&gameState->tempArena, u32, ArrayCount(indices));
            CopyMemory(indexBuffer, indices, sizeof(indices));

            RenderCommand_CreateMesh *createMesh =
                AllocateRenderCommand(memory, RenderCommand_CreateMesh);
            createMesh->vertices = vertexBuffer;
            createMesh->vertexCount = ArrayCount(vertices);
            createMesh->vertexLayout = VertexLayout_Skinning;
            createMesh->indices = indexBuffer;
            createMesh->indexCount = ArrayCount(indices);
            createMesh->id = Mesh_SkinningTest;
        }

        {
            Skeleton *skeleton = &gameState->assets.skeletons[0];
            skeleton->boneCount = 3;

            // Currently we always expect bone 0 to be root bone
            skeleton->parents[0] = INVALID_BONE_IDX;
            skeleton->parents[1] = 0;
            skeleton->parents[2] = 1;

            skeleton->bindPose[0].position = Vec3(0);
            skeleton->bindPose[0].scale = Vec3(1);
            skeleton->bindPose[0].rotation = Quat();

            skeleton->bindPose[1].position = Vec3(0, 0.5, 0);
            skeleton->bindPose[1].scale = Vec3(1);
            skeleton->bindPose[1].rotation = Quat();

            skeleton->bindPose[2].position = Vec3(0, 0.5, 0);
            skeleton->bindPose[2].scale = Vec3(1);
            skeleton->bindPose[2].rotation = Quat();

            // Calculate invBindPose
            for (u32 i = 0; i < skeleton->boneCount; ++i)
            {
                BoneTransform *transform = skeleton->bindPose + i;
                skeleton->invBindPose[i] = Scale(1.0f / transform->scale) *
                                           Rotate(Inverse(transform->rotation)) *
                                           Translate(-transform->position);
            }

            // FIXME: This is assuming that child bones always come after
            // their parents in the array
            for (u32 i = 0; i < skeleton->boneCount; ++i)
            {
                u8 parent = skeleton->parents[i];

                // TODO: Use a relationship table instead of just a parent
                // array so we can quickly do lookups in both directions.
                if (parent != INVALID_BONE_IDX)
                {
                    skeleton->invBindPose[i] = skeleton->invBindPose[parent] *
                                               skeleton->invBindPose[i];
                }
            }
        }

        {
            AnimationClip *clip = &gameState->assets.animationClips[0];
            clip->boneCount = 3;
            clip->sampleCount = 8;
            clip->samplesPerSecond = 8;

            for (u32 i = 0; i < ArrayCount(clip->boneTransforms); ++i)
            {
                clip->boneTransforms[i].rotation = Quat();
                clip->boneTransforms[i].scale = Vec3(1.0f);
            }

            // bone0 has no animation as it is the root bone

            // bone1
            clip->boneTransforms[8+0].position = Vec3(0, 0.5, 0);
            clip->boneTransforms[8+0].rotation = Quat();
            clip->boneTransforms[8+1].position = Vec3(0, 0.5, 0);
            clip->boneTransforms[8+1].rotation = Quat(Vec3(0, 0, 1), 0.2f);
            clip->boneTransforms[8+2].position = Vec3(0, 0.5, 0);
            clip->boneTransforms[8+2].rotation = Quat(Vec3(0, 0, 1), 0.4f);
            clip->boneTransforms[8+3].position = Vec3(0, 0.5, 0);
            clip->boneTransforms[8+3].rotation = Quat(Vec3(0, 0, 1), 0.8f);
            clip->boneTransforms[8+4].position = Vec3(0, 0.5, 0);
            clip->boneTransforms[8+4].rotation = Quat(Vec3(0, 0, 1), 0.8f);
            clip->boneTransforms[8+5].position = Vec3(0, 0.5, 0);
            clip->boneTransforms[8+5].rotation = Quat(Vec3(0, 0, 1), 0.4f);
            clip->boneTransforms[8+6].position = Vec3(0, 0.5, 0);
            clip->boneTransforms[8+6].rotation = Quat(Vec3(0, 0, 1), 0.2f);
            clip->boneTransforms[8+7].position = Vec3(0, 0.5, 0);
            clip->boneTransforms[8+7].rotation = Quat();

            // bone1
            clip->boneTransforms[16+0].position = Vec3(0, 0.5, 0);
            clip->boneTransforms[16+0].rotation = Quat();
            clip->boneTransforms[16+1].position = Vec3(0, 0.5, 0);
            clip->boneTransforms[16+1].rotation = Quat(Vec3(0, 0, 1), 0.2f);
            clip->boneTransforms[16+2].position = Vec3(0, 0.5, 0);
            clip->boneTransforms[16+2].rotation = Quat(Vec3(0, 0, 1), 0.4f);
            clip->boneTransforms[16+3].position = Vec3(0, 0.5, 0);
            clip->boneTransforms[16+3].rotation = Quat(Vec3(0, 0, 1), 0.8f);
            clip->boneTransforms[16+4].position = Vec3(0, 0.5, 0);
            clip->boneTransforms[16+4].rotation = Quat(Vec3(0, 0, 1), 0.5f);
            clip->boneTransforms[16+5].position = Vec3(0, 0.5, 0);
            clip->boneTransforms[16+5].rotation = Quat(Vec3(0, 0, 1), 0.3f);
            clip->boneTransforms[16+6].position = Vec3(0, 0.5, 0);
            clip->boneTransforms[16+6].rotation = Quat(Vec3(0, 0, 1), 0.1f);
            clip->boneTransforms[16+7].position = Vec3(0, 0.5, 0);
            clip->boneTransforms[16+7].rotation = Quat();
        }

        {
            Skeleton *skeleton = &gameState->assets.skeletons[0];
            EntityId entities[255];

            u32 entityCount =
                CreateEntities(world, entities, skeleton->boneCount);

            u32 components[] = {PositionComponentId, ScaleComponentId,
                RotationComponentId, WorldTransformComponentId,
                IsWorldTransformDirtyComponentId,
                DrawWorldTransformComponentId, BoneIdComponentId};
            AddComponents(world, entities, entityCount, components,
                ArrayCount(components));

            u32 parentComponents[] = {RenderedMeshComponentId,
                RenderedMaterialComponentId, IsVisibleComponentId};
            AddComponents(world, entities, 1, parentComponents,
                ArrayCount(parentComponents));

            u32 mesh = Mesh_SkinningTest;
            ecs_SetComponent(world, RenderedMeshComponentId, entities, 1, &mesh,
                sizeof(mesh));
            u32 material = Material_Skinning;
            ecs_SetComponent(world, RenderedMaterialComponentId, entities, 1,
                &material, sizeof(material));
            b32 isVisible = true;
            ecs_SetComponent(world, IsVisibleComponentId, entities, 1,
                    &isVisible, sizeof(isVisible));

            u32 childCount = entityCount - 1;
            u32 childComponents[] = {ParentComponentId,
                LocalTransformComponentId, IsLocalTransformDirtyComponentId};
            AddComponents(world, entities + 1, childCount, childComponents,
                ArrayCount(childComponents));

            u32 boneIds[3] = {0, 1, 2};
            ecs_SetComponent(world, BoneIdComponentId, entities, entityCount,
                boneIds, sizeof(boneIds[0]));

            vec3 positions[3];
            vec3 scales[3];
            quat rotations[3];
            Assert(ArrayCount(positions) == skeleton->boneCount);
            for (u32 i = 0; i < skeleton->boneCount; ++i)
            {
                positions[i] = skeleton->bindPose[i].position;
                scales[i] = skeleton->bindPose[i].scale;
                rotations[i] = skeleton->bindPose[i].rotation;
            }
            ecs_SetComponent(world, PositionComponentId, entities, entityCount,
                positions, sizeof(positions[0]));
            ecs_SetComponent(world, ScaleComponentId, entities, entityCount,
                scales, sizeof(scales[0]));
            ecs_SetComponent(world, RotationComponentId, entities, entityCount,
                rotations, sizeof(rotations[0]));

            for (u32 i = 0; i < skeleton->boneCount; ++i)
            {
                u8 parentBone = skeleton->parents[i];
                if (parentBone != INVALID_BONE_IDX)
                {
                    Assert(parentBone < skeleton->boneCount);
                    EntityId parentEntity = entities[parentBone];
                    ecs_SetComponent(world, ParentComponentId, entities + i, 1,
                        &parentEntity, sizeof(parentEntity));
                }
            }

            b32 values[3] = {true, true, true};
            Assert(skeleton->boneCount == ArrayCount(values));
            ecs_SetComponent(world, IsWorldTransformDirtyComponentId,
                entities, entityCount, values, sizeof(values[0]));
            ecs_SetComponent(world, IsLocalTransformDirtyComponentId,
                entities + 1, childCount, values, sizeof(values[0]));
        }

#if 0
        {
            SceneImportData *model = &gameState->assets.xbotModel;
            AnimationClip *clip = &gameState->assets.idleAnimation;

            InitializePlaybackState(&gameState->animPlaybackState, clip,
                &gameState->persistentArena);

            EntityId entities[256];
            Assert(model->nodeCount < ArrayCount(entities));

            u32 entityCount = CreateEntities(world, entities, model->nodeCount);
            Assert(entityCount == model->nodeCount);

            u32 components[] = {PositionComponentId, ScaleComponentId,
                RotationComponentId, LocalTransformComponentId,
                WorldTransformComponentId, IsLocalTransformDirtyComponentId,
                IsWorldTransformDirtyComponentId, BoneIdComponentId };
            AddComponents(world, entities, entityCount, components,
                ArrayCount(components));

            Assert(model->nodeCount == clip->skeleton->boneCount);

            vec3 positions[ArrayCount(entities)];
            vec3 scales[ArrayCount(entities)];
            quat rotations[ArrayCount(entities)];
            for (u32 i = 0; i < model->nodeCount; ++i)
            {
                SceneNode *node = model->nodes + i;

                BonePose *bonePose = clip->samples + i;
                positions[i] = bonePose->translation;
                scales[i] = Vec3(bonePose->scaling);
                rotations[i] = bonePose->rotation;

                if (node->parent >= 0)
                {
                    ecs_AddComponent(world, ParentComponentId, entities + i, 1);
                    ecs_AddComponent(world, IsBoneComponentId, entities + i, 1);

                    Assert(node->parent < (i32)entityCount);
                    EntityId parent = entities[node->parent];

                    Assert(parent != entities[i]);
                    ecs_SetComponent(world, ParentComponentId, entities + i, 1,
                        &parent, sizeof(parent));
                }

                b32 value = true;
                ecs_SetComponent(world, IsWorldTransformDirtyComponentId,
                    entities + i, 1, &value, sizeof(value));
                ecs_SetComponent(world, IsLocalTransformDirtyComponentId,
                    entities + i, 1, &value, sizeof(value));
                ecs_SetComponent(
                    world, BoneIdComponentId, entities + i, 1, &i, sizeof(i));
            }

            ecs_SetComponent(world, PositionComponentId, entities, entityCount,
                positions, sizeof(positions[0]));
            ecs_SetComponent(world, ScaleComponentId, entities, entityCount,
                scales, sizeof(scales[0]));
            ecs_SetComponent(world, RotationComponentId, entities, entityCount,
                rotations, sizeof(rotations[0]));
        }
#endif

        // TODO: Should be part of load map / generate map
        SpawnRocksOnTerrainSystem(
            world, &gameState->assets, &gameState->tempArena);
        GenerateTerrainQuadTreeSystem(world, &gameState->assets, true);

        gameState->gameContentLoaded = true;

        memory->setCursorMode(CursorMode_Locked);
    }
    else if (memory->wasCodeReloaded)
    {
        // TODO: Be smarter about this, only reload things that have changed
        LoadShaders(memory, &gameState->tempArena);
        InitializeMaterials(gameState);
    }

    UpdateBroadPhaseAabbsSystem(world, &gameState->assets);

    gameState->currentTime += dt;

    if (CVar_GetValueU32(&gameState->cvarTable, CVAR(enable_networking)))
    {
        for (u32 i = 0; i < memory->incomingPacketCount; ++i)
        {
            GamePacket *incomingPacket = memory->incomingPackets + i;
            Assert(incomingPacket->length == sizeof(GameUpdatePacket));
            GameUpdatePacket packetData = {};
            CopyMemory(
                &packetData, incomingPacket->data, incomingPacket->length);

            ProcessGameUpdatePacketFromServer(world, &packetData);

            // FIXME: Move networked events processing into
            // ProcessGameUpdatePacketFromServer
            for (u32 eventIdx = 0;
                 eventIdx < packetData.damageReceivedEventCount; ++eventIdx)
            {
                DamageReceivedEvent *event =
                    packetData.damageReceivedEvents + eventIdx;

                EntityId entity = NULL_ENTITY;
                MapNetEntityIdToEntityId(
                    world, &event->netEntityId, 1, &entity);

                // TODO: Do something?
            }

            for (u32 eventIdx = 0; eventIdx < packetData.bulletImpactEventCount;
                 ++eventIdx)
            {
                BulletImpactEvent *event =
                    packetData.bulletImpactEvents + eventIdx;
                SpawnEntityAtPosition(
                    world, EntityPrefab_BulletImpact, event->position);
            }

            for (u32 eventIdx = 0; eventIdx < packetData.gunShotEventCount;
                 ++eventIdx)
            {
                GunShotEvent *event = packetData.gunShotEvents + eventIdx;
                PlaySound(gameState, AudioClip_Pistol);
            }
        }
    }

    // TODO: Only run this in playing state
    // This should be a system, need concept of entity controlling camera and
    // input so that it can show the inventory
    // Open inventory if in container
    {
        EntityId playerEntity;
        if (GetFirstEntity(world, ActivePlayerComponentId, &playerEntity))
        {
            EntityId subject = NULL_ENTITY;
            ecs_GetComponent(world, OpenContainerComponentId,
                    &playerEntity, 1, &subject,
                    sizeof(subject));

            if (subject != NULL_ENTITY && (gameState->prevPlayerOpenContainer == NULL_ENTITY))
            {
                if (!gameState->showInventory)
                {
                    gameState->showInventory = true;
                    // TODO: Make this part of the input processing for inventory mode
                    memory->setCursorMode(CursorMode_Unlocked);
                }
            }

            // FIXME: Why isn't this a component?
            gameState->prevPlayerOpenContainer = subject;
        }
    }

    EntityId camera = NULL_ENTITY;
    if (!GetFirstEntity(world, ActiveCameraComponentId, &camera))
    {
        // No active camera, try attach to active player if there is one
        EntityId player = NULL_ENTITY;
        if (GetFirstEntity(world, ActivePlayerComponentId, &player))
        {
            ecs_AddComponent(world, ActiveCameraComponentId, &player, 1);
            camera = player;
            gameState->showGameUI = true;
        }
        else 
        {
            // else attach to debug camera
            EntityId debugCamera = NULL_ENTITY;
            if (GetFirstEntity(world, DebugCameraComponentId, &debugCamera))
            {
                ecs_AddComponent(world, ActiveCameraComponentId, &debugCamera, 1);
                camera = debugCamera;
                gameState->showGameUI = false;
            }
        }
    }
    Assert(camera != NULL_ENTITY);

    b32 isPlayerCamera =
        ecs_HasComponent(world, ActivePlayerComponentId, camera);
    b32 isDebugCamera =
        ecs_HasComponent(world, DebugCameraComponentId, camera);
    Assert(!(isPlayerCamera && isDebugCamera));

    gameState->showGameUI = isPlayerCamera;

    if (input->buttonStates[KEY_F1].isDown &&
        !input->buttonStates[KEY_F1].wasDown)
    {
        // Remove ActiveCameraComponentId tag from current camera
        ecs_RemoveComponent(world, ActiveCameraComponentId, &camera, 1);

        if (isPlayerCamera)
        {
            // Enable debug camera's perspective if there is a debug camera
            EntityId debugCamera = NULL_ENTITY;
            if (GetFirstEntity(world, DebugCameraComponentId, &debugCamera))
            {
                ecs_AddComponent(world, ActiveCameraComponentId, &debugCamera, 1);

                // TODO: Probably want this defined as part of the debug
                // camera's input function
                memory->setCursorMode(CursorMode_Unlocked);

                // TODO: Maybe explore the idea of not having having the
                // ActivePlayerComponent when using the debug camera?

                // Unhide the active player
                EntityId player = NULL_ENTITY;
                if (GetFirstEntity(world, ActivePlayerComponentId, &player))
                {
                    b32 isVisible = true;
                    ecs_SetComponent(&gameState->entityWorld,
                        IsVisibleComponentId, &player, 1, &isVisible,
                        sizeof(isVisible));
                }

                gameState->showGameUI = false;
            }
        }
        else if (isDebugCamera)
        {
            // TODO: We should be able to switch to and control any player
            // Switch to player's perspective
            EntityId player = NULL_ENTITY;
            if (GetFirstEntity(world, ActivePlayerComponentId, &player))
            {
                ecs_AddComponent(world, ActiveCameraComponentId, &player, 1);

                // TODO: Probably want this defined as part of the player's
                // input function
                memory->setCursorMode(CursorMode_Locked);

                // Hide the active player
                b32 isVisible = false;
                ecs_SetComponent(&gameState->entityWorld, IsVisibleComponentId,
                    &player, 1, &isVisible, sizeof(isVisible));

                gameState->showGameUI = true;
            }
        }
        else
        {
            LogMessage("Active camera tag is not attached to either player "
                       "or debug camera");
        }
    }

    PlayerCommand playerCommand = {};
    playerCommand.prevActions = gameState->prevPlayerCommand.actions;

    if (!CVar_GetValueU32(&gameState->cvarTable, CVAR(show_console)))
    {
        // Super dumb key binding code
        if (WasPressed(input->buttonStates + KEY_F6))
        {
            Cmd_Exec(&gameState->commandSystem,
                "server_exec set collision_debug_enabled 1;"
                "server_exec set collision_recording_enabled 1");
        }
        if (WasPressed(input->buttonStates + KEY_F7))
        {
            Cmd_Exec(&gameState->commandSystem,
                "server_exec set collision_recording_enabled 0");
        }

        // TODO: Rework this with entity possesion system
        if (isPlayerCamera)
        {
            // Player camera
            if (input->buttonStates[KEY_W].isDown)
            {
                playerCommand.actions |= PlayerAction_MoveForward;
            }
            if (input->buttonStates[KEY_S].isDown)
            {
                playerCommand.actions |= PlayerAction_MoveBackward;
            }
            if (input->buttonStates[KEY_A].isDown)
            {
                playerCommand.actions |= PlayerAction_MoveLeft;
            }
            if (input->buttonStates[KEY_D].isDown)
            {
                playerCommand.actions |= PlayerAction_MoveRight;
            }
            if (input->buttonStates[KEY_F].isDown)
            {
                playerCommand.actions |= PlayerAction_Interact;
            }
            if (input->buttonStates[KEY_SPACE].isDown)
            {
                playerCommand.actions |= PlayerAction_Jump;
            }
            if (input->buttonStates[KEY_LEFT_SHIFT].isDown)
            {
                playerCommand.actions |= PlayerAction_Sprint;
            }

            // Offset by 1 as value of 0 is used to indicate no change
            if (input->buttonStates[KEY_1].isDown)
            {
                playerCommand.switchToActionSlot = 1;
            }
            if (input->buttonStates[KEY_2].isDown)
            {
                playerCommand.switchToActionSlot = 2;
            }
            if (input->buttonStates[KEY_3].isDown)
            {
                playerCommand.switchToActionSlot = 3;
            }
            if (input->buttonStates[KEY_4].isDown)
            {
                playerCommand.switchToActionSlot = 4;
            }
            if (input->buttonStates[KEY_5].isDown)
            {
                playerCommand.switchToActionSlot = 5;
            }
            if (input->buttonStates[KEY_6].isDown)
            {
                playerCommand.switchToActionSlot = 6;
            }

            if (input->buttonStates[KEY_TAB].isDown &&
                !input->buttonStates[KEY_TAB].wasDown)
            {
                gameState->showInventory = !gameState->showInventory;

                if (gameState->showInventory)
                {
                    memory->setCursorMode(CursorMode_Unlocked);
                }
                else
                {
                    memory->setCursorMode(CursorMode_Locked);

                    playerCommand.inventoryCommand.action =
                        PlayerInventoryAction_Close;
                }
            }

            // Write inventory commands
            if (gameState->inventoryState.dragState == DragState_Complete)
            {
                // TODO: Validate dragStart and dragEnd before sending
                playerCommand.inventoryCommand.action =
                    PlayerInventoryAction_ChangeSlot;

                playerCommand.inventoryCommand.changeSlot.from =
                    gameState->inventoryState.dragStart;
                playerCommand.inventoryCommand.changeSlot.to =
                    gameState->inventoryState.dragEnd;

                MapEntityIdToNetEntityId(&gameState->entityWorld,
                    &gameState->inventoryState.dragStartEntity, 1,
                    &playerCommand.inventoryCommand.changeSlot.fromEntity);
                MapEntityIdToNetEntityId(&gameState->entityWorld,
                    &gameState->inventoryState.dragEndEntity, 1,
                    &playerCommand.inventoryCommand.changeSlot.toEntity);

                gameState->inventoryState.dragState = DragState_None;
            }

            // FIXME: Should be sending player view angles, not relative
            // mouse motions
            if (!gameState->showInventory)
            {
                playerCommand.mouseX =
                    (f32)(input->mouseRelPosX) / (f32)memory->frameBufferWidth;
                playerCommand.mouseY =
                    (f32)(input->mouseRelPosY) / (f32)memory->frameBufferHeight;

                if (input->buttonStates[KEY_MOUSE_BUTTON_LEFT].isDown)
                {
                    playerCommand.actions |= PlayerAction_PrimaryFire;
                }
            }
        }
        else
        {
            // Debug camera
            if (WasPressed(input->buttonStates + KEY_MOUSE_BUTTON_RIGHT))
            {
                memory->setCursorMode(CursorMode_Locked);
            }
            else if (WasReleased(input->buttonStates + KEY_MOUSE_BUTTON_RIGHT))
            {
                memory->setCursorMode(CursorMode_Unlocked);
            }

            MoveCamera(&gameState->entityWorld, input, dt,
                (f32)memory->frameBufferWidth, (f32)memory->frameBufferHeight);
        }
    }

    gameState->prevPlayerCommand = playerCommand;

    if (CVar_GetValueU32(&gameState->cvarTable, CVAR(enable_networking)))
    {
        Assert(
            memory->outgoingPacketCount < ArrayCount(memory->outgoingPackets));
        GamePacket *outgoingPacket =
            memory->outgoingPackets + memory->outgoingPacketCount++;
        CopyMemory(outgoingPacket->data, &playerCommand, sizeof(playerCommand));
        outgoingPacket->length = sizeof(playerCommand);
    }

    AllocateParticleEmittersSystem(gameState);
    BuildingPreviewSystem(world, &gameState->debugDrawingSystem);
    UpdateTimeToLiveSystem(world, dt);
    //UpdatePlaybackState(&gameState->animPlaybackState, gameState->currentTime);
    //UpdateBonesSystem(world, &gameState->animPlaybackState);
    UpdateAnimationsSystem(world, &gameState->assets, gameState->currentTime);
    GameRender(gameState, memory, input, dt);
}

extern "C" GAME_UPDATE(GameUpdate)
{
    LogMessage = memory->logMessage;
    g_DebugEventTable = memory->debugEventTable;

    TIMED_BLOCK();
    GameState *gameState = (GameState *)memory->persistentStorage;

    MemoryArena_Initialize(&gameState->tempArena, memory->temporaryStorage,
        memory->temporaryStorageSize);

    g_Console = &gameState->console;

    if (!gameState->isInitialized)
    {
        gameState->state = GameState_MainMenu;

        // Initialize particle systems free list
        for (u32 i = 0; i < ArrayCount(gameState->particleSystems) - 1; ++i)
        {
            ParticleSystemInstance *instance = gameState->particleSystems + i;
            instance->next = instance + 1;
        }
        gameState->particleSystemsFreeList = &gameState->particleSystems[0];

        LoadMinimalContent(gameState, memory);

        RegisterCVars(&gameState->cvarTable, g_CVarRegistrationData,
            ArrayCount(g_CVarRegistrationData));
        RegisterCommands(&gameState->commandSystem, g_CommandRegistrationData,
            ArrayCount(g_CommandRegistrationData));

        void *persistentMemory =
            (u8 *)memory->persistentStorage + sizeof(GameState);
        u64 persistentMemorySize =
            memory->persistentStorageSize - sizeof(GameState);

        MemoryArena_Initialize(
            &gameState->persistentArena, persistentMemory, persistentMemorySize);

        MemoryArena_Partition(
            &gameState->persistentArena, &gameState->entityArena, Megabytes(4));
        MemoryArena_Partition(&gameState->persistentArena,
            &gameState->collisionWorldArena, Megabytes(50));
        MemoryArena_Partition(
            &gameState->persistentArena, &gameState->assets.terrainArena, Megabytes(6));
        MemoryArena_Partition(
            &gameState->persistentArena, &gameState->assets.meshDataArena, Megabytes(2));
        MemoryArena_Partition(
            &gameState->persistentArena, &gameState->assets.audioDataArena, Megabytes(20));
        MemoryArena_Partition(&gameState->persistentArena,
            &gameState->assets.modelDataArena, Megabytes(4));
        MemoryArena_Partition(&gameState->persistentArena,
            &gameState->assets.terrainCollisionArena, Megabytes(64));
#ifdef COLLISION_DEBUG
        MemoryArena_Partition(&gameState->persistentArena,
            &gameState->collisionDebugState.memoryArena, Megabytes(2));
#endif

        InitializeEntityWorld(&gameState->entityWorld, &gameState->entityArena,
                g_ComponentInitData, ArrayCount(g_ComponentInitData));
        RegisterEntityPrefabs(&gameState->entityWorld);

        DebugDrawing_Init(&gameState->debugDrawingSystem,
            &gameState->persistentArena, MAX_DEBUG_LINES);

        gameState->rng.state = 34759837;

        gameState->isInitialized = true;
    }

    FpsGraph_OnFrameState(&gameState->fpsGraph, memory->frameTime, dt);

    if (gameState->isInitialized && memory->wasCodeReloaded)
    {
        RegisterCVars(&gameState->cvarTable, g_CVarRegistrationData,
            ArrayCount(g_CVarRegistrationData));
        RegisterCommands(&gameState->commandSystem, g_CommandRegistrationData,
            ArrayCount(g_CommandRegistrationData));
        RegisterEntityPrefabs(&gameState->entityWorld);
    }

    if ((memory->frameBufferWidth != gameState->prevWindowWidth) ||
        (memory->frameBufferHeight != gameState->prevWindowHeight))
    {
        {
            RenderCommand_CreateRenderTarget *createHdrBuffer =
                AllocateRenderCommand(memory, RenderCommand_CreateRenderTarget);
            createHdrBuffer->id = RenderTarget_HdrBuffer;
            createHdrBuffer->width = memory->frameBufferWidth;
            createHdrBuffer->height = memory->frameBufferHeight;
        }
        gameState->prevWindowWidth = memory->frameBufferWidth;
        gameState->prevWindowHeight = memory->frameBufferHeight;

        // FIXME: Don't need prevWindowWidth and prevWindowHeight if we use cvars
        CVar_SetValueU32(&gameState->cvarTable, CVAR(framebuffer_width),
            memory->frameBufferWidth);
        CVar_SetValueU32(&gameState->cvarTable, CVAR(framebuffer_height),
            memory->frameBufferHeight);
    }

    // Update platform data
    {
        PlatformData data;
        memory->getPlatformData(&data);

        data.vSyncEnabled =
            CVar_GetValueU32(&gameState->cvarTable, CVAR(vsync));
        data.frameRateCap =
            CVar_GetValueU32(&gameState->cvarTable, CVAR(max_fps));
        data.frameBufferWidth =
            CVar_GetValueU32(&gameState->cvarTable, CVAR(framebuffer_width));
        data.frameBufferHeight =
            CVar_GetValueU32(&gameState->cvarTable, CVAR(framebuffer_height));
        data.windowMode =
            CVar_GetValueU32(&gameState->cvarTable, CVAR(window_mode));
        memory->setPlatformData(&data);
    }

    g_CollisionDebugState = &gameState->collisionDebugState;
    g_Assets = &gameState->assets;

    gameState->collisionDebugState.isRecordingEnabled = CVar_GetValueU32(
        &gameState->cvarTable, CVAR(collision_recording_enabled));

    g_DebugDrawingSystem = &gameState->debugDrawingSystem;
    DebugDrawing_Cleanup(&gameState->debugDrawingSystem, dt);

    gameState->textBuffer.count = 0;
    gameState->textBuffer.capacity = TEXT_BUFFER_MAX_VERTICES;
    gameState->textBuffer.vertices = AllocateArray(
        &gameState->tempArena, Vertex, gameState->textBuffer.capacity);

    if (WasPressed(input->buttonStates + KEY_GRAVE_ACCENT))
    {
        b32 showConsole =
            CVar_GetValueU32(&gameState->cvarTable, CVAR(show_console));
        CVar_SetValueU32(
            &gameState->cvarTable, CVAR(show_console), !showConsole);
    }

    if (CVar_GetValueU32(&gameState->cvarTable, CVAR(show_console)))
    {
        UpdateConsole(&gameState->console, input, &gameState->commandSystem,
            &gameState->tempArena);
    }

    // Execute console commands
    {
        Cmd_System *commandSystem = &gameState->commandSystem;

        CommandExecutionEvent events[256];
        u32 count = Cmd_ExecBuffer(commandSystem, commandSystem->workingBuffer,
            commandSystem->workingBufferLength, events, ArrayCount(events));
        for (u32 i = 0; i < count; ++i)
        {
            CommandExecutionEvent *event = events + i;
            cl_HandleCommand(event, gameState, memory);
        }

        commandSystem->workingBufferLength = 0;
    }

    switch (gameState->state)
    {
        case GameState_MainMenu:
#ifdef SKIP_MAIN_MENU
            gameState->state = GameState_Experimental;
            if (CVar_GetValueU32(&gameState->cvarTable, CVAR(enable_networking)))
            {
                memory->startServer();
                memory->startClient("127.0.0.1");
            }
            DrawLoadingScreen(gameState, memory);
#else
            DrawMainMenu(gameState, memory, input);
#endif
            break;
        case GameState_CollisionDetectionTestBench:
            UpdateCollisionDetectionTestbench(
                &gameState->collisionDetectionTestbench, input, memory,
                &gameState->debugDrawingSystem, &gameState->tempArena,
                &gameState->cvarTable, dt);
            break;
        case GameState_Experimental:
            UpdateGameStateExperimental(gameState, memory, input, dt);
            break;
        default:
            InvalidCodePath();
    }

    ProcessEntityDeletionQueue(&gameState->entityWorld);

    for (u32 eventIdx = 0;
         eventIdx < gameState->entityWorld.componentRemovalEventCount;
         ++eventIdx)
    {
        ecs_ComponentRemovedEvent *event =
            gameState->entityWorld.componentRemovedEvents + eventIdx;

        switch (event->componentId)
        {
        case ParticleSystemComponentId:
            RemoveParticleSystem(gameState, event->entityId);
            break;
        case CollisionShapeComponentId:
            break;
        default:
            break;
        }
    }
    gameState->entityWorld.componentRemovalEventCount = 0;
    
    u32 showFps = CVar_GetValueU32(&gameState->cvarTable, CVAR(show_fps));
    if (showFps)
    {
        FpsGraph_Draw(&gameState->fpsGraph, memory, &gameState->textBuffer,
            &gameState->debugFont, showFps == 1);
    }

    if (CVar_GetValueU32(&gameState->cvarTable, CVAR(show_debug_counters)))
    {
        DisplayDebugCounters(gameState, memory);
    }

    if (CVar_GetValueU32(&gameState->cvarTable, CVAR(show_console)))
    {
        DrawConsole(&gameState->console, memory, input, &gameState->textBuffer,
            &gameState->debugFont);
    }

    // NOTE: Update commands are processed before draw commands in OpenGL layer
    {
        RenderCommand_UpdateVertexBuffer *updateTextBuffer =
            AllocateRenderCommand(memory, RenderCommand_UpdateVertexBuffer);
        updateTextBuffer->id = VertexBuffer_Text;
        updateTextBuffer->vertexLayout = VertexLayout_Standard;
        updateTextBuffer->vertices = gameState->textBuffer.vertices;
        updateTextBuffer->vertexCount = gameState->textBuffer.count;
    }
}

internal u32 Mixer_OutputSound(GameSoundOutputBuffer *outputBuffer,
    PlayingSound *playingSounds, u32 count, u32 *completedSounds,
    AudioData *audioClips, u32 audioClipCount, MemoryArena *tempArena)
{
    u32 completedCount = 0;
    // FIXME: Don't hardcode number of channels
    f32 *mixingBuffer =
        AllocateArray(tempArena, f32, outputBuffer->sampleCount * 2);
    ClearToZero(mixingBuffer, outputBuffer->sampleCount * 2 * sizeof(f32));
    for (u32 soundIdx = 0; soundIdx < count; ++soundIdx)
    {
        PlayingSound *sound = playingSounds + soundIdx;
        Assert(sound->audioClipId < audioClipCount);
        AudioData *clip = audioClips + sound->audioClipId;

        f32 *dest = mixingBuffer;
        for (u32 sampleIndex = 0; sampleIndex < outputBuffer->sampleCount;
             ++sampleIndex)
        {
            if (sound->runningSampleIndex + sampleIndex < clip->sampleCount)
            {
                i16 *source = clip->samples +
                              sound->runningSampleIndex + sampleIndex;

                *dest++ += *source++ * sound->volume;
                *dest++ += *source++ * sound->volume;
            }
            else
            {
                Assert(completedCount < count);
                completedSounds[completedCount++] = soundIdx;
                break;
            }
        }

        // NOTE: Set runningSampleIndx to clip sampleCount if the sound has finished playing
        sound->runningSampleIndex =
            Min((i32)sound->runningSampleIndex + outputBuffer->sampleCount,
                (i32)clip->sampleCount);
    }


    // TODO: Audio mixing should be done in unit space so we can then scale it
    // up to any output format
    i16 *dest = outputBuffer->samples;
    f32 *source = mixingBuffer;
    for (u32 sampleIndex = 0; sampleIndex < outputBuffer->sampleCount;
            ++sampleIndex)
    {
        *dest++ = (i16)Clamp(*source++ + 0.5f, (f32)-I16_MAX, (f32)I16_MAX);
        *dest++ = (i16)Clamp(*source++ + 0.5f, (f32)-I16_MAX, (f32)I16_MAX);
    }

    return completedCount;
}

// FIXME: Merge this function into GameUpdate 
extern "C" GAME_GET_SOUND_SAMPLES(GameGetSoundSamples)
{
    TIMED_BLOCK();
    GameState *gameState = (GameState *)memory->persistentStorage;
    u32 completedSounds[MAX_PLAYING_SOUNDS];

    u32 completedCount = Mixer_OutputSound(soundOutputBuffer,
        gameState->playingSounds, gameState->playingSoundsCount,
        completedSounds, gameState->assets.audioClips,
        ArrayCount(gameState->assets.audioClips), &gameState->tempArena);

    for (u32 i = 0; i < completedCount; ++i)
    {
        u32 idx = completedSounds[i];
        Assert(gameState->playingSoundsCount > 0);

        u32 last = gameState->playingSoundsCount - 1;
        gameState->playingSounds[idx] = gameState->playingSounds[last];
        gameState->playingSoundsCount--;
    }
}

extern "C" GAME_SERVER_UPDATE(GameServerUpdate)
{
    LogMessage = memory->logMessage;
    g_DebugEventTable = memory->debugEventTable;

    TIMED_BLOCK();
    ServerGameState *gameState = (ServerGameState *)memory->persistentStorage;

    MemoryArena_Initialize(&gameState->tempArena, memory->temporaryStorage,
        memory->temporaryStorageSize);

    RegisterEntityPrefabs(&gameState->entityWorld);

    if (!gameState->isInitialized)
    {
        gameState->isInitialized = true;

        RegisterCVars(&gameState->cvarTable, g_CVarRegistrationData,
            ArrayCount(g_CVarRegistrationData));
        RegisterCommands(&gameState->commandSystem, g_CommandRegistrationData,
            ArrayCount(g_CommandRegistrationData));

        void *persistentMemory =
            (u8 *)memory->persistentStorage + sizeof(ServerGameState);
        u64 persistentMemorySize =
            memory->persistentStorageSize - sizeof(ServerGameState);

        MemoryArena_Initialize(
            &gameState->persistentArena, persistentMemory, persistentMemorySize);

        MemoryArena_Partition(
            &gameState->persistentArena, &gameState->entityArena, Megabytes(4));
        MemoryArena_Partition(&gameState->persistentArena,
            &gameState->collisionWorldArena, Megabytes(50));
        MemoryArena_Partition(&gameState->persistentArena,
            &gameState->assets.terrainArena, Megabytes(6));
        MemoryArena_Partition(&gameState->persistentArena,
            &gameState->assets.meshDataArena, Megabytes(2));
        MemoryArena_Partition(&gameState->persistentArena,
            &gameState->assets.terrainCollisionArena, Megabytes(40));
#ifdef COLLISION_DEBUG
        MemoryArena_Partition(&gameState->persistentArena,
            &gameState->collisionDebugState.memoryArena, Megabytes(2));
#endif

        DebugDrawing_Init(&gameState->debugDrawingSystem,
            &gameState->persistentArena, MAX_DEBUG_LINES);

        gameState->rng.state = 87345435;

        PopulateLootTables(&gameState->assets);
        LoadSlowContent(&gameState->assets, memory, false, &gameState->tempArena);

        InitializeEntityWorld(&gameState->entityWorld, &gameState->entityArena,
                g_ComponentInitData, ArrayCount(g_ComponentInitData));
        LoadMap(&gameState->entityWorld, &gameState->assets);
        SpawnRocksOnTerrainSystem(
            &gameState->entityWorld, &gameState->assets, &gameState->tempArena);
#if 0
        // TODO: Need to properly handle creation of networked entities
        SpawnEntityAtPosition(&gameState->entityWorld, EntityPrefab_HealthTest,
            Vec3(-10, 1, -20));
        SpawnEntityAtPosition(&gameState->entityWorld, EntityPrefab_StorageBox,
            Vec3(-15, 1, -10));

#endif
#if 0
        SpawnEntityAtPosition(&gameState->entityWorld,
            EntityPrefab_LootTableTest, Vec3(-4, 0.3, 4));
        SpawnEntityAtPosition(&gameState->entityWorld,
            EntityPrefab_LootTableTest, Vec3(-7, 0.3, 4));
        SpawnEntityAtPosition(&gameState->entityWorld,
            EntityPrefab_LootTableTest, Vec3(-10, 0.3, 4));
        SpawnEntityAtPosition(&gameState->entityWorld,
            EntityPrefab_LootTableTest, Vec3(-14, 0.3, 4));
        SpawnEntityAtPosition(&gameState->entityWorld,
            EntityPrefab_StorageBox, Vec3(-16, 0.3, 4));
#endif
        ProcessLootTablesSystem(
            &gameState->entityWorld, &gameState->assets, &gameState->rng);

        SpawnEnemiesSystem(&gameState->entityWorld);


        InitializeQuadTree(&gameState->assets.heightMapQuadTree,
            TERRAIN_COLLISION_MAX_NODES, &gameState->assets.terrainCollisionArena);
        GenerateTerrainQuadTreeSystem(
            &gameState->entityWorld, &gameState->assets, false);
    }

    g_ServerCollisionDebugState = &gameState->collisionDebugState;

    g_CollisionDebugState = &gameState->collisionDebugState;
    g_Assets = &gameState->assets;

    g_ServerDebugDrawingSystem = &gameState->debugDrawingSystem;
    DebugDrawing_Cleanup(&gameState->debugDrawingSystem, dt);

    gameState->collisionDebugState.isRecordingEnabled = CVar_GetValueU32(
        &gameState->cvarTable, CVAR(collision_recording_enabled));

    UpdateBroadPhaseAabbsSystem(&gameState->entityWorld, &gameState->assets);

    for (u32 i = 0; i < g_ServerRpcQueueLength; ++i)
    {
        Rpc *rpc = g_ServerRpcQueue + i;
        if (StringCompare(rpc->functionName, "SpawnEntityAtPosition"))
        {
            SpawnEntityAtPositionRpc(&gameState->entityWorld, rpc);
        }

    }
    g_ServerRpcQueueLength = 0;

    for (u32 i = 0; i < memory->incomingPacketCount; ++i)
    {
        GamePacket *incomingPacket = memory->incomingPackets + i;

        ClientData *clientData = NULL;
        bool newClient = true;
        // Try find existing client
        for (u32 clientIdx = 0; clientIdx < gameState->clientCount; ++clientIdx)
        {
            if (gameState->clients[clientIdx].clientId ==
                incomingPacket->clientId)
            {
                newClient = false;
                clientData = gameState->clients + clientIdx;
                break;
            }
        }

        // Allocate new client
        if (newClient)
        {
            Assert(gameState->clientCount < ArrayCount(gameState->clients));
            clientData = gameState->clients + gameState->clientCount++;
            clientData->clientId = incomingPacket->clientId;

            // Spawn player on new client connected
            EntityId entityId = SpawnPlayer(&gameState->entityWorld);

            clientData->entityToControl = entityId;
        }

        PlayerCommand playerCommand = {};
        Assert(incomingPacket->length == sizeof(playerCommand));
        CopyMemory(&playerCommand, incomingPacket->data, sizeof(playerCommand));

        // TODO: Store player command and call MovePlayer for all players
        // NOPE, that will cause issues as it will limit us to processing a
        // single packet from client per tick which causes the queue to grow
        // indefinitely
        if (clientData->entityToControl != NULL_ENTITY)
        {
            // Check if the player entity has been deleted
            if (!ecs_HasComponent(&gameState->entityWorld, PositionComponentId,
                    clientData->entityToControl))
            {
                clientData->entityToControl = NULL_ENTITY;
                clientData->secondsUntilRespawn = 3.0f;
            }
        }

        if (clientData->entityToControl != NULL_ENTITY)
        {

            MovePlayer(&gameState->entityWorld, playerCommand,
                clientData->prevPlayerCommand, dt, clientData->entityToControl,
                &gameState->tempArena);

            PlaceEntitiesSystem(&gameState->entityWorld,
                clientData->entityToControl, playerCommand);

            AttackSystem(&gameState->entityWorld, &gameState->assets,
                clientData->entityToControl, playerCommand,
                clientData->prevPlayerCommand, &gameState->eventQueue, dt);

            InteractSystem(&gameState->entityWorld, clientData->entityToControl,
                playerCommand);

            UpdateInventory(&gameState->entityWorld,
                clientData->entityToControl, playerCommand.inventoryCommand);

            UpdateActionBar(&gameState->entityWorld,
                clientData->entityToControl, playerCommand);

            {
                vec3 position = {};
                ecs_GetComponent(&gameState->entityWorld, PositionComponentId,
                    &clientData->entityToControl, 1, &position,
                    sizeof(position));

                if (position.y < -20.0f)
                {
                    ecs_QueueEntitiesForDeletion(&gameState->entityWorld,
                        &clientData->entityToControl, 1);
                    RemoveAllItemsOwnedByEntity(
                        &gameState->entityWorld, clientData->entityToControl);
                    clientData->entityToControl = NULL_ENTITY;
                    clientData->secondsUntilRespawn = 3.0f;
                }
            }
        }
        else
        {
            clientData->secondsUntilRespawn -= dt;
            if (clientData->secondsUntilRespawn <= 0.0f)
            {
                // Respawn player
                EntityId entityId = SpawnPlayer(&gameState->entityWorld);

                clientData->entityToControl = entityId;
                clientData->secondsUntilRespawn = 0.0f;
            }
        }

        clientData->prevPlayerCommand = playerCommand;
    }

    // Copy reliable data into command system working buffer
    {
        Cmd_System *commandSystem = &gameState->commandSystem;
        Assert(commandSystem->workingBufferLength == 0);
        Assert(memory->reliableIncomingDataLength < sizeof(commandSystem->workingBuffer));
        CopyMemory(commandSystem->workingBuffer, memory->reliableIncomingData,
            memory->reliableIncomingDataLength);
        commandSystem->workingBufferLength += memory->reliableIncomingDataLength;
    }

    // Execute console commands
    {
        Cmd_System *commandSystem = &gameState->commandSystem;

        CommandExecutionEvent events[256];
        u32 count = Cmd_ExecBuffer(commandSystem, commandSystem->workingBuffer,
            commandSystem->workingBufferLength, events, ArrayCount(events));
        for (u32 i = 0; i < count; ++i)
        {
            CommandExecutionEvent *event = events + i;
            sv_HandleCommand(event, gameState, memory);
        }

        commandSystem->workingBufferLength = 0;
    }

    UpdateBulletSystem(&gameState->entityWorld, dt, &gameState->eventQueue,
        &gameState->assets, g_ServerDebugDrawingSystem);

    if (CVar_GetValueU32(&gameState->cvarTable, CVAR(enable_ai)))
    {
        AiMoveTowardsSystem(&gameState->entityWorld, dt);
        // NOTE: AiAttackSystem relies on the AiTargetComponent set by
        // AiMoveTowardsSystem
        AiAttackSystem(&gameState->entityWorld, &gameState->eventQueue, dt);
    }
    GroundCheckSystem(&gameState->entityWorld);
    FrictionSystem(&gameState->entityWorld, dt);
    GravitySystem(&gameState->entityWorld, dt);
    VelocityIntegrationSystem(&gameState->entityWorld, dt);

    // Process game events
    GameEventQueue *eventQueue = &gameState->eventQueue;
    for (u32 i = 0; i < eventQueue->length; ++i)
    {
        GameEvent *event = eventQueue->events + i;
        if (event->type == GameEvent_ReceivedDamage)
        {
            f32 currentHealth;
            ecs_GetComponent(&gameState->entityWorld, HealthComponentId,
                &event->receivedDamage.entity, 1, &currentHealth,
                sizeof(currentHealth));
            currentHealth -= event->receivedDamage.amount;
            currentHealth = Max(0.0f, currentHealth);
            ecs_SetComponent(&gameState->entityWorld, HealthComponentId,
                &event->receivedDamage.entity, 1, &currentHealth,
                sizeof(currentHealth));
            LogMessage("Remaining health for entity %u: %g",
                event->receivedDamage.entity, currentHealth);
        }
    }

    RemoveDeadEntitiesSystem(&gameState->entityWorld);
    UpdateTimeToLiveSystem(&gameState->entityWorld, dt);

    // All code after this should assert that a netEntityId is non-zero.
    AllocateNetEntityIdsSystem(&gameState->entityWorld, &gameState->server);

    ecs_EntityWorld *world = &gameState->entityWorld;
    for (u32 clientIdx = 0; clientIdx < gameState->clientCount; ++clientIdx)
    {
        ClientData *clientData = gameState->clients + clientIdx;
        NetEntityClient *client = &clientData->entityClient;

        GameUpdatePacket packetData = {};
        PopulateGamePacketForClient(client, clientData->entityToControl, world,
            eventQueue, &packetData);

        GamePacket *outgoingPacket =
            memory->outgoingPackets + memory->outgoingPacketCount++;
        Assert(sizeof(packetData) < sizeof(outgoingPacket->data));
        CopyMemory(outgoingPacket->data, &packetData, sizeof(packetData));
        outgoingPacket->length = sizeof(packetData);
        outgoingPacket->clientId = clientData->clientId;
    }

    if (CVar_GetValueU32(&gameState->cvarTable, CVAR(collision_debug_enabled)))
    {
        CollisionDebugState *state = &gameState->collisionDebugState;

        u32 selectedCall = CVar_GetValueU32(
            &gameState->cvarTable, CVAR(collision_debug_selected_call));
        selectedCall = Clamp((i32)selectedCall, 0, state->count);

        if (state->count > selectedCall)
        {
            CollisionDebugCall *call = state->calls + selectedCall;
            CollisionDebug_ReplayCall(
                call, g_ServerDebugDrawingSystem, &gameState->tempArena);
        }
    }
    else
    {
        u32 drawCollisionShape = CVar_GetValueU32(
            &gameState->cvarTable, CVAR(r_draw_collision_shapes));
        if (drawCollisionShape & DrawCollisionShapeFlag_Boxes)
        {
            DrawBoxesCollisionShape(&gameState->entityWorld,
                g_ServerDebugDrawingSystem, Vec3(1.0f, 0.4f, 0.1f));
        }

        if (drawCollisionShape & DrawCollisionShapeFlag_Spheres)
        {
            DrawSphereCollisionShapesSystem(&gameState->entityWorld,
                g_ServerDebugDrawingSystem, Vec3(1.0f, 0.4f, 0.1f));
        }

        if (drawCollisionShape & DrawCollisionShapeFlag_Capsule)
        {
            DrawCapsuleCollisionShapesSystem(&gameState->entityWorld,
                g_ServerDebugDrawingSystem, Vec3(1.0f, 0.4f, 0.1f));
        }

        if (drawCollisionShape & DrawCollisionShapeFlag_TriangleMeshes)
        {
            DrawTriangleMeshCollisionShapeSystem(&gameState->entityWorld,
                &gameState->assets, g_ServerDebugDrawingSystem,
                Vec3(1.0f, 0.4f, 0.1f), &gameState->tempArena);
        }

        if (drawCollisionShape & DrawCollisionShapeFlag_HeightMap)
        {
            DebugDrawTerrain(&gameState->entityWorld, &gameState->assets,
                g_ServerDebugDrawingSystem, Vec3(1.0, 0.4, 0.1),
                g_CameraPosition);
        }

        if (CVar_GetValueU32(&gameState->cvarTable, CVAR(r_draw_broadphase)))
        {
            DrawBroadPhase(&gameState->entityWorld, g_ServerDebugDrawingSystem,
                Vec3(0.0, 1.0, 1.0));
        }

    }

    // Must make sure no entity ids are persisted to the next frame in case they are deleted
    gameState->eventQueue.length = 0;

    ProcessEntityDeletionQueue(&gameState->entityWorld);

    for (u32 eventIdx = 0;
         eventIdx < gameState->entityWorld.componentRemovalEventCount;
         ++eventIdx)
    {
        ecs_ComponentRemovedEvent *event =
            gameState->entityWorld.componentRemovedEvents + eventIdx;

        switch (event->componentId)
        {
        case CollisionShapeComponentId:
            break;
        default:
            break;
        }
    }
    gameState->entityWorld.componentRemovalEventCount = 0;
}

struct DebugDisplayEntry
{
    const char *name;
    u32 hitCount;
    u64 cycleCount;
};

struct DebugDisplayNode
{
    const char *name;
    u64 cycleCount;
    u32 depth;

    DebugDisplayNode *children;
    DebugDisplayNode *next;
};

internal u32 CountDebugEvents(DebugEventTable *eventTable,
    DebugDisplayEntry *displayEntries, DebugDisplayNode *nodes,
    u32 nodeCapacity)
{
    for (u32 recordIndex = 0; recordIndex < ArrayCount(eventTable->records); ++recordIndex)
    {
        DebugRecord *record = eventTable->records + recordIndex;
        DebugDisplayEntry *entry = displayEntries + recordIndex;
        entry->name = record->name;
    }

    DebugDisplayNode *stack[64];
    u32 stackLength = 0;
    u32 nodeCount = 0;

    u32 readCursor = eventTable->readCursor;
    for (u32 eventIndex = 0; eventIndex < eventTable->count[readCursor]; ++eventIndex)
    {
        DebugEvent *event = eventTable->events[readCursor] + eventIndex;

        switch (event->type)
        {
            case DebugEventType_BeginBlock:
            {
                DebugDisplayNode *parent = NULL;
                if (stackLength > 0)
                {
                    parent = stack[stackLength - 1];
                }

                Assert(nodeCount < nodeCapacity);
                DebugDisplayNode *node = nodes + nodeCount++;

                Assert(stackLength < ArrayCount(stack));
                DebugDisplayNode **stackEntry = stack + stackLength++;
                *stackEntry = node;

                if (parent != NULL)
                {
                    node->next = parent->children;
                    parent->children = node;
                    node->depth = parent->depth + 1;
                }
            }
            break;
            case DebugEventType_EndBlock:
                Assert(eventIndex > 0);
                for (i32 prevEventIndex = eventIndex - 1; prevEventIndex >= 0; --prevEventIndex)
                {
                    DebugEvent *prevEvent = eventTable->events[readCursor] + prevEventIndex;
                    if ((prevEvent->recordIndex == event->recordIndex) &&
                        (prevEvent->type == DebugEventType_BeginBlock))
                    {
                        Assert(event->recordIndex < ArrayCount(eventTable->records));
                        DebugDisplayEntry *entry = displayEntries + event->recordIndex;
                        entry->hitCount++;
                        entry->cycleCount = event->cycleCount - prevEvent->cycleCount;

                        Assert(stackLength > 0);
                        DebugDisplayNode *node = stack[--stackLength];

                        node->name = entry->name;
                        node->cycleCount = entry->cycleCount;

                        break;
                    }
                }
                break;
            default:
                break;
        }
    }

    return nodeCount;
}

internal void DisplayDebugCounters(GameState *gameState, GameMemory *memory)
{
    DebugDisplayEntry displayEntries[ArrayCount(g_DebugEventTable->records)] =
        {};

    DebugDisplayNode displayNodes[ArrayCount(g_DebugEventTable->records)] = {};
    u32 nodeCount = CountDebugEvents(g_DebugEventTable, displayEntries,
        displayNodes, ArrayCount(displayNodes));

    mat4 orthographic = Orthographic(0.0f, (f32)memory->frameBufferWidth, 0.0f,
        (f32)memory->frameBufferHeight);

//#define DUMP_TO_CSV
#ifdef DUMP_TO_CSV
    LogMessage("Name, Cycle Count, Hit Count, Cycles Per Hit");
#endif
    f32 yOffset = 0.0f;
#if 1
    for (u32 nodeIndex = 0; nodeIndex < nodeCount; ++nodeIndex)
    {
        DebugDisplayNode *node = displayNodes + nodeIndex;
        char buffer[150];
        snprintf(buffer, sizeof(buffer), "%*s%40s: %16gM cycles", node->depth, "|", node->name,
            node->cycleCount / (1000.0 * 1000.0));

        ui_DrawStringArguments text = {};
        text.memory = memory;
        text.vertexBuffer = &gameState->textBuffer;
        text.text = buffer;
        text.position =
            Vec2(10.0f, (f32)memory->frameBufferHeight - 80.0f) -
            Vec2(0, yOffset);
        text.color = Vec4(1.0f);
        text.font = &gameState->debugFont;
        text.orthographicProjection = orthographic;
        text.horizontalAlignment = HorizontalAlign_Left;
        text.verticalAlignment = VerticalAlign_Top;
        text.drawShadow = true;
        text.shadowOffset = Vec2(1, -1);
        ui_DrawString(&text);
        yOffset += gameState->debugFont.lineSpacing;
    }
#else
    for (u32 entryIndex = 0; entryIndex < ArrayCount(g_DebugEventTable->records);
         ++entryIndex)
    {
        DebugDisplayEntry *entry = displayEntries + entryIndex;
        if (entry->hitCount > 0)
        {
            char buffer[150];
            snprintf(buffer, sizeof(buffer),
                "%40s: %16llu cycles %6u hits %16llu cycles/hit", entry->name,
                entry->cycleCount, entry->hitCount,
                entry->cycleCount / entry->hitCount);

            ui_DrawStringArguments text = {};
            text.memory = memory;
            text.vertexBuffer = &gameState->textBuffer;
            text.text = buffer;
            text.position =
                Vec2(10.0f, (f32)memory->frameBufferHeight - 80.0f) -
                Vec2(0, yOffset);
            text.color = Vec4(1.0f);
            text.font = &gameState->debugFont;
            text.orthographicProjection = orthographic;
            text.horizontalAlignment = HorizontalAlign_Left;
            text.verticalAlignment = VerticalAlign_Top;
            text.drawShadow = true;
            text.shadowOffset = Vec2(1, -1);
            ui_DrawString(&text);
            yOffset += gameState->debugFont.lineSpacing;

#ifdef DUMP_TO_CSV
            LogMessage("%s,%llu,%u,%llu", entry->name, entry->cycleCount,
                entry->hitCount, entry->cycleCount / entry->hitCount);
#endif
        }
    }
#endif

}
