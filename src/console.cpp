#define CONSOLE_LINE_WIDTH 80
#define CONSOLE_MAX_LINES 4096
#define CONSOLE_WINDOW_HEIGHT 26
#define CONSOLE_MAX_HISTORY 50
struct Console
{
    char outputBuffer[CONSOLE_MAX_LINES][CONSOLE_LINE_WIDTH];
    u32 outputBufferLineLengths[CONSOLE_MAX_LINES];
    u32 lineCount;
    u32 begin;
    u32 scrollOffset;

    char inputBuffer[CONSOLE_LINE_WIDTH];
    u32 inputBufferLength;

    char history[CONSOLE_MAX_HISTORY][CONSOLE_LINE_WIDTH];
    u32 historyLengths[CONSOLE_MAX_HISTORY];
    u32 historyCount;
    u32 historyBegin;
    u32 historyCursor;

    u32 completionLength;
    b32 isInputDirty;
    u32 completionCursor;
};

global Console *g_Console;

internal void DrawConsole(Console *console, GameMemory *memory,
    GameInput *input, VertexBuffer *textBuffer, Font *font)
{
    mat4 orthographic = Orthographic(0.0f, (f32)memory->frameBufferWidth, 0.0f,
        (f32)memory->frameBufferHeight);

    f32 panelHeight = 500.0f;
    ui_DrawQuadArguments drawPanel = {};
    drawPanel.memory = memory;
    drawPanel.orthographicProjection = orthographic;
    drawPanel.position = Vec2(0.0f, (f32)memory->frameBufferHeight);
    drawPanel.width = (f32)memory->frameBufferWidth;
    drawPanel.height = panelHeight;
    drawPanel.color = Vec4(0, 0, 0, 0.8);
    drawPanel.horizontalAlignment = HorizontalAlign_Left;
    drawPanel.verticalAlignment = VerticalAlign_Top;
    ui_DrawQuad(&drawPanel);

    f32 y = memory->frameBufferHeight - 1.0f;
    u32 start = console->begin + console->scrollOffset;
    u32 linesToDraw =
        MinU(CONSOLE_WINDOW_HEIGHT, console->lineCount - console->scrollOffset);
    for (u32 i = 0; i < linesToDraw; ++i)
    {
        u32 index = (start + i) % CONSOLE_MAX_LINES;
        ui_DrawStringArguments drawLine = {};
        drawLine.memory = memory;
        drawLine.vertexBuffer = textBuffer;
        drawLine.position = Vec2(4.0f, y);
        drawLine.color = Vec4(1.0);
        drawLine.font = font;
        drawLine.orthographicProjection = orthographic;
        drawLine.horizontalAlignment = HorizontalAlign_Left;
        drawLine.verticalAlignment = VerticalAlign_Top;
        drawLine.text = console->outputBuffer[index];
        drawLine.length = console->outputBufferLineLengths[index];
        ui_DrawString(&drawLine);

        y -= font->lineSpacing * 1.5f;
    }

    f32 inputBarHeight = font->lineSpacing + 8.0f;
    ui_DrawQuadArguments drawInputBar = {};
    drawInputBar.memory = memory;
    drawInputBar.orthographicProjection = orthographic;
    drawInputBar.position = Vec2(0.0f, (f32)memory->frameBufferHeight - panelHeight);
    drawInputBar.width = (f32)memory->frameBufferWidth;
    drawInputBar.height = inputBarHeight;
    drawInputBar.color = Vec4(0, 0, 0, 0.2);
    drawInputBar.horizontalAlignment = HorizontalAlign_Left;
    drawInputBar.verticalAlignment = VerticalAlign_Bottom;
    ui_DrawQuad(&drawInputBar);

    if (console->inputBufferLength > 0)
    {
        ui_DrawStringArguments drawInputText = {};
        drawInputText.memory = memory;
        drawInputText.vertexBuffer = textBuffer;
        drawInputText.position =
            Vec2(4.0f, (f32)memory->frameBufferHeight - panelHeight +
                           inputBarHeight - 4.0f);
        drawInputText.color = Vec4(1.0);
        drawInputText.font = font;
        drawInputText.orthographicProjection = orthographic;
        drawInputText.horizontalAlignment = HorizontalAlign_Left;
        drawInputText.verticalAlignment = VerticalAlign_Top;
        drawInputText.text = console->inputBuffer;
        drawInputText.length = console->inputBufferLength;
        ui_DrawString(&drawInputText);
    }
}

internal void Console_OutputString(
    Console *console, const char *str, u32 length = 0)
{
    if (length == 0)
    {
        length = StringLength(str);
    }

    // No line wrapping
    length = MinU(length, CONSOLE_LINE_WIDTH);

    u32 index = (console->begin + console->lineCount) % CONSOLE_MAX_LINES;
    char *dst = console->outputBuffer[index];
    console->outputBufferLineLengths[index] = length;
    CopyMemory(dst, str, length);
    if (console->lineCount < CONSOLE_MAX_LINES)
    {
        console->lineCount = console->lineCount + 1;
    }
    else
    {
        console->begin = (console->begin + 1) % CONSOLE_MAX_LINES; 
    }

    if (console->lineCount > CONSOLE_WINDOW_HEIGHT)
    {
        console->scrollOffset = console->lineCount - CONSOLE_WINDOW_HEIGHT;
    }
}

internal void Console_AddCharacterToInputBuffer(Console *console, char c)
{
    if (c == '\n')
    {
        console->inputBufferLength = 0;
    }
    else
    {
        if (console->inputBufferLength < ArrayCount(console->inputBuffer))
        {
            console->inputBuffer[console->inputBufferLength++] = c;
        }
    }
}

internal void Console_AddInputToHistory(Console *console)
{
    Assert(console->inputBufferLength < CONSOLE_LINE_WIDTH);
    u32 index = (console->historyBegin + console->historyCount) %
        CONSOLE_MAX_HISTORY;
    char *dst = console->history[index];
    console->historyLengths[index] = console->inputBufferLength;
    CopyMemory(dst, console->inputBuffer, console->inputBufferLength);
    if (console->historyCount < CONSOLE_MAX_HISTORY)
    {
        console->historyCount = console->historyCount + 1;
        console->historyCursor = console->historyCount;
    }
    else
    {
        console->historyBegin =
            (console->historyBegin + 1) % CONSOLE_MAX_HISTORY;
    }
}

internal void Console_UseHistory(Console *console)
{
    if (console->historyCount > 0 && console->historyCursor < console->historyCount)
    {
        u32 idx = (console->historyBegin + console->historyCursor) %
                  CONSOLE_MAX_HISTORY;

        CopyMemory(console->inputBuffer, console->history[idx],
            console->historyLengths[idx]);
        console->inputBufferLength = console->historyLengths[idx];
    }
}

internal void LogConsole(const char *fmt, ...)
{
    Assert(g_Console != NULL);

    char buffer[4096];
    va_list args;
    va_start(args, fmt);
    i32 charCount = vsnprintf(buffer, sizeof(buffer), fmt, args);
    va_end(args);

    if (charCount > 0)
    {
        charCount = Min(charCount, sizeof(buffer));

        char *start = buffer;
        u32 length = 0;
        for (u32 i = 0; i < (u32)charCount; ++i)
        {
            if (buffer[i] == '\n')
            {
                Console_OutputString(g_Console, start, length);
                start = buffer + i + 1; // Skip newline
                length = 0;
            }
            else
            {
                length++;
            }
        }
        if (length > 0)
        {
            Console_OutputString(g_Console, start, length);
        }
    }
}

internal void UpdateConsole(Console *console, GameInput *input,
    Cmd_System *commandSystem, MemoryArena *tempArena)
{
    for (u32 i = 0; i < input->characterCount; ++i)
    {
        if (input->characters[i] <= 127 && input->characters[i] != '`')
        {
            Console_AddCharacterToInputBuffer(
                console, (char)input->characters[i]);
            console->isInputDirty = true;
        }
    }

    if (WasPressed(input->buttonStates + KEY_TAB))
    {
        if (console->isInputDirty)
        {
            console->completionLength = console->inputBufferLength;
            console->completionCursor = 0;
        }
        else
        {
            console->completionCursor++; // Probably should mod this here
                                         // based on the match count rather
                                         // than just letting it overflow
        }

        const char *completion =
            Cmd_Completion(commandSystem, console->inputBuffer,
                console->completionLength, console->completionCursor);

        if (completion)
        {
            u32 length = StringLength(completion);
            Assert(length < ArrayCount(console->inputBuffer));
            CopyMemory(console->inputBuffer, completion, length);
            console->inputBufferLength = length;
            console->isInputDirty = false;
        }
    }

    if (WasPressed(input->buttonStates + KEY_UP))
    {
        if (console->historyCursor > 0)
        {
            console->historyCursor--;
        }
        Console_UseHistory(console);
        console->isInputDirty = true;
    }

    if (WasPressed(input->buttonStates + KEY_DOWN))
    {
        if ((console->historyCursor < console->historyCount - 1) &&
            (console->historyCount > 0))
        {
            console->historyCursor++;
            Console_UseHistory(console);
        }
        else
        {
            console->inputBufferLength = 0;
        }
        console->isInputDirty = true;
    }

    if (WasPressed(input->buttonStates + KEY_ENTER))
    {
        if (console->inputBufferLength > 0)
        {
            char *cmdline =
                AllocateArray(tempArena, char, console->inputBufferLength + 1);
            CopyMemory(
                cmdline, console->inputBuffer, console->inputBufferLength);
            cmdline[console->inputBufferLength] = '\0';

            LogConsole("> %s", cmdline);

            Cmd_Exec(commandSystem, cmdline);

            Console_AddInputToHistory(console);
        }
        console->inputBufferLength = 0;
        console->isInputDirty = true;
    }
    if (WasPressed(input->buttonStates + KEY_BACKSPACE))
    {
        if (console->inputBufferLength > 0)
        {
            console->inputBufferLength -= 1;
        }
        console->isInputDirty = true;
    }

    if (WasPressed(input->buttonStates + KEY_PAGE_UP))
    {
        console->scrollOffset =
            Clamp((i32)console->scrollOffset - CONSOLE_WINDOW_HEIGHT, 0,
                console->lineCount);
    }
    if (WasPressed(input->buttonStates + KEY_PAGE_DOWN))
    {
        console->scrollOffset =
            Clamp((i32)console->scrollOffset + CONSOLE_WINDOW_HEIGHT, 0,
                console->lineCount);
    }
}
