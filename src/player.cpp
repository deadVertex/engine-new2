enum
{
    GameEvent_ReceivedDamage,
    GameEvent_BulletImpact,
    GameEvent_GunShot,
};

struct GameEvent
{
    u32 type;

    union
    {
        struct
        {
            EntityId entity;
            f32 amount;
        } receivedDamage;

        struct
        {
            vec3 position;
            vec3 normal;
        } bulletImpact;

        struct
        {
            EntityId owner;
            vec3 position;
        } gunShot;
    };
};

struct GameEventQueue
{
    GameEvent events[16];
    u32 length;
};

// TODO: This won't work when we want the player to have a box component
internal b32 CanMoveToPosition(ecs_EntityWorld *world, vec3 currentPosition, vec3 newPosition)
{

    return true;
}

#if 0
internal void StepDown(HeightMap heightMap, vec3 *position, vec3 *velocity)
{
    if (velocity->y < 0.0f)
    {
        float playerHeight = 1.8f;
        f32 sampleY =
            SampleTerrainAtWorldPosition(heightMap, position->x, position->z);
        //LogMessage("sampleY: %g (%g %g)", sampleY, position->x, position->z);

        if (position->y > sampleY)
        {
            f32 deltaY = position->y - sampleY;
            if (deltaY < playerHeight + 0.3f)
            {
                position->y = sampleY + playerHeight;
                velocity->y = 0.0f;
            }
        }
    }
}
#endif

struct MoveAndCollideEntityResult
{
    vec3 position;
    vec3 velocity;
};

internal MoveAndCollideEntityResult MoveAndCollideEntity(
    ecs_EntityWorld *world, vec3 position, vec3 velocity,
    EntityId entity, f32 dt)
{
    MoveAndCollideEntityResult result = {};
    result.position = position;
    result.velocity = Vec3(0);

    //velocity.y = 0.0f;
    f32 dtRemaining = dt;

    vec3 currentPosition = position;
    vec3 targetPosition = position + velocity * dt;
    vec3 targetVelocity = velocity;

#define MAX_BUMPS 4
    for (u32 bumpCount = 0; bumpCount < MAX_BUMPS; ++bumpCount)
    {
        if (dtRemaining < EPSILON)
        {
            break;
        }

        vec3 start = currentPosition;
        vec3 delta = targetVelocity * dtRemaining;
        vec3 end = start + delta;

        EntityRaycastResult sweepResult =
            SphereSweepWorld(world, start, end, 0.5f, &entity, 1);
        Assert(sweepResult.raycastResult.tmin >= 0.0f);

        if (sweepResult.entity != NULL_ENTITY)
        {
            vec3 hitNormal = sweepResult.raycastResult.hitNormal;

            Assert(LengthSq(sweepResult.raycastResult.hitPoint - start) <= LengthSq(delta));

            // NOTE: This code does not work if EPSILON is used, might need to
            // define a different EPSILON constant
            currentPosition = sweepResult.raycastResult.hitPoint + hitNormal * 0.00001f;

            dtRemaining -= dtRemaining * sweepResult.raycastResult.tmin;

            targetVelocity =
                targetVelocity - Dot(targetVelocity, hitNormal) * hitNormal;

            targetPosition = currentPosition;
            result.position = targetPosition;
        }
        else
        {
            // Full move
            targetPosition = currentPosition + delta;
            result.velocity = targetVelocity;
            result.position = targetPosition;
            break;
        }
    }

    return result;
}

internal void MovePlayer(ecs_EntityWorld *world, PlayerCommand cmd,
    PlayerCommand prevCmd, float dt, EntityId player, MemoryArena *tempArena)
{
    TIMED_BLOCK();
    vec3 newRotation =
        Vec3(-cmd.mouseY, -cmd.mouseX, 0);

    vec3 eulerAngles;
    ecs_GetComponent(
        world, EulerAnglesComponentId, &player, 1, &eulerAngles, sizeof(eulerAngles));

    vec3 currentPosition;
    ecs_GetComponent(world, PositionComponentId, &player, 1, &currentPosition,
        sizeof(currentPosition));

    vec3 currentVelocity;
    ecs_GetComponent(world, VelocityComponentId, &player, 1, &currentVelocity,
        sizeof(currentVelocity));

    eulerAngles += newRotation;

    eulerAngles.x = Clamp(eulerAngles.x, -PI * 0.5f, PI * 0.5f);

    if (eulerAngles.y > 2.0f * PI)
    {
        eulerAngles.y -= 2.0f * PI;
    }
    if (eulerAngles.y < 2.0f * -PI)
    {
        eulerAngles.y += 2.0f * PI;
    }

    float forwardMove = 0.0f;
    float rightMove = 0.0f;
    if (cmd.actions & PlayerAction_MoveForward)
    {
        forwardMove = -1.0f;
    }

    if (cmd.actions & PlayerAction_MoveBackward)
    {
        forwardMove = 1.0f;
    }

    if (cmd.actions & PlayerAction_MoveLeft)
    {
        rightMove = -1.0f;
    }
    if (cmd.actions & PlayerAction_MoveRight)
    {
        rightMove = 1.0f;
    }

    quat movementRotation = Quat(Vec3(0, 1, 0), eulerAngles.y);
    vec3 forward = Rotate(movementRotation, Vec3(0, 0, 1));
    vec3 right = Rotate(movementRotation, Vec3(1, 0, 0));

    vec3 targetDir = forward * forwardMove + right * rightMove;
    if (LengthSq(targetDir) > 0.0f)
    {
        targetDir = Normalize(targetDir);
    }

    f32 onGroundBias = 0.05f;
    EntityRaycastResult groundTrace = RaycastWorld(world, currentPosition,
        currentPosition - Vec3(0, PLAYER_HEIGHT * 0.5f + onGroundBias, 0),
        &player, 1);

    b32 onGround = groundTrace.entity != NULL_ENTITY;
    if (onGround)
    {
    }

    float groundSpeed = 80.0f;
    float groundFriction = 9.0f;
    float groundSprintSpeed = groundSpeed * 1.8f;
    float airSpeed = 3.0f;
    float airFriction = 0.5f;
    vec3 gravity = Vec3(0, -10.0f, 0);

    float speed = airSpeed;
    float friction = onGround ? groundFriction : airFriction;
    vec3 playerGravity = onGround ? Vec3(0, 0, 0) : gravity * 4.0f;

    vec3 jumpVelocity = {};
    if (onGround)
    {
        if (currentPosition.y - groundTrace.raycastResult.hitPoint.y >
            onGroundBias)
        {
            currentPosition.y =
                groundTrace.raycastResult.hitPoint.y + PLAYER_HEIGHT * 0.5f;
        }

        if (currentVelocity.y <= 0.0f)
        {
            //currentVelocity.y = 0.0f;
            // Not sure about this, need step down
            // to avoid jerky vertical movement
        }

        speed = (cmd.actions & PlayerAction_Sprint)
                    ? groundSprintSpeed
                    : groundSpeed;

        if ((cmd.actions & PlayerAction_Jump) &&
            !(prevCmd.actions & PlayerAction_Jump))
        {
            jumpVelocity.y = 600.0f;
        }
    }

    vec3 targetVelocity = currentVelocity;
    targetVelocity -= currentVelocity * friction * dt;
    targetVelocity += targetDir * speed * dt;
    targetVelocity += playerGravity * dt;
    targetVelocity += jumpVelocity * (1.0f / 60.0f); // Fix jump height being
                                                     // frame dependent until
                                                     // we switch to a proper
                                                     // fixed dt.

    vec3 targetPosition = currentPosition;

    MoveAndCollideEntityResult moveAndCollideResult = MoveAndCollideEntity(
        world, currentPosition, targetVelocity, player, dt);

    targetPosition = moveAndCollideResult.position;
    targetVelocity = moveAndCollideResult.velocity;

    //StepDown(heightMap, &targetPosition, &targetVelocity);

    ecs_SetComponent(world, EulerAnglesComponentId, &player, 1, &eulerAngles,
        sizeof(eulerAngles));
    ecs_SetComponent(world, PositionComponentId, &player, 1, &targetPosition,
        sizeof(targetPosition));
    ecs_SetComponent(world, VelocityComponentId, &player, 1, &targetVelocity,
        sizeof(targetVelocity));

    quat rotation = FromEulerAngles(eulerAngles);
    ecs_SetComponent(world, RotationComponentId, &player, 1, &rotation,
        sizeof(rotation));
}

// TODO: Make this a table
inline u32 MapItemIdToDeployableId(u32 itemId)
{
    switch (itemId)
    {
        case ItemID_StorageBox:
            return DeployableID_StorageBox;
        case ItemID_Foundation:
            return DeployableID_Foundation;
        case ItemID_Wall:
            return DeployableID_Wall;
        case ItemID_Doorway:
            return DeployableID_Doorway;
        case ItemID_Ceiling:
            return DeployableID_Ceiling;
        default:
            InvalidCodePath();
            return DeployableID_None;
    }
}

inline u32 MapItemIdToEntityPrefabPreview(u32 itemId)
{
    switch (itemId)
    {
        case ItemID_StorageBox:
            return EntityPrefab_StorageBoxPreview;
        case ItemID_Foundation:
            return EntityPrefab_FoundationPreview;
        case ItemID_Wall:
            return EntityPrefab_WallPreview;
        case ItemID_Doorway:
            return EntityPrefab_DoorwayPreview;
        case ItemID_Ceiling:
            return EntityPrefab_CeilingPreview;
        default:
            InvalidCodePath();
            return EntityPrefab_StorageBoxPreview;
    }
}

inline u32 MapItemIdToEntityPrefab(u32 itemId)
{
    switch (itemId)
    {
        case ItemID_StorageBox:
            return EntityPrefab_StorageBox;
        case ItemID_Foundation:
            return EntityPrefab_Foundation;
        case ItemID_Wall:
            return EntityPrefab_Wall;
        case ItemID_Doorway:
            return EntityPrefab_Doorway;
        case ItemID_Ceiling:
            return EntityPrefab_Ceiling;
        default:
            InvalidCodePath();
            return EntityPrefab_StorageBox;
    }
}

inline b32 IsItemDeployable(u32 itemId)
{
    b32 result = (itemId == ItemID_StorageBox) ||
                 (itemId == ItemID_Foundation) || (itemId == ItemID_Wall) ||
                 (itemId == ItemID_Doorway) || (itemId == ItemID_Ceiling);

    return result;
}

struct EntityPlacementResult
{
    vec3 position;
    quat rotation;
    b32 isValid;
};

#define WALL_HEIGHT 2.7f
#define FOUNDATION_WIDTH 3.0f
#define FOUNDATION_HEIGHT 0.5f
#define CEILING_HEIGHT (FOUNDATION_HEIGHT * 0.5f)

internal EntityPlacementResult CalculatePlacement(
    ecs_EntityWorld *world, EntityId player, u32 itemId)
{
    EntityPlacementResult result = {};

    // Get player position and rotation
    vec3 playerPosition;
    quat playerRotation;
    ecs_GetComponent(world, PositionComponentId, &player, 1, &playerPosition,
            sizeof(playerPosition));
    ecs_GetComponent(world, RotationComponentId, &player, 1, &playerRotation,
            sizeof(playerRotation));

    // Calculate view direction vector
    vec3 direction = Normalize(Rotate(playerRotation, Vec3(0, 0, -1)));

    // Calculate ray start and end
    f32 range = 10.0f;
    vec3 rayStart = playerPosition + PLAYER_CAMERA_OFFSET;
    vec3 rayEnd = rayStart + direction * range;

    // Raycast against world to get hit position
    EntityRaycastResult worldRaycastResult =
        RaycastWorld(world, rayStart, rayEnd, &player, 1);

    // Raycast against world to get hit position
    if (worldRaycastResult.raycastResult.isValid)
    {
        EntityId hitEntity = worldRaycastResult.entity;
        vec3 hitPoint = worldRaycastResult.raycastResult.hitPoint;
        vec3 hitNormal = worldRaycastResult.raycastResult.hitNormal;

        result.position = hitPoint;
        result.rotation = Quat();
        result.isValid = true;

        vec3 hitEntityPosition;
        ecs_GetComponent(world, PositionComponentId, &hitEntity,
            1, &hitEntityPosition, sizeof(hitEntityPosition));

        quat hitEntityRotation;
        ecs_GetComponent(world, RotationComponentId, &hitEntity, 1,
            &hitEntityRotation, sizeof(hitEntityRotation));

        b32 isPlacingOnDeployable =
            ecs_HasComponent(world, DeployableIdComponentId, hitEntity);

        // TODO: ecs_GetComponent with default value for non-matches
        u32 deployableId = DeployableID_None;
        if (isPlacingOnDeployable)
        {
            ecs_GetComponent(world, DeployableIdComponentId, &hitEntity, 1,
                &deployableId, sizeof(deployableId));
        }

        switch (itemId)
        {
        case ItemID_Foundation:
            if (isPlacingOnDeployable &&
                (deployableId == DeployableID_Foundation))
            {
                vec3 d = SafeNormalize(hitPoint - hitEntityPosition);
                d.y = 0.0f;

                // Assuming that foundations have no rotation
                u32 largestAxis = GetLargestAxis(d);
                vec3 offset = {};
                offset.data[largestAxis] = Sign(d.data[largestAxis]);

                result.position = hitEntityPosition + offset * FOUNDATION_WIDTH;
            }
            break;
        case ItemID_Wall: // Fall through
        case ItemID_Doorway:
            if (isPlacingOnDeployable)
            {
                if (deployableId == DeployableID_Foundation ||
                    deployableId == DeployableID_Ceiling)
                {
                    vec3 d = SafeNormalize(hitPoint - hitEntityPosition);
                    d.y = 0.0f;

                    // Assuming that foundations have no rotation
                    u32 largestAxis = GetLargestAxis(d);
                    vec3 offset = {};
                    offset.data[largestAxis] = Sign(d.data[largestAxis]);

                    result.rotation = (largestAxis == 0)
                                          ? Quat(Vec3(0, 1, 0), PI * 0.5)
                                          : Quat();

                    f32 yOffset = deployableId == DeployableID_Foundation
                                      ? FOUNDATION_HEIGHT
                                      : CEILING_HEIGHT * 0.5f;
                    result.position = hitEntityPosition +
                                      offset * FOUNDATION_WIDTH * 0.5f +
                                      Vec3(0, yOffset, 0);
                    result.isValid = true;
                }
                else if (deployableId == DeployableID_Wall ||
                         deployableId == DeployableID_Doorway)
                {
                    result.rotation = hitEntityRotation;
                    result.position =
                        hitEntityPosition + Vec3(0, WALL_HEIGHT, 0);
                    result.isValid = true;
                }
            }
            else
            {
                result.isValid = false;
            }
            break;
        case ItemID_Ceiling:
            if (isPlacingOnDeployable)
            {
                if ((deployableId == DeployableID_Wall) ||
                    (deployableId == DeployableID_Doorway))
                {
                    if (Abs(hitNormal.y) < EPSILON)
                    {
                        result.position = hitEntityPosition +
                                          Vec3(0, WALL_HEIGHT - CEILING_HEIGHT * 0.5f, 0) +
                                          hitNormal * FOUNDATION_WIDTH * 0.5f;
                    }
                    else
                    {
                        result.isValid = false;
                    }
                }
                else if (deployableId == DeployableID_Ceiling)
                {
                    // Copy of foundation on foundation snapping
                    vec3 d = SafeNormalize(hitPoint - hitEntityPosition);
                    d.y = 0.0f;

                    // Assuming that foundations have no rotation
                    u32 largestAxis = GetLargestAxis(d);
                    vec3 offset = {};
                    offset.data[largestAxis] = Sign(d.data[largestAxis]);

                    result.position = hitEntityPosition + offset * FOUNDATION_WIDTH;
                }
            }
            break;
        case ItemID_StorageBox:
            result.position.y += 0.4f;
            break;
        default:
            break;
        }
    }

    return result;
}

internal void PlaceEntitiesSystem(
    ecs_EntityWorld *world, EntityId player, PlayerCommand command)
{
    if (command.actions & PlayerAction_PrimaryFire &&
        !(command.prevActions & PlayerAction_PrimaryFire))
    {
        u32 itemId = GetActiveItemId(world, player);
        if (IsItemDeployable(itemId))
        {

            EntityPlacementResult placement =
                CalculatePlacement(world, player, itemId);

            if (placement.isValid)
            {
                u32 prefab = MapItemIdToEntityPrefab(itemId);
                SpawnEntityAtPositionWithRotation(
                    world, prefab, placement.position, placement.rotation);
            }
        }
    }
}

internal void BuildingPreviewSystem(
    ecs_EntityWorld *world, DebugDrawingSystem *debugDrawingSystem)
{
    EntityId player = NULL_ENTITY;
    if (GetFirstEntity(world, ActivePlayerComponentId, &player))
    {
        u32 itemId = GetActiveItemId(world, player);

        EntityId previewEntity = NULL_ENTITY;
        b32 isPreviewEntityActive = GetFirstEntity(
                world, BuildablePreviewComponentId, &previewEntity);

        b32 showPreview = IsItemDeployable(itemId);

        if (showPreview)
        {
            EntityPlacementResult placement =
                CalculatePlacement(world, player, itemId);

            if (placement.isValid)
            {
                u32 expectedDeployableId = MapItemIdToDeployableId(itemId);

                // if building preview entity does not exist then create it
                b32 spawnNewPreviewEntity = false;
                if (isPreviewEntityActive)
                {
                    u32 deployableId = DeployableID_None;
                    ecs_GetComponent(world, DeployableIdComponentId,
                            &previewEntity, 1, &deployableId,
                            sizeof(deployableId));

                    if (deployableId != expectedDeployableId)
                    {
                        // NOTE: This could be quite risky as GetFirstEntity
                        // will still return the preview entity we are
                        // deleting for the remainder of the frame.
                        ecs_QueueEntitiesForDeletion(
                                world, &previewEntity, 1);
                        spawnNewPreviewEntity = true;
                        previewEntity = NULL_ENTITY;
                    }
                }
                else
                {
                    spawnNewPreviewEntity = true;
                }


                if (spawnNewPreviewEntity)
                {
                    u32 prefab = MapItemIdToEntityPrefabPreview(itemId);
                    previewEntity = SpawnEntityAtPositionWithRotation(
                        world, prefab, placement.position, placement.rotation);
                }

                Assert(previewEntity != NULL_ENTITY)
                if (previewEntity != NULL_ENTITY)
                {
                    // Check that the entity prefab has not been misconfigured
                    u32 deployableId;
                    ecs_GetComponent(world, DeployableIdComponentId,
                        &previewEntity, 1, &deployableId, sizeof(deployableId));
                    Assert(deployableId == expectedDeployableId);

                    // move building preview entity to hit position
                    ecs_SetComponent(world, PositionComponentId, &previewEntity,
                        1, &placement.position, sizeof(placement.position));
                    ecs_SetComponent(world, RotationComponentId, &previewEntity,
                        1, &placement.rotation, sizeof(placement.rotation));
                }
            }
            else if (isPreviewEntityActive)
            {
                ecs_QueueEntitiesForDeletion(world, &previewEntity, 1);
                previewEntity = NULL_ENTITY;
            }
        }
        else if (isPreviewEntityActive)
        {
            ecs_QueueEntitiesForDeletion(world, &previewEntity, 1);
        }
    }

    // Also do raycast on server and create entity if player press left mouse
}

internal void InteractSystem(
    ecs_EntityWorld *world, EntityId player, PlayerCommand command)
{
    if (command.actions & PlayerAction_Interact)
    {
        vec3 playerPosition;
        ecs_GetComponent(world, PositionComponentId, &player, 1,
            &playerPosition, sizeof(playerPosition));

        quat playerRotation;
        ecs_GetComponent(world, RotationComponentId, &player, 1,
                &playerRotation, sizeof(playerRotation));

        // Calculate view direction vector
        vec3 direction = Normalize(Rotate(playerRotation, Vec3(0, 0, -1)));

        f32 range = 4.5f;
        vec3 rayStart = playerPosition + PLAYER_CAMERA_OFFSET;
        vec3 rayEnd = rayStart + direction * range;

        EntityRaycastResult result =
            RaycastWorld(world, rayStart, rayEnd, &player, 1);

        if (result.entity != NULL_ENTITY)
        {
            u32 components[] = {InteractableComponentId, InventoryComponentId,
                NetEntityIdComponentId};

            if (HasComponentsSlow(
                    world, result.entity, components, ArrayCount(components)))
            {
                // TODO: Make this an event
                LogMessage("Opened container entity %u", result.entity);
                ecs_SetComponent(world, OpenContainerComponentId, &player, 1,
                    &result.entity, sizeof(result.entity));
            }
        }
    }
}

internal void AttackSystem(ecs_EntityWorld *world, GameAssets *assets,
    EntityId player, PlayerCommand command, PlayerCommand prevCommand,
    GameEventQueue *eventQueue, f32 dt)
{
    if (command.actions & PlayerAction_PrimaryFire &&
        !(prevCommand.actions & PlayerAction_PrimaryFire))
    {
        u32 itemId = GetActiveItemId(world, player);
        if (itemId == ItemID_Pistol)
        {
            EntityId itemEntities[INVENTORY_SLOT_COUNT];
            u32 itemCount = GetAllInstancesOfItemInInventory(world, player,
                ItemID_PistolAmmo, itemEntities, ArrayCount(itemEntities));

            if (itemCount > 0)
            {
                // Copied from InteractSystem
                vec3 playerPosition;
                ecs_GetComponent(world, PositionComponentId, &player, 1,
                    &playerPosition, sizeof(playerPosition));

                quat playerRotation;
                ecs_GetComponent(world, RotationComponentId, &player, 1,
                    &playerRotation, sizeof(playerRotation));

                // Calculate view direction vector
                vec3 direction =
                    Normalize(Rotate(playerRotation, Vec3(0, 0, -1)));

                f32 initialOffset = 0.1f;
                vec3 spawnPoint = playerPosition + PLAYER_CAMERA_OFFSET +
                                  direction * initialOffset;
                EntityId bullet = SpawnEntityAtPosition(
                    world, EntityPrefab_Bullet, spawnPoint);

                f32 bulletAcceleration = 64000.0f;
                vec3 bulletVelocity = direction * bulletAcceleration * dt;
                ecs_SetComponent(world, VelocityComponentId, &bullet, 1,
                    &bulletVelocity, sizeof(bulletVelocity));

                ecs_SetComponent(world, ProjectileOwnerComponentId, &bullet, 1,
                    &player, sizeof(player));

                // Generate gun shot event
                Assert(eventQueue->length < ArrayCount(eventQueue->events));
                GameEvent *event = eventQueue->events + eventQueue->length++;
                event->type = GameEvent_GunShot;
                event->gunShot.owner = player;
                event->gunShot.position = spawnPoint;

                DecrementQuantityOfItemInInventory(world, itemEntities, itemCount, 1);
            }
        }
        else if (itemId == ItemID_Hatchet)
        {
            // Copied from InteractSystem
            vec3 playerPosition;
            ecs_GetComponent(world, PositionComponentId, &player, 1,
                    &playerPosition, sizeof(playerPosition));

            quat playerRotation;
            ecs_GetComponent(world, RotationComponentId, &player, 1,
                    &playerRotation, sizeof(playerRotation));

            // Calculate view direction vector
            vec3 direction =
                Normalize(Rotate(playerRotation, Vec3(0, 0, -1)));

            f32 initialOffset = 0.1f;
            vec3 start = playerPosition + PLAYER_CAMERA_OFFSET +
                         direction * initialOffset;

            f32 range = 1.5f;
            vec3 end = start + direction * range;

            EntityRaycastResult sweepResult =
                SphereSweepWorld(world, start, end, 0.5f, &player, 1);

            if (sweepResult.entity != NULL_ENTITY)
            {
                if (ecs_HasComponent(world, HealthComponentId, sweepResult.entity))
                {
                    f32 damage = 15.0f;

                    // Generate damage received event
                    Assert(eventQueue->length < ArrayCount(eventQueue->events));
                    GameEvent *event = eventQueue->events + eventQueue->length++;
                    event->type = GameEvent_ReceivedDamage;
                    event->receivedDamage.entity = sweepResult.entity;
                    event->receivedDamage.amount = damage;
                }

                Assert(eventQueue->length < ArrayCount(eventQueue->events));
                GameEvent *event = eventQueue->events + eventQueue->length++;
                event->type = GameEvent_BulletImpact;
                event->bulletImpact.position = sweepResult.raycastResult.hitPoint;
                event->bulletImpact.normal = sweepResult.raycastResult.hitNormal;
            }
        }
    }
}

// NOTE: Only called on server
internal EntityId SpawnPlayer(ecs_EntityWorld *world)
{
    u32 join[] = {PlayerSpawnPointComponentId, PositionComponentId};

    EntityId entities[MAX_ENTITIES];
    u32 count =
        ecs_Join(world, join, ArrayCount(join), entities, ArrayCount(entities));

    vec3 positions[MAX_ENTITIES];
    ecs_GetComponent(world, PositionComponentId, entities, count, positions,
        sizeof(positions[0]));

    EntityId entityId =
        SpawnEntityAtPosition(world, EntityPrefab_Player, positions[0]);

    AddItemToInventory(world, entityId, ItemID_Pistol);
    AddItemToInventory(world, entityId, ItemID_Hatchet);
    AddItemToInventory(world, entityId, ItemID_StorageBox);
    //AddItemToInventory(world, entityId, ItemID_Foundation);
    //AddItemToInventory(world, entityId, ItemID_Wall);
    //AddItemToInventory(world, entityId, ItemID_Doorway);
    //AddItemToInventory(world, entityId, ItemID_Ceiling);
    AddItemToInventory(world, entityId, ItemID_PistolAmmo, 64);

    return entityId;
}

internal void GroundCheckSystem(ecs_EntityWorld *world, EntityId *entities,
    vec3 *positions, b32 *onGround, u32 count)
{
    f32 playerHeight = 1.8f;
    for (u32 i = 0; i < count; ++i)
    {
        vec3 currentPosition = positions[i];
        EntityRaycastResult groundTrace = RaycastWorld(world, currentPosition,
            currentPosition + Vec3(0, -playerHeight * 0.5f, 0), entities + i,
            1);

        b32 isOnGround = groundTrace.entity != NULL_ENTITY;

        currentPosition.y =
            isOnGround ? groundTrace.raycastResult.hitPoint.y + playerHeight * 0.5f
                       : currentPosition.y;

        onGround[i] = isOnGround;
        positions[i] = currentPosition;
    }
}

internal void GravitySystem(b32 *onGround, vec3 *velocities, u32 count, f32 dt)
{
    for (u32 i = 0; i < count; ++i)
    {
        vec3 gravity = onGround[i] ? Vec3(0, 0, 0) : Vec3(0, -10, 0);
        velocities[i] += gravity * dt;
    }
}

internal void FrictionSystem(b32 *onGround, vec3 *velocities, u32 count, f32 dt)
{
    f32 groundFriction = 9.0f;
    f32 airFriction = 0.5f;
    for (u32 i = 0; i < count; ++i)
    {
        f32 friction = onGround[i] ? groundFriction : airFriction;
        velocities[i] -= velocities[i] * friction * dt;
    }
}


internal void VelocityIntegrationSystem(vec3 *positions, vec3 *velocities, u32 count, f32 dt)
{
    for (u32 i = 0; i < count; ++i)
    {
        positions[i] += velocities[i] * dt;
    }
}

internal void GroundCheckSystem(ecs_EntityWorld *world)
{
    u32 join[] = {OnGroundComponentId, PositionComponentId};

    EntityId entities[MAX_ENTITIES];
    u32 count =
        ecs_Join(world, join, ArrayCount(join), entities, ArrayCount(entities));

    b32 onGround[MAX_ENTITIES];
    ecs_GetComponent(world, OnGroundComponentId, entities, count, onGround,
        sizeof(onGround[0]));

    vec3 positions[MAX_ENTITIES];
    ecs_GetComponent(world, PositionComponentId, entities, count, positions,
        sizeof(positions[0]));

    GroundCheckSystem(world, entities, positions, onGround, count);

    ecs_SetComponent(world, OnGroundComponentId, entities, count, onGround,
        sizeof(onGround[0]));
    ecs_SetComponent(world, PositionComponentId, entities, count, positions,
        sizeof(positions[0]));
}

internal void GravitySystem(ecs_EntityWorld *world, f32 dt)
{
    u32 join[] = {OnGroundComponentId, VelocityComponentId};

    EntityId entities[MAX_ENTITIES];
    u32 count =
        ecs_Join(world, join, ArrayCount(join), entities, ArrayCount(entities));

    b32 onGround[MAX_ENTITIES];
    ecs_GetComponent(world, OnGroundComponentId, entities, count, onGround,
        sizeof(onGround[0]));

    vec3 velocities[MAX_ENTITIES];
    ecs_GetComponent(world, VelocityComponentId, entities, count, velocities,
        sizeof(velocities[0]));

    GravitySystem(onGround, velocities, count, dt);

    ecs_SetComponent(world, VelocityComponentId, entities, count, velocities,
        sizeof(velocities[0]));
}

internal void FrictionSystem(ecs_EntityWorld *world, f32 dt)
{
    u32 join[] = {OnGroundComponentId, VelocityComponentId};

    EntityId entities[MAX_ENTITIES];
    u32 count =
        ecs_Join(world, join, ArrayCount(join), entities, ArrayCount(entities));

    b32 onGround[MAX_ENTITIES];
    ecs_GetComponent(world, OnGroundComponentId, entities, count, onGround,
        sizeof(onGround[0]));

    vec3 velocities[MAX_ENTITIES];
    ecs_GetComponent(world, VelocityComponentId, entities, count, velocities,
        sizeof(velocities[0]));

    FrictionSystem(onGround, velocities, count, dt);

    ecs_SetComponent(world, VelocityComponentId, entities, count, velocities,
        sizeof(velocities[0]));
}

internal void VelocityIntegrationSystem(ecs_EntityWorld *world, f32 dt)
{
    u32 join[] = {OnGroundComponentId, VelocityComponentId, PositionComponentId};

    EntityId entities[MAX_ENTITIES];
    u32 count =
        ecs_Join(world, join, ArrayCount(join), entities, ArrayCount(entities));

    vec3 positions[MAX_ENTITIES];
    ecs_GetComponent(world, PositionComponentId, entities, count, positions,
        sizeof(positions[0]));

    vec3 velocities[MAX_ENTITIES];
    ecs_GetComponent(world, VelocityComponentId, entities, count, velocities,
        sizeof(velocities[0]));

    for (u32 i = 0; i < count; ++i)
    {
        vec3 targetVelocity = velocities[i];
        MoveAndCollideEntityResult result = MoveAndCollideEntity(
            world, positions[i], targetVelocity, entities[i], dt);

        positions[i] = result.position;
        velocities[i] = result.velocity;
    }

    ecs_SetComponent(world, PositionComponentId, entities, count, positions,
        sizeof(positions[0]));
    ecs_SetComponent(world, VelocityComponentId, entities, count, velocities,
        sizeof(velocities[0]));
}

// FIXME: Don't use inventory component for choosing a target
internal b32 FindTargetSystem(ecs_EntityWorld *world, EntityId ignore, EntityId *target)
{
    b32 result = false;
    u32 components[] = {InventoryComponentId, PositionComponentId};

    EntityId entities[MAX_ENTITIES];
    u32 count = ecs_Join(world, components, ArrayCount(components), entities,
        ArrayCount(entities));

    for (u32 i = 0 ; i < count; ++i)
    {
        if (entities[i] != ignore)
        {
            *target = entities[i];
            result = true;
            break;
        }
    }

    return result;
}

// FIXME: Set rotation!
internal void AiMoveTowardsSystem(ecs_EntityWorld *world, f32 dt)
{
    u32 components[] = {AiBrainComponentId, PositionComponentId, VelocityComponentId};

    EntityId entities[MAX_ENTITIES];
    u32 count = ecs_Join(world, components, ArrayCount(components), entities,
        ArrayCount(entities));

    vec3 positions[MAX_ENTITIES];
    vec3 velocities[MAX_ENTITIES];

    ecs_GetComponent(world, PositionComponentId, entities, count,
            positions, sizeof(positions[0]));
    ecs_GetComponent(world, VelocityComponentId, entities, count,
            velocities, sizeof(velocities[0]));

    for (u32 entityIdx = 0; entityIdx < count; ++entityIdx)
    {
        // Find first entity with a health component that isn't ourselves
        EntityId target = NULL_ENTITY;
        if (FindTargetSystem(world, entities[entityIdx], &target))
        {
            // Get its position and calculate a direction vector by subtracting
            vec3 targetPosition;
            ecs_GetComponent(world, PositionComponentId, &target, 1,
                &targetPosition, sizeof(targetPosition));

            // zero y component of direction vector Multiply normalized
            vec3 delta = targetPosition - positions[entityIdx];
            delta.y = 0.0f;
            f32 distance = Length(delta);
            vec3 acceleration = SafeNormalize(delta);

            vec3 velocity = velocities[entityIdx];
            velocity.y = 0.0f;

            f32 maxSpeed = 150.0f;

            // multiply direction vector by desired movement speed
            f32 movementSpeed = 50.0f;
            velocity += acceleration * movementSpeed * dt;

            if (Length(velocity) > maxSpeed)
            {
                velocity = Normalize(velocity) * maxSpeed;
            }

            // Update velocity
            velocities[entityIdx].x = velocity.x;
            velocities[entityIdx].z = velocity.z;
        }
        ecs_SetComponent(world, AiTargetComponentId, entities + entityIdx, 1,
            &target, sizeof(target));
    }

    ecs_SetComponent(world, VelocityComponentId, entities, count, velocities,
        sizeof(velocities[0]));
}

internal void AiAttackSystem(
    ecs_EntityWorld *world, GameEventQueue *eventQueue, f32 dt)
{
    u32 components[] = {AiBrainComponentId, AiTargetComponentId};

    EntityId entities[MAX_ENTITIES];
    u32 entityCount = ecs_Join(world, components, ArrayCount(components),
        entities, ArrayCount(entities));

    EntityId targets[MAX_ENTITIES];
    ecs_GetComponent(world, AiTargetComponentId, entities, entityCount, targets,
        sizeof(targets[0]));

    vec3 positions[MAX_ENTITIES];
    ecs_GetComponent(world, PositionComponentId, entities, entityCount,
        positions, sizeof(positions[0]));

    vec3 velocities[MAX_ENTITIES];
    ecs_GetComponent(world, VelocityComponentId, entities, entityCount,
        velocities, sizeof(velocities[0]));

    f32 timeUntilNextAttack[MAX_ENTITIES];
    ecs_GetComponent(world, TimeUntilNextAttackComponentId, entities,
        entityCount, timeUntilNextAttack, sizeof(timeUntilNextAttack[0]));

    f32 attackAngle = 0.3f;
    f32 attackRange = 1.2f;
    f32 timeBetweenAttacks = 3.2f;
    for (u32 entityIdx = 0; entityIdx < entityCount; ++entityIdx)
    {
        if (timeUntilNextAttack[entityIdx] <= 0.0f)
        {
            if (targets[entityIdx] != NULL_ENTITY)
            {
                EntityId target = targets[entityIdx];

                vec3 targetPosition;
                ecs_GetComponent(world, PositionComponentId, &target, 1,
                    &targetPosition, sizeof(targetPosition));

                vec3 delta = targetPosition - positions[entityIdx];
                f32 distanceSq = LengthSq(delta);
                if (distanceSq < attackRange * attackRange)
                {
                    // FIXME: Use rotation instead of velocity so that the AI
                    // doesn't have to be moving!
                    vec3 forward = velocities[entityIdx];
                    forward = LengthSq(forward) > EPSILON ? Normalize(forward)
                                                          : Vec3(0, 0, -1);
                    // FIXME: Need delay between attacks

                    // Check facing direction
                    f32 cosine = Dot(forward, SafeNormalize(delta));
                    if (cosine > attackAngle)
                    {
                        // TODO: Play animation and add delay
                        f32 initialOffset = 0.1f;
                        vec3 start = positions[entityIdx] +
                                     PLAYER_CAMERA_OFFSET +
                                     forward * initialOffset;

                        vec3 end = start + forward * attackRange;

                        // Ignore all other AI entities
                        EntityRaycastResult sweepResult = SphereSweepWorld(
                            world, start, end, 0.5f, entities, entityCount);

                        if (sweepResult.entity != NULL_ENTITY)
                        {
                            if (ecs_HasComponent(world, HealthComponentId,
                                    sweepResult.entity))
                            {
                                f32 damage = 15.0f;

                                // Generate damage received event
                                Assert(eventQueue->length <
                                       ArrayCount(eventQueue->events));
                                GameEvent *event =
                                    eventQueue->events + eventQueue->length++;
                                event->type = GameEvent_ReceivedDamage;
                                event->receivedDamage.entity =
                                    sweepResult.entity;
                                event->receivedDamage.amount = damage;
                            }

                            Assert(eventQueue->length <
                                   ArrayCount(eventQueue->events));
                            GameEvent *event =
                                eventQueue->events + eventQueue->length++;
                            event->type = GameEvent_BulletImpact;
                            event->bulletImpact.position =
                                sweepResult.raycastResult.hitPoint;
                            event->bulletImpact.normal =
                                sweepResult.raycastResult.hitNormal;
                        }

                        timeUntilNextAttack[entityIdx] = timeBetweenAttacks;
                    }
                }
            }
        }
        else
        {
            timeUntilNextAttack[entityIdx] =
                Max(0.0f, timeUntilNextAttack[entityIdx] - dt);
        }
    }
    ecs_SetComponent(world, TimeUntilNextAttackComponentId, entities,
        entityCount, timeUntilNextAttack, sizeof(timeUntilNextAttack[0]));
}

internal void SpawnEnemiesSystem(ecs_EntityWorld *world)
{
    u32 join[] = {EnemySpawnPointComponentId, PositionComponentId};

    EntityId entities[MAX_ENTITIES];
    u32 count =
        ecs_Join(world, join, ArrayCount(join), entities, ArrayCount(entities));

    vec3 positions[MAX_ENTITIES];
    ecs_GetComponent(world, PositionComponentId, entities, count, positions,
        sizeof(positions[0]));

    for (u32 i = 0; i < count; ++i)
    {
        SpawnEntityAtPosition(world, EntityPrefab_Enemy, positions[i]);
    }
}

// TODO: Use generic gravity and friction systems
internal void UpdateBulletSystem(ecs_EntityWorld *world, f32 dt,
    GameEventQueue *eventQueue, GameAssets *assets,
    DebugDrawingSystem *debugDrawingSystem)
{
    u32 join[] = {BulletComponentId, ProjectileOwnerComponentId,
        VelocityComponentId, PositionComponentId};

    EntityId entities[MAX_ENTITIES];
    u32 count =
        ecs_Join(world, join, ArrayCount(join), entities, ArrayCount(entities));

    vec3 positions[MAX_ENTITIES];
    ecs_GetComponent(world, PositionComponentId, entities, count, positions,
        sizeof(positions[0]));

    vec3 velocities[MAX_ENTITIES];
    ecs_GetComponent(world, VelocityComponentId, entities, count, velocities,
        sizeof(velocities[0]));

    EntityId owners[MAX_ENTITIES];
    ecs_GetComponent(world, ProjectileOwnerComponentId, entities, count, owners,
        sizeof(owners[0]));

    // Gravity system
    vec3 gravity = Vec3(0, -10, 0);
    for (u32 i = 0; i < count; ++i)
    {
        velocities[i] += gravity * dt;
    }

    // Friction system
    f32 friction = 0.5f;
    for (u32 i = 0; i < count; ++i)
    {
        velocities[i] -= velocities[i] * friction * dt;
    }

    vec3 originalPositions[MAX_ENTITIES];
    CopyMemory(originalPositions, positions, sizeof(vec3) * count);
    VelocityIntegrationSystem(positions, velocities, count, dt);

    EntityId entitiesToRemove[MAX_ENTITIES];
    u32 removeCount = 0;

    // Raycast points system
    for (u32 i = 0; i < count; ++i)
    {
        vec3 start = originalPositions[i];
        vec3 end = positions[i];

        // TODO: Would love a MultiRaycastWorld function
        EntityRaycastResult raycastResult =
            RaycastWorld(world, start, end, owners + i, 1);
        if (raycastResult.entity != NULL_ENTITY)
        {
            velocities[i] = Vec3(0);
            positions[i] = raycastResult.raycastResult.hitPoint;
            end = raycastResult.raycastResult.hitPoint;
            entitiesToRemove[removeCount++] = entities[i];

            if (ecs_HasComponent(world, HealthComponentId, raycastResult.entity))
            {
                f32 damage = 15.0f;

                // Generate damage received event
                Assert(eventQueue->length < ArrayCount(eventQueue->events));
                GameEvent *event = eventQueue->events + eventQueue->length++;
                event->type = GameEvent_ReceivedDamage;
                event->receivedDamage.entity = raycastResult.entity;
                event->receivedDamage.amount = damage;
            }

            Assert(eventQueue->length < ArrayCount(eventQueue->events));
            GameEvent *event = eventQueue->events + eventQueue->length++;
            event->type = GameEvent_BulletImpact;
            event->bulletImpact.position = raycastResult.raycastResult.hitPoint;
            event->bulletImpact.normal = raycastResult.raycastResult.hitNormal;
        }

        DrawLine(debugDrawingSystem, start, end, Vec3(0, 1, 0), 20.0f);
    }

    ecs_SetComponent(world, VelocityComponentId, entities, count, velocities,
        sizeof(velocities[0]));
    ecs_SetComponent(world, PositionComponentId, entities, count, positions,
        sizeof(positions[0]));
    ecs_QueueEntitiesForDeletion(world, entitiesToRemove, removeCount);
}

internal void DrawViewModel(ecs_EntityWorld *world, GameMemory *memory,
    mat4 viewProjection, mat4 shadowViewProjection, vec3 cameraPosition,
    Material *materials, u32 materialCount)
{
    EntityId playerEntity;
    if (GetFirstEntity(world, ActivePlayerComponentId, &playerEntity))
    {
        vec3 playerPosition;
        ecs_GetComponent(world, PositionComponentId, &playerEntity, 1,
            &playerPosition, sizeof(playerPosition));
        vec3 eulerAngles;
        ecs_GetComponent(world, EulerAnglesComponentId, &playerEntity, 1,
            &eulerAngles, sizeof(eulerAngles));

        u32 itemId = GetActiveItemId(world, playerEntity);

        playerPosition += PLAYER_CAMERA_OFFSET;

        mat4 localToWorldMatrix = Identity();
        b32 drawViewModel = false;
        u32 mesh = 0;
        u32 materialId = 0;
        if (itemId == ItemID_Pistol)
        {
            mat4 parentTransform = Translate(playerPosition) *
                                   RotateY(eulerAngles.y) *
                                   RotateX(eulerAngles.x);
            mat4 pistolTransform = Translate(Vec3(0.07, -0.04, -0.15)) *
                                   RotateY(PI) * Scale(Vec3(1.25f));

            localToWorldMatrix = parentTransform * pistolTransform;

            mesh = Mesh_Pistol;
            materialId = Material_StorageBox;

            drawViewModel = true;
        }
        else if (itemId == ItemID_Hatchet)
        {
            mat4 parentTransform = Translate(playerPosition) *
                                   RotateY(eulerAngles.y) *
                                   RotateX(eulerAngles.x);
            mat4 hatchetTransform = Translate(Vec3(0.07, -0.05, -0.15)) *
                                    RotateY(0.5f * PI) * RotateX(0.05f * PI) * Scale(Vec3(0.012f));

            localToWorldMatrix = parentTransform * hatchetTransform;

            mesh = Mesh_Hatchet;
            materialId = Material_Hatchet;

            drawViewModel = true;
        }

        if (drawViewModel)
        {
            // TODO: Make this an entity instead
            RenderCommand_DrawMesh *drawMesh =
                AllocateRenderCommand(memory, RenderCommand_DrawMesh);
            drawMesh->mesh = mesh;

            Material material = materials[materialId];
            drawMesh->shader = material.shader;

            AddShaderUniformValueMat4(drawMesh, UniformValue_MvpMatrix,
                viewProjection * localToWorldMatrix);
            AddShaderUniformValueRenderTarget(
                drawMesh, UniformValue_ShadowMap, RenderTarget_ShadowBuffer);
            AddShaderUniformValueMat4(drawMesh,
                UniformValue_LightViewProjection, shadowViewProjection);
            AddShaderUniformValueMat4(
                drawMesh, UniformValue_ModelMatrix, localToWorldMatrix);
            AddShaderUniformValueBuffer(drawMesh, UniformValue_LightingData,
                UniformBuffer_LightingData);
            AddShaderUniformValueVec3(
                drawMesh, UniformValue_CameraPosition, cameraPosition);

            if (material.useTexture)
            {
                AddShaderUniformValueTexture(
                    drawMesh, UniformValue_AlbedoTexture, material.texture);
            }
            else
            {
                AddShaderUniformValueVec4(
                    drawMesh, UniformValue_Color, material.color);
            }
        }
    }
}
