#include <string.h> // for strerror

#ifdef PLATFORM_WINDOWS
#include <Winsock2.h>
#include <Mswsock.h>
#endif

#ifdef PLATFORM_LINUX
#define SOCKET int
#define INVALID_SOCKET -1
#define closesocket close
#endif
global SOCKET g_ServerSocket;
global SOCKET g_ClientSocket;
global struct sockaddr_in g_ServerAddress;

#define MAX_CLIENTS 64
global sockaddr_storage g_ClientAddresses[MAX_CLIENTS];
global u64 g_LastReceivedTimestamps[MAX_CLIENTS];
global u32 g_ClientCount = 1; // Treat 0 as NULL value

internal b32 Sock_ConfigureSocket(SOCKET socket)
{
#ifdef PLATFORM_WINDOWS
    u_long blocking = 0;
    if (ioctlsocket(socket, FIONBIO, &blocking) != 0)
    {
        LogMessage("ioctlsocket failed: %d", GetLastError());
        return false;
    }
    u_long udpConnReset = FALSE;
    if (ioctlsocket(socket, SIO_UDP_CONNRESET, &udpConnReset) != 0)
    {
        LogMessage("ioctlsocket failed: %d", WSAGetLastError());
        return false;
    }
#endif

    return true;
}

internal b32 Sock_InitServer()
{
    g_ServerSocket = socket(AF_INET, SOCK_DGRAM, 0);
    if (g_ServerSocket == INVALID_SOCKET)
    {
        // TODO: WSAGetLastError
        LogMessage("Failed to create server socket");
        return false;
    }

    if (!Sock_ConfigureSocket(g_ServerSocket))
    {
        return false;
    }

    struct sockaddr_in address = {};
    address.sin_family = AF_INET;
    address.sin_port = htons(18000);
    address.sin_addr.s_addr = htonl(INADDR_ANY);

    if (bind(g_ServerSocket, (struct sockaddr *)&address, sizeof(address)) != 0)
    {
        closesocket(g_ServerSocket);
        LogMessage("Failed to bind server socket");
        return false;
    }
    return true;
}

internal b32 Sock_InitClient(const char *address)
{
    g_ServerAddress.sin_family = AF_INET;
    g_ServerAddress.sin_port = htons(18000);
    inet_pton(AF_INET, address, &g_ServerAddress.sin_addr);

    g_ClientSocket = socket(AF_INET, SOCK_DGRAM, 0);
    if (g_ClientSocket == -1)
    {
        // TODO: WSAGetLastError
        LogMessage("Failed to create client socket");
        return false;
    }

    if (!Sock_ConfigureSocket(g_ClientSocket))
    {
        return false;
    }

    return true;
}

inline bool CompareAddresses(
    struct sockaddr_storage *a, struct sockaddr_storage *b)
{
    if (a->ss_family == b->ss_family)
    {
        if (a->ss_family == AF_INET)
        {
            auto a4 = (sockaddr_in *)a;
            auto b4 = (sockaddr_in *)b;
            return ((a4->sin_addr.s_addr == b4->sin_addr.s_addr) &&
                    (a4->sin_port == b4->sin_port));
        }
        else if (a->ss_family == AF_INET6)
        {
            auto a6 = (sockaddr_in6 *)a;
            auto b6 = (sockaddr_in6 *)b;
            auto addr1 = (uint32_t *)a6->sin6_addr.s6_addr;
            auto addr2 = (uint32_t *)b6->sin6_addr.s6_addr;
            return ((addr1[0] == addr2[0]) && (addr1[1] == addr2[1]) &&
                    (addr1[2] == addr2[2]) && (addr1[3] == addr2[3]) &&
                    (a6->sin6_port == b6->sin6_port));
        }
        else
        {
            LogMessage("Unknown address family");
        }
    }
    return false;
}

inline u32 FindClient(sockaddr_storage *receiveAddress)
{
    u32 idx = MAX_CLIENTS;
    for (u32 i = 1; i < g_ClientCount; ++i)
    {
        if (CompareAddresses(g_ClientAddresses + i, receiveAddress))
        {
            idx = i;
            break;
        }
    }

    return idx;
}

inline u32 ReceivePacket(SOCKET socket, void *data, u32 length,
    struct sockaddr *receiveAddress, u32 *receiveAddressLength)
{
    u32 bytesReceived = 0;
#ifdef PLATFORM_WINDOWS
    int dataReceived = recvfrom(socket, (char *)data, length, 0, receiveAddress,
        (i32 *)receiveAddressLength);
    if (dataReceived < 0)
    {
        int error = WSAGetLastError();
        LogMessage("Connection lost: %d", error);
        dataReceived = 0;
    }
    Assert(dataReceived >= 0);
    bytesReceived = (u32)dataReceived;
#elif PLATFORM_LINUX
    bytesReceived =
        recvfrom(socket, data, length, 0, receiveAddress, receiveAddressLength);
#endif
    return bytesReceived;
}

internal i32 Sock_NonBlockingReceive(i32 socket, void *data, u32 length,
    struct sockaddr *receiveAddress, u32 *receiveAddressLength)
{
    fd_set fds;
    struct timeval tv = {};
    FD_ZERO(&fds);
    FD_SET(socket, &fds);

    i32 ret = select(socket + 1, &fds, NULL, NULL, &tv);
    if (ret == -1)
    {
        LogMessage("select error: %s", strerror(errno));
        return -1;
    }
    else if (ret == 0)
    {
        return 0; // No data available
    }
    else
    {
        i32 dataReceived = ReceivePacket(
            socket, data, length, receiveAddress, receiveAddressLength);
        return dataReceived;
    }
}

internal u32 Sock_ServerReceivePackets(GamePacket *packets, u32 maxPackets)
{
    u32 packetCount = 0;
    for (u32 i = 0; i < maxPackets; ++i)
    {
        struct sockaddr_storage receiveAddress;
        // NOTE: On Win32 receiveAddressLength must be i32 but on Linux this
        // should be socklen_t
        u32 receiveAddressLength = sizeof(receiveAddress);
        char data[sizeof(GamePacket)];
        i32 length =
            Sock_NonBlockingReceive((i32)g_ServerSocket, data, sizeof(data),
                (struct sockaddr *)&receiveAddress, &receiveAddressLength);

        if (length > 0)
        {
            // Lookup into address cache
            u32 clientId = FindClient(&receiveAddress);
            if (clientId == MAX_CLIENTS)
            {
                // Store if not present
                Assert(g_ClientCount < ArrayCount(g_ClientAddresses));
                clientId = g_ClientCount++;
                g_ClientAddresses[clientId] = receiveAddress;
            }
            Assert((u32)length > sizeof(GamePacketHeader));
            CopyMemory(&packets[i].header, data, sizeof(GamePacketHeader));
            g_LastReceivedTimestamps[clientId] = packets[i].header.timestamp;

            length -= sizeof(GamePacketHeader);
            Assert((u32)length < sizeof(packets[i].data));
            CopyMemory(
                packets[i].data, data + sizeof(GamePacketHeader), length);
            packets[i].length = length;
            packets[i].clientId = clientId;
            packetCount++;
        }
        else if (length == 0)
        {
            // No data available
            break;
        }
        else // length == -1
        {
            if (errno == EAGAIN)
            {
                // FIXME: I don't believe this happens on windows as we use
                // select No more packets available
                break;
            }
            else
            {
                LogMessage("Server recvfrom encountered unknown error \"%s\".",
                    strerror(errno));
                break;
            }
        }
    }

    // LogMessage("Server received %u packets", packetCount);

    return packetCount;
}

internal void Sock_SendPacketsToClients(GamePacket *packets, u32 packetCount)
{
    for (u32 i = 0; i < packetCount; ++i)
    {
        Assert(packets[i].clientId != 0);
        u32 clientId = packets[i].clientId;

        GamePacketHeader header = {};
        header.timestamp = Platform_GetMilliseconds();
        header.lastReceivedTimestamp = g_LastReceivedTimestamps[clientId];

        char sendBuffer[sizeof(GamePacketHeader) + sizeof(packets[i].data)];
        u32 length = sizeof(GamePacketHeader) + packets[i].length;
        CopyMemory(sendBuffer, &header, sizeof(header));
        CopyMemory(
            sendBuffer + sizeof(header), packets[i].data, packets[i].length);

        // NOTE: On Win32 packetData must be a const char *
        int ret = sendto(g_ServerSocket, sendBuffer, length, 0,
            (struct sockaddr *)&g_ClientAddresses[clientId],
            sizeof(g_ClientAddresses[clientId]));
        if (ret == -1)
        {
            LogMessage("Sock_SendPacketToClient: %s", strerror(errno));
        }
        else
        {
            Assert(ret == (i32)length);
        }
    }
    //LogMessage("Sent %u packets to clients", packetCount);
}

// TODO: Suppport receiving multiple packets
internal u32 Sock_ClientReceivePackets(GamePacket *packets, u32 maxPackets)
{
    u32 count = 0;
    for (u32 i = 0; i < maxPackets; ++i)
    {
        struct sockaddr_storage receiveAddress;
        u32 receiveAddressLength = sizeof(receiveAddress);
        char data[sizeof(GamePacket)];
        i32 length =
            Sock_NonBlockingReceive((i32)g_ClientSocket, data, sizeof(data),
                    (struct sockaddr *)&receiveAddress, &receiveAddressLength);

        // TODO: Check that receive address matches server address

        if (length > 0)
        {
            Assert((u32)length > sizeof(GamePacketHeader));
            CopyMemory(&packets[i].header, data, sizeof(GamePacketHeader));
            length -= sizeof(GamePacketHeader);
            CopyMemory(packets[i].data, data + sizeof(GamePacketHeader), length);
            packets[i].length = length;
            count++;
        }
    }

    return count;
}

internal void Sock_SendPacketToServer(
    GamePacket *packet, u64 lastReceivedTimestamp)
{
    GamePacketHeader header = {};
    header.timestamp = Platform_GetMilliseconds();
    header.lastReceivedTimestamp = lastReceivedTimestamp;

    char sendBuffer[sizeof(GamePacketHeader) + sizeof(packet->data)];
    u32 length = sizeof(GamePacketHeader) + packet->length;
    CopyMemory(sendBuffer, &header, sizeof(header));
    CopyMemory(sendBuffer + sizeof(header), packet->data, packet->length);

    // NOTE: On Win32 packetData must be a const char *
    int ret = sendto(g_ClientSocket, sendBuffer, length, 0,
        (struct sockaddr *)&g_ServerAddress, sizeof(g_ServerAddress));
    if (ret == -1)
    {
        LogMessage("Sock_SendPacketToServer: %s", strerror(errno));
    }
    else
    {
        Assert(ret == (i32)length);
    }
}
