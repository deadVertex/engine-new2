struct MeshAssetDefinitions
{
    const char *path;
    u32 meshId;
    u32 triangleMeshId;
    u32 maxInstances;
};

internal void LoadMeshes(MeshAssetDefinitions *definitions, u32 count,
    GameAssets *assets, MemoryArena *tempArena, GameMemory *memory,
    b32 isClient)
{
    for (u32 i = 0; i < count; ++i)
    {
        MeshAssetDefinitions *definition = definitions + i;

        DebugReadFileResult fileResult =
            memory->readEntireFile(definition->path);
        if (fileResult.contents != NULL)
        {
            // -1 to remove nul character
            MeshData meshData = LoadMeshData((char *)fileResult.contents,
                fileResult.size - 1, tempArena, tempArena);
            memory->freeFileMemory(fileResult);

            if (isClient)
            {
                CreateMesh(definition->meshId, tempArena, meshData.vertices,
                    meshData.vertexCount, meshData.indices, meshData.indexCount,
                    definition->maxInstances, memory);
            }

            TriangleMesh triangleMesh =
                CreateTriangleMesh(meshData, &assets->meshDataArena);
            assets->triangleMeshes[definition->triangleMeshId] = triangleMesh;
            assets->triangleMeshBoundingBoxes[definition->triangleMeshId] =
                CalculateTriangleMeshBoundingBox(triangleMesh);
        }
    }
}

internal void LoadAudioClipFromFile(
    const char *path, GameMemory *memory, GameAssets *assets, u32 audioClipId)
{
    DebugReadFileResult fileResult = memory->readEntireFile(path);
    Assert(fileResult.contents != NULL);
    AudioData audioData = LoadAudioData(
            fileResult.contents, fileResult.size, &assets->audioDataArena);
    memory->freeFileMemory(fileResult);
    assets->audioClips[audioClipId] = audioData;
}


internal void LoadSlowContent(GameAssets *assets, GameMemory *memory,
    b32 isClient, MemoryArena *tempArena)
{
    TIMED_BLOCK();

    {
        MeshData meshData = GetCubeMeshData(tempArena);
        TriangleMesh triangleMesh =
            CreateTriangleMesh(meshData, &assets->meshDataArena);
        assets->triangleMeshes[TriangleMesh_Cube] = triangleMesh;
        assets->triangleMeshBoundingBoxes[TriangleMesh_Cube] =
            CalculateTriangleMeshBoundingBox(triangleMesh);
    }

#if 0
    if (isClient)
    {
        {
            DebugReadFileResult fileResult =
                memory->readEntireFile("../content/xbot.fbx");
            if (fileResult.contents != NULL)
            {
                assets->xbotModel = ImportFbx((const char *)fileResult.contents,
                    fileResult.size, &assets->modelDataArena, tempArena, 0.01f);

                CreateMesh(&assets->xbotModel, memory, tempArena, Mesh_Xbot01);

                assets->xbotModel.nodes[0].scale *= 0.01f;

                CreateSkeleton(&assets->skeleton, &assets->xbotModel,
                    &assets->modelDataArena);
            }
        }

        {
            DebugReadFileResult fileResult =
                memory->readEntireFile("../content/running.fbx");
            if (fileResult.contents != NULL)
            {
                AnimationImportData data =
                    ImportAnimationData(fileResult.contents, fileResult.size,
                        &assets->modelDataArena, 0.01f);

                CreateAnimationClip(&assets->idleAnimation, &data,
                    &assets->modelDataArena, &assets->skeleton, true);
            }
        }
    }
#endif

    MeshAssetDefinitions meshDefinitions[] = {
        { "../content/rock01.obj", Mesh_Rock01, TriangleMesh_Rock01, MAX_ENTITIES},
        { "../content/rock02.obj", Mesh_Rock02, TriangleMesh_Rock02, MAX_ENTITIES},
        { "../content/rock03.obj", Mesh_Rock03, TriangleMesh_Rock03, MAX_ENTITIES},
        { "../content/rock04.obj", Mesh_Rock04, TriangleMesh_Rock04, MAX_ENTITIES},
        { "../content/building_parts/foundation.obj", Mesh_Foundation, TriangleMesh_Foundation, 0},
        { "../content/building_parts/wall.obj", Mesh_Wall, TriangleMesh_Wall, 0},
        { "../content/building_parts/doorway.obj", Mesh_Doorway, TriangleMesh_Doorway, 0},
    };

    LoadMeshes(meshDefinitions, ArrayCount(meshDefinitions), assets, tempArena,
        memory, isClient);

    //assets->heightMap =
        //GenerateHeightMap(&assets->terrainArena, TERRAIN_HEIGHTMAP_SIZE);
    //GenerateTerrain(&assets->heightMap, &assets->layerMap, &assets->normalMap,
        //&assets->terrainArena, tempArena);

    if (isClient)
    {
        //GenerateTerrainTextures(assets->heightMap, assets->layerMap,
            //assets->normalMap, memory, tempArena);

        LoadMeshFromFile("../content/pistol.obj", memory, Mesh_Pistol, tempArena);
        LoadMeshFromFile("../content/hatchet.obj", memory, Mesh_Hatchet, tempArena);
        LoadMeshFromFile("../content/monkey.obj", memory, Mesh_Monkey, tempArena);

        LoadTextureFromFile("../content/dev_grid512.png", memory, Texture_DevGrid512, tempArena);
        LoadTextureFromFile("../content/hatchet_Base_Color.png", memory, Texture_HatchetBaseColor, tempArena);
        LoadTextureFromFile("../content/pistol_icon.png", memory, Texture_PistolIcon, tempArena);
        LoadTextureFromFile("../content/pistol_ammo_icon.png", memory, Texture_PistolAmmoIcon, tempArena);
        LoadTextureFromFile("../content/hatchet_icon.png", memory, Texture_HatchetIcon, tempArena);
        LoadTextureFromFile("../content/storage_box_icon.png", memory, Texture_StorageBoxIcon, tempArena);
        LoadTextureFromFile("../content/foundation_icon.png", memory, Texture_FoundationIcon, tempArena);
        LoadTextureFromFile("../content/wall_icon.png", memory, Texture_WallIcon, tempArena);
        LoadTextureFromFile("../content/doorway_icon.png", memory, Texture_DoorwayIcon, tempArena);
        //LoadTextureFromFile("../content/sand.jpg", memory, Texture_Sand, tempArena);
        //LoadTextureFromFile("../content/grass.jpg", memory, Texture_Grass, tempArena);
        //LoadTextureFromFile("../content/rock.jpg", memory, Texture_Rock, tempArena);
        //LoadTextureFromFile("../content/test_normalmap.png", memory, Texture_TestNormalMap, tempArena);
        //LoadTextureFromFile("../content/uv_grid.png", memory, Texture_UVGrid, tempArena);
        //LoadTextureFromFile("../content/test_normalmap2.png", memory, Texture_TestNormalMap2, tempArena);
        LoadTextureFromFile("../content/smoke_particle.png", memory, Texture_SmokeParticle, tempArena);
        LoadTextureFromFile("../content/sun.png", memory, Texture_SunIcon, tempArena);
        LoadTextureFromFile("../content/cloud.png", memory, Texture_CloudIcon, tempArena);

        //LoadCubeMapFromFile("../content/skybox_clear.png", memory, Texture_Skybox, tempArena);

        LoadAudioClipFromFile("../content/pistol.wav", memory, assets, AudioClip_Pistol);
        //LoadAudioClipFromFile("../content/ambience.wav", memory, assets, AudioClip_Ambience);
    }
}

internal void LoadShaders(GameMemory *memory, MemoryArena *tempArena)
{
    TIMED_BLOCK();

    LoadInternalShaders(memory, tempArena);

    {
        RenderCommand_CreateUniformBuffer *createLightingDataBuffer =
            AllocateRenderCommand(memory, RenderCommand_CreateUniformBuffer);
        createLightingDataBuffer->id = UniformBuffer_LightingData;
        createLightingDataBuffer->capacity = sizeof(LightingDataUniformBuffer);
    }

    {
        RenderCommand_CreateUniformBuffer *createBoneTransformsBuffer =
            AllocateRenderCommand(memory, RenderCommand_CreateUniformBuffer);
        createBoneTransformsBuffer->id = UniformBuffer_BoneTransforms;
        createBoneTransformsBuffer->capacity =
            sizeof(BoneTransformsUniformBuffer);
    }

    char *lightingDataShaderSource = NULL;
    {
        DebugReadFileResult lightingDataFile =
            memory->readEntireFile("../content/shaders/lightingData.glsl");
        if (lightingDataFile.contents != NULL)
        {
            lightingDataShaderSource =
                AllocateArray(tempArena, char, lightingDataFile.size + 1);
            CopyMemory(lightingDataShaderSource, lightingDataFile.contents,
                lightingDataFile.size);
            lightingDataShaderSource[lightingDataFile.size - 1] = '\n';
            lightingDataShaderSource[lightingDataFile.size] = '\0';

            memory->freeFileMemory(lightingDataFile);
        }
    }

    {
        DebugReadFileResult diffuseLightingFile =
            memory->readEntireFile("../content/shaders/diffuseLighting.glsl");
        if (diffuseLightingFile.contents != NULL)
        {
            char *shaderSource = AllocateArray(tempArena, char, diffuseLightingFile.size);
            CopyMemory(shaderSource, diffuseLightingFile.contents,
                diffuseLightingFile.size);
            shaderSource[diffuseLightingFile.size - 1] = '\0';

            RenderCommand_CreateShader baseCreateDiffuseShaderCommand = {};
            baseCreateDiffuseShaderCommand.id = MAX_SHADERS;
            baseCreateDiffuseShaderCommand.shaderSource = shaderSource;
            baseCreateDiffuseShaderCommand.includes[0] =
                lightingDataShaderSource;
            baseCreateDiffuseShaderCommand.includeCount = 1;
            baseCreateDiffuseShaderCommand.depthTestEnabled = true;
            baseCreateDiffuseShaderCommand.faceCullingMode =
                FaceCulling_BackFace;
            AddUniformDefinition(&baseCreateDiffuseShaderCommand,
                UniformValue_MvpMatrix, UniformType_Mat4, "mvp");
            AddUniformDefinition(&baseCreateDiffuseShaderCommand,
                UniformValue_ShadowMap, UniformType_RenderTarget,
                "inShadowMap");
            AddUniformDefinition(&baseCreateDiffuseShaderCommand,
                UniformValue_LightViewProjection, UniformType_Mat4,
                "inLightViewProjection");
            AddUniformDefinition(&baseCreateDiffuseShaderCommand,
                UniformValue_ModelMatrix, UniformType_Mat4, "modelMatrix");
            AddUniformDefinition(&baseCreateDiffuseShaderCommand,
                UniformValue_LightingData, UniformType_Buffer,
                "LightingDataUniformBuffer");
            AddUniformDefinition(&baseCreateDiffuseShaderCommand,
                UniformValue_CameraPosition, UniformType_Vec3,
                "cameraPosition");

            {
                RenderCommand_CreateShader *createDiffuseColorShader =
                    AllocateRenderCommand(memory, RenderCommand_CreateShader);
                CopyMemory(createDiffuseColorShader,
                    &baseCreateDiffuseShaderCommand,
                    sizeof(*createDiffuseColorShader));
                createDiffuseColorShader->id = Shader_DiffuseColor;
                AddUniformDefinition(createDiffuseColorShader,
                    UniformValue_Color, UniformType_Vec4, "color");
            }

            {
                RenderCommand_CreateShader *createDiffuseTextureShader =
                    AllocateRenderCommand(memory, RenderCommand_CreateShader);
                CopyMemory(createDiffuseTextureShader,
                    &baseCreateDiffuseShaderCommand,
                    sizeof(*createDiffuseTextureShader));
                createDiffuseTextureShader->id = Shader_DiffuseTexture;
                createDiffuseTextureShader->defines = "#define USE_TEXTURE\n";
                AddUniformDefinition(createDiffuseTextureShader,
                    UniformValue_AlbedoTexture, UniformType_Texture,
                    "albedoTexture");
            }

            {
                RenderCommand_CreateShader *createWorldDiffuseTextureShader =
                    AllocateRenderCommand(memory, RenderCommand_CreateShader);
                CopyMemory(createWorldDiffuseTextureShader,
                    &baseCreateDiffuseShaderCommand,
                    sizeof(*createWorldDiffuseTextureShader));
                createWorldDiffuseTextureShader->id = Shader_WorldDiffuseTexture;
                createWorldDiffuseTextureShader->defines =
                    "#define USE_TEXTURE\n#define WORLD_TEXTURE_COORDINATES\n";
                AddUniformDefinition(createWorldDiffuseTextureShader,
                    UniformValue_AlbedoTexture, UniformType_Texture,
                    "albedoTexture");
            }

            {
                RenderCommand_CreateShader *createDiffuseNormalMappingShader =
                    AllocateRenderCommand(memory, RenderCommand_CreateShader);
                CopyMemory(createDiffuseNormalMappingShader ,
                    &baseCreateDiffuseShaderCommand,
                    sizeof(*createDiffuseNormalMappingShader ));
                createDiffuseNormalMappingShader->id =
                    Shader_DiffuseNormalMapping;
                createDiffuseNormalMappingShader->defines =
                    "#define USE_NORMAL_MAP\n";
                AddUniformDefinition(createDiffuseNormalMappingShader ,
                    UniformValue_NormalMap, UniformType_Texture,
                    "normalMap");
                AddUniformDefinition(createDiffuseNormalMappingShader,
                    UniformValue_Color, UniformType_Vec4, "color");
            }

            {
                RenderCommand_CreateShader *createDiffuseColorInstancedShader =
                    AllocateRenderCommand(memory, RenderCommand_CreateShader);
                CopyMemory(createDiffuseColorInstancedShader,
                    &baseCreateDiffuseShaderCommand,
                    sizeof(*createDiffuseColorInstancedShader));
                createDiffuseColorInstancedShader->id =
                    Shader_DiffuseColorInstanced;
                createDiffuseColorInstancedShader->defines =
                    "#define USE_MESH_INSTANCING\n";
                AddUniformDefinition(createDiffuseColorInstancedShader,
                    UniformValue_Color, UniformType_Vec4, "color");
                AddUniformDefinition(createDiffuseColorInstancedShader,
                    UniformValue_ViewProjection, UniformType_Mat4,
                    "viewProjection");
            }

            {
                RenderCommand_CreateShader *createDiffuseColorSkinningShader =
                    AllocateRenderCommand(memory, RenderCommand_CreateShader);
                CopyMemory(createDiffuseColorSkinningShader,
                    &baseCreateDiffuseShaderCommand,
                    sizeof(*createDiffuseColorSkinningShader));
                createDiffuseColorSkinningShader->id =
                    Shader_Skinning;
                createDiffuseColorSkinningShader->defines =
                    "#define ENABLE_SKINNING\n";
                AddUniformDefinition(createDiffuseColorSkinningShader,
                    UniformValue_Color, UniformType_Vec4, "color");
                AddUniformDefinition(createDiffuseColorSkinningShader,
                    UniformValue_ViewProjection, UniformType_Mat4,
                    "viewProjection");
                AddUniformDefinition(createDiffuseColorSkinningShader,
                    UniformValue_BoneTransformsBuffer, UniformType_Buffer,
                    "BoneTransformsUniformBuffer");
            }

            memory->freeFileMemory(diffuseLightingFile);
        }
    }

    {
        DebugReadFileResult terrainShaderFile =
            memory->readEntireFile("../content/shaders/terrain.glsl");
        if (terrainShaderFile.contents != NULL)
        {
            char *shaderSource =
                AllocateArray(tempArena, char, terrainShaderFile.size);
            CopyMemory(shaderSource, terrainShaderFile.contents,
                terrainShaderFile.size);
            shaderSource[terrainShaderFile.size - 1] = '\0';

            {
                RenderCommand_CreateShader *createTerrainShader =
                    AllocateRenderCommand(memory, RenderCommand_CreateShader);
                createTerrainShader->id = Shader_Terrain;
                createTerrainShader->shaderSource = shaderSource;
                createTerrainShader->includes[0] = lightingDataShaderSource;
                createTerrainShader->includeCount = 1;
                createTerrainShader->depthTestEnabled = true;
                AddUniformDefinition(createTerrainShader,
                    UniformValue_MvpMatrix, UniformType_Mat4, "mvp");
                AddUniformDefinition(createTerrainShader, UniformValue_SplatMap,
                    UniformType_Texture, "splatMap");
                AddUniformDefinition(createTerrainShader,
                    UniformValue_SandTexture, UniformType_Texture,
                    "sandTexture");
                AddUniformDefinition(createTerrainShader,
                    UniformValue_RockTexture, UniformType_Texture,
                    "rockTexture");
                AddUniformDefinition(createTerrainShader,
                    UniformValue_GrassTexture, UniformType_Texture,
                    "grassTexture");
                AddUniformDefinition(createTerrainShader,
                    UniformValue_NormalMap, UniformType_Texture, "normalMap");
                AddUniformDefinition(createTerrainShader,
                    UniformValue_ShadowMap, UniformType_RenderTarget,
                    "inShadowMap");
                AddUniformDefinition(createTerrainShader,
                    UniformValue_LightViewProjection, UniformType_Mat4,
                    "inLightViewProjection");
                AddUniformDefinition(createTerrainShader,
                    UniformValue_ModelMatrix, UniformType_Mat4, "modelMatrix");
                AddUniformDefinition(createTerrainShader,
                    UniformValue_LightingData, UniformType_Buffer,
                    "LightingDataUniformBuffer");
                AddUniformDefinition(createTerrainShader,
                    UniformValue_Heightmap, UniformType_Texture, "heightmap");
                AddUniformDefinition(createTerrainShader,
                    UniformValue_CameraPosition, UniformType_Vec3,
                    "cameraPosition");
                AddUniformDefinition(createTerrainShader,
                    UniformValue_UVTransform, UniformType_Vec4, "uvTransform");
            }

            {
                RenderCommand_CreateShader *createTerrainDebugShader =
                    AllocateRenderCommand(memory, RenderCommand_CreateShader);
                createTerrainDebugShader->id = Shader_TerrainDebug;
                createTerrainDebugShader->shaderSource = shaderSource;
                createTerrainDebugShader->depthTestEnabled = true;
                createTerrainDebugShader->lineModeEnabled = true;
                createTerrainDebugShader->defines =
                    "#define DEBUG_WIREFRAME\n";
                AddUniformDefinition(createTerrainDebugShader,
                    UniformValue_MvpMatrix, UniformType_Mat4, "mvp");
                AddUniformDefinition(createTerrainDebugShader,
                    UniformValue_ModelMatrix, UniformType_Mat4, "modelMatrix");
                AddUniformDefinition(createTerrainDebugShader,
                    UniformValue_Heightmap, UniformType_Texture, "heightmap");
                AddUniformDefinition(createTerrainDebugShader,
                    UniformValue_UVTransform, UniformType_Vec4, "uvTransform");
            }
            memory->freeFileMemory(terrainShaderFile);
        }
    }
}

internal void CreateVertexBuffers(GameMemory *memory)
{
    RenderCommand_CreateVertexBuffer *createTextBuffer =
        AllocateRenderCommand(memory, RenderCommand_CreateVertexBuffer);
    createTextBuffer->id = VertexBuffer_Text;
    createTextBuffer->vertexLayout = VertexLayout_Standard;
    createTextBuffer->vertexCount = TEXT_BUFFER_MAX_VERTICES;

    RenderCommand_CreateVertexBuffer *createLineBuffer =
        AllocateRenderCommand(memory, RenderCommand_CreateVertexBuffer);
    createLineBuffer->id = VertexBuffer_DebugLines;
    createLineBuffer->vertexLayout = VertexLayout_PositionAndColor;
    createLineBuffer->vertexCount = MAX_DEBUG_LINES * 2;

    RenderCommand_CreateVertexBuffer *createCollisionGeometry =
        AllocateRenderCommand(memory, RenderCommand_CreateVertexBuffer);
    createCollisionGeometry->id = VertexBuffer_CollisionGeometry;
    createCollisionGeometry->vertexLayout = VertexLayout_Standard;
    createCollisionGeometry->vertexCount = COLLISION_GEOMETRY_MAX_VERTICES;

    RenderCommand_CreateVertexBuffer *createLineBufferWithDepth =
        AllocateRenderCommand(memory, RenderCommand_CreateVertexBuffer);
    createLineBufferWithDepth->id = VertexBuffer_DebugLinesWithDepth;
    createLineBufferWithDepth->vertexLayout = VertexLayout_PositionAndColor;
    createLineBufferWithDepth->vertexCount = MAX_DEBUG_LINES * 2;
}
