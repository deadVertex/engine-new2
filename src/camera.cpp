struct CameraState
{
    vec3 position;
    vec3 rotation;
    vec3 velocity;
};

struct MoveCameraParameters
{
    CameraState currentState;
    GameInput *input;
    f32 frameBufferWidth;
    f32 frameBufferHeight;
    f32 speed;
    f32 friction;
    f32 dt;
};

internal CameraState MoveCamera(MoveCameraParameters *params)
{
    vec3 currentPosition = params->currentState.position;
    vec3 currentRotation = params->currentState.rotation;
    vec3 currentVelocity = params->currentState.velocity;

    GameInput *input = params->input;

    f32 speed = params->speed;
    f32 friction = params->friction;
    f32 dt = params->dt;

    f32 mouseX = (f32)(input->mouseRelPosX) / (f32)params->frameBufferWidth;
    f32 mouseY = (f32)(input->mouseRelPosY) / (f32)params->frameBufferHeight;

    vec3 newRotation = Vec3(-mouseY, -mouseX, 0.0f);

    vec3 rotation = currentRotation;

    if (input->buttonStates[KEY_MOUSE_BUTTON_RIGHT].isDown)
    {
        rotation += newRotation;
    }

    rotation.x = Clamp(rotation.x, -PI * 0.5f, PI * 0.5f);

    if (rotation.y > 2.0f * PI)
    {
        rotation.y -= 2.0f * PI;
    }
    if (rotation.y < 2.0f * -PI)
    {
        rotation.y += 2.0f * PI;
    }

    f32 forwardMove = 0.0f;
    f32 rightMove = 0.0f;
    if (input->buttonStates[KEY_W].isDown)
    {
        forwardMove = -1.0f;
    }

    if (input->buttonStates[KEY_S].isDown)
    {
        forwardMove = 1.0f;
    }

    if (input->buttonStates[KEY_A].isDown)
    {
        rightMove = -1.0f;
    }
    if (input->buttonStates[KEY_D].isDown)
    {
        rightMove = 1.0f;
    }

    if (input->buttonStates[KEY_LEFT_SHIFT].isDown)
    {
        speed *= 10.0f;
    }

    mat4 rotationMatrix =
        RotateY(rotation.y) * RotateX(rotation.x);

    vec3 forward = (rotationMatrix * Vec4(0, 0, 1, 0)).xyz;
    vec3 right = (rotationMatrix * Vec4(1, 0, 0, 0)).xyz;

    vec3 targetDir = SafeNormalize(forward * forwardMove + right * rightMove);

    vec3 velocity = currentVelocity;
    velocity -= currentVelocity * friction * dt;
    velocity += targetDir * speed * dt;

    vec3 position = currentPosition + velocity * dt;

    CameraState result;
    result.position = position;
    result.rotation = rotation;
    result.velocity = velocity;

    return result;
}

