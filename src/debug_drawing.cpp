struct DebugLine
{
    vec3 start;
    vec3 end;
    vec3 color;
    f32 timeRemaining;
};

struct DebugLineBuffer
{
    DebugLine *lines;
    u32 count;
    u32 capacity;
};

struct DebugWorldText
{
    vec3 position;
    const char *text;
    u32 textLength;
};

struct DebugDrawingSystem
{
    DebugLineBuffer buffer;
    DebugLineBuffer depthBuffer;

    DebugWorldText *worldText;
    u32 worldTextCount;
    u32 worldTextCapacity;

    char *textBuffer;
    u32 textBufferLength;
    u32 textBufferCapacity;
};

inline u32 AddLineVertices(
    VertexPC *vertices, u32 maxVertices, vec3 start, vec3 end, vec3 color)
{
    if (maxVertices >= 2)
    {
        VertexPC p0, p1;
        p0.position = start;
        p0.color = color;

        p1.position = end;
        p1.color = color;

        vertices[0] = p0;
        vertices[1] = p1;
        return 2;
    }

    return 0;
}

inline DebugLineBuffer AllocateDebugLineBuffer(MemoryArena *arena, u32 capacity)
{
    DebugLineBuffer result = {};
    result.lines = AllocateArray(arena, DebugLine, capacity);
    result.capacity = capacity;

    return result;
}

inline DebugLine* AllocateDebugLine(DebugLineBuffer *buffer)
{
    Assert(buffer->count < buffer->capacity);
    DebugLine *line = buffer->lines + buffer->count++;
    return line;
}

inline void FreeLine(DebugLineBuffer *buffer, DebugLine *line)
{
    Assert(buffer->count > 0);
    Assert(line >= buffer->lines && line < buffer->lines + buffer->count);
    DebugLine *lastLine = buffer->lines + (--buffer->count);
    *line = *lastLine;
}

internal void CleanUpOldLines(DebugLineBuffer *buffer, f32 dt)
{
    u32 i = 0;
    while (i < buffer->count)
    {
        DebugLine *line = buffer->lines + i;
        if (line->timeRemaining < 0.0f)
        {
            FreeLine(buffer, line);
            // NOTE: Run loop again with same value because new value is in its place.
        }
        else
        {
            line->timeRemaining -= dt;
            i++;
        }
    }
}

inline u32 GetVertices(
    DebugLineBuffer *buffer, VertexPC *vertices, u32 maxVertices)
{
    u32 vertexCount = 0;
    for (u32 i = 0; i < buffer->count; ++i)
    {
        DebugLine *line = buffer->lines + i;

        u32 remainingVertices = maxVertices - vertexCount;
        VertexPC *verticesCursor = vertices + vertexCount;

        vertexCount += AddLineVertices(verticesCursor, remainingVertices,
                line->start, line->end, line->color);
    }

    return vertexCount;
}





internal void DebugDrawing_Init(DebugDrawingSystem *system, MemoryArena *arena, u32 maxLines)
{
    system->buffer = AllocateDebugLineBuffer(arena, maxLines);
    system->depthBuffer = AllocateDebugLineBuffer(arena, maxLines);
    system->worldText = AllocateArray(arena, DebugWorldText, 512);
    system->worldTextCapacity = 512;
    system->textBuffer = AllocateArray(arena, char, 8192);
    system->textBufferCapacity = 8192;
}

inline void DrawLine(DebugDrawingSystem *system, vec3 start, vec3 end,
    vec3 color, f32 lifeTime = 0.0f, b32 ignoreDepth = true)
{
    DebugLine *line = ignoreDepth ? AllocateDebugLine(&system->buffer)
                                  : AllocateDebugLine(&system->depthBuffer);
    line->start = start;
    line->end = end;
    line->color = color;
    line->timeRemaining = lifeTime;
}

inline void DrawPoint(DebugDrawingSystem *system, vec3 position, f32 scale,
    vec3 color, f32 lifeTime = 0.0f)
{
    vec3 xmin = position - Vec3(scale, 0, 0);
    vec3 xmax = position + Vec3(scale, 0, 0);
    vec3 ymin = position - Vec3(0, scale, 0);
    vec3 ymax = position + Vec3(0, scale, 0);
    vec3 zmin = position - Vec3(0, 0, scale);
    vec3 zmax = position + Vec3(0, 0, scale);

    DrawLine(system, xmin, xmax, color, lifeTime);
    DrawLine(system, ymin, ymax, color, lifeTime);
    DrawLine(system, zmin, zmax, color, lifeTime);
}

inline void DrawAxis(DebugDrawingSystem *system, mat4 transform, f32 scale, f32 lifeTime = 0.0f)
{
    // TODO: Just read these values out of the transform, rather than doing matrix multiplication!
    vec3 origin = transform.columns[3].xyz;
    vec3 x = transform.columns[0].xyz * scale;
    vec3 y = transform.columns[1].xyz * scale;
    vec3 z = transform.columns[2].xyz * scale;

    DrawLine(system, origin, origin + x, Vec3(1, 0, 0), lifeTime);
    DrawLine(system, origin, origin + y, Vec3(0, 1, 0), lifeTime);
    DrawLine(system, origin, origin + z, Vec3(0, 0, 1), lifeTime);
}

inline void DrawBox(DebugDrawingSystem *system, vec3 min, vec3 max, vec3 color,
        f32 lifeTime = 0.0f, b32 ignoreDepth = true)
{
    DrawLine(system, Vec3(min.x, min.y, min.z), Vec3(max.x, min.y, min.z),
        color, lifeTime, ignoreDepth);
    DrawLine(system, Vec3(max.x, min.y, min.z), Vec3(max.x, min.y, max.z),
        color, lifeTime, ignoreDepth);
    DrawLine(system, Vec3(max.x, min.y, max.z), Vec3(min.x, min.y, max.z),
        color, lifeTime, ignoreDepth);
    DrawLine(system, Vec3(min.x, min.y, max.z), Vec3(min.x, min.y, min.z),
        color, lifeTime, ignoreDepth);

    DrawLine(system, Vec3(min.x, max.y, min.z), Vec3(max.x, max.y, min.z),
        color, lifeTime, ignoreDepth);
    DrawLine(system, Vec3(max.x, max.y, min.z), Vec3(max.x, max.y, max.z),
        color, lifeTime, ignoreDepth);
    DrawLine(system, Vec3(max.x, max.y, max.z), Vec3(min.x, max.y, max.z),
        color, lifeTime, ignoreDepth);
    DrawLine(system, Vec3(min.x, max.y, max.z), Vec3(min.x, max.y, min.z),
        color, lifeTime, ignoreDepth);

    DrawLine(system, Vec3(min.x, min.y, min.z), Vec3(min.x, max.y, min.z),
        color, lifeTime, ignoreDepth);
    DrawLine(system, Vec3(max.x, min.y, min.z), Vec3(max.x, max.y, min.z),
        color, lifeTime, ignoreDepth);
    DrawLine(system, Vec3(max.x, min.y, max.z), Vec3(max.x, max.y, max.z),
        color, lifeTime, ignoreDepth);
    DrawLine(system, Vec3(min.x, min.y, max.z), Vec3(min.x, max.y, max.z),
        color, lifeTime, ignoreDepth);
}

inline void DrawCircle(DebugDrawingSystem *system, vec3 center, f32 radius,
    u32 segmentCount, quat rotation, vec3 color, f32 lifeTime = 0.0f,
    b32 ignoreDepth = true)
{
    f32 increment = (2.0f * PI) / segmentCount;
    for (u32 i = 0; i < segmentCount; ++i)
    {
        f32 t0 = increment * i;
        f32 t1 = Fmod(increment * (i + 1), 2.0f * PI);

        f32 x0 = Sin(t0);
        f32 y0 = Cos(t0);

        f32 x1 = Sin(t1);
        f32 y1 = Cos(t1);

        vec3 q0 = Vec3(x0, 0, y0) * radius;
        vec3 q1 = Vec3(x1, 0, y1) * radius;

        vec3 r0 = Rotate(rotation, q0);
        vec3 r1 = Rotate(rotation, q1);

        vec3 p0 = center + r0;
        vec3 p1 = center + r1;

        DrawLine(system, p0, p1, color, lifeTime, ignoreDepth);
    }
}

inline void DrawSphere(DebugDrawingSystem *system, vec3 center, f32 radius,
    vec3 color, f32 lifeTime = 0.0f, b32 ignoreDepth = true)
{
    quat xz = Quat();
    quat xy = Quat(Vec3(1, 0, 0), PI * 0.5f);
    quat yz = Quat(Vec3(0, 0, 1), PI * 0.5f);

    u32 segmentCount = 24;

    DrawCircle(
        system, center, radius, segmentCount, xz, color, lifeTime, ignoreDepth);
    DrawCircle(
        system, center, radius, segmentCount, xy, color, lifeTime, ignoreDepth);
    DrawCircle(
        system, center, radius, segmentCount, yz, color, lifeTime, ignoreDepth);
}

internal void DrawCapsule(DebugDrawingSystem *system, vec3 a, vec3 b,
    f32 radius, u32 segmentCount, vec3 color, f32 lifeTime = 0.0f,
    b32 ignoreDepth = true)
{
    vec3 v = b - a;

    vec3 up = v;
    vec3 right = {};
    if (Abs(Dot(up, Vec3(0, 0, 1))) > 0.0f)
    {
        right = Cross(Vec3(0, 1, 0), up);
    }
    else
    {
        right = Cross(up, Vec3(0, 0, 1));
    }
    vec3 forward = SafeNormalize(Cross(right, up));
    up = SafeNormalize(up);
    right = SafeNormalize(right);

    // Draw circle on local XZ plane
    for (u32 i = 0; i < segmentCount; ++i)
    {
        f32 t0 = (i / (f32)segmentCount) * 2.0f * PI;
        f32 t1 = ((i + 1) / (f32)segmentCount) * 2.0f * PI;

        f32 x0 = Sin(t0) * radius;
        f32 y0 = Cos(t0) * radius;
        vec3 p0 = right * x0 + forward * y0;

        f32 x1 = Sin(t1) * radius;
        f32 y1 = Cos(t1) * radius;
        vec3 p1 = right * x1 + forward * y1;

        DrawLine(system, p0 + a, p1 + a, color, lifeTime, ignoreDepth);
        DrawLine(system, p0 + b, p1 + b, color, lifeTime, ignoreDepth);
    }

    // Draw half circle on XY plane
    for (u32 i = 0; i < segmentCount; ++i)
    {
        f32 t0 = (i / (f32)segmentCount) * PI - 0.5f * PI;
        f32 t1 = ((i + 1) / (f32)segmentCount) *  PI - 0.5f * PI;

        f32 x0 = Sin(t0) * radius;
        f32 y0 = Cos(t0) * radius;
        vec3 p0 = right * x0 + -up * y0;
        vec3 q0 = right * x0 + up * y0;

        f32 x1 = Sin(t1) * radius;
        f32 y1 = Cos(t1) * radius;
        vec3 p1 = right * x1 + -up * y1;
        vec3 q1 = right * x1 + up * y1;

        DrawLine(system, p0 + a, p1 + a, color, lifeTime, ignoreDepth);
        DrawLine(system, q0 + b, q1 + b, color, lifeTime, ignoreDepth);
    }

    // Draw half circle on ZY plane
    for (u32 i = 0; i < segmentCount; ++i)
    {
        f32 t0 = (i / (f32)segmentCount) * PI - 0.5f * PI;
        f32 t1 = ((i + 1) / (f32)segmentCount) *  PI - 0.5f * PI;

        f32 x0 = Sin(t0) * radius;
        f32 y0 = Cos(t0) * radius;
        vec3 p0 = forward * x0 + -up * y0;
        vec3 q0 = forward * x0 + up * y0;

        f32 x1 = Sin(t1) * radius;
        f32 y1 = Cos(t1) * radius;
        vec3 p1 = forward * x1 + -up * y1;
        vec3 q1 = forward * x1 + up * y1;

        DrawLine(system, p0 + a, p1 + a, color, lifeTime, ignoreDepth);
        DrawLine(system, q0 + b, q1 + b, color, lifeTime, ignoreDepth);
    }

    DrawLine(system, right * radius + a, right * radius + b, color, lifeTime, ignoreDepth);
    DrawLine(system, -right * radius + a, -right * radius + b, color, lifeTime, ignoreDepth);
    DrawLine(system, forward * radius + a, forward * radius + b, color, lifeTime, ignoreDepth);
    DrawLine(system, -forward * radius + a, -forward * radius + b, color, lifeTime, ignoreDepth);
}

internal void DebugDrawing_Cleanup(DebugDrawingSystem *system, float dt)
{
    CleanUpOldLines(&system->buffer, dt);
    CleanUpOldLines(&system->depthBuffer, dt);

    system->textBufferLength = 0;
    system->worldTextCount = 0;
}

internal u32 Debug_GetVertices(DebugDrawingSystem *system, VertexPC *vertices,
    u32 maxVertices, b32 ignoreDepth)
{
    u32 vertexCount = 0;

    if (ignoreDepth)
    {
        vertexCount = GetVertices(&system->buffer, vertices, maxVertices);
    }
    else
    {
        vertexCount = GetVertices(&system->depthBuffer, vertices, maxVertices);
    }

    return vertexCount;
}

internal void Debug_DrawTextInWorld(DebugDrawingSystem *debugDrawingSystem,
        vec3 position, const char *text)
{
    Assert(debugDrawingSystem->worldTextCount <
           debugDrawingSystem->worldTextCapacity);

    u32 textLength = StringLength(text);
    Assert(debugDrawingSystem->textBufferLength + textLength <
           debugDrawingSystem->textBufferCapacity);

    DebugWorldText *worldText =
        debugDrawingSystem->worldText + debugDrawingSystem->worldTextCount++;

    char *dst =
        debugDrawingSystem->textBuffer + debugDrawingSystem->textBufferLength;
    CopyMemory(dst, text, textLength);
    debugDrawingSystem->textBufferLength += textLength;

    worldText->position = position;
    worldText->text = dst;
    worldText->textLength = textLength;
}
