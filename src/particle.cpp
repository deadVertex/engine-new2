struct Particle
{
    vec3 position;
    vec3 initialVelocity;
    vec3 targetVelocity;
    vec3 initialScale;
    vec3 targetScale;
    vec4 initialColor;
    vec4 targetColor;
    f32 initialRotation;
    f32 targetRotation;
    f32 lifeTime;
    f32 initialLifeTime;
};

enum
{
    SpawnMethod_Continuous,
    SpawnMethod_Burst,
};

enum
{
    RenderMode_Texture,
    RenderMode_Color,
};

struct ParticleSystemInstance
{
    Particle particles[64];
    u32 count;
    f32 spawnRate;
    u32 burstSize;
    u32 spawnMethod;
    u32 renderMode;
    f32 timeUntilNextParticleSpawn;
    vec3 origin;
    u32 id;
    ParticleSystemInstance *next;
};

inline void InitializeParticles(Particle *particles, u32 count,
    RandomNumberGenerator *rng, vec3 origin, u32 id)
{
    ClearToZero(particles, sizeof(Particle) * count);

    for (u32 i = 0; i < count; ++i)
    {
        Particle *particle = particles + i;
        switch (id)
        {
        case ParticleSystem_SmokeImpact:
            particle->initialVelocity =
                Vec3(0, 0.1, 0) + Vec3(1.6f * RandomBilateral(rng),
                                      0.2f * RandomUnilateral(rng),
                                      1.6f * RandomBilateral(rng));
            particle->targetVelocity = particle->initialVelocity * 0.6f;

            particle->position = origin;

            particle->initialScale = Vec3(0.0f);
            particle->targetScale = Vec3(0.2f + RandomUnilateral(rng) * 0.1f);

            particle->initialRotation = RandomBilateral(rng) * PI * 2.0f;
            particle->targetRotation =
                particle->initialRotation + RandomUnilateral(rng) * 0.2f;

            particle->initialColor = Vec4(0.1, 0.6, 1.0, 0.6);
            particle->targetColor = Vec4(
                particle->initialColor.xyz + Vec3(RandomBilateral(rng) * 0.2f),
                0.0f);

            particle->initialLifeTime = 0.2f + 0.1f * RandomUnilateral(rng);
            particle->lifeTime = particle->initialLifeTime;
            break;
        case ParticleSystem_Sparks:
            particle->initialVelocity =
                Vec3(0, 0.1, 0) + Vec3(1.6f * RandomBilateral(rng),
                                      2.2f * RandomUnilateral(rng),
                                      1.6f * RandomBilateral(rng));
            particle->targetVelocity = particle->initialVelocity * 0.6f + Vec3(0, -2, 0);

            particle->position = origin;

            particle->initialScale = Vec3(0.01f);
            particle->targetScale = Vec3(0.05f * RandomUnilateral(rng));
            particle->initialRotation = 0.0f;
            particle->targetRotation = 0.0f;
            particle->initialColor = Vec4(0.1, 0.1, 0.1, 1.0);
            particle->targetColor = Vec4(
                particle->initialColor.xyz + Vec3(RandomBilateral(rng) * 0.1f),
                1.0f);
            particle->initialLifeTime = 0.3f + 0.1f * RandomUnilateral(rng);
            particle->lifeTime = particle->initialLifeTime;
            break;
        default:
            InvalidCodePath();
            break;
        }
    }
}

internal void InitializeParticleSystem(
    ParticleSystemInstance *system, RandomNumberGenerator *rng, u32 id)
{
    system->count = 0;
    system->timeUntilNextParticleSpawn = 0.0f;
    system->id = id;
    system->spawnRate = 0.0f;
    switch (id)
    {
    case ParticleSystem_SmokeImpact:
        system->burstSize = 32;
        system->spawnMethod = SpawnMethod_Burst;
        system->renderMode = RenderMode_Texture;
        break;
    case ParticleSystem_Sparks:
        system->burstSize = 20;
        system->spawnMethod = SpawnMethod_Burst;
        system->renderMode = RenderMode_Color;
        break;
    default:
        InvalidCodePath();
        break;
    }
}

internal void RenderParticleSystems(ParticleSystemInstance *system, f32 dt,
    DebugDrawingSystem *debugDrawingSystem, GameMemory *memory, mat4 projection,
    mat4 view, vec3 cameraPosition, RandomNumberGenerator *rng)
{
    // Remove dead particles
    u32 particleIdx = 0;
    while (particleIdx < system->count)
    {
        system->particles[particleIdx].lifeTime -= dt;
        if (system->particles[particleIdx].lifeTime <= 0.0f)
        {
            // Swap and reduce
            u32 last = system->count - 1;
            system->particles[particleIdx] = system->particles[last];
            system->count--;
        }
        else
        {
            particleIdx++;
        }
    }

    if (system->spawnMethod == SpawnMethod_Continuous)
    {
        // Continuously emit new particles
        if (system->count < ArrayCount(system->particles))
        {
            u32 spawnCount = 0;
            system->timeUntilNextParticleSpawn -= dt;
            if (system->timeUntilNextParticleSpawn <= 0.0f)
            {
                // TODO: Handle spawning multiple particles per frame
                system->timeUntilNextParticleSpawn = 1.0f / system->spawnRate;
                spawnCount = 1;
            }

            u32 maxSpawnCount = ArrayCount(system->particles) - system->count;
            spawnCount = MinU(spawnCount, maxSpawnCount);

            InitializeParticles(system->particles + system->count, spawnCount,
                rng, system->origin, system->id);
            system->count += spawnCount;
        }
    }
    else if (system->spawnMethod == SpawnMethod_Burst)
    {
        // Burst emitter
        if (system->count == 0)
        {
            u32 spawnCount = system->burstSize;
            InitializeParticles(system->particles + system->count, spawnCount,
                rng, system->origin, system->id);
            system->count += spawnCount;
        }
    }
    else
    {
        InvalidCodePath();
    }

    mat4 localToWorldMatrices[64];
    for (u32 i = 0; i < system->count; ++i)
    {
        // Modulate color based on lifetime remaining
        f32 t = system->particles[i].lifeTime /
                system->particles[i].initialLifeTime;

        t = 1.0f - t; // Start at 0
        vec3 velocity = Lerp(system->particles[i].initialVelocity,
            system->particles[i].targetVelocity, t * t);

        system->particles[i].position += velocity * dt;

        vec3 p = system->particles[i].position;
        vec3 forward = SafeNormalize(cameraPosition - p);
        // TODO: Handle case when forward and global up are parallel
        vec3 right = SafeNormalize(Cross(Vec3(0, 1, 0), forward));
        vec3 up = SafeNormalize(Cross(forward, right));

        // Debug screen aligned quad code
        //DrawLine(debugDrawingSystem, p, p + right, Vec3(1, 0, 0));
        //DrawLine(debugDrawingSystem, p, p + up, Vec3(0, 1, 0));
        //DrawLine(debugDrawingSystem, p, p + forward, Vec3(0, 0, 1));

        localToWorldMatrices[i].columns[0] = Vec4(right, 0);
        localToWorldMatrices[i].columns[1] = Vec4(up, 0);
        localToWorldMatrices[i].columns[2] = Vec4(forward, 0);
        localToWorldMatrices[i].columns[3] = Vec4(p, 1);
    }

    // TODO: Sort particles by depth!

    for (u32 i = 0; i < system->count; ++i)
    {
        //DrawPoint(debugDrawingSystem, system->particles[i].position, 0.25f, Vec3(0.1, 0.6, 1.0));
        RenderCommand_DrawMesh *drawMesh =
            AllocateRenderCommand(memory, RenderCommand_DrawMesh);
        drawMesh->mesh = Mesh_Quad;

        // Modulate color based on lifetime remaining
        f32 t = system->particles[i].lifeTime /
                system->particles[i].initialLifeTime;

        t = 1.0f - t; // Start at 0
        vec3 scale = Lerp(system->particles[i].initialScale,
            system->particles[i].targetScale, t);

        f32 rotation = Lerp(system->particles[i].initialRotation,
            system->particles[i].targetRotation, t);

        vec4 color = Lerp(system->particles[i].initialColor,
            system->particles[i].targetColor, t);

        AddShaderUniformValueMat4(drawMesh, UniformValue_MvpMatrix,
                projection * view * localToWorldMatrices[i] * RotateZ(rotation) * Scale(scale));
        AddShaderUniformValueVec4(drawMesh, UniformValue_Color, color);

        if (system->renderMode == RenderMode_Texture)
        {
            drawMesh->shader = Shader_ParticleTexture; // Don't use depth buffer

            AddShaderUniformValueTexture(
                drawMesh, UniformValue_AlbedoTexture, Texture_SmokeParticle);
        }
        else
        {
            drawMesh->shader = Shader_ColorUI;
        }
    }
}
