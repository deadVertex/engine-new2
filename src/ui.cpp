enum HorizontalAlignment
{
    HorizontalAlign_Left,
    HorizontalAlign_Center,
    HorizontalAlign_Right,
};

enum VerticalAlignment
{
    VerticalAlign_Top,
    VerticalAlign_Center,
    VerticalAlign_Bottom,
};

struct ui_DrawStringArguments
{
    GameMemory *memory;
    VertexBuffer *vertexBuffer;
    const char *text;
    u32 length;
    vec2 position;
    vec4 color;
    b32 drawShadow;
    vec4 shadowColor;
    vec2 shadowOffset;
    Font *font;
    mat4 orthographicProjection;
    HorizontalAlignment horizontalAlignment;
    VerticalAlignment verticalAlignment;
};

internal void ui_DrawString(ui_DrawStringArguments *args)
{
    Assert(args->memory);
    Assert(args->vertexBuffer);
    Assert(args->text);
    Assert(args->font);
    AddTextToBufferResult addedText = AddTextToBuffer(
        args->vertexBuffer, args->text, args->font, args->length);

    vec2 position = args->position;

    if (args->horizontalAlignment != HorizontalAlign_Left)
    {
        float length = CalculateTextLength(args->font, args->text, args->length);
        if (args->horizontalAlignment == HorizontalAlign_Center)
        {
            position.x -= length * 0.5f;
        }
        else if (args->horizontalAlignment == HorizontalAlign_Right)
        {
            position.x -= length;
        }
        else
        {
            InvalidCodePath();
        }
    }

    if (args->verticalAlignment == VerticalAlign_Top)
    { 
        position.y -= args->font->height;
    }
    else if (args->verticalAlignment == VerticalAlign_Center)
    {
        position.y -= args->font->height * 0.5f;
    }

    GameMemory *memory = args->memory;
    if (args->drawShadow)
    {
        RenderCommand_DrawVertexBuffer *drawVertexBuffer =
            AllocateRenderCommand(memory, RenderCommand_DrawVertexBuffer);
        drawVertexBuffer->id = VertexBuffer_Text;
        drawVertexBuffer->shader = Shader_AlphaMapping;
        drawVertexBuffer->primitive = Primitive_Triangles;
        drawVertexBuffer->firstVertex = addedText.firstVertex;
        drawVertexBuffer->vertexCount = addedText.vertexCount;

        AddShaderUniformValueMat4(drawVertexBuffer, UniformValue_MvpMatrix,
            args->orthographicProjection *
                WorldTransform(
                    Vec3(position + args->shadowOffset, 0.0f), Vec3(1.0f)));
        AddShaderUniformValueTexture(
            drawVertexBuffer, UniformValue_AlphaMap, args->font->texture);

        AddShaderUniformValueVec4(
            drawVertexBuffer, UniformValue_Color, args->shadowColor);
    }

    {
        RenderCommand_DrawVertexBuffer *drawVertexBuffer =
            AllocateRenderCommand(memory, RenderCommand_DrawVertexBuffer);
        drawVertexBuffer->id = VertexBuffer_Text;
        drawVertexBuffer->shader = Shader_AlphaMapping;
        drawVertexBuffer->primitive = Primitive_Triangles;
        drawVertexBuffer->firstVertex = addedText.firstVertex;
        drawVertexBuffer->vertexCount = addedText.vertexCount;

        AddShaderUniformValueMat4(drawVertexBuffer, UniformValue_MvpMatrix,
            args->orthographicProjection *
                WorldTransform(Vec3(position, 0.0f), Vec3(1.0f)));
        AddShaderUniformValueTexture(
            drawVertexBuffer, UniformValue_AlphaMap, args->font->texture);

        AddShaderUniformValueVec4(drawVertexBuffer, UniformValue_Color, args->color);
    }
}

struct ui_DrawQuadArguments
{
    GameMemory *memory;
    mat4 orthographicProjection;
    vec2 position;
    float width;
    float height;
    vec4 color;
    u32 texture;
    HorizontalAlignment horizontalAlignment;
    VerticalAlignment verticalAlignment;
};

// Designed for a unit sized quad (-0.5, 0.5)
inline mat4 CalculateModelMatrix(vec2 p, float w, float h,
                                 vec2 anchorPoint)
{
    mat4 result = Translate(Vec3(p.x, p.y, 0.0)) *
                  Scale(Vec3(w, h, 0.0f)) *
                  Translate(Vec3(anchorPoint.x, anchorPoint.y, 0) * 0.5f);
    return result;
}

inline vec2 CalculateAnchorPoint(HorizontalAlignment horizontalAlignment,
    VerticalAlignment verticalAlignment)
{
    vec2 anchorPoint = Vec2(1, -1); // Top left alignment
    if (horizontalAlignment == HorizontalAlign_Center)
    {
        anchorPoint.x = 0.0f;
    }
    else if (horizontalAlignment == HorizontalAlign_Right)
    {
        anchorPoint.x = -1.0f;
    }

    if (verticalAlignment == VerticalAlign_Center)
    {
        anchorPoint.y = 0.0f;
    }
    else if (verticalAlignment == VerticalAlign_Bottom)
    {
        anchorPoint.y = 1.0f;
    }

    return anchorPoint;
}

internal void ui_DrawQuad(ui_DrawQuadArguments *args)
{
    Assert(args->memory);

    RenderCommand_DrawMesh *drawQuad =
        AllocateRenderCommand(args->memory, RenderCommand_DrawMesh);
    drawQuad->mesh = Mesh_Quad;

    if (args->texture != 0)
    {
        drawQuad->shader = Shader_TextureAlpha;
        AddShaderUniformValueTexture(
                drawQuad, UniformValue_AlbedoTexture, args->texture);
    }
    else
    {
        drawQuad->shader = Shader_ColorUI;
        AddShaderUniformValueVec4(drawQuad, UniformValue_Color, args->color);
    }

    vec2 anchorPoint = CalculateAnchorPoint(
        args->horizontalAlignment, args->verticalAlignment);

    mat4 modelMatrix = CalculateModelMatrix(
        args->position, args->width, args->height, anchorPoint);

    AddShaderUniformValueMat4(drawQuad, UniformValue_MvpMatrix,
        args->orthographicProjection * modelMatrix);
}

struct ui_DrawButtonArguments
{
    GameMemory *memory;
    mat4 orthographicProjection;
    VertexBuffer *vertexBuffer;
    Font *font;
    vec4 color;
    vec4 hoverColor;
    vec4 clickColor;
    vec4 textColor;
    GameInput *input;
    const char *text;
    vec2 position;
};

// TODO: Support alignment
internal b32 ui_DrawButton(ui_DrawButtonArguments *args)
{
    Assert(args->input);
    Assert(args->memory);
    GameInput *input = args->input;
    GameMemory *memory = args->memory;
    vec2 p = args->position;

    b32 result = false;
    float width = CalculateTextLength(args->font, args->text);
    float height = args->font->height;
    vec4 color = args->color;

    vec2 mousePosition = Vec2((f32)input->mousePosX,
        (f32)memory->frameBufferHeight - (f32)input->mousePosY);

    // TODO: Make this configurable
    float horizontalPadding = 25.0f;
    float verticalPadding = 5.0f;

    // Top left alignment
    rect2 rect = {};
    rect.min.x = p.x;
    rect.min.y = p.y - height - verticalPadding * 2.0f;
    rect.max.x = p.x + width + horizontalPadding * 2.0f;
    rect.max.y = p.y;
    if (ContainsPoint(rect, mousePosition))
    {
        if (input->buttonStates[KEY_MOUSE_BUTTON_LEFT].isDown)
        {
            // Click
            color = args->clickColor;
        }
        else
        {
            if (input->buttonStates[KEY_MOUSE_BUTTON_LEFT].wasDown &&
                !input->buttonStates[KEY_MOUSE_BUTTON_LEFT].isDown)
            {
                // Process click
                result = true;
            }
            else
            {
                // Hover
                color = args->hoverColor;
            }
        }
    }

    ui_DrawQuadArguments drawQuad = {};
    drawQuad.memory = memory;
    drawQuad.orthographicProjection = args->orthographicProjection;
    drawQuad.position = p;
    drawQuad.width = width + 2.0f * horizontalPadding;
    drawQuad.height = height + 2.0f * verticalPadding;
    drawQuad.color = color;
    drawQuad.horizontalAlignment = HorizontalAlign_Left;
    drawQuad.verticalAlignment = VerticalAlign_Top;
    ui_DrawQuad(&drawQuad);

    ui_DrawStringArguments drawString = {};
    drawString.memory = memory;
    drawString.vertexBuffer = args->vertexBuffer;
    drawString.text = args->text;
    drawString.position = p + Vec2(horizontalPadding, -verticalPadding);
    drawString.color = args->textColor;
    drawString.font = args->font;
    drawString.orthographicProjection = args->orthographicProjection;
    drawString.horizontalAlignment = HorizontalAlign_Left;
    drawString.verticalAlignment = VerticalAlign_Top;
    ui_DrawString(&drawString);

    return result;
}

struct ui_Builder
{
    GameMemory *memory;
    mat4 orthographicProjection;
    vec2 position;
    float width;
    float height;
    vec4 color;
    u32 texture;
    HorizontalAlignment horizontalAlignment;
    VerticalAlignment verticalAlignment;
};

internal void ui_InitializeBuilder(ui_Builder *builder, GameMemory *memory)
{
    builder->memory = memory;
    builder->orthographicProjection = Orthographic(0.0f,
        (f32)memory->frameBufferWidth, 0.0f, (f32)memory->frameBufferHeight);
    builder->horizontalAlignment = HorizontalAlign_Left;
    builder->verticalAlignment = VerticalAlign_Top;
}

internal void ui_DrawQuad(
    ui_Builder *builder, vec2 position, f32 width, f32 height, vec4 color)
{
    ui_DrawQuadArguments drawQuad = {};
    drawQuad.memory = builder->memory;
    drawQuad.orthographicProjection = builder->orthographicProjection;
    drawQuad.position = position;
    drawQuad.width = width;
    drawQuad.height = height;
    drawQuad.color = color;
    drawQuad.horizontalAlignment = builder->horizontalAlignment;
    drawQuad.verticalAlignment = builder->verticalAlignment;

    ui_DrawQuad(&drawQuad);
}

struct ui_DrawBarGraphArguments
{
    vec4 color;
    vec4 axisColor;
    vec2 position;
    u32 sampleCount;
    f32 width;
    f32 height;
    f32 axisWidth;
    f32 maxValue;
    f32 *samples;
};

internal void ui_DrawBarGraph(ui_Builder *builder, ui_DrawBarGraphArguments *args)
{
    ui_DrawQuad(builder, args->position + Vec2(0.0f, -args->height),
        args->width, args->axisWidth, args->axisColor);

    ui_DrawQuad(
        builder, args->position, args->width, args->axisWidth, args->axisColor);

    f32 sampleWidth = args->width / (f32)args->sampleCount;

    VerticalAlignment prevVerticalAlignment = builder->verticalAlignment;
    builder->verticalAlignment = VerticalAlign_Bottom;
    for (u32 i = 0; i < args->sampleCount; ++i)
    {
        f32 sampleHeight = args->samples[i] / args->maxValue;
        sampleHeight *= args->height;
        ui_DrawQuad(builder,
            args->position + Vec2(sampleWidth * i, -args->height),
            sampleWidth - 1.0f, sampleHeight, args->color);
    }
    builder->verticalAlignment = prevVerticalAlignment;
}

struct Rectangle
{
    vec2 min;
    vec2 max;
};

// TODO: Support other variations such as position being the center so that we
// can support other alignments
inline Rectangle Rect(vec2 topLeft, f32 width, f32 height)
{
    Rectangle result = {topLeft, topLeft + Vec2(width, -height)};
    return result;
}

// Assume vertical for now
struct StackLayout
{
    f32 capacity;
    f32 size;
    f32 start;
    f32 padding;
};

struct imgui_State
{
    GameMemory *memory;
    VertexBuffer *vertexBuffer;
    mat4 orthographicProjection;
    Font *defaultFont;
    vec4 defaultColor;
    vec4 defaultHoverColor;
    vec4 defaultClickColor;
    vec4 defaultTextColor;
    HorizontalAlignment defaultHorizontalAlignment;
    VerticalAlignment defaultVerticalAlignment;
    GameInput *input;
    StackLayout stackLayout;
};

inline void imgui_BeginPanel(
    imgui_State *state, vec2 position, f32 width, f32 height, vec4 color)
{
    ui_DrawQuadArguments args = {};
    args.memory = state->memory;
    args.orthographicProjection = state->orthographicProjection;
    args.position = position;
    args.width = width;
    args.height = height;
    args.color = color;
    args.horizontalAlignment = state->defaultHorizontalAlignment;
    args.verticalAlignment = state->defaultVerticalAlignment;

    ui_DrawQuad(&args);

    state->stackLayout.start = position.y;
    state->stackLayout.capacity = height;
    state->stackLayout.padding = 8.0f;
}

inline void imgui_EndPanel(imgui_State *state)
{
    // TODO: Implement this
}

inline void imgui_Text(imgui_State *state, vec2 position, const char *text, vec4 color)
{
    StackLayout *layout = &state->stackLayout;
    f32 y = layout->start - layout->size - layout->padding;
    layout->size += state->defaultFont->height + layout->padding;

    ui_DrawStringArguments args = {};
    args.memory = state->memory;
    args.vertexBuffer = state->vertexBuffer;
    args.text = text;
    args.position = position + Vec2(0, y);
    args.color = color;
    args.font = state->defaultFont;
    args.orthographicProjection = state->orthographicProjection;
    args.horizontalAlignment = HorizontalAlign_Left;
    args.verticalAlignment = VerticalAlign_Top;

    ui_DrawString(&args);
}

inline void imgui_Button(imgui_State *state, vec2 position, const char *text)
{
    StackLayout *layout = &state->stackLayout;
    f32 y = layout->start - layout->size - layout->padding;
    layout->size += 22.0f + layout->padding; // No idea why 22.0f

    ui_DrawButtonArguments args = {};
    args.memory = state->memory;
    args.orthographicProjection = state->orthographicProjection;
    args.vertexBuffer = state->vertexBuffer;
    args.font = state->defaultFont;
    args.color = state->defaultColor;
    args.hoverColor = state->defaultHoverColor;
    args.clickColor = state->defaultClickColor;
    args.textColor = state->defaultTextColor;
    args.input = state->input;
    args.text = text;
    args.position = position + Vec2(0, y);

    ui_DrawButton(&args);
}





struct ui_DrawProgressBar
{
    GameMemory *memory;
    VertexBuffer *vertexBuffer;
    const char *text;
    vec2 position;
    f32 width;
    f32 height;
    vec4 color;
    vec4 bgColor;
    vec4 textColor;
    Font *font;
    mat4 orthographic;
    f32 percentage;
};

internal void DrawProgressBar(ui_DrawProgressBar *args)
{
    ui_DrawQuadArguments drawHealthBarBg = {};
    drawHealthBarBg.memory = args->memory;
    drawHealthBarBg.orthographicProjection = args->orthographic;
    drawHealthBarBg.position = args->position;
    drawHealthBarBg.width = args->width;
    drawHealthBarBg.height = args->height;
    drawHealthBarBg.color = args->bgColor;
    drawHealthBarBg.horizontalAlignment = HorizontalAlign_Left;
    drawHealthBarBg.verticalAlignment = VerticalAlign_Top;

    ui_DrawQuad(&drawHealthBarBg);

    ui_DrawQuadArguments drawHealthBar = {};
    drawHealthBar.memory = args->memory;
    drawHealthBar.orthographicProjection = args->orthographic;
    drawHealthBar.position = args->position;
    drawHealthBar.width = args->width * args->percentage;
    drawHealthBar.height = args->height;
    drawHealthBar.color = args->color;
    drawHealthBar.horizontalAlignment = HorizontalAlign_Left;
    drawHealthBar.verticalAlignment = VerticalAlign_Top;

    ui_DrawQuad(&drawHealthBar);

    ui_DrawStringArguments drawHealth = {};
    drawHealth.memory = args->memory;
    drawHealth.vertexBuffer = args->vertexBuffer;
    drawHealth.text = args->text;
    drawHealth.position = args->position + Vec2(args->width, -args->height) * 0.5f;
    drawHealth.color = args->textColor;
    drawHealth.font = args->font;
    drawHealth.orthographicProjection = args->orthographic;
    drawHealth.horizontalAlignment = HorizontalAlign_Center;
    drawHealth.verticalAlignment = VerticalAlign_Center;
    drawHealth.drawShadow = true;
    drawHealth.shadowOffset = Vec2(1, -1);
    drawHealth.shadowColor = Vec4(0, 0, 0, 1);
    ui_DrawString(&drawHealth);
}

struct LayoutState
{
    Rectangle stack[32];
    u32 length;
};

inline void PushRect(LayoutState *state, Rectangle rect)
{
    Assert(state->length < ArrayCount(state->stack));
    state->stack[state->length++] = rect;
}

inline void PopRect(LayoutState *state)
{
    Assert(state->length > 0);
    state->length--;
}

inline void InitLayout(
    LayoutState *state, f32 frameBufferWidth, f32 frameBufferHeight)
{
    Rectangle rect = {Vec2(0, 0), Vec2(frameBufferWidth, frameBufferHeight)};
    PushRect(state, rect);
}

inline Rectangle TopRect(LayoutState *state)
{
    Assert(state->length > 0);
    u32 idx = state->length - 1;
    return state->stack[idx];
}

inline vec2 GetBottomLeft(LayoutState *state)
{
    return TopRect(state).min;
}

inline vec2 GetTopLeft(LayoutState *state)
{
    Rectangle rect = TopRect(state);
    return Vec2(rect.min.x, rect.max.y);
}

inline vec2 GetTopCenter(LayoutState *state)
{
    Rectangle rect = TopRect(state);
    return Vec2((rect.max.x + rect.min.x) * 0.5f, rect.max.y);
}

inline vec2 GetCenterCenter(LayoutState *state)
{
    Rectangle rect = TopRect(state);
    return (rect.min + rect.max) * 0.5f;
}

inline f32 GetWidth(LayoutState *state)
{
    Rectangle rect = TopRect(state);
    return (rect.max.x - rect.min.x);
};

inline f32 GetHeight(LayoutState *state)
{
    Rectangle rect = TopRect(state);
    return (rect.max.y - rect.min.y);
}

struct im2_Theme
{
    vec4 buttonBgColor;
    vec4 buttonHoverColor;
    vec4 buttonClickColor;
    vec4 buttonTextColor;
};

struct im2_State
{
    LayoutState layoutState;
    GameMemory *memory;
    mat4 orthographicProjection;
    VertexBuffer *vertexBuffer;
    Font *font;
    vec2 mousePosition;
    GameInput *input;
    im2_Theme theme;
};

internal void im2_DrawPanel(
    im2_State *state, vec2 position, f32 width, f32 height, vec4 color)
{
    Assert(state->memory);

    RenderCommand_DrawMesh *drawQuad =
        AllocateRenderCommand(state->memory, RenderCommand_DrawMesh);
    drawQuad->mesh = Mesh_Quad;

    drawQuad->shader = Shader_ColorUI;
    AddShaderUniformValueVec4(drawQuad, UniformValue_Color, color);

    mat4 modelMatrix = Translate(Vec3(position.x, position.y, 0.0)) *
                       Scale(Vec3(width, height, 0.0f)) *
                       Translate(Vec3(1, -1, 0) * 0.5f);

    AddShaderUniformValueMat4(drawQuad, UniformValue_MvpMatrix,
        state->orthographicProjection * modelMatrix);

    Rectangle rect = {position - Vec2(0, height), position + Vec2(width, 0)};
    PushRect(&state->layoutState, rect);
}

internal void im2_DrawText(
    im2_State *state, vec2 position, vec4 color, const char *format, ...)
{
    Assert(state->memory);
    Assert(state->vertexBuffer);
    Assert(state->font);

    char text[4096];
    va_list args;
    va_start(args, format);
    i32 charCount = vsnprintf(text, sizeof(text), format, args);
    va_end(args);

    if (charCount > 0)
    {
        AddTextToBufferResult addedText =
            AddTextToBuffer(state->vertexBuffer, text, state->font, charCount);

        // TODO: Text alignment
        position.y -= state->font->height; // For default top-left alignment

#if 0
        if (args->drawShadow)
        {
            RenderCommand_DrawVertexBuffer *drawVertexBuffer =
                AllocateRenderCommand(memory, RenderCommand_DrawVertexBuffer);
            drawVertexBuffer->id = VertexBuffer_Text;
            drawVertexBuffer->shader = Shader_AlphaMapping;
            drawVertexBuffer->primitive = Primitive_Triangles;
            drawVertexBuffer->firstVertex = addedText.firstVertex;
            drawVertexBuffer->vertexCount = addedText.vertexCount;

            AddShaderUniformValueMat4(drawVertexBuffer, UniformValue_MvpMatrix,
                args->orthographicProjection *
                    WorldTransform(
                        Vec3(position + args->shadowOffset, 0.0f), Vec3(1.0f)));
            AddShaderUniformValueTexture(
                drawVertexBuffer, UniformValue_AlphaMap, args->font->texture);

            AddShaderUniformValueVec4(
                drawVertexBuffer, UniformValue_Color, args->shadowColor);
        }
#endif

        {
            RenderCommand_DrawVertexBuffer *drawVertexBuffer =
                AllocateRenderCommand(
                    state->memory, RenderCommand_DrawVertexBuffer);
            drawVertexBuffer->id = VertexBuffer_Text;
            drawVertexBuffer->shader = Shader_AlphaMapping;
            drawVertexBuffer->primitive = Primitive_Triangles;
            drawVertexBuffer->firstVertex = addedText.firstVertex;
            drawVertexBuffer->vertexCount = addedText.vertexCount;

            AddShaderUniformValueMat4(drawVertexBuffer, UniformValue_MvpMatrix,
                state->orthographicProjection *
                    WorldTransform(Vec3(position, 0.0f), Vec3(1.0f)));
            AddShaderUniformValueTexture(
                drawVertexBuffer, UniformValue_AlphaMap, state->font->texture);

            AddShaderUniformValueVec4(
                drawVertexBuffer, UniformValue_Color, color);
        }
    }
}

internal b32 im2_DrawButton(im2_State *state, vec2 position, f32 width,
    f32 height, const char *format, ...)
{
    Assert(state->memory);
    Assert(state->vertexBuffer);
    Assert(state->font);
    Assert(state->input);

    b32 result = false;

    char text[4096];
    va_list args;
    va_start(args, format);
    i32 charCount = vsnprintf(text, sizeof(text), format, args);
    va_end(args);

    // TODO: Make this configurable
    f32 horizontalPadding = 25.0f;
    f32 verticalPadding = 5.0f;

    rect2 rect = {};
    rect.min.x = position.x;
    rect.min.y = position.y - height - verticalPadding * 2.0f;
    rect.max.x = position.x + width + horizontalPadding * 2.0f;
    rect.max.y = position.y;

    im2_Theme *theme = &state->theme;
    vec4 color = theme->buttonBgColor;
    vec4 textColor = theme->buttonTextColor;

    GameInput *input = state->input;
    if (ContainsPoint(rect, state->mousePosition))
    {
        if (input->buttonStates[KEY_MOUSE_BUTTON_LEFT].isDown)
        {
            // Click
            color = theme->buttonClickColor;
        }
        else
        {
            if (input->buttonStates[KEY_MOUSE_BUTTON_LEFT].wasDown &&
                !input->buttonStates[KEY_MOUSE_BUTTON_LEFT].isDown)
            {
                // Process click
                result = true;
            }
            else
            {
                // Hover
                color = theme->buttonHoverColor;
            }
        }
    }

    im2_DrawPanel(state, GetTopLeft(&state->layoutState),
        width + 2.0f * horizontalPadding, height + 2.0f * verticalPadding,
        color);
    im2_DrawText(state,
        GetTopLeft(&state->layoutState) + Vec2(2, -2) +
            Vec2(horizontalPadding, -verticalPadding),
        textColor, text);

    return result;
}
