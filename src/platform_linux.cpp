#include <dlfcn.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <stdarg.h>

#define LINUX_GAME_LIB "./game.so"

struct Linux_State
{
    time_t gameCodeLastWriteTime;
    void *gameCodeLibrary;
};

global Linux_State g_PlatformState;

internal void Linux_FreeMemory(void *p);
internal void *Linux_AllocateMemory(size_t numBytes, size_t baseAddress = 0);

inline void Linux_LogMessage(const char *fmt, ...)
{
    char buffer[256];
    va_list args;
    va_start(args, fmt);
    vsnprintf(buffer, sizeof(buffer), fmt, args);
    va_end(args);
    puts(buffer);
}

internal void *Linux_AllocateMemory(size_t numBytes, size_t baseAddress)
{
    void *result = mmap((void *)baseAddress, numBytes, PROT_READ | PROT_WRITE,
                        MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    Assert(result != MAP_FAILED);

    return result;
}

internal void Linux_FreeMemory(void *p)
{
    munmap(p, 0);
}

internal DebugFreeFileMemory(Linux_FreeFileMemory)
{
    Linux_FreeMemory(fileResult.contents);
}

// NOTE: bytesToRead must equal the size of the file, if great we will enter an
// infinite loop.
internal bool ReadFile(int file, void *buf, int bytesToRead)
{
    while (bytesToRead)
    {
        int bytesRead = read(file, buf, bytesToRead);
        if (bytesRead == -1)
        {
            return false;
        }
        bytesToRead -= bytesRead;
        buf = (u8 *)buf + bytesRead;
    }
    return true;
}


internal DebugReadEntireFile(Linux_ReadEntireFile)
{
    DebugReadFileResult result = {};
    int file = open(path, O_RDONLY);
    if (file != -1)
    {
        struct stat fileStatus;
        if (fstat(file, &fileStatus) != -1)
        {
            result.size = SafeTruncateU64ToU32(fileStatus.st_size);
            // NOTE: Size + 1 to allow for null to be written if needed.
            result.contents = Linux_AllocateMemory(result.size + 1);
            if (result.contents)
            {
                if (!ReadFile(file, result.contents, result.size))
                {
                    LogMessage("Failed to read file %s", path);
                    Linux_FreeMemory(result.contents);
                    result.contents = nullptr;
                    result.size = 0;
                }
            }
            else
            {
                LogMessage("Failed to allocate %d bytes for file %s",
                          result.size, path);
                result.size = 0;
            }
        }
        else
        {
            LogMessage("Failed to read file size for file %s", path);
        }
        close(file);
    }
    else
    {
        LogMessage("Failed to open file %s", path);
    }
    return result;
}

internal b32 WriteFile(i32 file, void *buf, i32 bytesToWrite)
{
    while (bytesToWrite)
    {
        i32 bytesWritten = write(file, buf, bytesToWrite);
        if (bytesToWrite == -1)
        {
            return false;
        }
        bytesToWrite -= bytesWritten;
        buf = (u8 *)buf + bytesWritten;
    }
    return true;
}

internal DebugWriteEntireFile(Linux_WriteEntireFile)
{
    b32 result = false;
    i32 file = open(path, O_RDWR | O_CREAT | O_TRUNC,
        S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
    if (file != -1)
    {
        result = WriteFile(file, data, length);
        close(file);
    }
    return result;
}

internal time_t Linux_GetFileLastWriteTime(const char *path)
{
    struct stat attr;
    stat(path, &attr);
    return attr.st_mtime;
}

internal GameCode Linux_LoadGameCode(Linux_State *state, const char *libraryName)
{
    GameCode result = {};

    state->gameCodeLastWriteTime = Linux_GetFileLastWriteTime(libraryName);
    state->gameCodeLibrary = dlopen(libraryName, RTLD_NOW);
    if (state->gameCodeLibrary)
    {
        auto error = dlerror();
        if (error)
        {
            fprintf(stderr, "Open: %s\n", error);
        }
        result.update =
                (GameUpdateFunction *)dlsym(state->gameCodeLibrary, "GameUpdate");
        error = dlerror();
        if (error)
        {
            fprintf(stderr, "GameUpdate: %s\n", error);
        }

        result.serverUpdate = (GameServerUpdateFunction *)dlsym(
                state->gameCodeLibrary,
                "GameServerUpdate");

        result.getSoundSamples = (GameGetSoundSamplesFunction *)dlsym(
                state->gameCodeLibrary,
                "GameGetSoundSamples");
    }
    else
    {
        fprintf(stderr, "%s\n", dlerror());
    }

    return result;
}

internal void Linux_UnloadGameCode(Linux_State *state)
{
    if (state->gameCodeLibrary)
    {
        dlclose(state->gameCodeLibrary);
        state->gameCodeLibrary = 0;
        auto error = dlerror();
        if (error)
        {
            fprintf(stderr, "Error while closing game code library: %s\n",
                    error);
        }
    }
}

internal void Linux_InitializeGameMemory(GameMemory *memory,
        u64 persistentStorageSize, u64 temporaryStorageSize, u64 storageLocation)
{
    memory->persistentStorageSize = persistentStorageSize;
    memory->temporaryStorageSize = temporaryStorageSize;

    u64 totalStorageSize =
        memory->persistentStorageSize + memory->temporaryStorageSize;

    void *gameMemory = Linux_AllocateMemory(totalStorageSize, storageLocation);
    Assert(gameMemory);

    memory->persistentStorage = gameMemory;
    memory->temporaryStorage =
        (u8 *)memory->persistentStorage + memory->persistentStorageSize;
    memory->readEntireFile = &Linux_ReadEntireFile;
    memory->writeEntireFile = &Linux_WriteEntireFile;
    memory->freeFileMemory = &Linux_FreeFileMemory;
    memory->logMessage = &Linux_LogMessage;
    memory->setCursorMode = &SetCursorMode;
}

internal b32 Linux_Init(GameMemory *memory, GameCode *gameCode)
{
    LogMessage = &Linux_LogMessage;

    *gameCode = Linux_LoadGameCode(&g_PlatformState, LINUX_GAME_LIB);
    Assert(gameCode->update && gameCode->serverUpdate &&gameCode->getSoundSamples);

    u64 storageLocation = 0;
#ifdef DEBUG_FIXED_ADDRESSES
    storageLocation = Terabytes(2);
#endif

    Linux_InitializeGameMemory(memory, MAX_GAME_PERSISTENT_MEMORY, MAX_GAME_TEMPORARY_MEMORY,
            storageLocation);

    return true;
}

internal b32 Linux_HandleGameCodeReload(GameCode *gameCode)
{
    b32 result = false;
    time_t newWriteTime = Linux_GetFileLastWriteTime(LINUX_GAME_LIB);
    if (newWriteTime != g_PlatformState.gameCodeLastWriteTime)
    {
        Linux_UnloadGameCode(&g_PlatformState);
        ClearToZero(gameCode, sizeof(GameCode));
        *gameCode = Linux_LoadGameCode(&g_PlatformState, LINUX_GAME_LIB);
        result = (gameCode->update && gameCode->serverUpdate &&
                  gameCode->getSoundSamples);
    }

    return result;
}
