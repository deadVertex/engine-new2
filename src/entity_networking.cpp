struct NetEntityClient
{
    EntityId entities[MAX_NET_ENTITIES];
    NetEntityId netEntityIds[MAX_NET_ENTITIES];
    f32 priorities[MAX_NET_ENTITIES];
    u32 entityCount;
};

struct NetEntityServer
{
    NetEntityId nextNetEntityId;
};

#define DEFAULT_NET_ENTITY_PRIORITY 0.5f

inline b32 LinearSearch(NetEntityId *array, u32 count, NetEntityId element, u32 *index)
{
    b32 result = false;
    for (u32 i = 0; i < count; ++i)
    {
        if (array[i] == element)
        {
            *index = i;
            result = true;
            break;
        }
    }
    return result;
}

internal void RemoveNetEntityIdsFromClient(
    NetEntityClient *client, NetEntityId *netEntityIds, u32 count)
{
    for (u32 i = 0; i < count; ++i)
    {
        NetEntityId netEntityId = netEntityIds[i];

        u32 index = 0;
        if (LinearSearch(
                client->netEntityIds, client->entityCount, netEntityId, &index))
        {
            u32 last = client->entityCount - 1;
            client->netEntityIds[index] = client->netEntityIds[last];
            client->netEntityIds[last] = 0;
            client->entities[index] = client->entities[last];
            client->entities[last] = NULL_ENTITY;
            client->entityCount -= 1;
        }
    }
}

inline b32 DoesClientHaveEntity(NetEntityClient *client, EntityId id)
{
    for (u32 i = 0; i < client->entityCount; ++i)
    {
        if (client->entities[i] == id)
        {
            return true;
        }
    }

    return false;
}

internal u32 GetEntitiesToCreate(ecs_EntityWorld *world, NetEntityClient *client,
    EntityId *entitiesToCreate, u32 maxEntities)
{
    EntityId entities[MAX_ENTITIES];
    u32 entityCount = ecs_GetEntitiesWithComponent(
        world, NetEntityIdComponentId, entities, ArrayCount(entities));

    u32 createdCount = 0;
    for (u32 entityIdx = 0; entityIdx < entityCount; ++entityIdx)
    {
        EntityId entityId = entities[entityIdx];

        if (!DoesClientHaveEntity(client, entityId))
        {
            // Need to create entity
            Assert(createdCount < maxEntities);
            entitiesToCreate[createdCount++] = entityId;
        }
    }

    return createdCount;
}

internal void OnEntitiesCreated(
    NetEntityClient *client, EntityId *entitiesCreated, NetEntityId *netEntityIds, u32 count)
{
    Assert(client->entityCount + count < ArrayCount(client->entities));
    if (count > 0)
    {
        CopyMemory(client->entities + client->entityCount,
                entitiesCreated, count * sizeof(entitiesCreated[0]));
        CopyMemory(client->netEntityIds + client->entityCount,
                netEntityIds, count * sizeof(netEntityIds[0]));
        for (u32 i = 0; i < count; ++i)
        {
            u32 idx = i + client->entityCount;
            client->priorities[idx] = DEFAULT_NET_ENTITY_PRIORITY;
        }
        client->entityCount += count;
    }
}

// FIXME: Need a more efficient way to search for entities by component value,
// something that isn't N^2
internal void MapNetEntityIdToEntityId(ecs_EntityWorld *world, NetEntityId *netEntityIds,
        u32 count, EntityId *entityIds)
{
    EntityId entities[MAX_ENTITIES];
    u32 entityCount = ecs_GetEntitiesWithComponent(
        world, NetEntityIdComponentId, entities, ArrayCount(entities));

    NetEntityId storedNetEntityIds[MAX_ENTITIES];
    ecs_GetComponent(world, NetEntityIdComponentId, entities, entityCount,
        storedNetEntityIds, sizeof(storedNetEntityIds[0]));

    for (u32 netEntityIdx = 0; netEntityIdx < count; ++netEntityIdx)
    {
        Assert(netEntityIds[netEntityIdx] != 0);

        b32 found = false;
        for (u32 entityIdx = 0; entityIdx < entityCount; ++entityIdx)
        {
            NetEntityId value = storedNetEntityIds[entityIdx];
            if (value == netEntityIds[netEntityIdx])
            {
                entityIds[netEntityIdx] = entities[entityIdx];
                found = true;
                break;
            }
        }
        Assert(found);
    }
}

inline void MapEntityIdToNetEntityId(ecs_EntityWorld *world, EntityId *entityIds,
    u32 count, NetEntityId *netEntityIds)
{
    ecs_GetComponent(world, NetEntityIdComponentId, entityIds, count,
        netEntityIds, sizeof(netEntityIds[0]));

    for (u32 i = 0; i < count; ++i)
    {
        Assert(netEntityIds[i] != 0);
    }
}

internal u32 AllocateNetEntityIdsSystem(ecs_EntityWorld *world, NetEntityServer *server)
{
    u32 count = 0;

    EntityId entities[MAX_ENTITIES];
    u32 entityCount = ecs_GetEntitiesWithComponent(
        world, NetEntityIdComponentId, entities, ArrayCount(entities));

    NetEntityId netEntityIds[MAX_ENTITIES];
    ecs_GetComponent(world, NetEntityIdComponentId, entities, entityCount,
        netEntityIds, sizeof(netEntityIds[0]));

    for (u32 entityIdx = 0; entityIdx < entityCount; ++entityIdx)
    {
        if (netEntityIds[entityIdx] == 0)
        {
            netEntityIds[entityIdx] = ++server->nextNetEntityId;
            ecs_SetComponent(world, NetEntityIdComponentId,
                entities + entityIdx, 1, netEntityIds + entityIdx,
                sizeof(netEntityIds[0]));
            count++;
        }
    }

    return count;
}
