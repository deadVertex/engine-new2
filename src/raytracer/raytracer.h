struct Plane
{
    vec3 normal;
    f32 distance;
    u32 materialIndex;
};

struct Sphere
{
    vec3 center;
    f32 radius;
    u32 materialIndex;
};

struct Aabb
{
    vec3 min;
    vec3 max;
    u32 materialIndex;
};

struct Material
{
    vec3 emitColor;
    vec3 color;
    f32 roughness;
};

struct Scene
{
    u32 materialCount;
    Material *materials;
    u32 planeCount;
    Plane *planes;
    u32 sphereCount;
    Sphere *spheres;
    u32 aabbCount;
    Aabb *aabbs;

    u32 samplesPerPixel;
    u32 maxBounces;
};

struct Image
{
    u32 width;
    u32 height;
    u32 *pixels;
};

struct WorkOrder
{
    Image *image;
    Scene *scene;
    u32 minX;
    u32 minY;
    u32 onePastMaxX;
    u32 onePastMaxY;
};

struct WorkQueue
{
    WorkOrder *contents;
    volatile u32 length;
    volatile u32 head;
    volatile u32 tilesProcessed;

    volatile u64 usedBounceCount;
    volatile u64 totalBounceCount;
};
