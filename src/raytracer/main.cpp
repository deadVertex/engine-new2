#include "platform.h"

#include "math_lib.h"
#include "simd.h"
#include "raytracer/raytracer.h"

#include <stdio.h>
#include <time.h>

#ifdef PLATFORM_WINDOWS
#include <Windows.h>
#else
#include <pthread.h>
#include <sys/time.h>
#endif

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

inline f32 RayIntersectPlane(vec3 normal, f32 d, vec3 rayOrigin, vec3 rayDirection)
{
    f32 t = F32_MAX;
    f32 denom = Dot(rayDirection, normal);
    if (Abs(denom) > EPSILON)
    {
        t = (d - Dot(normal, rayOrigin)) / denom;
    }

    return t;
}

inline f32 RayIntersectSphere(vec3 center, f32 radius, vec3 rayOrigin, vec3 rayDirection)
{
    vec3 m = rayOrigin - center; // Use sphere center as origin

    f32 a = Dot(rayDirection, rayDirection);
    f32 b = 2.0f * Dot(m, rayDirection);
    f32 c = Dot(m, m) - radius * radius;

    f32 d = b*b - 4.0f * a * c; // Discriminant

    f32 t = F32_MAX;
    if (d > 0.0f)
    {
        f32 denom = 2.0f * a;
        f32 t0 = (-b - Sqrt(d)) / denom;
        f32 t1 = (-b + Sqrt(d)) / denom;

        t = t0; // Pick the entry point
    }

    return t;
}

inline f32 RayIntersectAabb(
    vec3 boxMin, vec3 boxMax, vec3 start, vec3 dir, vec3 *hitNormal)
{
    f32 tmin = -1.0f;
    f32 tmax = F32_MAX;

    b32 negateNormal = false;
    u32 normalIdx = 0;
    for (u32 axis = 0; axis < 3; ++axis)
    {
        f32 d = dir.data[axis];
        f32 p = start.data[axis];
        f32 min = boxMin.data[axis];
        f32 max = boxMax.data[axis];

        if (Abs(d) < EPSILON)
        {
            if (p < min || p > max)
            {
                tmin = -1.0f;
                break;
            }
        }
        else
        {
            f32 a = (min - p) / d;
            f32 b = (max - p) / d;

            f32 t0 = Min(a, b);
            f32 t1 = Max(a, b);

            if (t0 > tmin)
            {
                negateNormal = (d >= 0.0f);
                normalIdx = axis;
            }

            tmin = Max(tmin, t0);
            tmax = Min(tmax, t1);

            if (tmin > tmax)
            {
                tmin = -1.0f;
                break;
            }
        }
    }

    //if (tmin >= 0.0f && tmin < 1.0f)
    //{
        vec3 normal = Vec3(0, 0, 0);
        normal.data[normalIdx] = negateNormal ? -1.0f : 1.0f;

        *hitNormal = normal;
    //}
    //else
    //{
        //tmin = -1.0f;
    //}

    return tmin;
}

inline u32 LockedAddAndReturnPreviousValue(volatile u32 *value, u32 addend)
{
#ifdef PLATFORM_WINDOWS
    u32 result = InterlockedExchangeAdd(value, addend);
#else
    u32 result = __atomic_fetch_add(value, addend, __ATOMIC_RELAXED);
#endif
    return result;
}

inline u64 LockedAddAndReturnPreviousValue(volatile u64 *value, u64 addend)
{
#ifdef PLATFORM_WINDOWS
    u64 result = InterlockedExchangeAdd64((volatile LONG64*)value, addend);
#else
    u32 result = __atomic_fetch_add(value, addend, __ATOMIC_RELAXED);
#endif
    return result;
}

inline f32 LinearToSRGB(f32 x)
{
    if (x > 0.0031308f)
    {
        x = 1.055f * (Pow(x, (1.0f / 2.4f))) - 0.055f;
    }
    else
    {
        x = 12.92f * x;
    }

    return x;
}

internal void RaytraceRegion(Image image, Scene *scene, u32 minX, u32 minY,
    u32 onePastMaxX, u32 onePastMaxY, u64 *usedBounceCount, u64 *totalBounceCount,
    RandomNumberGenerator *rng)
{
    // TODO: Move this out into a camera structure
    simd_vec3 cameraPosition = SIMD_VEC3(0, 1.5, 4);
    simd_vec3 cameraForward = SafeNormalize(SIMD_VEC3(0, 0.4, 1));
    simd_vec3 cameraRight = SafeNormalize(Cross(SIMD_VEC3(0, 1, 0), cameraForward));
    simd_vec3 cameraUp = SafeNormalize(Cross(cameraForward, cameraRight));

    f32 filmWidth = 1.0f;
    f32 filmHeight = 1.0f;
    if (image.width > image.height)
    {
        filmHeight = (f32)image.height / (f32)image.width;
    }
    else if (image.width < image.height)
    {
        filmWidth = (f32)image.width / (f32)image.height;
    }

    simd_f32 filmDistance = SIMD_F32(1.0f);
    simd_vec3 filmCenter = -cameraForward * filmDistance + cameraPosition;

    f32 pixelWidth = 1.0f / (f32)image.width;
    f32 pixelHeight = 1.0f / (f32)image.height;

    simd_f32 halfPixelWidth = SIMD_F32(0.5f * pixelWidth);
    simd_f32 halfPixelHeight = SIMD_F32(0.5f * pixelHeight);

    simd_f32 halfFilmWidth = SIMD_F32(filmWidth * 0.5f);
    simd_f32 halfFilmHeight = SIMD_F32(filmHeight * 0.5f);

    for (u32 y = minY; y < onePastMaxY; ++y)
    {
        for (u32 x = minX; x < onePastMaxX; ++x)
        {
            simd_f32 filmX =
                SIMD_F32(-1.0f + 2.0f * ((f32)x / (f32)image.width));

            simd_f32 filmY =
                SIMD_F32(-1.0f + 2.0f * (1.0f - ((f32)y / (f32)image.height)));

            vec3 outputColor = {};

            u32 laneWidth = SIMD_LANE_WIDTH;
            Assert(scene->samplesPerPixel % laneWidth == 0);

            u32 samplesPerLane = scene->samplesPerPixel / laneWidth;
            simd_f32 contribution = SIMD_F32(1.0f / (f32)scene->samplesPerPixel);

            for (u32 sampleIndex = 0; sampleIndex < samplesPerLane;
                 ++sampleIndex)
            {
                simd_u32 laneMask = SIMD_U32(1);

                simd_f32 offsetX = filmX + halfPixelWidth +
                                   halfPixelWidth * RandomBilateral(rng);

                simd_f32 offsetY = filmY + halfPixelHeight +
                                   halfPixelHeight * RandomBilateral(rng);

                simd_vec3 filmP = halfFilmWidth * offsetX * cameraRight +
                                  halfFilmHeight * offsetY * cameraUp +
                                  filmCenter;

                simd_vec3 rayOrigin = cameraPosition;
                simd_vec3 rayDirection = SafeNormalize(filmP - cameraPosition);

                simd_vec3 sampleColor = {};
                simd_vec3 attenuation = SIMD_VEC3(1, 1, 1);

                for (u32 bounceCount = 0; bounceCount < scene->maxBounces; ++bounceCount)
                {
                    simd_u32 laneIncrement = SIMD_U32(1) & laneMask;
                    *usedBounceCount += HorizontalAdd(laneIncrement);
                    *totalBounceCount += laneWidth;

                    simd_u32 materialHitIndex = {};
                    simd_f32 tmin = SIMD_F32(F32_MAX);
                    simd_vec3 hitNormal = {};

                    // TODO: Pull out all SIMD_* functions for constants out of the loop
                    for (u32 planeIndex = 0; planeIndex < scene->planeCount; ++planeIndex)
                    {
                        // Fetch data
                        Plane plane = scene->planes[planeIndex];
                        simd_u32 planeMaterialIndex = SIMD_U32(plane.materialIndex);
                        simd_vec3 planeNormal = SIMD_VEC3(plane.normal);
                        simd_f32 planeDistance = SIMD_F32(plane.distance);

                        // Ray plane intersection test
                        simd_f32 denom = Dot(rayDirection, planeNormal);
                        simd_u32 denomMask = (Abs(denom) > SIMD_F32(EPSILON));
                        simd_f32 t = (planeDistance - Dot(planeNormal, rayOrigin)) / denom;
                        simd_u32 tMask = ((t > SIMD_F32(0.0f)) & (t < tmin));
                        simd_u32 hitMask = denomMask & tMask;

                        // Store result
                        ConditionalAssign(&materialHitIndex, planeMaterialIndex, hitMask);
                        ConditionalAssign(&tmin, t, hitMask);
                        ConditionalAssign(&hitNormal, planeNormal, hitMask);
                    }

                    for (u32 sphereIndex = 0; sphereIndex < scene->sphereCount; ++sphereIndex)
                    {
                        Sphere sphere = scene->spheres[sphereIndex];
                        simd_vec3 sphereCenter = SIMD_VEC3(sphere.center);
                        simd_f32 sphereRadius = SIMD_F32(sphere.radius);
                        simd_u32 sphereMaterialIndex = SIMD_U32(sphere.materialIndex);

                        simd_vec3 m = rayOrigin - sphereCenter; // Use sphere center as origin
                        simd_f32 a = Dot(rayDirection, rayDirection);
                        simd_f32 b = SIMD_F32(2.0f) * Dot(m, rayDirection);
                        simd_f32 c = Dot(m, m) - sphereRadius * sphereRadius;

                        simd_f32 d = b*b - SIMD_F32(4.0f) * a * c; // Discriminant
                        simd_u32 dMask = (d > SIMD_F32(0.0f));

                        simd_f32 denom = SIMD_F32(2.0f) * a;
                        simd_f32 t0 = (-b - Sqrt(d)) / denom;
                        simd_f32 t1 = (-b + Sqrt(d)) / denom;
                        simd_f32 t = t0;
                        simd_u32 pickMask = ((t1 > SIMD_F32(0.0f)) & (t1 < t0));
                        ConditionalAssign(&t, t1, pickMask); // Not sure if we need to do this?

                        simd_vec3 p = rayOrigin + t * rayDirection;
                        simd_vec3 normal = SafeNormalize(p - sphereCenter);

                        simd_u32 tMask = ((t > SIMD_F32(0.0f)) & (t < tmin));
                        simd_u32 hitMask = (tMask & dMask);

                        ConditionalAssign(&materialHitIndex, sphereMaterialIndex, hitMask);
                        ConditionalAssign(&tmin, t, hitMask);
                        ConditionalAssign(&hitNormal, normal , hitMask);
                    }

                    for (u32 aabbIndex = 0; aabbIndex < scene->aabbCount; ++aabbIndex)
                    {
                        Aabb aabb = scene->aabbs[aabbIndex];
                        simd_vec3 aabbMin = SIMD_VEC3(aabb.min);
                        simd_vec3 aabbMax = SIMD_VEC3(aabb.max);
                        simd_u32 aabbMaterialIndex = SIMD_U32(aabb.materialIndex);

                        simd_f32 aabbTmin = SIMD_F32(-1.0f);
                        simd_f32 aabbTmax = SIMD_F32(F32_MAX);
                        simd_u32 negateNormal = SIMD_U32(0);
                        simd_u32 normalAxis = SIMD_U32(0);
                        for (u32 axis = 0; axis < 3; ++axis)
                        {
                            simd_f32 d = rayDirection.data[axis];
                            simd_f32 p = rayOrigin.data[axis];
                            simd_f32 min = aabbMin.data[axis];
                            simd_f32 max = aabbMax.data[axis];

                            // FIXME: Handle case when ray direction is parallel!
                            //simd_u32 parallelToAxisMask = (Abs(d) < SIMD_F32(EPSILON));
                            //simd_u32 doesNotContainOriginMask = ((p < min) || (p > max));

                            simd_f32 a = (min - p) / d;
                            simd_f32 b = (max - p) / d;

                            simd_f32 t0 = Min(a, b);
                            simd_f32 t1 = Max(a, b);

                            simd_u32 normalMask = (t0 > aabbTmin);
                            ConditionalAssign(&normalAxis, SIMD_U32(axis), normalMask);

                            simd_u32 positiveDValue = (d > SIMD_F32(0.0f));
                            ConditionalAssign(&negateNormal, positiveDValue, normalMask);

                            aabbTmin = Max(aabbTmin, t0);
                            aabbTmax = Min(aabbTmax, t1);

                        }

                        simd_u32 hitMask = (aabbTmin < aabbTmax) &
                                           (aabbTmin > SIMD_F32(0.0f)) &
                                           (aabbTmin < tmin);

                        ConditionalAssign(&materialHitIndex, aabbMaterialIndex, hitMask);
                        ConditionalAssign(&tmin, aabbTmin, hitMask);

                        simd_vec3 normal = {};
                        ConditionalAssign(&normal, SIMD_VEC3(1, 0, 0), (normalAxis == SIMD_U32(0)));
                        ConditionalAssign(&normal, SIMD_VEC3(0, 1, 0), (normalAxis == SIMD_U32(1)));
                        ConditionalAssign(&normal, SIMD_VEC3(0, 0, 1), (normalAxis == SIMD_U32(2)));
                        simd_vec3 negatedNormal = normal * SIMD_F32(-1.0f);
                        ConditionalAssign(&normal, negatedNormal, negateNormal);

                        ConditionalAssign(&hitNormal, normal , hitMask);
                    }

                    // FIXME: Should use != 0 here as > may intrinsic may be for signed integers
                    laneMask = laneMask & (materialHitIndex > SIMD_U32(0));

                    simd_vec3 materialEmitColor = GatherVec3(
                        scene->materials, materialHitIndex, emitColor);

                    simd_vec3 materialColor =
                        GatherVec3(scene->materials, materialHitIndex, color);

                    simd_f32 materialRoughness = GatherF32(
                        scene->materials, materialHitIndex, roughness);

                    sampleColor = sampleColor + Hadamard(attenuation, materialEmitColor);

                    // Directional light
                    simd_vec3 lightDir = SIMD_VEC3(Normalize(Vec3(-0.2, -0.5, 0.5)));
                    simd_vec3 lightColor = SIMD_VEC3(Vec3(1.0, 0.7, 0.5));
                    simd_f32 lightNDotL = Max(Dot(-rayDirection, lightDir), SIMD_F32(0.0f));

                    simd_vec3 lightContrib = Hadamard(attenuation, lightColor) * lightNDotL;

                    ConditionalAssign(&lightContrib, SIMD_VEC3(0, 0, 0),
                        (materialHitIndex > SIMD_U32(0)));
                    sampleColor = sampleColor + lightContrib;


                    simd_f32 nDotL = Max(Dot(hitNormal, -rayDirection), SIMD_F32(0.0f));
                    attenuation = Hadamard(attenuation, materialColor) * nDotL;

                    simd_vec3 reflection =
                        rayDirection - SIMD_F32(2.0f) *
                                           Dot(rayDirection, hitNormal) *
                                           hitNormal;
                    simd_vec3 peturb = SIMD_VEC3(RandomBilateral(rng),
                        RandomBilateral(rng), RandomBilateral(rng));

                    simd_vec3 diffuseBounce = SafeNormalize(hitNormal + peturb);

                    simd_vec3 hitPoint = rayOrigin + rayDirection * tmin;
                    rayOrigin = hitPoint + hitNormal * SIMD_F32(EPSILON);
                    rayDirection =
                        Lerp(reflection, diffuseBounce, materialRoughness);




                    if (AllZero(laneMask))
                    {
                        break;
                    }
                }

                outputColor += HorizontalAdd(sampleColor * contribution);
            }

            outputColor.x = LinearToSRGB(outputColor.x);
            outputColor.y = LinearToSRGB(outputColor.y);
            outputColor.z = LinearToSRGB(outputColor.z);

            outputColor *= 255.0f;
            u32 bgra = (0xFF000000 | ((u32)outputColor.z) << 16 |
                    ((u32)outputColor.y) << 8) | (u32)outputColor.x;

            image.pixels[y * image.width + x] = bgra;
        }
    }
}

internal Image AllocateImage(u32 width, u32 height)
{
    Image result = {};
    result.width = width;
    result.height = height;
    result.pixels = (u32*)malloc(sizeof(u32) * width * height);

    return result;
}

internal b32 ProcessWorkOrder(WorkQueue *queue)
{
    u32 head = LockedAddAndReturnPreviousValue(&queue->head, 1);

    if (head < queue->length)
    {
        //printf("Processing work order %u/%u\n", head, queue->length);
        WorkOrder *order = queue->contents + head;

        // TODO: Better source of entropy
        RandomNumberGenerator rng = {
            SIMD_U32(345982321 + order->minX * 3451 ^ order->minY * 93473,
                76345 + order->minX * 8823 ^ order->minY * 234998,
                134597 + order->minX * 23483434 ^ order->minY * 34350,
                845235 + order->minX * 134587 ^ order->minY * 9827435,
                45983 + order->minX * 945803 ^ order->minY * 485345345,
                34534056 + order->minX * 8309485 ^ order->minY * 345345345,
                237344 + order->minX * 360934572 ^ order->minY * 34345345,
                34597237 + order->minX * 459845678 ^ order->minY * 4564345)};

        u64 usedBounceCount = 0;
        u64 totalBounceCount = 0;
        RaytraceRegion(*order->image, order->scene, order->minX, order->minY,
            order->onePastMaxX, order->onePastMaxY, &usedBounceCount,
            &totalBounceCount, &rng);
        LockedAddAndReturnPreviousValue(&queue->usedBounceCount, usedBounceCount);
        LockedAddAndReturnPreviousValue(&queue->totalBounceCount, totalBounceCount);
        LockedAddAndReturnPreviousValue(&queue->tilesProcessed, 1);
        return true;
    }

    return false;
}

internal void* WorkerThread(void *data)
{
    WorkQueue *queue = (WorkQueue *)data;
    while (ProcessWorkOrder(queue))
    {
    }

    return NULL;
}

#ifdef PLATFORM_WINDOWS
u64 GetTimeStamp()
{
    clock_t result = clock();
    return (u64)result;
}
#else
u64 GetTimeStamp()
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return tv.tv_sec * (u64)1000000 + tv.tv_usec;
}
#endif

#ifdef PLATFORM_WINDOWS
DWORD Win32_ThreadProc(void *data)
{
    WorkerThread(data);
    return 0;
}
#endif

internal void CreateWorkerThread(void *data)
{
#ifdef PLATFORM_WINDOWS
    CreateThread(NULL, 0, &Win32_ThreadProc, data, 0, NULL);
#else
    pthread_t thread;
    pthread_create(&thread, NULL, WorkerThread, data);
#endif
}

int main(int argc, char **argv)
{
    Image image = AllocateImage(1280, 720);

    Material materials[8] = {};
    materials[0].emitColor = Vec3(0.3, 0.3, 1.0);
    materials[1].color = Vec3(0.6, 0.6, 0.6);
    materials[1].roughness = 0.8f;
    materials[2].color = Vec3(0.3, 1.0, 0.3);
    materials[2].roughness = 0.2f;
    materials[3].color = Vec3(0.98, 0.91, 0.86);
    materials[3].roughness = 0.05f;
    materials[4].color = Vec3(1.0, 0.8, 0.3);
    materials[4].roughness = 0.9f;
    materials[5].emitColor = Vec3(1.0, 0.8, 0.2);
    materials[6].color = Vec3(0.3, 0.6, 1.0);
    materials[6].roughness = 0.98f;
    materials[7].color = Vec3(1.0, 0.3, 0.2);
    materials[7].roughness = 0.98f;

    Plane plane = {};
    plane.normal = Vec3(0, 1, 0);
    plane.distance = 0;
    plane.materialIndex = 1;

    Sphere spheres[4] = {};
    spheres[0].center = Vec3(0.8, 0.2, -5);
    spheres[0].radius = 1.0f;
    spheres[0].materialIndex = 2;
    spheres[1].center = Vec3(-1, 0.5, -0.5);
    spheres[1].radius = 0.8f;
    spheres[1].materialIndex = 3;
    spheres[2].center = Vec3(1.1, 0.4, -0.2);
    spheres[2].radius = 0.5f;
    spheres[2].materialIndex = 4;
    spheres[3].center = Vec3(0.5, 0.2, 0.5);
    spheres[3].radius = 0.2f;
    spheres[3].materialIndex = 5;

    vec3 aabbCenter = Vec3(-1, 0.2, 0.8);
    vec3 aabbHalfDim = Vec3(0.2, 0.2, 0.2);
    Aabb aabbs[2] = {};
    aabbs[0].min = aabbCenter - aabbHalfDim;
    aabbs[0].max = aabbCenter + aabbHalfDim;
    aabbs[0].materialIndex = 6;

    aabbCenter += Vec3(0.6f, 0.0f, 0.15f);
    aabbs[1].min = aabbCenter - aabbHalfDim;
    aabbs[1].max = aabbCenter + aabbHalfDim;
    aabbs[1].materialIndex = 7;

    Scene scene = {};
    scene.planeCount = 1;
    scene.planes = &plane;
    scene.sphereCount = ArrayCount(spheres);
    scene.spheres = spheres;
    scene.aabbCount = ArrayCount(aabbs);
    scene.aabbs = aabbs;
    scene.materialCount = ArrayCount(materials);
    scene.materials = materials;
    scene.samplesPerPixel = 16;
    scene.maxBounces = 8;

    u32 tileWidth = 64;
    u32 tileHeight = 64;
    u32 maxThreads = 16;
    printf("Configuration:\n");
    printf("\tImage width: %u\n\tImage height: %u\n\tSamples per pixel: "
           "%u\n\tMax "
           "bounces: %u\n\tMax threads: %u\n\tSIMD lane width: %u\n\tTile "
           "width: %u\n\tTile height: %u\n",
        image.width, image.height, scene.samplesPerPixel, scene.maxBounces,
        maxThreads, SIMD_LANE_WIDTH, tileWidth, tileHeight);

    printf("\n\n");



    u32 tileCountX = (image.width + tileWidth - 1) / tileWidth;
    u32 tileCountY = (image.height + tileHeight - 1) / tileHeight;

    WorkQueue queue = {};
    queue.contents =
        (WorkOrder *)malloc(sizeof(WorkOrder) * tileCountX * tileCountY);

    for (u32 tileY = 0; tileY < tileCountY; ++tileY)
    {
        for (u32 tileX = 0; tileX < tileCountX; ++tileX)
        {
            Assert(queue.length < tileCountX * tileCountY);
            WorkOrder *order = queue.contents + queue.length++;

            order->scene = &scene;
            order->image = &image;
            order->minX = tileX * tileWidth;
            order->minY = tileY * tileHeight;
            order->onePastMaxX = MinU(image.width, order->minX + tileWidth);
            order->onePastMaxY = MinU(image.height, order->minY + tileHeight);
        }
    }

    LockedAddAndReturnPreviousValue(&queue.length, 0);

    u64 startTime = GetTimeStamp();

    for (u32 threadCount = 1; threadCount < maxThreads; ++threadCount)
    {
        CreateWorkerThread(&queue);
    }

    u64 threadCreationTime = GetTimeStamp();
    u64 timeSpentCreatingThreads = threadCreationTime - startTime;
    printf("Time spent creating threads %f seconds\n",
        (f64)timeSpentCreatingThreads / 1000000.0);

    u32 totalTileCount = tileCountX * tileCountY;
    while (queue.tilesProcessed < totalTileCount)
    {
        // Additional check as ProcessWorkOrder increments queue.head
        if (queue.head < queue.length)
        {
            ProcessWorkOrder(&queue);
        }

        f32 percentage = (f32)queue.tilesProcessed / (f32)totalTileCount;
#ifdef PLATFORM_WINDOWS
        printf("\rPROGRESS: %u%%\tRAYS TRACED: %llu", (u32)(percentage * 100.0f),
                queue.usedBounceCount);
#else
        printf("\rPROGRESS: %u%%\tRAYS TRACED: %lu", (u32)(percentage * 100.0f),
                queue.usedBounceCount);
#endif
        fflush(stdout);
    }

    u64 endTime = GetTimeStamp();

    f32 percentage = (f32)queue.tilesProcessed / (f32)totalTileCount;
#ifdef PLATFORM_WINDOWS
    printf("\rPROGRESS: %u%%\tRAYS TRACED: %llu", (u32)(percentage * 100.0f),
            queue.usedBounceCount);
#else
    printf("\rPROGRESS: %u%%\tRAYS TRACED: %lu", (u32)(percentage * 100.0f),
            queue.usedBounceCount);
#endif
    printf("\n");

    u64 elapsedTime = endTime - startTime;
#ifdef PLATFORM_WINDOWS
    f64 denom = (f64)CLOCKS_PER_SEC;
#else
    f64 denom = 1000000.0;
#endif
    f64 secondsElapsed = (f64)elapsedTime / denom;
    printf("Time taken: %f seconds\n", secondsElapsed);
    printf("Processed %u/%u tiles\n", queue.tilesProcessed, tileCountX * tileCountY);
    printf("Milliseconds per bounce %fms\n",
        (secondsElapsed * 1000.0) / (f64)queue.usedBounceCount);

    f64 utilisation = (f64)queue.usedBounceCount / (f64)queue.totalBounceCount;
#ifdef PLATFORM_WINDOWS
    printf("Total computed bounces: %llu,\t SIMD utilisation: %.02f%%\n",
            queue.totalBounceCount, utilisation);
#else
    printf("Total computed bounces: %lu,\t SIMD utilisation: %.02f%%\n",
            queue.totalBounceCount, 100.0 * utilisation);

#endif

    if (!stbi_write_bmp("output.bmp", image.width, image.height, 4, image.pixels))
    {
        printf("Failed to write image!\n");
    }

    return 0;
}
