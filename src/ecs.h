#pragma once

#define MAX_ENTITIES 1024
#define MAX_COMPONENT_REMOVED_EVENTS 1024
#define NULL_ENTITY 0
typedef u32 EntityId;

struct ComponentMetaData
{
    u32 id;
    const char *name;
    u32 dataType;
};

struct ComponentValue
{
    u32 dataType;
    union
    {
        u32 u;
        f32 f;
        vec3 v3;
        quat q;
    } value;
};

struct EntityPrefab
{
    const char *name;
    u32 componentIds[20];
    ComponentValue componentValues[20];
    u32 count;
};

struct ecs_ComponentRemovedEvent
{
    EntityId entityId;
    u32 componentId;
};

// NOTE: Currently entries in the table must be sorted based on their entity so
// that look ups can be done via binary search
struct ecs_ComponentTable
{
    EntityId *keys;
    void *values;
    u32 count;
    u32 capacity;
    u32 objectSize;
    b32 notifyOnRemoveEntry;
};

// FIXME: handle entityId wrap
struct ecs_EntityWorld
{
    EntityId entityIds[MAX_ENTITIES];
    u32 entityCount;

    EntityId deletionQueue[MAX_ENTITIES];
    u32 deletionQueueLength;

    ComponentMetaData componentMetaData[MaxComponentsIds];

    ecs_ComponentTable tables[MaxComponentsIds];
    EntityId lastEntityId; // Id of last entity created

    EntityPrefab prefabs[MaxEntityPrefabs];

    ecs_ComponentRemovedEvent
        componentRemovedEvents[MAX_COMPONENT_REMOVED_EVENTS];
    u32 componentRemovalEventCount;
};

struct ComponentInitializationData
{
    u32 componentId;
    const char *name;
    u32 dataType;
    u32 maxEntities;
    b32 notifyOnRemoveEntry;
};
