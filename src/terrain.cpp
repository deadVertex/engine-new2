// Dumping ground for terrain related code


//#define DEBUG_RAY_INTERSECT_HEIGHTMAP
//#define DEBUG_DRAW_TERRAIN_QUAD_TREE_NODES

inline f32 SampleTerrainAtWorldPosition(
    HeightMap heightMap, vec3 terrainPosition, vec3 terrainScale, f32 x, f32 z)
{
    f32 minx = terrainPosition.x - terrainScale.x * 0.5f;
    f32 maxx = terrainPosition.x + terrainScale.x * 0.5f;
    f32 minz = terrainPosition.z - terrainScale.z * 0.5f;
    f32 maxz = terrainPosition.z + terrainScale.z * 0.5f;

    x = Clamp(x, minx, maxx);
    z = Clamp(z, minz, maxz);
    f32 u = MapToUnitRange(minx, maxx, x);
    f32 v = MapToUnitRange(minz, maxz, z);

    // Flip Y axis
    //v = 1.0f - v;

    f32 height = HeightMapSampleBilinear(heightMap, u, v);

    height = terrainPosition.y + height * terrainScale.y;

    return height;
}

internal void GetRandomPointsInLayer(TerrainLayerMap layerMap, vec3 *points, u32 requestCount,
        MemoryArena *tempArena)
{
    u32 *xValues = AllocateArray(tempArena, u32, layerMap.width * layerMap.height);
    u32 *yValues = AllocateArray(tempArena, u32, layerMap.width * layerMap.height);
    u32 count = 0;

    for (u32 y = 0; y < layerMap.height; ++y)
    {
        for (u32 x = 0; x < layerMap.width; ++x)
        {
            if (layerMap.values[y*layerMap.width + x] & Bit(0))
            {
                xValues[count] = x;
                yValues[count] = y;
                count++;
            }
        }
    }

    f32 halfTerrainScale = TERRAIN_PATCH_SCALE * 0.5f;
    for (u32 i = 0; i < requestCount; ++i)
    {
        u32 index = g_Entropy[i] % count;

        f32 x = xValues[index] / (f32)layerMap.width;
        f32 y = yValues[index] / (f32)layerMap.height;

        y = 1.0f - y;

        points[i].x = Lerp(-halfTerrainScale, halfTerrainScale, x);
        points[i].z = Lerp(-halfTerrainScale, halfTerrainScale, y);
    }
}

#if 1
// TODO: Take rotation into bounding account for bounding boxes
// TODO: Bias spawn location of rocks based on height map gradient
internal void SpawnRocks(ecs_EntityWorld *world, vec3 terrainPosition,
    vec3 terrainScale, GameAssets *assets, MemoryArena *tempArena)
{
#define ROCK_COUNT 800
    HeightMap heightMap = assets->heightMap;
    TerrainLayerMap layerMap = assets->layerMap;
    vec3 positions[ROCK_COUNT];
    GetRandomPointsInLayer(layerMap, positions, ArrayCount(positions), tempArena);

    for (u32 i = 0; i < ArrayCount(positions); ++i)
    {
        positions[i].y = SampleTerrainAtWorldPosition(heightMap,
            terrainPosition, terrainScale, positions[i].x, positions[i].z);
    }

    quat rotations[ROCK_COUNT];
    for (u32 i = 0; i < ArrayCount(rotations); ++i)
    {
        f32 yRotation = (f32)g_Entropy[i] / (f32)MAX_RANDOM_NUMBER;
        rotations[i] = FromEulerAngles(
            Vec3(0.0f, 2.0f * PI * yRotation, 0.0f));
    }

    vec3 scales[ROCK_COUNT];
    for (u32 i = 0; i < ArrayCount(scales); ++i)
    {
        f32 scaleX = (f32)g_Entropy[i] / (f32)MAX_RANDOM_NUMBER;
        f32 scaleY = (f32)g_Entropy[(i + 0x123) % ArrayCount(g_Entropy)] /
                        (f32)MAX_RANDOM_NUMBER;
        f32 scaleZ = (f32)g_Entropy[(i + 0x61) % ArrayCount(g_Entropy)] /
                        (f32)MAX_RANDOM_NUMBER;
        scales[i].x = scaleX * 4.2f;
        scales[i].y = scaleX * 4.2f + (scaleY - 0.2f);
        scales[i].z = scaleX * 4.2f + (scaleZ - 0.15f);
    }

    u32 prefabs[] = {EntityPrefab_Rock01, EntityPrefab_Rock02,
        EntityPrefab_Rock03, EntityPrefab_Rock04};
    for (u32 i = 0; i < ROCK_COUNT; ++i)
    {
        u32 prefabId = prefabs[i % ArrayCount(prefabs)];
        SpawnEntityWithTransform(world, prefabId, positions[i], rotations[i], scales[i]);
    }
}
#endif

inline vec3 GetHeightMapWorldPosition(
    HeightMap heightMap, f32 u, f32 v, vec3 terrainPosition, vec3 terrainScale)
{
    f32 x = (u - 0.5f);
    f32 z = (v - 0.5f);
    f32 y = HeightMapSampleBilinear(heightMap, u, v);

    vec3 result = Vec3(x, y, z);
    result = Hadamard(result, terrainScale) + terrainPosition;

    return result;
}

internal void DrawHeightMapTriangles(HeightMap heightMap, QuadTree quadTree,
    vec3 terrainPosition, vec3 terrainScale,
    DebugDrawingSystem *debugDrawingSystem, vec3 color, BoundingBox drawAabb)
{
    DrawBox(debugDrawingSystem, drawAabb.min, drawAabb.max, Vec3(0.2, 0.6, 1.0));

    QuadTreeNode *stack[256];
    u32 stackLength = 1;
    stack[0] = quadTree.root;

    while (stackLength > 0)
    {
        QuadTreeNode *node = stack[--stackLength];
        if (AabbIntersect(node->aabb.min, node->aabb.max, drawAabb.min, drawAabb.max))
        {
            //DrawBox(debugDrawingSystem, node->aabb.min, node->aabb.max, Vec3(0.6, 1.0, 0.2) * 0.5);
            if (node->isLeaf)
            {
                // Draw triangles
                for (u32 baseRow = 0; baseRow < quadTree.samplesPerNode;
                        ++baseRow)
                {
                    for (u32 baseCol = 0; baseCol < quadTree.samplesPerNode;
                         ++baseCol)
                    {
                        u32 row =
                            MinU(baseRow + node->row, TERRAIN_COLLISION_GRID_DIM);
                        u32 col =
                            MinU(baseCol + node->col, TERRAIN_COLLISION_GRID_DIM);

                        f32 u0 = (f32)col / (f32)TERRAIN_COLLISION_GRID_DIM;
                        f32 v0 = (f32)row / (f32)TERRAIN_COLLISION_GRID_DIM;
                        f32 u1 =
                            (f32)(col + 1) / (f32)TERRAIN_COLLISION_GRID_DIM;
                        f32 v1 =
                            (f32)(row + 1) / (f32)TERRAIN_COLLISION_GRID_DIM;

                        vec3 a = GetHeightMapWorldPosition(
                            heightMap, u0, v0, terrainPosition, terrainScale);
                        vec3 b = GetHeightMapWorldPosition(
                            heightMap, u1, v0, terrainPosition, terrainScale);
                        vec3 c = GetHeightMapWorldPosition(
                            heightMap, u1, v1, terrainPosition, terrainScale);
                        vec3 d = GetHeightMapWorldPosition(
                            heightMap, u0, v1, terrainPosition, terrainScale);

                        vec3 vertices[6] = {c, a, b, d, a, c};
                        DrawTriangles(debugDrawingSystem, vertices, 2, color,
                            0.25f, Vec3(1, 0, 1));
                    }
                }
            }
            else
            {
                for (u32 i = 0; i < 4; ++i)
                {
                    Assert(stackLength < ArrayCount(stack));
                    stack[stackLength++] = node->children[i];
                }
            }
        }
    }
}

inline BoundingBox AabbUnion(BoundingBox a, BoundingBox b)
{
    BoundingBox result;
    result.min = Min(a.min, b.min);
    result.max = Max(a.max, b.max);
    return result;
}

inline b32 AabbContains(BoundingBox aabb, vec3 p)
{
    b32 result = false;
    if (p.x >= aabb.min.x && p.x < aabb.max.x)
    {
        if (p.y >= aabb.min.y && p.y < aabb.max.y)
        {
            if (p.z >= aabb.min.z && p.z < aabb.max.z)
            {
                result = true;
            }
        }
    }

    return result;
}

internal void InitializeQuadTree(QuadTree *quadTree, u32 maxNodes, MemoryArena *arena)
{
    TIMED_BLOCK();
    ClearToZero(quadTree, sizeof(quadTree));
    quadTree->nodes = AllocateArray(arena, QuadTreeNode, maxNodes);
    quadTree->maxNodes = maxNodes;
}

internal void BuildHeightMapQuadTree(QuadTree *quadTree, HeightMap heightMap,
    vec3 terrainPosition, vec3 terrainScale, u32 samplesPerNode)
{
    Assert(quadTree->maxNodes > 0);
    quadTree->nodeCount = 0;
    quadTree->root = NULL;
    quadTree->samplesPerNode = samplesPerNode;

    BEGIN_TIMED_BLOCK(BuildHeightMapQuadTree_CreateLeafNodes);
    u32 height = TERRAIN_COLLISION_GRID_DIM / samplesPerNode;
    u32 width = TERRAIN_COLLISION_GRID_DIM / samplesPerNode;
    for (u32 row = 0; row < height; ++row)
    {
        for (u32 col = 0; col < width; ++col)
        {
            f32 u0 = (f32)col / (f32)width;
            f32 v0 = (f32)row / (f32)height;
            f32 u1 = (f32)(col + 1) / (f32)width;
            f32 v1 = (f32)(row + 1) / (f32)height;

            BoundingBox aabb = {};
            aabb.min = Vec3(u0 - 0.5f, 1.0f, v0 - 0.5f);
            aabb.max = Vec3(u1 - 0.5f, -1.0f, v1 - 0.5f);

            for (u32 z = 0; z <= samplesPerNode; ++z)
            {
                for (u32 x = 0; x <= samplesPerNode; ++x)
                {
                    f32 s = (f32)x / (f32)samplesPerNode;
                    f32 t = (f32)z / (f32)samplesPerNode;

                    f32 u = Lerp(u0, u1, s);
                    f32 v = Lerp(v0, v1, t);

                    f32 y = HeightMapSampleNearest(heightMap, u, v);

                    aabb.min.y = Min(aabb.min.y, y);
                    aabb.max.y = Max(aabb.max.y, y);

//#define DEBUG_VISUALIZE_NODE_SAMPLES
#ifdef DEBUG_VISUALIZE_NODE_SAMPLES
                    vec3 p = Vec3(Lerp(aabb.min.x, aabb.max.x, s), y,
                        Lerp(aabb.min.z, aabb.max.z, t));

                    p = Hadamard(p, terrainScale) + terrainPosition;

                    DrawPoint(g_ServerDebugDrawingSystem, p, 1.5f, Vec3(1, 0, 1));
#endif
                }
            }

            aabb.min = Hadamard(aabb.min, terrainScale) + terrainPosition;
            aabb.max = Hadamard(aabb.max, terrainScale) + terrainPosition;

            Assert(quadTree->nodeCount < quadTree->maxNodes);
            QuadTreeNode *node = quadTree->nodes + quadTree->nodeCount++;
            node->isLeaf = true;
            node->aabb = aabb;
            node->row = row * samplesPerNode;
            node->col = col * samplesPerNode;
        }
    }
    END_TIMED_BLOCK(BuildHeightMapQuadTree_CreateLeafNodes);

    BEGIN_TIMED_BLOCK(BuildHeightMapQuadTree_CreateTree);
    u32 nodesAtCurrentDepth = quadTree->nodeCount;
    QuadTreeNode *firstNode = quadTree->nodes;
    while (nodesAtCurrentDepth > 1)
    {
        u32 newNodeCount = 0;
        for (u32 row = 0; row < height / 2; ++row)
        {
            for (u32 col = 0; col < width / 2; ++col)
            {
                // Find nodes to merge
                QuadTreeNode newNode = {};
                newNode.isLeaf = false;
                // TODO: Range checking
                newNode.children[0] = firstNode;
                newNode.children[1] = firstNode + 1;
                newNode.children[2] = firstNode + width;
                newNode.children[3] = firstNode + width + 1;
                newNode.aabb = firstNode->aabb;
                for (u32 i = 1; i < 4; ++i)
                {
                    newNode.aabb = AabbUnion(newNode.aabb, newNode.children[i]->aabb);
                }

                Assert(quadTree->nodeCount < quadTree->maxNodes);
                quadTree->nodes[quadTree->nodeCount++] = newNode;
                newNodeCount++;

                firstNode += 2;
            }
            firstNode += width;
        }
        nodesAtCurrentDepth = newNodeCount;
        height /= 2;
        width /= 2;
    }
    END_TIMED_BLOCK(BuildHeightMapQuadTree_CreateTree);

    quadTree->root = firstNode;
}

internal RaycastResult RaycastHeightMap(HeightMap heightMap, QuadTree quadTree,
    vec3 terrainPosition, vec3 terrainScale, vec3 start, vec3 end)
{
    RaycastResult result = {};

    vec3 dir = end - start;
    f32 dist = Length(dir);
    dir = SafeNormalize(dir);
    if ((Abs(dir.x) <= EPSILON) && (Abs(dir.z) <= EPSILON))
    {
        // Easy case
        f32 sampleY = SampleTerrainAtWorldPosition(
            heightMap, terrainPosition, terrainScale, start.x, start.z);

        if (start.y > sampleY)
        {
            if (start.y - sampleY < dist)
            {
                result.isValid = true;
                result.tmin = (start.y - sampleY) / dist;
                result.hitPoint = Vec3(start.x, sampleY, start.z);
                result.hitNormal = Vec3(0, 1, 0);

#ifdef DEBUG_RAY_INTERSECT_HEIGHTMAP
                DrawPoint(g_ServerDebugDrawingSystem, result.hitPoint, 0.5f, Vec3(0, 1, 1));
#endif
            }
        }
    }
    else
    {
        // Option 2. Build aabb tree to narrow down which triangles to raycast
        // against
        u32 nodesTestedCount = 0;
        u32 trianglesTestedCount = 0;
        RaycastResult closestResult = {};

        // TODO: SAT for line segment vs aabb rather than ray intersect

        Assert(quadTree.nodeCount > 0);
        QuadTreeNode *stack[256];
        u32 stackLength = 1;
        stack[0] = quadTree.root;

        while (stackLength > 0)
        {
            QuadTreeNode *currentNode = stack[--stackLength];
            BoundingBox aabb = currentNode->aabb;
            RaycastResult raycastResult =
                RayIntersectAabb_(aabb.min, aabb.max, start, end);
            nodesTestedCount++;
            if (raycastResult.isValid)
            {
                // FIXME: Don't hardcode g_ServerDebugDrawingSystem
#ifdef DEBUG_RAY_INTERSECT_HEIGHTMAP
                DrawBox(g_ServerDebugDrawingSystem, currentNode->aabb.min,
                    currentNode->aabb.max, Vec3(0, 1, 1), 30.0f);
#endif
                if (currentNode->isLeaf)
                {
                    for (u32 baseRow = 0; baseRow < quadTree.samplesPerNode;
                         ++baseRow)
                    {
                        for (u32 baseCol = 0; baseCol < quadTree.samplesPerNode;
                             ++baseCol)
                        {
                            u32 row = MinU(baseRow + currentNode->row,
                                TERRAIN_COLLISION_GRID_DIM);
                            u32 col = MinU(baseCol + currentNode->col,
                                TERRAIN_COLLISION_GRID_DIM);

                            f32 u0 = (f32)col / (f32)TERRAIN_COLLISION_GRID_DIM;
                            f32 v0 = (f32)row / (f32)TERRAIN_COLLISION_GRID_DIM;
                            f32 u1 = (f32)(col + 1) /
                                     (f32)TERRAIN_COLLISION_GRID_DIM;
                            f32 v1 = (f32)(row + 1) /
                                     (f32)TERRAIN_COLLISION_GRID_DIM;

                            vec3 a = GetHeightMapWorldPosition(heightMap, u0,
                                v0, terrainPosition, terrainScale);
                            vec3 b = GetHeightMapWorldPosition(heightMap, u1,
                                v0, terrainPosition, terrainScale);
                            vec3 c = GetHeightMapWorldPosition(heightMap, u1,
                                v1, terrainPosition, terrainScale);
                            vec3 d = GetHeightMapWorldPosition(heightMap, u0,
                                v1, terrainPosition, terrainScale);

#ifdef DEBUG_RAY_INTERSECT_HEIGHTMAP
                            vec3 vertices[6] = {c, a, b, d, a, c};
                            DrawTriangles(g_ServerDebugDrawingSystem, vertices,
                                2, Vec3(1, 0.6, 0.1), 0.25f, Vec3(1, 0, 1), 30.0f);
#endif

                            RaycastResult r0 =
                                RayIntersectTriangle_(c, a, b, start, end);
                            RaycastResult r1 =
                                RayIntersectTriangle_(d, a, c, start, end);
                            trianglesTestedCount += 2;

                            if (r1.isValid)
                            {
                                if (!r0.isValid || r1.tmin < r0.tmin)
                                {
                                    r0 = r1;
                                }
                            }

                            if (r0.isValid)
                            {
                                if (!closestResult.isValid ||
                                    r0.tmin < closestResult.tmin)
                                {
                                    closestResult = r0;
                                }
                            }
                        }
                    }
                }
                else
                {
                    b32 found = false;
                    for (u32 i = 0; i < 4; ++i)
                    {
                        Assert(stackLength < ArrayCount(stack));
                        stack[stackLength++] = currentNode->children[i];
                    }
                }
            }
        }

        result = closestResult;
        LogMessage("Tested %u triangles, tested %u bounding volumes",
            trianglesTestedCount, nodesTestedCount);
    }

    return result;
}

internal HeightMap GenerateHeightMap(MemoryArena *arena, u32 size)
{
    HeightMap result = {};

    result.width = size;
    result.height = size;
    result.values = AllocateArray(arena, f32, size *size);

    for (u32 y = 0; y < size; ++y)
    {
        for (u32 x = 0; x < size; ++x)
        {
            f32 v = OctavePerlin(
                    ((f32)x / (f32)size) * 2.4f, ((f32)y / (f32)size) * 2.4f, 2.0f,
                    4, 0.6f);

            //f32 v = (f32)x / (f32)size;
            //v = 0.5f * Sin(v*30.0f) + 0.5f;
            result.values[y*size + x] = v;
        }
    }

    return result;
}

internal void CreateGradientMap(float *heightMapValues, u32 size, float *gradient)
{
    f32 sqrt2 = Sqrt(2.0f);
    for (i32 y = 0; y < (i32)size; ++y)
    {
        for (i32 x = 0; x < (i32)size; ++x)
        {
            i32 x0 = Clamp(x - 1, 0, size);
            i32 x1 = Clamp(x + 1, 0, size);

            float t0 = heightMapValues[y * size + x0];
            float t1 = heightMapValues[y * size + x1];

            i32 y0 = Clamp(y - 1, 0, size);
            i32 y1 = Clamp(y + 1, 0, size);

            float t2 = heightMapValues[y0 * size + x];
            float t3 = heightMapValues[y1 * size + x];

            float dx = t1 - t0;
            float dy = t3 - t2;

            // d is in (-sqrt2 .. sqrt2) range
            float d = Sqrt(dx * dx + dy * dy);

            // map back to -1 .. 1 range and then 0 .. 1 range
            d *= 1.0f / sqrt2;

            gradient[y * size + x] = d;
        }
    }
}

internal void CreateNormalMap(TerrainNormalMap *normalMap, HeightMap heightMap)
{
    Assert(normalMap->width == heightMap.width);
    Assert(normalMap->height == heightMap.height);
    Assert(heightMap.width == heightMap.height);

    u32 size = heightMap.width;
    for (i32 y = 0; y < (i32)heightMap.height; ++y)
    {
        for (i32 x = 0; x < (i32)heightMap.width; ++x)
        {
            i32 x1 = Clamp(x + 1, 0, size);
            i32 y1 = Clamp(y + 1, 0, size);

            f32 t0 = heightMap.values[y * size + x];
            f32 tx = heightMap.values[y * size + x1];
            f32 ty = heightMap.values[y1 * size + x];

            Assert(t0 >= 0.0f && t0 <= 1.0f);
            f32 fx = (f32)x / (f32)size;
            f32 fy = (f32)y / (f32)size;
            f32 fx1 = (f32)x1 / (f32)size;
            f32 fy1 = (f32)y1 / (f32)size;

            vec3 p0 = Vec3(fx, t0, fy);
            vec3 px = Vec3(fx1, tx, fy);
            vec3 py = Vec3(fx, ty, fy1);

            vec3 dx = px - p0;
            vec3 dy = py - p0;
            dx = SafeNormalize(dx);
            dy = SafeNormalize(dy);
            vec3 normal = Cross(dx, dy);
            normal = SafeNormalize(normal);

            normalMap->values[y * size + x] = normal;
        }
    }
}

internal void CreateLayerMap(float *gradient, u32 size, TerrainLayerMap layerMap)
{
    for (u32 y = 0; y < size; ++y)
    {
        for (u32 x = 0; x < size; ++x)
        {
            if (gradient[y * size + x] > 0.006f)
            {
                layerMap.values[y*size + x] |= Bit(0);
            }
        }
    }
}

internal void GenerateTerrain(HeightMap *heightMap, TerrainLayerMap *layerMap,
    TerrainNormalMap *normalMap, MemoryArena *terrainArena,
    MemoryArena *tempArena)
{
    // Clear terrain arena
    terrainArena->size = 0;

    u32 size = TERRAIN_HEIGHTMAP_SIZE;
    *heightMap = GenerateHeightMap(terrainArena, size);

    layerMap->width = size;
    layerMap->height = size;
    layerMap->values = AllocateArray(terrainArena, u8, size*size);

    normalMap->width = size;
    normalMap->height = size;
    normalMap->values =
        AllocateArray(terrainArena, vec3, normalMap->width * normalMap->height);

    f32 *gradient = AllocateArray(tempArena, f32, size*size);
    CreateGradientMap(heightMap->values, size, gradient);
    CreateLayerMap(gradient, size, *layerMap);
    CreateNormalMap(normalMap, *heightMap);
}

internal void GenerateTerrainTextures(HeightMap heightMap,
    TerrainLayerMap layerMap, TerrainNormalMap normalMap, GameMemory *memory,
    MemoryArena *tempArena)
{
    u32 size = heightMap.width;
    u16 *pixels = AllocateArray(tempArena, u16, size*size);

    for (u32 y = 0; y < size; ++y)
    {
        for (u32 x = 0; x < size; ++x)
        {
            float v = heightMap.values[y*size + x];
            pixels[y*size + x] = (u16)(v * U16_MAX);
        }
    }

    u32 *splatMap = AllocateArray(tempArena, u32, size*size);
    for (u32 y = 0; y < size; ++y)
    {
        for (u32 x = 0; x < size; ++x)
        {
            u8 i = layerMap.values[y * size + x] > 0 ? 0xFF : 0;
            splatMap[y * size + x] = 0xFF000000 | (i << 16) | (i << 8) | i;
        }
    }

    u32 *normals = AllocateArray(tempArena, u32 , size*size);
    for (u32 y = 0; y < size; ++y)
    {
        for (u32 x = 0; x < size; ++x)
        {
            vec3 normal = normalMap.values[y * size + x];
            u8 r = (u8)(normal.x * 255.0f);
            u8 g = (u8)(normal.y * 255.0f);
            u8 b = (u8)(normal.z * 255.0f);
            normals[y * size + x] = 0xFF000000 | (b << 16) | (g << 8) | r;
        }
    }

    {
        RenderCommand_CreateTexture *createNoiseTexture =
            AllocateRenderCommand(memory, RenderCommand_CreateTexture);
        createNoiseTexture->pixels = pixels;
        createNoiseTexture->width = size;
        createNoiseTexture->height = size;
        createNoiseTexture->colorFormat = ColorFormat_R16;
        createNoiseTexture->samplerMode = TextureSamplerMode_Linear;
        createNoiseTexture->id = Texture_Noise;
    }

    {
        RenderCommand_CreateTexture *createSplatMap =
            AllocateRenderCommand(memory, RenderCommand_CreateTexture);
        createSplatMap->pixels = splatMap;
        createSplatMap->width = size;
        createSplatMap->height = size;
        createSplatMap->colorFormat = ColorFormat_RGBA8;
        createSplatMap->samplerMode = TextureSamplerMode_Linear;
        createSplatMap->id = Texture_Gradient;
    }

    {
        RenderCommand_CreateTexture *createNormalMap =
            AllocateRenderCommand(memory, RenderCommand_CreateTexture);
        createNormalMap->pixels = normals;
        createNormalMap->width = size;
        createNormalMap->height = size;
        createNormalMap->colorFormat = ColorFormat_RGBA8;
        createNormalMap->samplerMode = TextureSamplerMode_Linear;
        createNormalMap->id = Texture_TerrainNormalMap;
    }
}

inline vec3 AabbCenter(BoundingBox aabb)
{
    vec3 result = (aabb.min + aabb.max) * 0.5f;
    return result;
}

inline vec3 AabbClosestPoint(BoundingBox aabb, vec3 p)
{
    vec3 result = Min(Max(p, aabb.min), aabb.max);
    return result;
}

struct QuadTreeStackEntry
{
    QuadTreeNode *node;
    u32 depth;
};

internal void RenderQuadTreeTerrain(GameMemory *memory, mat4 view,
    mat4 projection, vec3 cameraPosition, mat4 lightViewProjection,
    HeightMap heightMap, QuadTree quadTree, vec3 terrainPosition,
    vec3 terrainScale, DebugDrawingSystem *debugDrawingSystem,
    b32 drawWireFrame)
{
    QuadTreeStackEntry stack[256];
    u32 stackLength = 1;
    stack[0].node = quadTree.root;
    stack[0].depth = 0;

    f32 minHalfDim = quadTree.samplesPerNode * 0.5f;

    // FIXME: Need to calculate this, this will be wrong if we change the size
    // of the terrain which will reduce the number of depth levels
    f32 maxDistanceSq[9] = {
        minHalfDim * 256.0f,
        minHalfDim * 128.0f,
        minHalfDim * 64.0f,
        minHalfDim * 32.0f,
        minHalfDim * 16.0f,
        minHalfDim * 8.0f,
        minHalfDim * 4.0f,
        minHalfDim * 2.0f,
        minHalfDim,
    };

    vec3 rootMin = terrainPosition - terrainScale * 0.5f;
    vec3 rootMax = terrainPosition + terrainScale * 0.5f;

    while (stackLength > 0)
    {
        QuadTreeStackEntry entry = stack[--stackLength];
        QuadTreeNode *node = entry.node;
        u32 depth = entry.depth;

        vec3 centerP = AabbCenter(node->aabb);
        vec3 closestPoint = AabbClosestPoint(node->aabb, cameraPosition);
        f32 distSq = LengthSq(cameraPosition - closestPoint);

        b32 drawTerrainNode = false;
        Assert(depth < ArrayCount(maxDistanceSq));
        if (!node->isLeaf)
        {
            if (distSq < maxDistanceSq[depth] * maxDistanceSq[depth])
            {
                for (u32 i = 0; i < 4; ++i)
                {
                    Assert(stackLength < ArrayCount(stack));
                    QuadTreeStackEntry *child = stack + stackLength++;
                    child->node = node->children[i];
                    child->depth = depth + 1;
                }
            }
            else
            {
                // Draw mesh
#ifdef DEBUG_DRAW_TERRAIN_QUAD_TREE_NODES
                DrawBox(debugDrawingSystem, node->aabb.min, node->aabb.max,
                    Vec3(1, 0, 1));
#endif
                drawTerrainNode = true;
            }
        }
        else
        {
            // Draw leaf mesh
#ifdef DEBUG_DRAW_TERRAIN_QUAD_TREE_NODES
            DrawBox(debugDrawingSystem, node->aabb.min, node->aabb.max,
                Vec3(0, 1, 1));
#endif
            drawTerrainNode = true;
        }

        if (drawTerrainNode)
        {
            vec3 nodeScale = node->aabb.max - node->aabb.min;
            vec3 nodePosition = (node->aabb.max + node->aabb.min) * 0.5f;
            nodePosition.y = terrainPosition.y;
            nodeScale.y = terrainScale.y;

            vec4 uvTransform = {};
            uvTransform.x = MapToUnitRange(rootMin.x, rootMax.x, node->aabb.min.x);
            uvTransform.y = MapToUnitRange(rootMin.z, rootMax.z, node->aabb.min.z);
            uvTransform.z = nodeScale.x / terrainScale.x;
            uvTransform.w = nodeScale.z / terrainScale.z;

            mat4 localToWorldMatrix =
                CalculateLocalToWorldMatrix(nodePosition, Quat(), nodeScale);

            if (drawWireFrame)
            {
                RenderCommand_DrawMesh *drawTerrain =
                    AllocateRenderCommand(memory, RenderCommand_DrawMesh);
                drawTerrain->mesh = Mesh_TerrainPatch;
                drawTerrain->shader = Shader_TerrainDebug;
                AddShaderUniformValueTexture(
                    drawTerrain, UniformValue_Heightmap, Texture_Noise);

                AddShaderUniformValueMat4(drawTerrain, UniformValue_MvpMatrix,
                    projection * view * localToWorldMatrix);
                AddShaderUniformValueMat4(
                    drawTerrain, UniformValue_ModelMatrix, localToWorldMatrix);
                AddShaderUniformValueVec4(
                    drawTerrain, UniformValue_UVTransform, uvTransform);
            }
            else
            {
                RenderCommand_DrawMesh *drawTerrain =
                    AllocateRenderCommand(memory, RenderCommand_DrawMesh);
                drawTerrain->mesh = Mesh_TerrainPatch;
                drawTerrain->shader = Shader_Terrain;
                AddShaderUniformValueTexture(
                    drawTerrain, UniformValue_SplatMap, Texture_Gradient);
                AddShaderUniformValueTexture(
                    drawTerrain, UniformValue_SandTexture, Texture_Sand);
                AddShaderUniformValueTexture(
                    drawTerrain, UniformValue_GrassTexture, Texture_Grass);
                AddShaderUniformValueTexture(
                    drawTerrain, UniformValue_RockTexture, Texture_Rock);
                AddShaderUniformValueTexture(
                    drawTerrain, UniformValue_Heightmap, Texture_Noise);
                AddShaderUniformValueTexture(drawTerrain,
                    UniformValue_NormalMap, Texture_TerrainNormalMap);

                AddShaderUniformValueMat4(drawTerrain, UniformValue_MvpMatrix,
                    projection * view * localToWorldMatrix);
                AddShaderUniformValueMat4(drawTerrain,
                    UniformValue_LightViewProjection, lightViewProjection);
                AddShaderUniformValueMat4(
                    drawTerrain, UniformValue_ModelMatrix, localToWorldMatrix);

                AddShaderUniformValueBuffer(drawTerrain,
                    UniformValue_LightingData, UniformBuffer_LightingData);
                AddShaderUniformValueVec3(
                    drawTerrain, UniformValue_CameraPosition, cameraPosition);

                AddShaderUniformValueRenderTarget(drawTerrain,
                    UniformValue_ShadowMap, RenderTarget_ShadowBuffer);
                AddShaderUniformValueVec4(
                    drawTerrain, UniformValue_UVTransform, uvTransform);
            }
        }
    }
}

internal void RenderQuadtreeTerrainSystem(ecs_EntityWorld *world,
    GameMemory *memory, vec3 cameraPosition, mat4 view, mat4 projection,
    mat4 lightViewProjection, Material *materials, u32 materialCount,
    b32 drawWireFrame, DebugDrawingSystem *debugDrawingSystem,
    HeightMap heightMap, QuadTree quadTree)
{
    u32 components[] = {
        TerrainComponentId, PositionComponentId, ScaleComponentId};

    EntityId entities[MAX_ENTITIES];
    u32 count = ecs_Join(world, components, ArrayCount(components), entities,
        ArrayCount(entities));

    vec3 positions[MAX_ENTITIES];
    vec3 scales[MAX_ENTITIES];
    ecs_GetComponent(world, PositionComponentId, entities, count, positions,
        sizeof(positions[0]));
    ecs_GetComponent(
        world, ScaleComponentId, entities, count, scales, sizeof(scales[0]));

    Assert(count <= 1);
    for (u32 entityIdx = 0; entityIdx < count; ++entityIdx)
    {
        RenderQuadTreeTerrain(memory, view, projection, cameraPosition,
            lightViewProjection, heightMap, quadTree, positions[entityIdx],
            scales[entityIdx], debugDrawingSystem, drawWireFrame);
    }
}

internal void DebugDrawTerrain(ecs_EntityWorld *world, GameAssets *assets,
    DebugDrawingSystem *debugDrawingSystem, vec3 color, vec3 cameraPosition)
{
    u32 join[] = {TerrainComponentId, PositionComponentId,
        ScaleComponentId};

    EntityId entities[MAX_ENTITIES];
    u32 count =
        ecs_Join(world, join, ArrayCount(join), entities, ArrayCount(entities));

    vec3 positions[MAX_ENTITIES];
    vec3 scales[MAX_ENTITIES];

    ecs_GetComponent(world, PositionComponentId, entities, count, positions,
        sizeof(positions[0]));
    ecs_GetComponent(
        world, ScaleComponentId, entities, count, scales, sizeof(scales[0]));

    for (u32 i = 0; i < count; ++i)
    {
        // TODO: Expose aabb dimensions
        BoundingBox drawAabb = {};
        drawAabb.min = cameraPosition - Vec3(20.0f);
        drawAabb.max = cameraPosition + Vec3(20.0f);
        DrawHeightMapTriangles(assets->heightMap, assets->heightMapQuadTree,
            positions[i], scales[i], debugDrawingSystem, color, drawAabb);
    }
}

internal void GenerateTerrainQuadTreeSystem(
    ecs_EntityWorld *world, GameAssets *assets, b32 generateRenderQuadTree)
{
    TIMED_BLOCK();

    u32 join[] = {TerrainComponentId, PositionComponentId,
        ScaleComponentId};

    EntityId entities[MAX_ENTITIES];
    u32 count =
        ecs_Join(world, join, ArrayCount(join), entities, ArrayCount(entities));

    vec3 positions[MAX_ENTITIES];
    vec3 scales[MAX_ENTITIES];

    ecs_GetComponent(world, PositionComponentId, entities, count, positions,
        sizeof(positions[0]));
    ecs_GetComponent(
        world, ScaleComponentId, entities, count, scales, sizeof(scales[0]));

    Assert(count <= 1);
    for (u32 i = 0; i < count; ++i)
    {
        BEGIN_TIMED_BLOCK(BuildHeightMapCollisionQuadTree);
        BuildHeightMapQuadTree(&assets->heightMapQuadTree, assets->heightMap,
            positions[i], scales[i], SAMPLES_PER_TREE_NODE);
        END_TIMED_BLOCK(BuildHeightMapCollisionQuadTree);

        if (generateRenderQuadTree)
        {
            BEGIN_TIMED_BLOCK(BuildHeightMapRenderingQuadTree);
            BuildHeightMapQuadTree(&assets->renderQuadTree, assets->heightMap,
                positions[i], scales[i], TERRAIN_VERTICES_PER_NODE);
            END_TIMED_BLOCK(BuildHeightMapRenderingQuadTree);
        }
    }
}

internal void SpawnEntityOnTerrain(
    ecs_EntityWorld *world, GameAssets *assets, u32 prefabId, vec3 position)
{
    u32 join[] = {TerrainComponentId, PositionComponentId,
        ScaleComponentId};

    EntityId entities[MAX_ENTITIES];
    u32 count =
        ecs_Join(world, join, ArrayCount(join), entities, ArrayCount(entities));

    vec3 positions[MAX_ENTITIES];
    vec3 scales[MAX_ENTITIES];

    ecs_GetComponent(world, PositionComponentId, entities, count, positions,
        sizeof(positions[0]));
    ecs_GetComponent(
        world, ScaleComponentId, entities, count, scales, sizeof(scales[0]));

    Assert(count <= 1);
    for (u32 i = 0; i < count; ++i)
    {
        // TODO: Test if position.xz is contained in terrain volume

        f32 y = SampleTerrainAtWorldPosition(
            assets->heightMap, positions[i], scales[i], position.x, position.z);

        SpawnEntityAtPosition(world, prefabId, position + Vec3(0, y, 0));
    }
}
