inline b32 AabbIntersect(vec3 minA, vec3 maxA, vec3 minB, vec3 maxB)
{
    for (u32 axis = 0; axis < 3; ++axis)
    {
        f32 a0 = minA.data[axis];
        f32 a1 = maxA.data[axis];
        f32 b0 = minB.data[axis];
        f32 b1 = maxB.data[axis];

        // No overlap
        if (b1 < a0 || b0 > a1)
        {
            return false;
        }
    }

    return true;
}

inline b32 AabbContains(vec3 minA, vec3 maxA, vec3 p)
{
    for (u32 axis = 0; axis < 3; ++axis)
    {
        f32 a0 = minA.data[axis];
        f32 a1 = maxA.data[axis];
        f32 p0 = p.data[axis];

        // No overlap
        if (p0 < a0 || p0 >= a1)
        {
            return false;
        }
    }

    return true;
}

// TODO: Maybe look at making more optimal version of this function that
// doesn't calculate hitPoint and hitNormal
inline RaycastResult
RayIntersectAabb_(vec3 boxMin, vec3 boxMax, vec3 start, vec3 end)
{
    RaycastResult result = {};

    f32 tmin = -1.0f;
    f32 tmax = F32_MAX;

    b32 negateNormal = false;
    u32 normalIdx = 0;
    vec3 dir = end - start;
    b32 containsRayOrigin = true;
    for (u32 axis = 0; axis < 3; ++axis)
    {
        f32 d = dir.data[axis];
        f32 p = start.data[axis];
        f32 min = boxMin.data[axis];
        f32 max = boxMax.data[axis];

        if (p < min || p > max)
        {
            containsRayOrigin = false;
        }

        if (Abs(d) < EPSILON)
        {
            if (p < min || p > max)
            {
                tmin = -1.0f;
                break;
            }
        }
        else
        {
            f32 a = (min - p) / d;
            f32 b = (max - p) / d;

            f32 t0 = Min(a, b);
            f32 t1 = Max(a, b);

            if (t0 > tmin)
            {
                negateNormal = (d >= 0.0f);
                normalIdx = axis;
            }

            tmin = Max(tmin, t0);
            tmax = Min(tmax, t1);

            if (tmin > tmax)
            {
                tmin = -1.0f;
                break;
            }
        }
    }

    if (tmin >= 0.0f && tmin < 1.0f)
    {
        vec3 hitNormal = Vec3(0, 0, 0);
        hitNormal.data[normalIdx] = negateNormal ? -1.0f : 1.0f;

        result.isValid = true;
        result.tmin = tmin;
        result.hitPoint = start + (end - start) * tmin;
        result.hitNormal = hitNormal;
    }
    else if (containsRayOrigin)
    {
        // Ray origin is inside AABB
        result.isValid = true;
        result.tmin = 0.0f;
        result.hitPoint = start;
        result.hitNormal = SafeNormalize(start - end);
    }

    return result;
}

#define RayIntersectAabb(                                                      \
    MINPOINTS, MAXPOINTS, COUNT, START, END, RESULTS, DEBUG_STATE)             \
    RayIntersectAabb_(MINPOINTS, MAXPOINTS, COUNT, START, END, RESULTS,        \
        DEBUG_STATE, __FILE__, __FUNCTION__, __LINE__)

inline void RayIntersectAabb_(vec3 *minPoints, vec3 *maxPoints, u32 count,
    vec3 start, vec3 end, RaycastResult *results,
    CollisionDebugState *debugState = NULL, const char *file = NULL,
    const char *function = NULL, u32 line = 0)
{
#ifdef COLLISION_DEBUG
    if (debugState && debugState->isRecordingEnabled)
    {
        CollisionDebugCall call = {};
        call.file = file;
        call.function = function;
        call.line = line;
        call.type = CollisionDebugCall_RayIntersectAabb;
        call.start = start;
        call.end = end;
        call.count = count;

        if (CanAllocateArray(&debugState->memoryArena, vec3, count * 2))
        {
            call.aabb.minPoints =
                AllocateArray(&debugState->memoryArena, vec3, count);
            CopyMemory(call.aabb.minPoints, minPoints, count * sizeof(vec3));

            call.aabb.maxPoints =
                AllocateArray(&debugState->memoryArena, vec3, count);
            CopyMemory(call.aabb.maxPoints, maxPoints, count * sizeof(vec3));

            CollisionDebug_AddCall(debugState, call);
        }
    }
#endif

    for (u32 i = 0; i < count; ++i)
    {
        // TODO: Manually inline and optimize this
        results[i] = RayIntersectAabb_(minPoints[i], maxPoints[i], start, end);
    }
}

