inline RaycastResult RayIntersectSphere_(
    vec3 center, f32 radius, vec3 start, vec3 end)
{
    RaycastResult result = {};

    vec3 delta = end - start;
    vec3 dir = SafeNormalize(delta);
    f32 length = Length(delta);

    vec3 m = start - center; // Use sphere center as origin

    f32 a = Dot(dir, dir);
    f32 b = 2.0f * Dot(m, dir);
    f32 c = Dot(m, m) - radius * radius;

    f32 d = b * b - 4.0f * a * c; // Discriminant

    f32 t = F32_MAX;
    if (d > 0.0f)
    {
        f32 denom = 2.0f * a;
        f32 t0 = (-b - Sqrt(d)) / denom;
        f32 t1 = (-b + Sqrt(d)) / denom;

        t = t0; // Pick the entry point

        if (t <= length && t >= 0.0f)
        {
            result.isValid = true;
            result.tmin = t / length;
            Assert(result.tmin >= 0.0f);
            Assert(result.tmin <= 1.0f);
            result.hitPoint = start + dir * t;
            result.hitNormal = SafeNormalize(result.hitPoint - center);
        }
    }

    return result;
}

inline RaycastResult SphereSweepSphere_(
    vec3 center, f32 radius, vec3 start, vec3 end, f32 sweepRadius)
{
    f32 combinedRadius = radius + sweepRadius;
    return RayIntersectSphere_(center, combinedRadius, start, end);
}

#define RayIntersectSphere(                                                    \
    CENTERS, RADII, COUNT, START, END, RESULTS, DEBUG_STATE)                   \
    RayIntersectSphere_(CENTERS, RADII, COUNT, START, END, RESULTS,            \
        DEBUG_STATE, __FILE__, __FUNCTION__, __LINE__)

inline void RayIntersectSphere_(vec3 *centers, f32 *radii, u32 count, vec3 start,
    vec3 end, RaycastResult *results, CollisionDebugState *debugState = NULL,
    const char *file = NULL, const char *function = NULL, u32 line = 0)
{
#ifdef COLLISION_DEBUG
    if (debugState && debugState->isRecordingEnabled)
    {
        CollisionDebugCall call = {};
        call.file = file;
        call.function = function;
        call.line = line;
        call.type = CollisionDebugCall_RayIntersectSphere;
        call.start = start;
        call.end = end;
        call.count = count;

        if (MemoryArena_CanAllocate(&debugState->memoryArena,
                sizeof(vec3) * count + sizeof(f32) * count))
        {
            call.sphere.centers =
                AllocateArray(&debugState->memoryArena, vec3, count);
            CopyMemory(call.sphere.centers, centers, count * sizeof(vec3));
            call.sphere.radii = AllocateArray(&debugState->memoryArena, f32, count);
            CopyMemory(call.sphere.radii, radii, count * sizeof(f32));
            CollisionDebug_AddCall(debugState, call);
        }
    }
#endif

    for (u32 i = 0; i < count; ++i)
    {
        results[i] = RayIntersectSphere_(centers[i], radii[i], start, end);
    }
}

#define SphereSweepSpheres(                                                    \
    CENTERS, RADII, COUNT, START, END, SWEEP_RADIUS, RESULTS, DEBUG_STATE)     \
    SphereSweepSpheres_(CENTERS, RADII, COUNT, START, END, SWEEP_RADIUS,       \
        RESULTS, DEBUG_STATE, __FILE__, __FUNCTION__, __LINE__)

inline void SphereSweepSpheres_(vec3 *centers, f32 *radii, u32 count,
    vec3 start, vec3 end, f32 sweepRadius, RaycastResult *results,
    CollisionDebugState *debugState = NULL, const char *file = NULL,
    const char *function = NULL, u32 line = 0)
{
#ifdef COLLISION_DEBUG
    if (debugState && debugState->isRecordingEnabled)
    {
        CollisionDebugCall call = {};
        call.file = file;
        call.function = function;
        call.line = line;
        call.type = CollisionDebugCall_SphereSweepSpheres;
        call.start = start;
        call.end = end;
        call.count = count;
        call.sweepRadius = sweepRadius;

        if (MemoryArena_CanAllocate(&debugState->memoryArena,
                sizeof(vec3) * count + sizeof(f32) * count))
        {
            call.sphere.centers =
                AllocateArray(&debugState->memoryArena, vec3, count);
            CopyMemory(call.sphere.centers, centers, count * sizeof(vec3));
            call.sphere.radii = AllocateArray(&debugState->memoryArena, f32, count);
            CopyMemory(call.sphere.radii, radii, count * sizeof(f32));
            CollisionDebug_AddCall(debugState, call);
        }
    }
#endif

    for (u32 i = 0; i < count; ++i)
    {
        results[i] =
            SphereSweepSphere_(centers[i], radii[i], start, end, sweepRadius);
    }
}
