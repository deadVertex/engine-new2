// NOTE: This expects vertices in clockwise winding order!
internal RaycastResult RayIntersectTriangle_(
    vec3 a, vec3 b, vec3 c, vec3 start, vec3 end)
{
    RaycastResult result = {};

    // Calculate triangle face plane
    vec3 ab = b - a;
    vec3 ca = a - c;

    vec3 normal = Cross(ab, ca);
    Assert(LengthSq(normal) > 0.0f);
    normal = Normalize(normal);
    f32 distance = Dot(normal, a);

    vec3 direction = end - start;

    // Ignore any collision if we are not raycasting in the direction opposite
    // to the normal.
    // This creates the effect of single sided triangles, double sided would be
    // more complex to implement.
    if (Dot(normal, direction) <= 0.0f)
    {
        f32 t = RayPlaneIntersectT(normal, distance, start, end);
        if (t >= 0.0f && t < 1.0f)
        {
            vec3 normalAb = Normalize(Cross(normal, ab));
            vec3 normalCa = Normalize(Cross(normal, ca));

            vec3 bc = c - b;
            vec3 normalBc = Normalize(Cross(normal, bc));

#ifdef COLLISION_DEBUG_RAY_TRIANGLE
            {
                vec3 centroidAb = (a + b) * 0.5f;
                DrawLine(g_DebugDrawingSystem, centroidAb,
                    centroidAb + normalAb * 0.5f, Vec3(1, 0, 0), 30.0f);

                vec3 centroidCa = (c + a) * 0.5f;
                DrawLine(g_DebugDrawingSystem, centroidCa,
                    centroidCa + normalCa * 0.5f, Vec3(1, 0, 0), 30.0f);

                vec3 centroidBc = (b + c) * 0.5f;
                DrawLine(g_DebugDrawingSystem, centroidBc,
                    centroidBc + normalBc * 0.5f, Vec3(1, 0, 0), 30.0f);
            }
#endif

            vec3 p = start + direction * t;
            if (Dot(normalAb, p - a) <= 0.0f)
            {
                if (Dot(normalCa, p - a) <= 0.0f)
                {
                    if (Dot(normalBc, p - b) <= 0.0f)
                    {
                        result.isValid = true;
                        result.tmin = t;
                        result.hitPoint = p;
                        result.hitNormal = normal;
                    }
                }
            }
        }
    }

    return result;
}

internal RaycastResult SphereSweepTriangle_(vec3 a, vec3 b, vec3 c, vec3 start,
    vec3 end, f32 sweepRadius, DebugDrawingSystem *debugDrawingSystem)
{
    RaycastResult result = {};

    // Calculate triangle face plane
    vec3 ab = b - a;
    vec3 ca = a - c;

    vec3 normal = Cross(ab, ca);
    Assert(LengthSq(normal) > 0.0f);
    normal = Normalize(normal);
    f32 distance = Dot(normal, a);

    vec3 direction = end - start;

//#define COLLISION_DEBUG_DRAW_SPHERE_TRIANGLE_MINKOWSKI
#ifdef COLLISION_DEBUG_DRAW_SPHERE_TRIANGLE_MINKOWSKI
    vec3 edgeColor = Vec3(1.0, 0.6, 0.1);
    f32 lifeTime = 30.0f;
    b32 ignoreDepth = true;
    DrawLine(debugDrawingSystem, a + normal * sweepRadius,
        b + normal * sweepRadius, edgeColor, lifeTime, ignoreDepth);
    DrawLine(debugDrawingSystem, b + normal * sweepRadius,
        c + normal * sweepRadius, edgeColor, lifeTime, ignoreDepth);
    DrawLine(debugDrawingSystem, c + normal * sweepRadius,
        a + normal * sweepRadius, edgeColor, lifeTime, ignoreDepth);
    DrawLine(debugDrawingSystem, a - normal * sweepRadius,
        b - normal * sweepRadius, edgeColor, lifeTime, ignoreDepth);
    DrawLine(debugDrawingSystem, b - normal * sweepRadius,
        c - normal * sweepRadius, edgeColor, lifeTime, ignoreDepth);
    DrawLine(debugDrawingSystem, c - normal * sweepRadius,
        a - normal * sweepRadius, edgeColor, lifeTime, ignoreDepth);

    DrawCapsule(debugDrawingSystem, a, b, sweepRadius, 24, edgeColor, lifeTime,
        ignoreDepth);
    DrawCapsule(debugDrawingSystem, b, c, sweepRadius, 24, edgeColor, lifeTime,
        ignoreDepth);
    DrawCapsule(debugDrawingSystem, c, a, sweepRadius, 24, edgeColor, lifeTime,
        ignoreDepth);
#endif

    // Ignore any collision if we are not raycasting in the direction opposite
    // to the normal.
    // This creates the effect of single sided triangles, double sided would be
    // more complex to implement.
    if (Dot(normal, direction) <= 0.0f)
    {
        RaycastResult planeResult =
            SphereSweepPlane(normal, distance, start, end, sweepRadius);
        if (planeResult.isValid)
        {
            f32 t = planeResult.tmin;

            vec3 normalAb = Normalize(Cross(normal, ab));
            vec3 normalCa = Normalize(Cross(normal, ca));

            vec3 bc = c - b;
            vec3 normalBc = Normalize(Cross(normal, bc));

            // Point in triangle test
            vec3 p = start + direction * t;
            b32 isPointInTriangle = false;
            if (Dot(normalAb, p - a) <= 0.0f)
            {
                if (Dot(normalCa, p - a) <= 0.0f)
                {
                    if (Dot(normalBc, p - b) <= 0.0f)
                    {
                        result = planeResult;
                        isPointInTriangle = true;
                    }
                }
            }

            if (!isPointInTriangle)
            {
                // Raycast against edge capsules
                RaycastResult edgeResultAb =
                    RayIntersectCapsule_(a, b, sweepRadius, start, end);
                RaycastResult edgeResultBc =
                    RayIntersectCapsule_(b, c, sweepRadius, start, end);
                RaycastResult edgeResultCa =
                    RayIntersectCapsule_(c, a, sweepRadius, start, end);

                result = edgeResultAb;
                if (result.isValid)
                {
                    Assert(result.tmin >= 0.0f);
                }
                if (edgeResultBc.isValid)
                {
                    if (edgeResultBc.tmin < result.tmin || !result.isValid)
                    {
                        result = edgeResultBc;
                    }
                }

                if (edgeResultCa.isValid)
                {
                    if (edgeResultCa.tmin < result.tmin || !result.isValid)
                    {
                        result = edgeResultCa;
                    }
                }
            }
        }
    }

    if (result.isValid)
    {
        Assert(result.tmin >= 0.0f);
    }

    return result;
}


// NOTE: This function assumes that the vertices are in clockwise
// winding order
internal void DrawTriangles(DebugDrawingSystem *debugDrawingSystem,
    vec3 *vertices, u32 triangleCount, vec3 edgeColor, f32 normalLength,
    vec3 normalColor, f32 lifeTime = 0.0f, b32 ignoreDepth = true)
{
    for (u32 i = 0; i < triangleCount; ++i)
    {
        u32 vertexIdx = i * 3;
        vec3 a = vertices[vertexIdx];
        vec3 b = vertices[vertexIdx + 1];
        vec3 c = vertices[vertexIdx + 2];

        DrawLine(debugDrawingSystem, a, b, edgeColor, lifeTime, ignoreDepth);
        DrawLine(debugDrawingSystem, b, c, edgeColor, lifeTime, ignoreDepth);
        DrawLine(debugDrawingSystem, c, a, edgeColor, lifeTime, ignoreDepth);

        if (normalLength > 0.0f)
        {
            // Calculate triangle face plane, copied from RayIntersectTriangle
            vec3 ab = b - a;
            vec3 ca = a - c;

            vec3 normal = Cross(ab, ca);
            Assert(LengthSq(normal) > 0.0f);
            normal = SafeNormalize(normal);
            f32 distance = Dot(normal, a);

            vec3 centroid = (a + b + c) * (1.0f / 3.0f);

            DrawLine(debugDrawingSystem, centroid,
                centroid + normal * normalLength, normalColor, lifeTime,
                ignoreDepth);
        }
    }
}

#define RayIntersectTriangle(                                                  \
    VERTICES, TRIANGLE_COUNT, START, END, RESULTS, DEBUG_STATE)                \
    RayIntersectTriangle_(VERTICES, TRIANGLE_COUNT, START, END, RESULTS,       \
        DEBUG_STATE, __FILE__, __FUNCTION__, __LINE__)

// NOTE: This function assumes that the vertices are in clockwise
// winding order
internal void RayIntersectTriangle_(vec3 *vertices, u32 triangleCount,
    vec3 start, vec3 end, RaycastResult *results,
    CollisionDebugState *debugState = NULL, const char *file = NULL,
    const char *function = NULL, u32 line = 0)
{

#ifdef COLLISION_DEBUG
    if (debugState && debugState->isRecordingEnabled)
    {
        CollisionDebugCall call = {};
        call.file = file;
        call.function = function;
        call.line = line;
        call.type = CollisionDebugCall_RayIntersectTriangle;
        call.start = start;
        call.end = end;
        call.count = triangleCount;

        u32 vertexCount = triangleCount * 3;
        if (CanAllocateArray(&debugState->memoryArena, vec3, vertexCount))
        {
            call.triangle.vertices =
                AllocateArray(&debugState->memoryArena, vec3, vertexCount);
            CopyMemory(call.triangle.vertices, vertices, vertexCount * sizeof(vec3));

            CollisionDebug_AddCall(debugState, call);
        }
    }
#endif

    for (u32 i = 0; i < triangleCount; ++i)
    {
        u32 vertexIdx = i * 3;
        vec3 a = vertices[vertexIdx];
        vec3 b = vertices[vertexIdx + 1];
        vec3 c = vertices[vertexIdx + 2];

        results[i] = RayIntersectTriangle_(a, b, c, start, end);
    }
}

#define SphereSweepTriangles(                                                  \
    VERTICES, TRIANGLE_COUNT, START, END, SWEEP_RADIUS, RESULTS, DEBUG_STATE)  \
    SphereSweepTriangles_(VERTICES, TRIANGLE_COUNT, START, END, SWEEP_RADIUS,  \
        RESULTS, DEBUG_STATE, __FILE__, __FUNCTION__, __LINE__)

// NOTE: This function assumes that the vertices are in clockwise
// winding order
internal void SphereSweepTriangles_(vec3 *vertices, u32 triangleCount,
    vec3 start, vec3 end, f32 sweepRadius, RaycastResult *results,
    CollisionDebugState *debugState = NULL, const char *file = NULL,
    const char *function = NULL, u32 line = 0)
{
#ifdef COLLISION_DEBUG
    if (debugState && debugState->isRecordingEnabled)
    {
        CollisionDebugCall call = {};
        call.file = file;
        call.function = function;
        call.line = line;
        call.type = CollisionDebugCall_SphereSweepTriangles;
        call.start = start;
        call.end = end;
        call.count = triangleCount;
        call.sweepRadius = sweepRadius;

        u32 vertexCount = triangleCount * 3;
        if (CanAllocateArray(&debugState->memoryArena, vec3, vertexCount))
        {
            call.triangle.vertices =
                AllocateArray(&debugState->memoryArena, vec3, vertexCount);
            CopyMemory(call.triangle.vertices, vertices, vertexCount * sizeof(vec3));

            CollisionDebug_AddCall(debugState, call);
        }
    }
#endif

    for (u32 i = 0; i < triangleCount; ++i)
    {
        u32 vertexIdx = i * 3;
        vec3 a = vertices[vertexIdx];
        vec3 b = vertices[vertexIdx + 1];
        vec3 c = vertices[vertexIdx + 2];

        // TODO: Decide how we would pass in the debug drawing state
        results[i] =
            SphereSweepTriangle_(a, b, c, start, end, sweepRadius, NULL);
    }
}
