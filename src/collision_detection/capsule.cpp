inline RaycastResult RayIntersectCapsule_(
    vec3 p, vec3 q, f32 radius, vec3 start, vec3 end)
{
    RaycastResult result = {};

    vec3 m = start - p;
    vec3 n = end - start;
    vec3 d = q - p;

    f32 nn = Dot(n, n);
    f32 nd = Dot(n, d);
    f32 mn = Dot(m, n);
    f32 md = Dot(m, d);
    f32 dd = Dot(d, d);
    f32 mm = Dot(m, m);
    f32 rr = radius * radius;

    // Check if segment is inside 'p' side of cylinder
    if (md > 0.0f || (md + nd > 0.0f))
    {
        // Check if segment is inside 'q' side of cylinder
        if (md < dd || (md + nd < dd))
        {
            f32 a = dd * nn - nd * nd;
            f32 k = mm - rr;
            f32 c = dd * k - md * md;

            if (Abs(a) < EPSILON)
            {
                // Segment runs parallel to cylinder axis
                if (c <= 0.0f)
                {
                    result.isValid = true;
                    if (md < 0.0f)
                    {
                        // Test against p sphere
                        result = RayIntersectSphere_(p, radius, start, end);
                    }
                    else if (md > dd)
                    {
                        // Test against q sphere
                        result = RayIntersectSphere_(q, radius, start, end);
                    }
                    else
                    {
                        result.tmin = 0.0f; // Segment is inside cylinder
                        result.hitPoint = start + result.tmin * n;
                        result.hitNormal = SafeNormalize(-n);
                    }
                }
                // else: segment is outside of cylinder
            }
            else
            {
                f32 b = dd * mn - nd * md;

                f32 disc = b * b - a * c; // Discriminant

                if (disc > 0.0f)
                {
                    f32 t = (-b - Sqrt(disc)) / a;

                    if (md + t * nd < 0.0f)
                    {
                        // Intersection outside of cylinder on 'p' side
                        // Test against 'p' sphere
                        result = RayIntersectSphere_(p, radius, start, end);
                    }
                    else if (md + t * nd > dd)
                    {
                        // Intersection outside of cylinder on 'q' side
                        // Test against 'q' sphere
                        result = RayIntersectSphere_(q, radius, start, end);
                    }
                    else
                    {
                        if (t > 0.0f && t < 1.0f)
                        {
                            result.tmin = t;
                            result.isValid = true;
                            result.hitPoint = start + n * t;
                            result.hitNormal = SafeNormalize(
                                result.hitPoint - ClosestPointOnLineSegment(
                                                      result.hitPoint, p, q));
                        }
                        // else: Intersection lies outside of segment
                    }
                }
            }
        }
    }

    if (result.isValid)
    {
        Assert(result.tmin >= 0.0f);
    }
    return result;
}

inline RaycastResult SphereSweepCapsule_(
    vec3 p, vec3 q, f32 radius, vec3 start, vec3 end, f32 sweepRadius,
    DebugDrawingSystem *debugDrawingSystem)
{
    RaycastResult result =
        RayIntersectCapsule_(p, q, radius + sweepRadius, start, end);

    return result;
}

#define RayIntersectCapsule(                                                   \
    ENDPOINTSA, ENDPOINTSB, RADII, COUNT, START, END, RESULTS, DEBUG_STATE)    \
    RayIntersectCapsule_(ENDPOINTSA, ENDPOINTSB, RADII, COUNT, START, END,     \
        RESULTS, DEBUG_STATE, __FILE__, __FUNCTION__, __LINE__)

inline void RayIntersectCapsule_(vec3 *endPointsA, vec3 *endPointsB, f32 *radii,
    u32 count, vec3 start, vec3 end, RaycastResult *results,
    CollisionDebugState *debugState = NULL, const char *file = NULL,
    const char *function = NULL, u32 line = 0)
{
#ifdef COLLISION_DEBUG
    if (debugState && debugState->isRecordingEnabled)
    {
        CollisionDebugCall call = {};
        call.file = file;
        call.function = function;
        call.line = line;
        call.type = CollisionDebugCall_RayIntersectCapsule;
        call.count = count;
        call.start = start;
        call.end = end;

        if (MemoryArena_CanAllocate(&debugState->memoryArena,
                sizeof(vec3) * 2 * count + sizeof(f32) * count))
        {
            call.capsule.endPointsA =
                AllocateArray(&debugState->memoryArena, vec3, count);
            CopyMemory(call.capsule.endPointsA, endPointsA, count * sizeof(vec3));

            call.capsule.endPointsB =
                AllocateArray(&debugState->memoryArena, vec3, count);
            CopyMemory(call.capsule.endPointsB, endPointsB, count * sizeof(vec3));

            call.capsule.radii = AllocateArray(&debugState->memoryArena, f32, count);
            CopyMemory(call.capsule.radii, radii, count * sizeof(f32));

            CollisionDebug_AddCall(debugState, call);
        }
    }
#endif

    for (u32 i = 0; i < count; ++i)
    {
        // TODO: Manually inline and optimize this
        results[i] = RayIntersectCapsule_(
            endPointsA[i], endPointsB[i], radii[i], start, end);
    }
}

#define SphereSweepCapsules(ENDPOINTSA, ENDPOINTSB, RADII, COUNT, START, END,  \
    SWEEP_RADIUS, RESULTS, DEBUG_STATE)                                        \
    SphereSweepCapsules_(ENDPOINTSA, ENDPOINTSB, RADII, COUNT, START, END,     \
        SWEEP_RADIUS, RESULTS, DEBUG_STATE, __FILE__, __FUNCTION__, __LINE__)

inline void SphereSweepCapsules_(vec3 *endPointsA, vec3 *endPointsB, f32 *radii,
    u32 count, vec3 start, vec3 end, f32 sweepRadius, RaycastResult *results,
    CollisionDebugState *debugState = NULL, const char *file = NULL,
    const char *function = NULL, u32 line = 0)
{
#ifdef COLLISION_DEBUG
    if (debugState && debugState->isRecordingEnabled)
    {
        CollisionDebugCall call = {};
        call.file = file;
        call.function = function;
        call.line = line;
        call.type = CollisionDebugCall_SphereSweepCapsules;
        call.count = count;
        call.start = start;
        call.end = end;
        call.sweepRadius = sweepRadius;

        if (MemoryArena_CanAllocate(&debugState->memoryArena,
                sizeof(vec3) * 2 * count + sizeof(f32) * count))
        {
            call.capsule.endPointsA =
                AllocateArray(&debugState->memoryArena, vec3, count);
            CopyMemory(call.capsule.endPointsA, endPointsA, count * sizeof(vec3));

            call.capsule.endPointsB =
                AllocateArray(&debugState->memoryArena, vec3, count);
            CopyMemory(call.capsule.endPointsB, endPointsB, count * sizeof(vec3));

            call.capsule.radii = AllocateArray(&debugState->memoryArena, f32, count);
            CopyMemory(call.capsule.radii, radii, count * sizeof(f32));

            CollisionDebug_AddCall(debugState, call);
        }
    }
#endif

    for (u32 i = 0; i < count; ++i)
    {
        // TODO: Manually inline and optimize this
        results[i] = SphereSweepCapsule_(
            endPointsA[i], endPointsB[i], radii[i], start, end, sweepRadius, NULL);
    }
}
