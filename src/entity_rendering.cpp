internal void RenderMeshesSystem(ecs_EntityWorld *world, MemoryArena *tempArena,
    GameMemory *memory, mat4 view, mat4 projection, vec3 cameraPosition,
    mat4 lightViewProjection, Material *materials, u32 materialCount)
{
    TIMED_BLOCK();

    BEGIN_TIMED_BLOCK(RenderMeshesSystem_GetData);
    u32 join[] = {RenderedMeshComponentId, WorldTransformComponentId,
        RenderedMaterialComponentId, IsVisibleComponentId};

    EntityId entitiesToRender[MAX_ENTITIES];
    u32 count = ecs_Join(world, join, ArrayCount(join), entitiesToRender,
        ArrayCount(entitiesToRender));

    u32 meshes[MAX_ENTITIES];
    u32 materialIds[MAX_ENTITIES];
    mat4 worldTransforms[MAX_ENTITIES];
    b32 isVisible[MAX_ENTITIES];

    ecs_GetComponent(world, RenderedMeshComponentId, entitiesToRender, count,
        meshes, sizeof(meshes[0]));
    ecs_GetComponent(world, RenderedMaterialComponentId, entitiesToRender,
        count, materialIds, sizeof(materialIds[0]));
    ecs_GetComponent(world, WorldTransformComponentId, entitiesToRender, count, worldTransforms,
        sizeof(worldTransforms[0]));
    ecs_GetComponent(world, IsVisibleComponentId, entitiesToRender, count, isVisible,
        sizeof(isVisible[0]));
    END_TIMED_BLOCK(RenderMeshesSystem_GetData);

    BEGIN_TIMED_BLOCK(RenderMeshesSystem_CreateRenderCommands);

    mat4 viewProjection = projection * view;
    // Actual system code starts here
    for (u32 entityIdx = 0; entityIdx < count; ++entityIdx)
    {
        u32 mesh = meshes[entityIdx];
        b32 isInstancedMesh = (mesh == Mesh_Rock01 || mesh == Mesh_Rock02 ||
                               mesh == Mesh_Rock03 || mesh == Mesh_Rock04);
        if (isVisible[entityIdx] && !isInstancedMesh)
        {
            u32 materialIdx = materialIds[entityIdx];
            Assert(materialIdx < materialCount);
            Material material = materials[materialIdx];

            RenderCommand_DrawMesh *drawMesh =
                AllocateRenderCommand(memory, RenderCommand_DrawMesh);
            drawMesh->mesh = meshes[entityIdx];
            drawMesh->shader = material.shader;

            if (material.useLightingData)
            {
                AddShaderUniformValueRenderTarget(drawMesh,
                    UniformValue_ShadowMap, RenderTarget_ShadowBuffer);
                AddShaderUniformValueMat4(drawMesh,
                    UniformValue_LightViewProjection, lightViewProjection);
                AddShaderUniformValueBuffer(drawMesh, UniformValue_LightingData,
                    UniformBuffer_LightingData);
                AddShaderUniformValueVec3(
                    drawMesh, UniformValue_CameraPosition, cameraPosition);
            }

            if (material.useTexture)
            {
                AddShaderUniformValueTexture(
                    drawMesh, UniformValue_AlbedoTexture, material.texture);
            }
            else if (!material.ignoreColor)
            {
                AddShaderUniformValueVec4(
                    drawMesh, UniformValue_Color, material.color);
            }

            if (material.useNormalMap)
            {
                AddShaderUniformValueTexture(
                    drawMesh, UniformValue_NormalMap, material.texture);
            }

            if (material.useBoneTransformsBuffer)
            {
                AddShaderUniformValueBuffer(drawMesh,
                    UniformValue_BoneTransformsBuffer,
                    UniformBuffer_BoneTransforms);
                AddShaderUniformValueMat4(
                    drawMesh, UniformValue_ViewProjection, viewProjection);
            }
            else
            {
                AddShaderUniformValueMat4(drawMesh, UniformValue_MvpMatrix,
                    viewProjection * worldTransforms[entityIdx]);
                AddShaderUniformValueMat4(drawMesh, UniformValue_ModelMatrix,
                    worldTransforms[entityIdx]);
            }
        }
    }
    END_TIMED_BLOCK(RenderMeshesSystem_CreateRenderCommands);
}

struct QSortEntry
{
    u32 mesh;
    EntityId entityId;
};

inline int CompareQSortEntry(const void *p0, const void *p1)
{
    QSortEntry *a = (QSortEntry *)p0;
    QSortEntry *b = (QSortEntry *)p1;

    if (a->mesh < b->mesh) return -1;
    if (a->mesh == b->mesh) return 0;
    else return 1;
};

// TODO: Slowest part of rendering at the moment
// FIXME: Should probably put a bit more effort and testing into sorting
internal void SortEntitiesByMesh(u32 *meshes, EntityId *entities, u32 count)
{
    TIMED_BLOCK();
#if 1
    QSortEntry entries[MAX_ENTITIES];
    for (u32 i = 0; i < count; ++i)
    {
        entries[i].mesh = meshes[i];
        entries[i].entityId = entities[i];
    }
    qsort(entries, count, sizeof(entries[0]), CompareQSortEntry);
    for (u32 i = 0; i < count; ++i)
    {
        meshes[i] = entries[i].mesh;
        entities[i] = entries[i].entityId;
    }
#else
    if (count > 1)
    {
        for (u32 i = 0; i < count - 1; ++i)
        {
            for (u32 j = 0; j < count - 1; ++j)
            {
                u32 meshA = meshes[j];
                u32 meshB = meshes[j + 1];
                if (meshA > meshB)
                {
                    meshes[j] = meshB;
                    meshes[j + 1] = meshA;
                    EntityId temp = entities[j];
                    entities[j] = entities[j + 1];
                    entities[j + 1] = temp;
                }
            }
        }
    }
#endif
}

// NOTE: Input array must be sorted!
inline u32 Partition(u32 *input, u32 count, u32 *offsets, u32 *lengths)
{
    TIMED_BLOCK();
    u32 result = 1;
    u32 stringLength = 1;
    offsets[0] = 0;
    lengths[0] = count;
    for (u32 i = 1; i < count; ++i)
    {
        if (input[i] != input[i-1])
        {
            u32 outputIdx = result++;
            offsets[outputIdx] = i;
            lengths[outputIdx] = count - i;
            lengths[outputIdx-1] = stringLength;
            stringLength = 1;
        }
        else
        {
            stringLength++;
        }
    }

    return result;
}

internal void RenderInstancedMeshesSystem(ecs_EntityWorld *world,
    MemoryArena *tempArena, GameMemory *memory, mat4 view, mat4 projection,
    vec3 cameraPosition, mat4 lightViewProjection, Material *materials,
    u32 materialCount)
{
    TIMED_BLOCK();

    u32 join[] = {RenderedMeshComponentId, StaticComponentId};

    BEGIN_TIMED_BLOCK(RenderInstancedMeshesSystem_Join);
    EntityId allEntities[MAX_ENTITIES];
    u32 allEntitiesCount = ecs_Join(
        world, join, ArrayCount(join), allEntities, ArrayCount(allEntities));
    END_TIMED_BLOCK(RenderInstancedMeshesSystem_Join);

    // TODO: Instancing should be supported at a material level, all meshes should support it

    // Filter by list of known meshes which support instancing
    BEGIN_TIMED_BLOCK(RenderInstancedMeshesSystem_FilterMeshes);
    u32 meshes[MAX_ENTITIES];
    ecs_GetComponent(world, RenderedMeshComponentId, allEntities,
        allEntitiesCount, meshes, sizeof(meshes[0]));

    EntityId entitiesToRender[MAX_ENTITIES];
    u32 count = 0;
    for (u32 entityIdx = 0; entityIdx < allEntitiesCount; ++entityIdx)
    {
        u32 mesh = meshes[entityIdx];
        b32 isInstancedMesh = (mesh == Mesh_Rock01 || mesh == Mesh_Rock02 ||
                               mesh == Mesh_Rock03 || mesh == Mesh_Rock04);
        if (isInstancedMesh)
        {
            entitiesToRender[count++] = allEntities[entityIdx];
        }
    }
    ecs_GetComponent(world, RenderedMeshComponentId, entitiesToRender, count,
        meshes, sizeof(meshes[0]));

    SortEntitiesByMesh(meshes, entitiesToRender, count);
    END_TIMED_BLOCK(RenderInstancedMeshesSystem_FilterMeshes);

    u32 offsets[MAX_ENTITIES];
    u32 lengths[MAX_ENTITIES];
    u32 uniqueCount = Partition(meshes, count, offsets, lengths);

    BEGIN_TIMED_BLOCK(RenderInstancedMeshesSystem_GetComponentData);
    u32 materialIds[MAX_ENTITIES];
    mat4 *localToWorldMatrices = AllocateArray(tempArena, mat4, count);

    ecs_GetComponent(world, RenderedMaterialComponentId, entitiesToRender,
        count, materialIds, sizeof(materialIds[0]));
    ecs_GetComponent(world, WorldTransformComponentId, entitiesToRender, count,
        localToWorldMatrices, sizeof(localToWorldMatrices[0]));
    END_TIMED_BLOCK(RenderInstancedMeshesSystem_GetComponentData);

    BEGIN_TIMED_BLOCK(RenderInstancedMeshesSystem_CreateRenderCommands);
    for (u32 batchIdx = 0; batchIdx < uniqueCount; ++batchIdx)
    {
        u32 offset = offsets[batchIdx];
        u32 length = lengths[batchIdx];

        if (length > 0)
        {
            u32 mesh = meshes[offset];

            RenderCommand_DrawInstancedMesh *drawMesh =
                AllocateRenderCommand(memory, RenderCommand_DrawInstancedMesh);
            drawMesh->mesh = mesh;
            drawMesh->shader = Shader_DiffuseColorInstanced;
            drawMesh->instanceCount = length;
            drawMesh->modelMatrices = localToWorldMatrices + offset;

            SetShaderUniformValueMat4(AllocateShaderUniformValue(drawMesh),
                UniformValue_ViewProjection, projection * view);

            SetShaderUniformValueRenderTarget(
                AllocateShaderUniformValue(drawMesh), UniformValue_ShadowMap,
                RenderTarget_ShadowBuffer);
            SetShaderUniformValueMat4(AllocateShaderUniformValue(drawMesh),
                UniformValue_LightViewProjection, lightViewProjection);
            SetShaderUniformValueBuffer(AllocateShaderUniformValue(drawMesh),
                UniformValue_LightingData, UniformBuffer_LightingData);
            SetShaderUniformValueVec3(AllocateShaderUniformValue(drawMesh),
                UniformValue_CameraPosition, cameraPosition);

            SetShaderUniformValueVec4(AllocateShaderUniformValue(drawMesh),
                UniformValue_Color, Vec4(0.1f, 0.2f, 0.25f, 1.0f));
        }
    }
    END_TIMED_BLOCK(RenderInstancedMeshesSystem_CreateRenderCommands);
}

internal void DrawShadowsSystem(ecs_EntityWorld *world, MemoryArena *tempArena,
        GameMemory *memory, mat4 lightViewProjection)
{
    TIMED_BLOCK();

    BEGIN_TIMED_BLOCK(DrawShadowsSystem_GetData);
    u32 join[] = {ShadowCasterComponentId, RenderedMeshComponentId,
        PositionComponentId, RotationComponentId, ScaleComponentId,
        IsVisibleComponentId};

    EntityId entitiesToRender[MAX_ENTITIES];
    u32 count = ecs_Join(world, join, ArrayCount(join), entitiesToRender,
        ArrayCount(entitiesToRender));

    u32 meshes[MAX_ENTITIES];
    vec3 positions[MAX_ENTITIES];
    quat rotations[MAX_ENTITIES];
    vec3 scales[MAX_ENTITIES];
    b32 isVisible[MAX_ENTITIES];
    mat4 worldTransforms[MAX_ENTITIES];

    ecs_GetComponent(world, RenderedMeshComponentId, entitiesToRender, count,
        meshes, sizeof(meshes[0]));
    ecs_GetComponent(world, PositionComponentId, entitiesToRender, count,
        positions, sizeof(positions[0]));
    ecs_GetComponent(world, RotationComponentId, entitiesToRender, count,
        rotations, sizeof(rotations[0]));
    ecs_GetComponent(world, ScaleComponentId, entitiesToRender, count, scales,
        sizeof(scales[0]));
    ecs_GetComponent(world, IsVisibleComponentId, entitiesToRender, count, isVisible,
        sizeof(isVisible[0]));
    ecs_GetComponent(world, WorldTransformComponentId, entitiesToRender, count,
        worldTransforms, sizeof(worldTransforms[0]));
    END_TIMED_BLOCK(DrawShadowsSystem_GetData);

    BEGIN_TIMED_BLOCK(DrawShadowsSystem_CreateRenderCommands);
    // Actual system code starts here
    for (u32 entityIdx = 0; entityIdx < count; ++entityIdx)
    {
        u32 mesh = meshes[entityIdx];
        b32 isInstancedMesh = (mesh == Mesh_Rock01 || mesh == Mesh_Rock02 ||
                               mesh == Mesh_Rock03 || mesh == Mesh_Rock04);
        if (isVisible[entityIdx] && !isInstancedMesh)
        {
            RenderCommand_DrawMesh *drawMesh =
                AllocateRenderCommand(memory, RenderCommand_DrawMesh);
            drawMesh->mesh = meshes[entityIdx];
            drawMesh->shader = Shader_Color;
            AddShaderUniformValueVec4(
                    drawMesh, UniformValue_Color, Vec4(0.0f, 0.0f, 0.0f, 1.0f));
            AddShaderUniformValueMat4(drawMesh, UniformValue_MvpMatrix,
                    lightViewProjection * worldTransforms[entityIdx]);
        }
    }
    END_TIMED_BLOCK(DrawShadowsSystem_CreateRenderCommands);
}

internal void RenderPointLightsSystem(
    ecs_EntityWorld *world, LightingDataUniformBuffer *lightingData)
{
    u32 join[] = {PointLightComponentId, LightEmitterRadiusComponentId,
        LightMaximumRangeComponentId, LightColorComponentId,
        PositionComponentId};

    EntityId entities[MAX_ENTITIES];
    u32 count =
        ecs_Join(world, join, ArrayCount(join), entities, ArrayCount(entities));

    f32 radii[MAX_ENTITIES];
    f32 ranges[MAX_ENTITIES];
    vec3 colors[MAX_ENTITIES];
    vec3 positions[MAX_ENTITIES];

    ecs_GetComponent(world, LightEmitterRadiusComponentId, entities, count,
            radii, sizeof(radii[0]));
    ecs_GetComponent(world, LightMaximumRangeComponentId, entities, count,
            ranges, sizeof(ranges[0]));
    ecs_GetComponent(world, LightColorComponentId, entities, count,
            colors, sizeof(colors[0]));
    ecs_GetComponent(world, PositionComponentId, entities, count,
            positions, sizeof(positions[0]));

    lightingData->pointLightCount = 0;
    for (u32 i = 0; i < count; ++i)
    {
        if (i < ArrayCount(lightingData->pointLights))
        {
            lightingData->pointLights[i].position.xyz = positions[i];
            lightingData->pointLights[i].color.xyz = colors[i];

            lightingData->pointLights[i].attenuation.x = radii[i];
            // TODO: Hardcode this in the shader
            lightingData->pointLights[i].attenuation.y = 0.02f;
            lightingData->pointLights[i].attenuation.z = ranges[i];

            lightingData->pointLightCount++;
        }
    }
}

internal void RenderSpotLightsSystem(
    ecs_EntityWorld *world, LightingDataUniformBuffer *lightingData)
{
    u32 join[] = {SpotLightComponentId, SpotLightInnerAngleComponentId,
        SpotLightOuterAngleComponentId, LightEmitterRadiusComponentId,
        LightMaximumRangeComponentId, LightColorComponentId,
        RotationComponentId, PositionComponentId};

    EntityId entities[MAX_ENTITIES];
    u32 count =
        ecs_Join(world, join, ArrayCount(join), entities, ArrayCount(entities));

    f32 innerAngles[MAX_ENTITIES];
    f32 outerAngles[MAX_ENTITIES];
    f32 radii[MAX_ENTITIES];
    f32 ranges[MAX_ENTITIES];
    vec3 colors[MAX_ENTITIES];
    vec3 positions[MAX_ENTITIES];
    quat rotations[MAX_ENTITIES];

    ecs_GetComponent(world, SpotLightInnerAngleComponentId, entities, count,
        innerAngles, sizeof(innerAngles[0]));
    ecs_GetComponent(world, SpotLightOuterAngleComponentId, entities, count,
        outerAngles, sizeof(outerAngles[0]));
    ecs_GetComponent(world, LightEmitterRadiusComponentId, entities, count,
        radii, sizeof(radii[0]));
    ecs_GetComponent(world, LightMaximumRangeComponentId, entities, count,
        ranges, sizeof(ranges[0]));
    ecs_GetComponent(world, LightColorComponentId, entities, count, colors,
        sizeof(colors[0]));
    ecs_GetComponent(world, PositionComponentId, entities, count, positions,
        sizeof(positions[0]));
    ecs_GetComponent(world, RotationComponentId, entities, count, rotations,
        sizeof(rotations[0]));

    lightingData->spotLightCount = 0;
    for (u32 i = 0; i < count; ++i)
    {
        if (i < ArrayCount(lightingData->spotLights))
        {
            lightingData->spotLights[i].position.xyz = positions[i];
            lightingData->spotLights[i].color.xyz = colors[i];

            vec3 direction = Normalize(Rotate(rotations[i], Vec3(0, -1, 0)));
            lightingData->spotLights[i].direction.xyz = direction;

            lightingData->spotLights[i].attenuation[0].x = radii[i];
            // TODO: Hardcode this in shader
            lightingData->spotLights[i].attenuation[0].y = 0.02f;
            lightingData->spotLights[i].attenuation[0].z = ranges[i];

            lightingData->spotLights[i].attenuation[1].x =
                Cos(Radians(outerAngles[i]));
            lightingData->spotLights[i].attenuation[1].y =
                Cos(Radians(innerAngles[i]));

            lightingData->spotLightCount++;
        }
    }
}

internal void RenderDirectionalLightsSystem(
    ecs_EntityWorld *world, LightingDataUniformBuffer *lightingData)
{
    u32 join[] = {DirectionalLightComponentId, LightColorComponentId,
        RotationComponentId};

    EntityId entities[MAX_ENTITIES];
    u32 count =
        ecs_Join(world, join, ArrayCount(join), entities, ArrayCount(entities));

    vec3 colors[MAX_ENTITIES];
    vec3 positions[MAX_ENTITIES];
    quat rotations[MAX_ENTITIES];

    ecs_GetComponent(world, LightColorComponentId, entities, count,
            colors, sizeof(colors[0]));
    ecs_GetComponent(world, RotationComponentId, entities, count, rotations,
        sizeof(rotations[0]));
    ecs_GetComponent(world, PositionComponentId, entities, count, positions,
        sizeof(positions[0]));


    lightingData->directionalLightCount = 0;
    for (u32 i = 0; i < count; ++i)
    {
        if (i < ArrayCount(lightingData->directionalLights))
        {
            vec3 direction = Normalize(Rotate(rotations[i], Vec3(0, -1, 0)));
            DrawLine(g_DebugDrawingSystem, positions[i],
                positions[i] + direction * 20.0, colors[i]);

            DrawLine(g_DebugDrawingSystem, positions[i], positions[i] + Vec3(1, 0, 0), Vec3(1, 0, 0));
            DrawLine(g_DebugDrawingSystem, positions[i], positions[i] + Vec3(0, 1, 0), Vec3(0, 1, 0));
            DrawLine(g_DebugDrawingSystem, positions[i], positions[i] + Vec3(0, 0, 1), Vec3(0, 0, 1));

            lightingData->directionalLights[i].direction.xyz = direction;
            lightingData->directionalLights[i].color.xyz = colors[i];
            lightingData->directionalLightCount++;
        }
    }
}

internal void RenderAmbientLightSystem(
    ecs_EntityWorld *world, LightingDataUniformBuffer *lightingData)
{
    u32 join[] = {AmbientLightComponentId, LightColorComponentId};

    EntityId entities[MAX_ENTITIES];
    u32 count =
        ecs_Join(world, join, ArrayCount(join), entities, ArrayCount(entities));

    vec3 colors[MAX_ENTITIES];

    ecs_GetComponent(world, LightColorComponentId, entities, count,
            colors, sizeof(colors[0]));


    for (u32 i = 0; i < count; ++i)
    {
        if (i < 1)
        {
            lightingData->ambientLightColor.xyz = colors[i];
        }
    }
}

internal void RenderPositionSystem(
    ecs_EntityWorld *world, DebugDrawingSystem *debugDrawingSystem)
{
    EntityId entities[MAX_ENTITIES];
    u32 entityCount = ecs_GetEntitiesWithComponent(
        world, PositionComponentId, entities, ArrayCount(entities));

    vec3 positions[MAX_ENTITIES];
    ecs_GetComponent(world, PositionComponentId, entities, entityCount,
        positions, sizeof(positions[0]));

    for (u32 entityIdx = 0; entityIdx < entityCount; ++entityIdx)
    {
        vec3 position = positions[entityIdx];

        DrawLine(debugDrawingSystem, position, position + Vec3(1, 0, 0),
            Vec3(1, 0, 0));
        DrawLine(debugDrawingSystem, position, position + Vec3(0, 1, 0),
            Vec3(0, 1, 0));
        DrawLine(debugDrawingSystem, position, position + Vec3(0, 0, 1),
            Vec3(0, 0, 1));
    }
}

internal void RenderSkyboxSystem(ecs_EntityWorld *world, GameMemory *memory,
    vec3 cameraRotation, mat4 projection, Material *materials,
    u32 materialCount)
{
    mat4 skyboxView = RotateX(-cameraRotation.x) * RotateY(-cameraRotation.y);

    u32 components[] = {SkyboxComponentId, RenderedMeshComponentId,
        RenderedMaterialComponentId};

    EntityId entities[MAX_ENTITIES];
    u32 count = ecs_Join(world, components, ArrayCount(components), entities,
        ArrayCount(entities));

    u32 meshes[MAX_ENTITIES];
    u32 materialIds[MAX_ENTITIES];

    ecs_GetComponent(world, RenderedMeshComponentId, entities, count,
            meshes, sizeof(meshes[0]));
    ecs_GetComponent(world, RenderedMaterialComponentId, entities, count,
        materialIds, sizeof(materialIds[0]));

    for (u32 i = 0; i < count; ++i)
    {
        u32 materialIdx = materialIds[i];
        Assert(materialIdx < materialCount);
        Material material = materials[materialIdx];
        Assert(material.useTexture);

        RenderCommand_DrawMesh *drawSkybox =
            AllocateRenderCommand(memory, RenderCommand_DrawMesh);
        drawSkybox->mesh = meshes[i];
        drawSkybox->shader = material.shader;
        AddShaderUniformValueMat4(
                drawSkybox, UniformValue_MvpMatrix, projection * skyboxView);
        AddShaderUniformValueTexture(
            drawSkybox, UniformValue_CubeMap, material.texture);
    }

}

internal void DrawLightIconsSystem(ecs_EntityWorld *world, GameMemory *memory,
    mat4 viewProjection, vec3 cameraPosition)
{
    u32 join[] = { LightColorComponentId, PositionComponentId };
    EntityId entities[MAX_ENTITIES];
    u32 entityCount =
        ecs_Join(world, join, ArrayCount(join), entities, ArrayCount(entities));

    vec3 positions[MAX_ENTITIES];
    ecs_GetComponent(world, PositionComponentId, entities, entityCount,
        positions, sizeof(positions[0]));

    vec3 colors[MAX_ENTITIES];
    ecs_GetComponent(world, LightColorComponentId, entities, entityCount,
        colors, sizeof(colors[0]));

    for (u32 entityIdx = 0; entityIdx < entityCount; ++entityIdx)
    {
        vec3 forward = SafeNormalize(cameraPosition - positions[entityIdx]);
        // TODO: Handle case when forward and global up are parallel
        vec3 right = SafeNormalize(Cross(Vec3(0, 1, 0), forward));
        vec3 up = SafeNormalize(Cross(forward, right));

        // Debug screen aligned quad code
        //DrawLine(debugDrawingSystem, p, p + right, Vec3(1, 0, 0));
        //DrawLine(debugDrawingSystem, p, p + up, Vec3(0, 1, 0));
        //DrawLine(debugDrawingSystem, p, p + forward, Vec3(0, 0, 1));

        mat4 localToWorldMatrix;
        localToWorldMatrix.columns[0] = Vec4(right, 0);
        localToWorldMatrix.columns[1] = Vec4(up, 0);
        localToWorldMatrix.columns[2] = Vec4(forward, 0);
        localToWorldMatrix.columns[3] = Vec4(positions[entityIdx], 1);

        u32 icon = Texture_CloudIcon;
        if (ecs_HasComponent(world, DirectionalLightComponentId, entities[entityIdx]))
        {
            icon = Texture_SunIcon;
        }

        RenderCommand_DrawMesh *drawIcon =
            AllocateRenderCommand(memory, RenderCommand_DrawMesh);
        drawIcon->mesh = Mesh_Quad;
        drawIcon->shader = Shader_ParticleTexture;
        AddShaderUniformValueMat4(
                drawIcon, UniformValue_MvpMatrix, viewProjection * localToWorldMatrix);
        AddShaderUniformValueTexture(
            drawIcon, UniformValue_AlbedoTexture, icon);

        f32 distance = Length(positions[entityIdx] - cameraPosition);
        f32 alpha = MapToUnitRange(0.2f, 1.0f, distance);
        vec4 color = Vec4(colors[entityIdx] * 4.0f, alpha);
        AddShaderUniformValueVec4(drawIcon, UniformValue_Color, color);
    }
}

internal void DirtyWorldTransformSystem(ecs_EntityWorld *world)
{
    TIMED_BLOCK();
    u32 join[] = {LocalTransformComponentId, IsLocalTransformDirtyComponentId,
        IsWorldTransformDirtyComponentId, WorldTransformComponentId};

    EntityId entities[MAX_ENTITIES];
    u32 entityCount =
        ecs_Join(world, join, ArrayCount(join), entities, ArrayCount(entities));

    b32 isLocalTransformDirty[MAX_ENTITIES];
    ecs_GetComponent(world, IsLocalTransformDirtyComponentId, entities,
        entityCount, isLocalTransformDirty, sizeof(isLocalTransformDirty[0]));

    for (u32 entityIdx = 0; entityIdx < entityCount; ++entityIdx)
    {
        if (isLocalTransformDirty[entityIdx])
        {
            b32 value = true;
            ecs_SetComponent(world, IsWorldTransformDirtyComponentId,
                entities + entityIdx, 1, &value, sizeof(value));

            value = false;
            ecs_SetComponent(world, IsLocalTransformDirtyComponentId,
                    entities + entityIdx, 1, &value, sizeof(value));
        }
    }
}

internal void RecursivelyDirtyWorldTransform(
    ecs_EntityWorld *world, EntityId parent, EntityId *children, EntityId *parents, u32 childCount)
{
    for (u32 childIdx = 0; childIdx < childCount; ++childIdx)
    {
        if (parents[childIdx] == parent)
        {
            b32 value = true;
            ecs_SetComponent(world, IsWorldTransformDirtyComponentId,
                children + childIdx, 1, &value, sizeof(value));
            RecursivelyDirtyWorldTransform(
                world, children[childIdx], children, parents, childCount);
        }
    }
}

internal void DirtyChildWorldTransformSystem(ecs_EntityWorld *world)
{
    TIMED_BLOCK();
    u32 join[] = {IsWorldTransformDirtyComponentId, WorldTransformComponentId};

    EntityId entities[MAX_ENTITIES];
    u32 entityCount =
        ecs_Join(world, join, ArrayCount(join), entities, ArrayCount(entities));

    b32 isWorldTransformDirty[MAX_ENTITIES];
    ecs_GetComponent(world, IsWorldTransformDirtyComponentId, entities,
        entityCount, isWorldTransformDirty, sizeof(isWorldTransformDirty[0]));

    EntityId children[MAX_ENTITIES];
    u32 childCount = ecs_GetEntitiesWithComponent(
        world, ParentComponentId, children, ArrayCount(children));

    EntityId parents[MAX_ENTITIES];
    ecs_GetComponent(world, ParentComponentId, children, childCount, parents,
        sizeof(parents[0]));

    for (u32 entityIdx = 0; entityIdx < entityCount; ++entityIdx)
    {
        if (isWorldTransformDirty[entityIdx])
        {
            RecursivelyDirtyWorldTransform(
                world, entities[entityIdx], children, parents, childCount);
        }
    }
}

internal mat4 CalculateWorldTransform(ecs_EntityWorld *world, EntityId entity)
{
    if (ecs_HasComponent(world, ParentComponentId, entity))
    {
        EntityId parent = NULL_ENTITY;
        ecs_GetComponent(
                world, ParentComponentId, &entity, 1, &parent, sizeof(parent));

        mat4 localTransform = Identity();
        ecs_GetComponent(world, LocalTransformComponentId, &entity, 1,
            &localTransform, sizeof(localTransform));

        mat4 parentTransform = CalculateWorldTransform(world, parent);
        mat4 worldTransform = parentTransform * localTransform;

        return worldTransform;
    }
    else
    {
        vec3 position;
        vec3 scale;
        quat rotation;
        ecs_GetComponent(world, PositionComponentId, &entity, 1, &position,
            sizeof(position));
        ecs_GetComponent(world, RotationComponentId, &entity, 1, &rotation,
            sizeof(rotation));
        ecs_GetComponent(
            world, ScaleComponentId, &entity, 1, &scale, sizeof(scale));

        mat4 worldTransform =
            CalculateLocalToWorldMatrix(position, rotation, scale);

        return worldTransform;
    }
}

internal void UpdateWorldTransformSystem(ecs_EntityWorld *world)
{
    TIMED_BLOCK();

    u32 join[] = {WorldTransformComponentId, IsWorldTransformDirtyComponentId};
    EntityId entities[MAX_ENTITIES];
    u32 entityCount =
        ecs_Join(world, join, ArrayCount(join), entities, ArrayCount(entities));

    vec3 positions[MAX_ENTITIES];
    quat rotations[MAX_ENTITIES];
    vec3 scales[MAX_ENTITIES];

    ecs_GetComponent(world, PositionComponentId, entities, entityCount,
        positions, sizeof(positions[0]));
    ecs_GetComponent(world, RotationComponentId, entities, entityCount,
        rotations, sizeof(rotations[0]));
    ecs_GetComponent(world, ScaleComponentId, entities, entityCount, scales,
        sizeof(scales[0]));

    b32 hasLocalTransformComponent[MAX_ENTITIES];
    ecs_HasComponent(world, LocalTransformComponentId, entities,
        entityCount, hasLocalTransformComponent);

    b32 isDirty[MAX_ENTITIES];
    ecs_GetComponent(world, IsWorldTransformDirtyComponentId, entities,
        entityCount, isDirty, sizeof(isDirty[0]));

    for (u32 entityIdx = 0; entityIdx < entityCount; ++entityIdx)
    {
        if (isDirty[entityIdx])
        {
            mat4 worldTransform =
                CalculateWorldTransform(world, entities[entityIdx]);

            ecs_SetComponent(world, WorldTransformComponentId,
                entities + entityIdx, 1, &worldTransform,
                sizeof(worldTransform));

            b32 value = false;
            ecs_SetComponent(world, IsWorldTransformDirtyComponentId,
                entities + entityIdx, 1, &value, sizeof(value));
        }
    }
}

internal void UpdateLocalTransformSystem(ecs_EntityWorld *world)
{
    TIMED_BLOCK();
    EntityId entities[MAX_ENTITIES];
    u32 entityCount = ecs_GetEntitiesWithComponent(
        world, LocalTransformComponentId, entities, ArrayCount(entities));

    vec3 positions[MAX_ENTITIES];
    vec3 scales[MAX_ENTITIES];
    quat rotations[MAX_ENTITIES];

    ecs_GetComponent(world, PositionComponentId, entities, entityCount,
        positions, sizeof(positions[0]));
    ecs_GetComponent(world, ScaleComponentId, entities, entityCount, scales,
        sizeof(scales[0]));
    ecs_GetComponent(world, RotationComponentId, entities, entityCount,
        rotations, sizeof(rotations[0]));

    mat4 transforms[MAX_ENTITIES];
    for (u32 entityIdx = 0; entityIdx < entityCount; ++entityIdx)
    {
        transforms[entityIdx] = CalculateLocalToWorldMatrix(
            positions[entityIdx], rotations[entityIdx], scales[entityIdx]);
    }

    ecs_SetComponent(world, LocalTransformComponentId, entities, entityCount,
        transforms, sizeof(transforms[0]));
    // TODO: Dirty LocalTransformComponentId
}

internal void DrawWorldTransformsSystem(
    ecs_EntityWorld *world, DebugDrawingSystem *debugDrawingSystem)
{
    TIMED_BLOCK();
    EntityId entities[MAX_ENTITIES];
    u32 join[] = {DrawWorldTransformComponentId, WorldTransformComponentId};
    u32 entityCount =
        ecs_Join(world, join, ArrayCount(join), entities, ArrayCount(entities));

    mat4 localToWorldMatrices[MAX_ENTITIES];
    ecs_GetComponent(world, WorldTransformComponentId, entities,
        entityCount, localToWorldMatrices, sizeof(localToWorldMatrices[0]));

    for (u32 entityIdx = 0; entityIdx < entityCount; ++entityIdx)
    {
        DrawAxis(debugDrawingSystem, localToWorldMatrices[entityIdx], 1.0f);
    }
}

internal void DrawBonesSystem(
    ecs_EntityWorld *world, DebugDrawingSystem *debugDrawingSystem)
{
    TIMED_BLOCK();
    u32 join[] = { BoneIdComponentId, ParentComponentId, WorldTransformComponentId };
    EntityId entities[MAX_ENTITIES];
    u32 entityCount = ecs_Join(world, join, ArrayCount(join), entities, ArrayCount(entities));

    EntityId parents[MAX_ENTITIES];
    mat4 worldTransforms[MAX_ENTITIES];
    ecs_GetComponent(world, ParentComponentId, entities, entityCount, parents,
        sizeof(parents[0]));
    ecs_GetComponent(world, WorldTransformComponentId, entities,
        entityCount, worldTransforms, sizeof(worldTransforms[0]));

    for (u32 entityIdx = 0; entityIdx < entityCount; ++entityIdx)
    {
        vec3 origin = worldTransforms[entityIdx].columns[3].xyz;

        mat4 parentTransform = Identity();
        if (ecs_HasComponent(world, WorldTransformComponentId, parents[entityIdx]))
        {
            ecs_GetComponent(world, WorldTransformComponentId, parents + entityIdx, 1,
                    &parentTransform, sizeof(parentTransform));
        }
        else
        {
            // This path should only be taken for the root node
            ecs_GetComponent(world, LocalTransformComponentId, parents + entityIdx, 1,
                    &parentTransform, sizeof(parentTransform));
        }
        vec3 parentOrigin = parentTransform.columns[3].xyz;

        DrawLine(debugDrawingSystem, origin, parentOrigin, Vec3(1, 0.7, 0.5));
    }
}

internal void UpdateAnimationsSystem(
    ecs_EntityWorld *world, GameAssets *assets, f32 animationClock)
{
    TIMED_BLOCK();
    u32 join[] = { BoneIdComponentId, ParentComponentId, WorldTransformComponentId };
    EntityId entities[MAX_ENTITIES];
    u32 entityCount = ecs_Join(world, join, ArrayCount(join), entities, ArrayCount(entities));

    u32 boneIds[MAX_ENTITIES];
    ecs_GetComponent(world, BoneIdComponentId, entities, entityCount,
            boneIds, sizeof(boneIds[0]));

    f32 rate = 1.0f;
    f32 startT = 0.0f;
    u32 animationClipIndex = 0;
    AnimationClip *animationClip = assets->animationClips + animationClipIndex;
    u32 sampleCount = animationClip->sampleCount;
    u32 boneCount = animationClip->boneCount;
    f32 secondsPerSample = 1.0f / animationClip->samplesPerSecond;

    for (u32 entityIdx = 0; entityIdx < entityCount; ++entityIdx)
    {
        f32 t = rate * (animationClock - startT); // Map global clock to local clock
        Assert(t >= 0.0f);
        // TODO: handle precision issues

        i32 sampleIndex0 = (i32)Floor(t * animationClip->samplesPerSecond);
        i32 sampleIndex1 = sampleIndex0 + 1; // Avoid chance of sampleIndices being the same

        f32 start = sampleIndex0 * secondsPerSample;
        f32 lerpT = (t - start) / secondsPerSample;

        i32 s0 = sampleIndex0 % (i32)sampleCount;
        i32 s1 = sampleIndex1 % (i32)sampleCount;

        Assert(boneIds[entityIdx] < boneCount);
        u32 boneOffset = boneIds[entityIdx] * sampleCount;

        u32 transformIndex0 = boneOffset + s0;
        u32 transformIndex1 = boneOffset + s1;

        BoneTransform *t0 = animationClip->boneTransforms + transformIndex0;
        BoneTransform *t1 = animationClip->boneTransforms + transformIndex1;

        // Lerp transform between frame N and N+1
        vec3 position = Lerp(t0->position, t1->position, lerpT);
        vec3 scale = Lerp(t0->scale, t1->scale, lerpT);
        quat rotation = Lerp(t0->rotation, t1->rotation, lerpT);

        ecs_SetComponent(world, PositionComponentId, entities + entityIdx, 1,
            &position, sizeof(position));
        ecs_SetComponent(world, ScaleComponentId, entities + entityIdx, 1,
            &scale, sizeof(scale));
        ecs_SetComponent(world, RotationComponentId, entities + entityIdx, 1,
            &rotation, sizeof(rotation));

        b32 value = true;
        ecs_SetComponent(world, IsLocalTransformDirtyComponentId,
            entities + entityIdx, 1, &value, sizeof(value));
    }
}

internal void UpdateBoneTransformsSystem(
    ecs_EntityWorld *world, MemoryArena *tempArena, GameMemory *memory, GameAssets *assets)
{
    TIMED_BLOCK();
    u32 join[] = { BoneIdComponentId, WorldTransformComponentId };
    EntityId entities[MAX_ENTITIES];
    u32 entityCount = ecs_Join(world, join, ArrayCount(join), entities, ArrayCount(entities));

    u32 boneIds[MAX_ENTITIES];
    ecs_GetComponent(world, BoneIdComponentId, entities, entityCount,
            boneIds, sizeof(boneIds[0]));

    mat4 transforms[MAX_ENTITIES];
    ecs_GetComponent(world, WorldTransformComponentId, entities, entityCount,
        transforms, sizeof(transforms[0]));

    BoneTransformsUniformBuffer *boneTransforms =
        AllocateStruct(tempArena, BoneTransformsUniformBuffer);
    ClearToZero(boneTransforms, sizeof(*boneTransforms));

    Skeleton *skeleton = &assets->skeletons[0];

    for (u32 entityIdx = 0; entityIdx < entityCount; ++entityIdx)
    {
        u32 boneId = boneIds[entityIdx];
        Assert(boneId < ArrayCount(boneTransforms->transforms));

        boneTransforms->transforms[boneId] =
            transforms[entityIdx] * skeleton->invBindPose[boneId];
    }
    boneTransforms->length = entityCount;

    RenderCommand_UpdateUniformBuffer *updateBoneTransforms =
        AllocateRenderCommand(memory, RenderCommand_UpdateUniformBuffer);
    updateBoneTransforms->id = UniformBuffer_BoneTransforms;
    updateBoneTransforms->data = boneTransforms;
    updateBoneTransforms->length = sizeof(*boneTransforms);
    updateBoneTransforms->offset = 0;
}
