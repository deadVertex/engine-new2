struct EntityRaycastResult
{
    EntityId entity;
    RaycastResult raycastResult;
};

inline BoundingBox CalculateTriangleMeshBoundingBox(TriangleMesh mesh)
{
    BoundingBox result = {};

    result.min = mesh.vertices[0];
    result.max = mesh.vertices[0];
    for (u32 i = 1; i < mesh.count; ++i)
    {
        result.min = Min(result.min, mesh.vertices[i]);
        result.max = Max(result.max, mesh.vertices[i]);
    }

    return result;
}

inline BoundingBox CalculateAabbBoundingBox(vec3 min, vec3 max, mat4 transform)
{
    BoundingBox result = {};
#if 0
    vec3 center = (min + max) * 0.5f;
    vec3 r0 = TransformVector(Abs(min - center), transform);
    vec3 r1 = TransformVector(Abs(max - center), transform);
    f32 radius = Max(Max(r0, r1));

    center = TransformPoint(center, transform);
    result.min = center - Vec3(radius);
    result.max = center + Vec3(radius);
#else
    // clang-format off
    vec3 vertices[8] = {
        { min.x, min.y, min.z },
        { min.x, min.y, max.z },
        { max.x, min.y, max.z },
        { max.x, min.y, min.z },
        { min.x, max.y, min.z },
        { min.x, max.y, max.z },
        { max.x, max.y, max.z },
        { max.x, max.y, min.z }
    };
    // clang-format on

    for (u32 i = 0; i < ArrayCount(vertices); ++i)
    {
        vertices[i] = TransformPoint(vertices[i], transform);
    }

    result.min = vertices[0];
    result.max = vertices[0];
    for (u32 i = 1; i < ArrayCount(vertices); ++i)
    {
        result.min = Min(result.min, vertices[i]);
        result.max = Max(result.max, vertices[i]);
    }
#endif

    return result;
}

internal void UpdateBroadPhaseAabbsSystem(
    ecs_EntityWorld *world, GameAssets *assets)
{
    TIMED_BLOCK();

    BEGIN_TIMED_BLOCK(UpdateBroadPhaseAabbsSystem_Join);
    u32 join[] = {CollisionShapeComponentId, RotationComponentId};

    EntityId entities[MAX_ENTITIES];
    u32 entityCount = ecs_Join(
        world, join, ArrayCount(join), entities, ArrayCount(entities));
    END_TIMED_BLOCK(UpdateBroadPhaseAabbsSystem_Join);

    BEGIN_TIMED_BLOCK(UpdateBroadPhaseAabbsSystem_GetData);
    u32 collisionShapes[MAX_ENTITIES];
    ecs_GetComponent(world, CollisionShapeComponentId, entities, entityCount,
        collisionShapes, sizeof(collisionShapes[0]));

    vec3 positions[MAX_ENTITIES];
    ecs_GetComponent(world, PositionComponentId, entities, entityCount,
        positions, sizeof(positions[0]));

    vec3 scales[MAX_ENTITIES];
    ecs_GetComponent(world, ScaleComponentId, entities, entityCount, scales,
        sizeof(scales[0]));

    quat rotations[MAX_ENTITIES];
    ecs_GetComponent(world, RotationComponentId, entities, entityCount,
        rotations, sizeof(rotations[0]));

    b32 hasBroadPhase[MAX_ENTITIES];
    ecs_HasComponent(
        world, BroadPhaseAabbComponentId, entities, entityCount, hasBroadPhase);
    END_TIMED_BLOCK(UpdateBroadPhaseAabbsSystem_GetData);

    BEGIN_TIMED_BLOCK(UpdateBroadPhaseAabbsSystem_CalculateAabbs);
    BoundingBox aabbs[MAX_ENTITIES] = {};
    for (u32 entityIdx = 0; entityIdx < entityCount; ++entityIdx)
    {
        b32 updatedAabb = false;
        switch (collisionShapes[entityIdx])
        {
        case CollisionShape_Box:
        {
            vec3 halfDims;
            ecs_GetComponent(world, BoxHalfDimensionsComponentId,
                    entities + entityIdx, 1, &halfDims, sizeof(halfDims));

            BoundingBox *aabb = aabbs + entityIdx;
            aabb->min =
                positions[entityIdx] - Hadamard(halfDims, scales[entityIdx]);
            aabb->max =
                positions[entityIdx] + Hadamard(halfDims, scales[entityIdx]);

            updatedAabb = true;
            break;
        }
        case CollisionShape_Sphere:
        {
            f32 radius;
            ecs_GetComponent(world, RadiusComponentId, entities + entityIdx, 1,
                &radius, sizeof(radius));
            BoundingBox *aabb = aabbs + entityIdx;
            aabb->min = positions[entityIdx] - scales[entityIdx] * radius;
            aabb->max = positions[entityIdx] + scales[entityIdx] * radius;

            updatedAabb = true;
            break;
        }
        case CollisionShape_Capsule:
        {
            f32 radius = 0.0f;
            ecs_GetComponent(world, RadiusComponentId, entities + entityIdx, 1,
                &radius, sizeof(radius));
            radius *= Max(scales[entityIdx]);

            vec3 endPointA = {};
            ecs_GetComponent(world, CapsuleEndPointAComponentId,
                entities + entityIdx, 1, &endPointA, sizeof(endPointA));

            vec3 endPointB = {};
            ecs_GetComponent(world, CapsuleEndPointBComponentId,
                entities + entityIdx, 1, &endPointB, sizeof(endPointB));

            mat4 localToWorldMatrix = CalculateLocalToWorldMatrix(
                positions[entityIdx], rotations[entityIdx], scales[entityIdx]);

            vec3 a = TransformPoint(endPointA, localToWorldMatrix);
            vec3 b = TransformPoint(endPointB, localToWorldMatrix);

            vec3 min = Min(a, b);
            vec3 max = Max(a, b);

            BoundingBox *aabb = aabbs + entityIdx;
            aabb->min = min - Vec3(radius);
            aabb->max = max + Vec3(radius);

            updatedAabb = true;
            break;
        }
        case CollisionShape_TriangleMesh:
        {
            if (!hasBroadPhase[entityIdx])
            {
                u32 triangleMeshId;
                ecs_GetComponent(world, TriangleMeshComponentId,
                    entities + entityIdx, 1, &triangleMeshId,
                    sizeof(triangleMeshId));

                Assert(triangleMeshId < ArrayCount(assets->triangleMeshBoundingBoxes));
                BoundingBox localBoundingBox =
                    assets->triangleMeshBoundingBoxes[triangleMeshId];

                mat4 localToWorldMatrix =
                    CalculateLocalToWorldMatrix(positions[entityIdx],
                        rotations[entityIdx], scales[entityIdx]);

                aabbs[entityIdx] =
                    CalculateAabbBoundingBox(localBoundingBox.min,
                        localBoundingBox.max, localToWorldMatrix);

                updatedAabb = true;
            }
            break;
        }
        case CollisionShape_HeightMap:
        {
            if (!hasBroadPhase[entityIdx])
            {
                mat4 localToWorldMatrix =
                    CalculateLocalToWorldMatrix(positions[entityIdx],
                        rotations[entityIdx], scales[entityIdx]);
                aabbs[entityIdx] =
                    CalculateAabbBoundingBox(Vec3(-0.5f, 0.0f, -0.5f),
                        Vec3(0.5f, 1.0f, 0.5f), localToWorldMatrix);

                updatedAabb = true;
            }
            break;
        }
        default:
            break;
        }

        if (!hasBroadPhase[entityIdx])
        {
            ecs_AddComponent(world, BroadPhaseAabbComponentId, entities + entityIdx, 1);
        }

        if (updatedAabb)
        {
            ecs_SetComponent(world, BroadPhaseAabbComponentId,
                entities + entityIdx, 1, aabbs + entityIdx, sizeof(aabbs[0]));
        }
    }
    END_TIMED_BLOCK(UpdateBroadPhaseAabbsSystem_CalculateAabbs);

}

internal u32 GetEntities(ecs_EntityWorld *world, EntityId *entities, u32 maxEntities,
        EntityId *ignoredEntities, u32 ignoreCount)
{
    TIMED_BLOCK();
    Assert(maxEntities == MAX_ENTITIES);
    u32 join[] = {CollisionShapeComponentId, BroadPhaseAabbComponentId};

    BEGIN_TIMED_BLOCK(GetEntities_Join);
    u32 entityCount =
        ecs_Join(world, join, ArrayCount(join), entities, maxEntities);
    END_TIMED_BLOCK(GetEntities_Join);

    // Remove ignored entities from the list
    for (u32 ignoreIdx = 0; ignoreIdx < ignoreCount; ++ignoreIdx)
    {
        for (u32 entityIdx = 0; entityIdx < entityCount; entityIdx++)
        {
            if (entities[entityIdx] == ignoredEntities[ignoreIdx])
            {
                u32 lastIndex = entityCount - 1;
                entities[entityIdx] = entities[lastIndex];
                entityCount--;
                break;
            }
        }
    }

    return entityCount;
}

global CollisionDebugState *g_CollisionDebugState;
global GameAssets *g_Assets;

internal EntityRaycastResult RaycastWorld(ecs_EntityWorld *world,
    vec3 start, vec3 end, EntityId *ignoredEntities, u32 ignoreCount)
{
    TIMED_BLOCK();

    vec3 aabbMin[MAX_ENTITIES];
    vec3 aabbMax[MAX_ENTITIES];
    EntityId entities[MAX_ENTITIES];
    BoundingBox aabbs[MAX_ENTITIES];

    u32 entityCount = GetEntities(
        world, entities, ArrayCount(entities), ignoredEntities, ignoreCount);

    BEGIN_TIMED_BLOCK(RaycastWorld_GetBroadPhaseAabbs);
    ecs_GetComponent(world, BroadPhaseAabbComponentId, entities, entityCount,
        aabbs, sizeof(aabbs[0]));
    for (u32 entityIdx = 0; entityIdx < entityCount; entityIdx++)
    {
        // TODO: Either modify RayIntersectAabb to take BoundingBoxes or store
        // aabbMin,aabbMax in the collision world so that we can just use
        // CopyMemory
        aabbMin[entityIdx] = aabbs[entityIdx].min;
        aabbMax[entityIdx] = aabbs[entityIdx].max;
    }
    END_TIMED_BLOCK(RaycastWorld_GetBroadPhaseAabbs);

    b32 broadPhaseResults[MAX_ENTITIES];
    BEGIN_TIMED_BLOCK(RaycastWorld_BroadPhase);
    BoundingBox rayAabb;
    rayAabb.min = Min(start, end);
    rayAabb.max = Max(start, end);
    for (u32 i = 0; i < entityCount; ++i)
    {
        broadPhaseResults[i] =
            AabbIntersect(rayAabb.min, rayAabb.max, aabbMin[i], aabbMax[i]);
    }
    END_TIMED_BLOCK(RaycastWorld_BroadPhase);

    BEGIN_TIMED_BLOCK(RaycastWorld_CollectEntityIndices);
    u32 entityIndices[MAX_ENTITIES];
    u32 narrowPhaseCount = 0;
    for (u32 entityIdx = 0; entityIdx < entityCount; ++entityIdx)
    {
        if (broadPhaseResults[entityIdx])
        {
            entityIndices[narrowPhaseCount++] = entityIdx;
        }
    }
    END_TIMED_BLOCK(RaycastWorld_CollectEntityIndices);

    BEGIN_TIMED_BLOCK(RaycastWorld_NarrowPhase);
    // Playing it safe, no early termination if a narrow phase intersection is found
    RaycastResult closestRaycastResult = {};
    EntityId closestEntity = NULL_ENTITY;
    for (u32 raycastIdx = 0; raycastIdx < narrowPhaseCount; ++raycastIdx)
    {
        u32 entityIdx = entityIndices[raycastIdx];
        EntityId entity = entities[entityIdx];

        u32 collisionShape;
        ecs_GetComponent(world, CollisionShapeComponentId, &entity, 1,
            &collisionShape, sizeof(collisionShape));

        vec3 position;
        ecs_GetComponent(world, PositionComponentId, &entity, 1, &position,
            sizeof(position));

        vec3 scale;
        ecs_GetComponent(
            world, ScaleComponentId, &entity, 1, &scale, sizeof(scale));

        quat rotation;
        ecs_GetComponent(world, RotationComponentId, &entity, 1, &rotation,
            sizeof(rotation));

        RaycastResult potentialResult = {};
        switch (collisionShape)
        {
            case CollisionShape_Box:
            {
                vec3 halfDims = {};
                ecs_GetComponent(world, BoxHalfDimensionsComponentId, &entity,
                        1, &halfDims, sizeof(halfDims));
                halfDims = Hadamard(halfDims, scale);

                vec3 min = position - halfDims;
                vec3 max = position + halfDims;

                RayIntersectAabb(&min, &max, 1, start, end, &potentialResult,
                    g_CollisionDebugState);
                break;
            }
            case CollisionShape_Sphere:
            {
                f32 radius;
                ecs_GetComponent(world, RadiusComponentId, &entity, 1, &radius,
                    sizeof(radius));
                radius = radius * Max(scale);
                RayIntersectSphere(&position, &radius, 1, start, end,
                    &potentialResult, g_CollisionDebugState);
                break;
            }
            case CollisionShape_Capsule:
            {
                f32 radius = 0.0f;
                ecs_GetComponent(world, RadiusComponentId, &entity, 1, &radius,
                    sizeof(radius));
                radius *= Max(scale);

                vec3 endPointA = {};
                ecs_GetComponent(world, CapsuleEndPointAComponentId, &entity, 1,
                    &endPointA, sizeof(endPointA));

                vec3 endPointB = {};
                ecs_GetComponent(world, CapsuleEndPointBComponentId, &entity, 1,
                    &endPointB, sizeof(endPointB));

                mat4 localToWorldMatrix =
                    CalculateLocalToWorldMatrix(position, rotation, scale);

                endPointA = TransformPoint(endPointA, localToWorldMatrix);
                endPointB = TransformPoint(endPointB, localToWorldMatrix);

                RayIntersectCapsule(&endPointA, &endPointB, &radius, 1, start,
                    end, &potentialResult, g_CollisionDebugState);
                break;
            }
            case CollisionShape_TriangleMesh:
            {
                u32 triangleMeshId;
                ecs_GetComponent(world, TriangleMeshComponentId, &entity, 1,
                    &triangleMeshId, sizeof(triangleMeshId));

                Assert(triangleMeshId < ArrayCount(g_Assets->triangleMeshes));
                TriangleMesh mesh = g_Assets->triangleMeshes[triangleMeshId];

                mat4 localToWorldMatrix =
                    CalculateLocalToWorldMatrix(position, rotation, scale);

                // TODO: Pass in tempArena
                vec3 vertices[4096];
                Assert(mesh.count < ArrayCount(vertices));
                for (u32 i = 0; i < mesh.count; ++i)
                {
                    vertices[i] =
                        TransformPoint(mesh.vertices[i], localToWorldMatrix);
                }

                RaycastResult raycastResults[1024];
                u32 triangleCount = mesh.count / 3;
                Assert(triangleCount < ArrayCount(raycastResults));
                RayIntersectTriangle(vertices, triangleCount, start, end,
                    raycastResults, g_CollisionDebugState);

                RaycastResult *closestTriangleResult =
                    FindClosestRaycastResult(raycastResults, triangleCount);
                if (closestTriangleResult != NULL)
                {
                    potentialResult = *closestTriangleResult;
                }
                break;
            }
            case CollisionShape_HeightMap:
            {
                potentialResult = RaycastHeightMap(g_Assets->heightMap,
                    g_Assets->heightMapQuadTree, position, scale, start, end);
                break;
            }
            default:
            break;
        }

        if (potentialResult.isValid)
        {
            if (potentialResult.tmin < closestRaycastResult.tmin ||
                    !closestRaycastResult.isValid)
            {
                closestRaycastResult = potentialResult;
                closestEntity = entity;
            }
        }
    }
    END_TIMED_BLOCK(RaycastWorld_NarrowPhase);

    EntityRaycastResult result = {};
    result.raycastResult = closestRaycastResult;
    result.entity = closestEntity;
    return result;
}

internal EntityRaycastResult SphereSweepWorld(ecs_EntityWorld *world,
    vec3 start, vec3 end, f32 sweepRadius, EntityId *ignoredEntities,
    u32 ignoreCount)
{
    TIMED_BLOCK();

    vec3 aabbMin[MAX_ENTITIES];
    vec3 aabbMax[MAX_ENTITIES];
    EntityId entities[MAX_ENTITIES];
    BoundingBox aabbs[MAX_ENTITIES];

    u32 entityCount = GetEntities(
        world, entities, ArrayCount(entities), ignoredEntities, ignoreCount);

    BEGIN_TIMED_BLOCK(SphereSweepWorld_GetBroadPhaseAabbs);
    ecs_GetComponent(world, BroadPhaseAabbComponentId, entities, entityCount,
        aabbs, sizeof(aabbs[0]));
    for (u32 entityIdx = 0; entityIdx < entityCount; entityIdx++)
    {
        // TODO: Either modify RayIntersectAabb to take BoundingBoxes or store
        // aabbMin,aabbMax in the collision world so that we can just use
        // CopyMemory
        aabbMin[entityIdx] = aabbs[entityIdx].min;
        aabbMax[entityIdx] = aabbs[entityIdx].max;
    }
    END_TIMED_BLOCK(SphereSweepWorld_GetBroadPhaseAabbs);

    b32 broadPhaseResults[MAX_ENTITIES];
    BEGIN_TIMED_BLOCK(SphereSweepWorld_BroadPhase);
    BoundingBox rayAabb;
    rayAabb.min = Min(start, end) - Vec3(sweepRadius);
    rayAabb.max = Max(start, end) + Vec3(sweepRadius);
    for (u32 i = 0; i < entityCount; ++i)
    {
        broadPhaseResults[i] =
            AabbIntersect(rayAabb.min, rayAabb.max, aabbMin[i], aabbMax[i]);
    }
    END_TIMED_BLOCK(SphereSweepWorld_BroadPhase);

    BEGIN_TIMED_BLOCK(SphereSweepWorld_CollectEntityIndices);
    u32 entityIndices[MAX_ENTITIES];
    u32 narrowPhaseCount = 0;
    for (u32 entityIdx = 0; entityIdx < entityCount; ++entityIdx)
    {
        if (broadPhaseResults[entityIdx])
        {
            entityIndices[narrowPhaseCount++] = entityIdx;
        }
    }
    END_TIMED_BLOCK(SphereSweepWorld_CollectEntityIndices);

    BEGIN_TIMED_BLOCK(SphereSweepWorld_NarrowPhase);
    // Playing it safe, no early termination if a narrow phase intersection is found
    RaycastResult closestRaycastResult = {};
    EntityId closestEntity = NULL_ENTITY;
    for (u32 raycastIdx = 0; raycastIdx < narrowPhaseCount;
         ++raycastIdx)
    {
        u32 entityIdx = entityIndices[raycastIdx];
        EntityId entity = entities[entityIdx];

        u32 collisionShape;
        ecs_GetComponent(world, CollisionShapeComponentId, &entity, 1,
            &collisionShape, sizeof(collisionShape));

        vec3 position;
        ecs_GetComponent(world, PositionComponentId, &entity, 1, &position,
            sizeof(position));

        vec3 scale;
        ecs_GetComponent(
            world, ScaleComponentId, &entity, 1, &scale, sizeof(scale));

        quat rotation;
        ecs_GetComponent(world, RotationComponentId, &entity, 1, &rotation,
            sizeof(rotation));

        RaycastResult potentialResult = {};
        switch (collisionShape)
        {
        case CollisionShape_Box:
        {
            vec3 halfDims = {};
            ecs_GetComponent(world, BoxHalfDimensionsComponentId, &entity, 1,
                &halfDims, sizeof(halfDims));
            halfDims = Hadamard(halfDims, scale);

            vec3 min = position - halfDims;
            vec3 max = position + halfDims;

            // clang-format off
            vec3 vertices[] = {
                // -Y face
                Vec3(min.x, min.y, max.z),
                Vec3(max.x, min.y, max.z),
                Vec3(max.x, min.y, min.z),
                Vec3(max.x, min.y, min.z),
                Vec3(min.x, min.y, min.z),
                Vec3(min.x, min.y, max.z),

                // +Y face
                Vec3(max.x, max.y, min.z),
                Vec3(max.x, max.y, max.z),
                Vec3(min.x, max.y, max.z),
                Vec3(min.x, max.y, max.z),
                Vec3(min.x, max.y, min.z),
                Vec3(max.x, max.y, min.z),

                // -X face
                Vec3(min.x, min.y, min.z),
                Vec3(min.x, max.y, min.z),
                Vec3(min.x, max.y, max.z),
                Vec3(min.x, max.y, max.z),
                Vec3(min.x, min.y, max.z),
                Vec3(min.x, min.y, min.z),

                // +X face
                Vec3(max.x, max.y, max.z),
                Vec3(max.x, max.y, min.z),
                Vec3(max.x, min.y, min.z),
                Vec3(max.x, min.y, min.z),
                Vec3(max.x, min.y, max.z),
                Vec3(max.x, max.y, max.z),

                // -Z face
                Vec3(max.x, min.y, min.z),
                Vec3(max.x, max.y, min.z),
                Vec3(min.x, max.y, min.z),
                Vec3(min.x, max.y, min.z),
                Vec3(min.x, min.y, min.z),
                Vec3(max.x, min.y, min.z),

                // +Z face
                Vec3(min.x, max.y, max.z),
                Vec3(max.x, max.y, max.z),
                Vec3(max.x, min.y, max.z),
                Vec3(max.x, min.y, max.z),
                Vec3(min.x, min.y, max.z),
                Vec3(min.x, max.y, max.z),
            };
            // clang-format on
            RaycastResult triangleResults[1024];
            u32 triangleCount = ArrayCount(vertices) / 3;
            Assert(triangleCount < ArrayCount(triangleResults));
            SphereSweepTriangles(vertices, triangleCount, start, end,
                sweepRadius, triangleResults, g_CollisionDebugState);

            RaycastResult *closestTriangleResult =
                FindClosestRaycastResult(triangleResults, triangleCount);
            if (closestTriangleResult != NULL)
            {
                potentialResult = *closestTriangleResult;
                Assert(potentialResult.tmin >= 0.0f);
            }
            break;
        }
        case CollisionShape_Sphere:
        {
            f32 radius = 0.0f;
            ecs_GetComponent(
                world, RadiusComponentId, &entity, 1, &radius, sizeof(radius));
            radius *= Max(scale);

            SphereSweepSpheres(&position, &radius, 1, start, end, sweepRadius,
                &potentialResult, g_CollisionDebugState);
            break;
        }
        case CollisionShape_Capsule:
        {
            f32 radius = 0.0f;
            ecs_GetComponent(world, RadiusComponentId, &entity, 1, &radius,
                sizeof(radius));
            radius *= Max(scale);

            vec3 endPointA = {};
            ecs_GetComponent(world, CapsuleEndPointAComponentId, &entity, 1,
                &endPointA, sizeof(endPointA));

            vec3 endPointB = {};
            ecs_GetComponent(world, CapsuleEndPointBComponentId, &entity, 1,
                &endPointB, sizeof(endPointB));

            mat4 localToWorldMatrix =
                CalculateLocalToWorldMatrix(position, rotation, scale);

            endPointA = TransformPoint(endPointA, localToWorldMatrix);
            endPointB = TransformPoint(endPointB, localToWorldMatrix);

            SphereSweepCapsules(&endPointA, &endPointB, &radius, 1, start, end,
                sweepRadius, &potentialResult, g_CollisionDebugState);
            break;
        }
        case CollisionShape_TriangleMesh:
        {
            u32 triangleMeshId;
            ecs_GetComponent(world, TriangleMeshComponentId, &entity, 1,
                &triangleMeshId, sizeof(triangleMeshId));

            Assert(triangleMeshId < ArrayCount(g_Assets->triangleMeshes));
            TriangleMesh mesh = g_Assets->triangleMeshes[triangleMeshId];

            mat4 localToWorldMatrix =
                CalculateLocalToWorldMatrix(position, rotation, scale);

            // TODO: Pass in tempArena
            vec3 vertices[4096];
            Assert(mesh.count < ArrayCount(vertices));
            for (u32 i = 0; i < mesh.count; ++i)
            {
                vertices[i] = TransformPoint(mesh.vertices[i], localToWorldMatrix);
            }

            RaycastResult raycastResults[1024];
            u32 triangleCount = mesh.count / 3;
            Assert(triangleCount < ArrayCount(raycastResults));
            SphereSweepTriangles(vertices, triangleCount, start, end,
                sweepRadius, raycastResults, g_CollisionDebugState);

            RaycastResult *closestTriangleResult =
                FindClosestRaycastResult(raycastResults, triangleCount);
            if (closestTriangleResult != NULL)
            {
                potentialResult = *closestTriangleResult;
                Assert(potentialResult.tmin >= 0.0f);
            }
            break;
        }
        default:
            break;
        }
        if (potentialResult.isValid)
        {
            if (potentialResult.tmin < closestRaycastResult.tmin ||
                !closestRaycastResult.isValid)
            {
                closestRaycastResult = potentialResult;
                closestEntity = entity;
            }
        }
    }
    END_TIMED_BLOCK(SphereSweepWorld_NarrowPhase);

    EntityRaycastResult result = {};
    result.raycastResult = closestRaycastResult;
    result.entity = closestEntity;
    return result;
}

internal void DrawBroadPhase(
    ecs_EntityWorld *world, DebugDrawingSystem *debugDrawingSystem, vec3 color)
{
    EntityId entities[MAX_ENTITIES];

    u32 join[] = { BroadPhaseAabbComponentId, PositionComponentId };
    u32 entityCount = ecs_Join(world, join, ArrayCount(join), entities, ArrayCount(entities));

    BoundingBox aabbs[MAX_ENTITIES];
    ecs_GetComponent(world, BroadPhaseAabbComponentId, entities, entityCount,
        aabbs, sizeof(aabbs[0]));

    for (u32 entityIdx = 0; entityIdx < entityCount; ++entityIdx)
    {
        DrawBox(debugDrawingSystem, aabbs[entityIdx].min, aabbs[entityIdx].max,
            color);
    }

}

internal void DrawBoxesCollisionShape(ecs_EntityWorld *world,
    DebugDrawingSystem *debugDrawingSystem, vec3 color)
{
    u32 join[] = {BoxHalfDimensionsComponentId, PositionComponentId, ScaleComponentId};

    EntityId entities[MAX_ENTITIES];
    u32 count =
        ecs_Join(world, join, ArrayCount(join), entities, ArrayCount(entities));

    vec3 positions[MAX_ENTITIES];
    vec3 halfDims[MAX_ENTITIES];
    vec3 scales[MAX_ENTITIES];

    ecs_GetComponent(world, PositionComponentId, entities, count, positions,
        sizeof(positions[0]));
    ecs_GetComponent(world, BoxHalfDimensionsComponentId, entities, count,
        halfDims, sizeof(halfDims[0]));
    ecs_GetComponent(
        world, ScaleComponentId, entities, count, scales, sizeof(scales[0]));

    for (u32 i = 0; i < count; ++i)
    {
        vec3 radius = Hadamard(halfDims[i], scales[i]);
        vec3 min = positions[i] - radius;
        vec3 max = positions[i] + radius;

        DrawBox(debugDrawingSystem, min, max, color);
    }
}

internal void DrawSphereCollisionShapesSystem(ecs_EntityWorld *world,
        DebugDrawingSystem *debugDrawingSystem, vec3 color)
{
    u32 join[] = {RadiusComponentId, CollisionShapeComponentId, PositionComponentId};

    EntityId entities[MAX_ENTITIES];
    u32 entityCount =
        ecs_Join(world, join, ArrayCount(join), entities, ArrayCount(entities));

    u32 collisionShapes[MAX_ENTITIES];
    ecs_GetComponent(world, CollisionShapeComponentId, entities, entityCount,
        collisionShapes, sizeof(collisionShapes[0]));

    for (u32 entityIdx = 0; entityIdx < entityCount; )
    {
        if (collisionShapes[entityIdx] != CollisionShape_Sphere)
        {
            u32 lastIdx = entityCount - 1;
            entities[entityIdx] = entities[lastIdx];
            collisionShapes[entityIdx] = collisionShapes[lastIdx];
            entityCount--;
        }
        else
        {
            entityIdx++;
        }
    }

    vec3 positions[MAX_ENTITIES];
    f32 radii[MAX_ENTITIES];

    ecs_GetComponent(world, PositionComponentId, entities, entityCount, positions,
        sizeof(positions[0]));
    ecs_GetComponent(world, RadiusComponentId, entities, entityCount, radii,
        sizeof(radii[0]));

    for (u32 i = 0; i < entityCount; ++i)
    {
        DrawSphere(debugDrawingSystem, positions[i], radii[i], color);
    }
}

internal void DrawCapsuleCollisionShapesSystem(ecs_EntityWorld *world,
        DebugDrawingSystem *debugDrawingSystem, vec3 color)
{
    u32 join[] = {CapsuleEndPointAComponentId, CapsuleEndPointBComponentId,
        RadiusComponentId, PositionComponentId};

    EntityId entities[MAX_ENTITIES];
    u32 count =
        ecs_Join(world, join, ArrayCount(join), entities, ArrayCount(entities));

    vec3 positions[MAX_ENTITIES];
    vec3 scales[MAX_ENTITIES];
    quat rotations[MAX_ENTITIES];
    f32 radii[MAX_ENTITIES];
    vec3 endPointA[MAX_ENTITIES];
    vec3 endPointB[MAX_ENTITIES];

    ecs_GetComponent(world, PositionComponentId, entities, count, positions,
        sizeof(positions[0]));
    ecs_GetComponent(world, ScaleComponentId, entities, count, scales,
        sizeof(scales[0]));
    ecs_GetComponent(world, RotationComponentId, entities, count, rotations,
        sizeof(rotations[0]));
    ecs_GetComponent(world, RadiusComponentId, entities, count, radii,
        sizeof(radii[0]));
    ecs_GetComponent(world, CapsuleEndPointAComponentId, entities, count, endPointA,
        sizeof(endPointA[0]));
    ecs_GetComponent(world, CapsuleEndPointBComponentId, entities, count, endPointB,
        sizeof(endPointB[0]));

    mat4 localToWorldMatrices[MAX_ENTITIES];
    for (u32 entityIdx = 0; entityIdx < count; ++entityIdx)
    {
        localToWorldMatrices[entityIdx] = CalculateLocalToWorldMatrix(
            positions[entityIdx], rotations[entityIdx], scales[entityIdx]);
    }

    for (u32 i = 0; i < count; ++i)
    {
        vec3 a = TransformPoint(endPointA[i], localToWorldMatrices[i]);
        vec3 b = TransformPoint(endPointB[i], localToWorldMatrices[i]);
        f32 radius = radii[i] * Max(scales[i]);
        DrawCapsule(debugDrawingSystem, a, b, radius, 15, color);
    }
}

// TODO: Only draw meshes that are contained in aabb
internal void DrawTriangleMeshCollisionShapeSystem(ecs_EntityWorld *world,
    GameAssets *assets, DebugDrawingSystem *debugDrawingSystem, vec3 color,
    MemoryArena *tempArena)
{
    u32 join[] = {TriangleMeshComponentId, PositionComponentId,
        ScaleComponentId, RotationComponentId};

    EntityId entities[MAX_ENTITIES];
    u32 count =
        ecs_Join(world, join, ArrayCount(join), entities, ArrayCount(entities));

    vec3 positions[MAX_ENTITIES];
    vec3 scales[MAX_ENTITIES];
    quat rotations[MAX_ENTITIES];
    u32 triangleMeshIndices[MAX_ENTITIES];

    ecs_GetComponent(world, PositionComponentId, entities, count, positions,
        sizeof(positions[0]));
    ecs_GetComponent(
        world, ScaleComponentId, entities, count, scales, sizeof(scales[0]));
    ecs_GetComponent(world, RotationComponentId, entities, count, rotations,
        sizeof(rotations[0]));
    ecs_GetComponent(world, TriangleMeshComponentId, entities, count, triangleMeshIndices,
        sizeof(triangleMeshIndices[0]));

    mat4 localToWorldMatrices[MAX_ENTITIES];
    for (u32 entityIdx = 0; entityIdx < count; ++entityIdx)
    {
        localToWorldMatrices[entityIdx] = CalculateLocalToWorldMatrix(
            positions[entityIdx], rotations[entityIdx], scales[entityIdx]);
    }

    for (u32 i = 0; i < count; ++i)
    {
        u32 meshIndex = triangleMeshIndices[i];
        Assert(meshIndex < ArrayCount(assets->triangleMeshes));
        TriangleMesh mesh = assets->triangleMeshes[meshIndex];

        // TODO: Seems silly to have to allocate a new array just to offset each vertex,
        // maybe better to just inline draw triangles function
        vec3 *vertices = AllocateArray(tempArena, vec3, mesh.count);
        for (u32 vertexIdx = 0; vertexIdx < mesh.count; ++vertexIdx)
        {
            vertices[vertexIdx] = TransformPoint(
                mesh.vertices[vertexIdx], localToWorldMatrices[i]);
        }

        DrawTriangles(debugDrawingSystem, vertices, mesh.count / 3, color, 0.5f,
            Vec3(1, 0, 1), 0.0f);

        // FIXME: Support freeing memory!
    }
}
