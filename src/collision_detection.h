#pragma once

#define COLLISION_DEBUG

struct RaycastResult
{
    f32 tmin;
    b32 isValid;
    vec3 hitNormal;
    vec3 hitPoint;
};

#define COLLISION_DEBUG_NORMAL_COLOR Vec3(1, 0, 1)
#define COLLISION_DEBUG_RAY_COLOR Vec3(0.6, 0.6, 1)
#define COLLISION_DEBUG_RAY_INTERSECT_COLOR Vec3(0.4, 0.4, 1)
#define COLLISION_DEBUG_GEOMETRY_COLOR Vec3(1, 0.5, 0)
#define COLLISION_DEBUG_GEOMETRY_HIT_COLOR Vec3(1, 0.2, 0)

//#define COLLISION_DEBUG_RAY_TRIANGLE

enum
{
    CollisionDebugCall_RayIntersectAabb,
    CollisionDebugCall_RayIntersectTriangle,
    CollisionDebugCall_RayIntersectSphere,
    CollisionDebugCall_RayIntersectCapsule,

    CollisionDebugCall_SphereSweepSpheres,
    CollisionDebugCall_SphereSweepCapsules,
    CollisionDebugCall_SphereSweepTriangles,
};

struct CollisionDebugCall
{
    // FIXME: char* are going to break after code reload!!
    const char *file;
    const char *function;
    u32 line;
    u32 type;
    vec3 start;
    vec3 end;
    u32 count;
    f32 sweepRadius;

    union
    {
        struct
        {
            vec3 *minPoints;
            vec3 *maxPoints;
        } aabb;

        struct
        {
            vec3 *centers;
            f32 *radii;
        } sphere;

        struct
        {
            vec3 *endPointsA;
            vec3 *endPointsB;
            f32 *radii;
        } capsule;

        struct
        {
            vec3 *vertices;
        } triangle;
    };
};

#define COLLISION_DEBUG_MAX_CALLS 0x1000
struct CollisionDebugState
{
    CollisionDebugCall calls[COLLISION_DEBUG_MAX_CALLS];
    u32 count;

    b32 isRecordingEnabled;

    MemoryArena memoryArena;
};
