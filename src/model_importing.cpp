#undef global
#undef internal
#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#define internal static
#define global static

internal void AssimpLogCallback(const char *msg, char *user)
{
    //LogMessage("ASSIMP: %s", msg);
}

// NOTE: Only triangles are supported
internal u32 CopyIndicesToArray(u32 *indices, u32 length, aiMesh *mesh)
{
    u32 count = 0;
    for (u32 faceIdx = 0; faceIdx < mesh->mNumFaces; ++faceIdx)
    {
        aiFace face = mesh->mFaces[faceIdx];
        Assert(face.mNumIndices == 3);
        for (u32 j = 0; j < face.mNumIndices; ++j)
        {
            indices[count++] = face.mIndices[j];
        }
    }

    return count;
}

internal u32 CountNodesInScene(const aiScene *scene)
{
    Assert(scene->mRootNode != NULL);

    const aiNode *stack[64];
    u32 stackLength = 1;
    stack[0] = scene->mRootNode;

    u32 count = 1;
    while (stackLength > 0)
    {
        const aiNode *node = stack[--stackLength];
        Assert(node != NULL);
        count += node->mNumChildren;
        for (u32 i = 0; i < node->mNumChildren; ++i)
        {
            Assert(node->mChildren[i] != NULL);
            Assert(stackLength < ArrayCount(stack));
            stack[stackLength++] = node->mChildren[i];
        }
    }

    return count;
}

#define NODE_STACK_LENGTH 64
internal void ComputeDepthFirstTraversal(const aiNode **nodeTraversal,
    i32 *parentIndices, u32 nodeCount, const aiScene *scene)
{
    Assert(scene->mRootNode != NULL);

    const aiNode *stack[NODE_STACK_LENGTH] = {};
    i32 parentIndicesStack[NODE_STACK_LENGTH] = {};
    u32 stackLength = 1;
    stack[0] = scene->mRootNode;
    parentIndicesStack[0] = -1;

    u32 index = 0;
    while (stackLength > 0)
    {
        stackLength--;
        const aiNode *node = stack[stackLength];
        Assert(node != NULL);

        nodeTraversal[index] = node;
        parentIndices[index] = parentIndicesStack[stackLength];

        // Add nodes to stack in reverse order for depth first traversal
        for (i32 i = node->mNumChildren - 1; i >= 0; --i)
        {
            Assert(node->mChildren[i] != NULL);
            Assert(stackLength < ArrayCount(stack));
            stack[stackLength] = node->mChildren[i];
            parentIndicesStack[stackLength] = index;
            stackLength++;
        }
        index++;
    }

    Assert(index == nodeCount);
}

internal u32 CountNodeToMeshMappings(const aiNode **nodeTraversal, u32 nodeCount)
{
    u32 count = 0;
    for (u32 i = 0; i < nodeCount; ++i)
    {
        count += nodeTraversal[i]->mNumMeshes;
    }

    return count;
}

internal void CreateNodeToMeshMappings(NodeToMeshMapping *mappings, u32 maxMappings,
                                    const aiNode **nodeTraversal, u32 nodeCount)
{
    u32 count = 0;
    for (u32 nodeIdx = 0; nodeIdx < nodeCount; ++nodeIdx)
    {
        const aiNode *node = nodeTraversal[nodeIdx];
        for (u32 meshIdx = 0; meshIdx < node->mNumMeshes; ++meshIdx)
        {
            NodeToMeshMapping mapping;
            mapping.node = nodeIdx;
            mapping.mesh = node->mMeshes[meshIdx];

            Assert(count < maxMappings);
            mappings[count++] = mapping;
        }
    }
    Assert(count == maxMappings);
}

inline char *StringCopy(MemoryArena *arena, const char *src)
{
    u32 length = StringLength(src) + 1;
    char *dst = AllocateArray(arena, char, length);
    CopyMemory(dst, src, length);
    dst[length-1] = '\0';
    return dst;
}

internal SceneImportData ImportFbx(const char *fileData, u32 fileDataLength,
    MemoryArena *arena, MemoryArena *tempArena, f32 scale = 1.0f)
{
    SceneImportData result = {};

    aiLogStream logStream = {};
    logStream.callback = &AssimpLogCallback;
    aiAttachLogStream(&logStream);
    //aiEnableVerboseLogging(AI_TRUE);
    aiPropertyStore *properties = aiCreatePropertyStore();
    aiSetImportPropertyFloat(
        properties, AI_CONFIG_GLOBAL_SCALE_FACTOR_KEY, scale);
    const aiScene *scene = aiImportFileFromMemoryWithProperties(
        (const char *)fileData, fileDataLength,
        aiProcess_Triangulate | aiProcess_JoinIdenticalVertices |
            aiProcess_OptimizeMeshes | aiProcess_OptimizeGraph |
            aiProcess_LimitBoneWeights | aiProcess_GlobalScale |
            aiProcess_CalcTangentSpace,
        "", properties);
    aiDetachLogStream(&logStream);

    if (!scene)
    {
        LogMessage("Model import failed: %s", aiGetErrorString());
    }

    b32 importHeirarchy = true;
    if (importHeirarchy)
    {
        result.nodeCount = CountNodesInScene(scene);
        result.nodes = AllocateArray(arena, SceneNode, result.nodeCount);
        result.nodeNames = AllocateArray(arena, const char *, result.nodeCount);
        i32 *parentIndices = AllocateArray(tempArena, i32, result.nodeCount);

        const aiNode **nodeTraversal =
            AllocateArray(tempArena, const aiNode *, result.nodeCount);
        ComputeDepthFirstTraversal(
            nodeTraversal, parentIndices, result.nodeCount, scene);

        for (u32 nodeIdx = 0; nodeIdx < result.nodeCount; ++nodeIdx)
        {
            SceneNode *dst = result.nodes + nodeIdx;
            const aiNode *src = nodeTraversal[nodeIdx];
            i32 parent = parentIndices[nodeIdx];

            aiVector3t<f32> scaling;
            aiQuaterniont<f32> rotation;
            aiVector3t<f32> position;
            src->mTransformation.Decompose(scaling, rotation, position);

            dst->position = Vec3(position.x, position.y, position.z);
            dst->rotation =
                Quat(rotation.x, rotation.y, rotation.z, rotation.w);
            dst->scale = Vec3(scaling.x, scaling.y, scaling.z);
            dst->parent = parent;

            result.nodeNames[nodeIdx] = StringCopy(arena, src->mName.data);
        }

        result.nodeToMeshMappingCount =
            CountNodeToMeshMappings(nodeTraversal, result.nodeCount);

        result.nodeToMeshMappings = AllocateArray(
            arena, NodeToMeshMapping, result.nodeToMeshMappingCount);

        CreateNodeToMeshMappings(result.nodeToMeshMappings,
            result.nodeToMeshMappingCount, nodeTraversal, result.nodeCount);
    }

    // Allocate array for mesh data
    result.meshCount = scene->mNumMeshes;
    result.meshes = AllocateArray(arena, MeshImportData, result.meshCount);

    for (u32 meshIdx = 0; meshIdx < result.meshCount; ++meshIdx)
    {
        aiMesh *mesh = scene->mMeshes[meshIdx];
        MeshImportData *meshData = result.meshes + meshIdx;

        // Allocate array in mesh data for indices
        meshData->indexCount = mesh->mNumFaces * 3;
        meshData->indices = AllocateArray(arena, u32, meshData->indexCount);

        // Copy indices into allocated array
        u32 count = CopyIndicesToArray(meshData->indices, meshData->indexCount, mesh);
        Assert(count == meshData->indexCount);

        // Allocate vertex positional data
        meshData->vertexCount = mesh->mNumVertices;
        vec3 *vertexPositions =
            AllocateArray(arena, vec3, meshData->vertexCount);

        meshData->vertexData[VertexData_Positions].type = DataType_Vec3;
        meshData->vertexData[VertexData_Positions].data = vertexPositions;

        // Copy vertex positions into allocated array
        for (u32 vertexIdx = 0; vertexIdx < mesh->mNumVertices; ++vertexIdx)
        {
            aiVector3D v = mesh->mVertices[vertexIdx];
            vertexPositions[vertexIdx].x = v.x;
            vertexPositions[vertexIdx].y = v.y;
            vertexPositions[vertexIdx].z = v.z;
        }

        b32 importNormals = true;
        if (importNormals)
        {
            Assert(mesh->mNormals != NULL);
            vec3 *vertexNormals = AllocateArray(arena, vec3, meshData->vertexCount);

            meshData->vertexData[VertexData_Normals].type = DataType_Vec3;
            meshData->vertexData[VertexData_Normals].data = vertexNormals;

            // Copy vertex normals into allocated array
            for (u32 vertexIdx = 0; vertexIdx < meshData->vertexCount; ++vertexIdx)
            {
                aiVector3D v = mesh->mNormals[vertexIdx];
                vertexNormals[vertexIdx].x = v.x;
                vertexNormals[vertexIdx].y = v.y;
                vertexNormals[vertexIdx].z = v.z;
            }
        }

        b32 importTextureCoordinates = true;
        if (importTextureCoordinates)
        {
            // FIXME: Support multiple sets of texture coordinates
            Assert(mesh->mTextureCoords[0] != NULL);

            vec2 *vertexTextureCoordinates =
                AllocateArray(arena, vec2, mesh->mNumVertices);

            meshData->vertexData[VertexData_TextureCoords0].type =
                DataType_Vec2;
            meshData->vertexData[VertexData_TextureCoords0].data =
                vertexTextureCoordinates;

            // Copy vertex normals into allocated array
            for (u32 vertexIdx = 0; vertexIdx < mesh->mNumVertices; ++vertexIdx)
            {
                aiVector3D v = mesh->mTextureCoords[0][vertexIdx];
                vertexTextureCoordinates[vertexIdx].x = v.x;
                vertexTextureCoordinates[vertexIdx].y = v.y;
            }
        }

        b32 importTangentAndBitangent = true;
        if (importTangentAndBitangent)
        {
            Assert(mesh->mTangents != NULL);
            Assert(mesh->mBitangents != NULL);
            vec3 *vertexTangents = AllocateArray(arena, vec3, meshData->vertexCount);
            vec3 *vertexBitangents = AllocateArray(arena, vec3, meshData->vertexCount);

            meshData->vertexData[VertexData_Tangents].type = DataType_Vec3;
            meshData->vertexData[VertexData_Tangents].data = vertexTangents;
            meshData->vertexData[VertexData_Bitangents].type = DataType_Vec3;
            meshData->vertexData[VertexData_Bitangents].data = vertexBitangents;

            // Copy vertex normals into allocated array
            for (u32 vertexIdx = 0; vertexIdx < meshData->vertexCount; ++vertexIdx)
            {
                aiVector3D t = mesh->mTangents[vertexIdx];
                aiVector3D b = mesh->mBitangents[vertexIdx];
                vertexTangents[vertexIdx].x = t.x;
                vertexTangents[vertexIdx].y = t.y;
                vertexTangents[vertexIdx].z = t.z;
                vertexBitangents[vertexIdx].x = b.x;
                vertexBitangents[vertexIdx].y = b.y;
                vertexBitangents[vertexIdx].z = b.z;
            }
        }
    }

    aiReleaseImport(scene);
    aiReleasePropertyStore(properties);

    return result;
}

internal void CreateMesh(
    SceneImportData *scene, GameMemory *memory, MemoryArena *tempArena, u32 meshId)
{
    // TODO: Dynamic mesh IDs that OpenGL can use is going to be a fun problem
    Assert(scene->meshCount > 1);
    for (u32 meshIdx = 1; meshIdx < 2; ++meshIdx)
    {
        MeshImportData *mesh = scene->meshes + meshIdx;
        Assert(mesh->vertexCount > 0);
        Assert(mesh->indexCount > 0);

        Vertex *vertices = AllocateArray(tempArena, Vertex, mesh->vertexCount);
        for (u32 i = 0; i < mesh->vertexCount; ++i)
        {
            vertices[i].position = *((vec3 *)mesh->vertexData[VertexData_Positions].data + i);
            vertices[i].normal = *((vec3 *)mesh->vertexData[VertexData_Normals].data + i);
            vertices[i].tangent = *((vec3 *)mesh->vertexData[VertexData_Tangents].data + i);
            vertices[i].texCoord = *((vec2 *)mesh->vertexData[VertexData_TextureCoords0].data + i);
        }

        RenderCommand_CreateMesh *createMesh =
            AllocateRenderCommand(memory, RenderCommand_CreateMesh);
        createMesh->vertices = vertices;
        createMesh->vertexCount = mesh->vertexCount;
        createMesh->indices = mesh->indices;
        createMesh->indexCount = mesh->indexCount;
        createMesh->id = meshId;
    }
}

aiVectorKey FindVectorKeyForFrame(aiVectorKey *keys, u32 count, u32 frameIndex)
{
    Assert(count > 0);
    for (u32 i = 0; i < count; ++i)
    {
        if ((u32)keys[i].mTime == frameIndex)
        {
            return keys[i];
        }
        else if ((u32)keys[i].mTime > frameIndex)
        {
            return (i > 0) ? keys[i-1] : keys[0];
        }
    }
    return keys[0];
}

aiQuatKey FindQuatKeyForFrame(aiQuatKey *keys, u32 count, u32 frameIndex)
{
    Assert(count > 0);
    for (u32 i = 0; i < count; ++i)
    {
        if ((u32)keys[i].mTime == frameIndex)
        {
            return keys[i];
        }
        else if ((u32)keys[i].mTime > frameIndex)
        {
            return (i > 0) ? keys[i-1] : keys[0];
        }
    }
    return keys[0];
}

internal AnimationImportData ImportAnimationData(const void *assetData, u32 assetDataLength,
    MemoryArena *arena, f32 globalScale = 1.0f)
{
    AnimationImportData result = {};
    aiLogStream logStream = {};
    logStream.callback = &AssimpLogCallback;
    aiAttachLogStream(&logStream);
    aiPropertyStore *properties = aiCreatePropertyStore();
    aiSetImportPropertyFloat(
        properties, AI_CONFIG_GLOBAL_SCALE_FACTOR_KEY, globalScale);
    const aiScene *scene =
        aiImportFileFromMemoryWithProperties((const char *)assetData,
            assetDataLength, aiProcess_GlobalScale, "", properties);
    aiDetachLogStream(&logStream);

    if (!scene)
    {
        LogMessage("Animation import failed: %s", aiGetErrorString());
        return result;
    }

    result.clipCount = scene->mNumAnimations;
    result.clips = AllocateArray(arena, AnimationClipData, result.clipCount);

    for (u32 animationIndex = 0; animationIndex < scene->mNumAnimations; ++animationIndex)
    {
        const aiAnimation *animation = scene->mAnimations[animationIndex];
        AnimationClipData *clip = result.clips + animationIndex;

        clip->name = StringCopy(arena, animation->mName.data);
        clip->frameCount = (u32)animation->mDuration + 1;
        clip->framesPerSecond = (f32)animation->mTicksPerSecond;
        clip->channelCount = animation->mNumChannels;
        clip->channels = AllocateArray(arena, AnimationChannelData, clip->channelCount);

        // Channel = bone
        for (u32 channelIndex = 0; channelIndex < animation->mNumChannels; ++channelIndex)
        {
            const aiNodeAnim* src = animation->mChannels[channelIndex];
            AnimationChannelData *dst = clip->channels + channelIndex;
            dst->name = StringCopy(arena, src->mNodeName.data);
            dst->frames = AllocateArray(arena, AnimationKeyFrame, clip->frameCount);

            for (u32 frameIdx = 0; frameIdx < clip->frameCount; ++frameIdx)
            {
                aiVectorKey position = FindVectorKeyForFrame(
                    src->mPositionKeys, src->mNumPositionKeys, frameIdx);
                aiVectorKey scale = FindVectorKeyForFrame(
                    src->mScalingKeys, src->mNumScalingKeys, frameIdx);
                aiQuatKey rotation = FindQuatKeyForFrame(
                    src->mRotationKeys, src->mNumRotationKeys, frameIdx);

                AnimationKeyFrame *frame = dst->frames + frameIdx;
                frame->position = Vec3(
                    position.mValue.x, position.mValue.y, position.mValue.z);

                frame->scale =
                    Vec3(scale.mValue.x, scale.mValue.y, scale.mValue.z);

                frame->rotation = Normalize(Quat(rotation.mValue.x,
                    rotation.mValue.y, rotation.mValue.z, rotation.mValue.w));
            }
        }
    }

    aiReleaseImport(scene);
    aiReleasePropertyStore(properties);

    return result;
}

#if 0
void CalculatePoseWorldInvTransforms(mat4 *invWorldTransforms, BonePose *pose,
    Skeleton *skeleton, MemoryArena *arena)
{
    mat4 *invTransforms = AllocateArray(arena, mat4, skeleton->boneCount);

    for (u32 bone = 0; bone < skeleton->boneCount; ++bone)
    {
        BonePose *bonePose = skeleton->bindPose + bone;
        invTransforms[bone] = Scale(Vec3(1.0f / bonePose->scaling)) *
                              Rotate(-bonePose->rotation) *
                              Translate(-bonePose->translation);
    }

    for (u32 bone = 0; bone < skeleton->boneCount; ++bone)
    {
        u32 parent = (u32)skeleton->parents[bone];
        if (parent != INVALID_BONE)
        {
            Assert(parent < bone);
            invWorldTransforms[bone] =
                invTransforms[bone] * invWorldTransforms[parent];
        }
        else
        {
            invWorldTransforms[bone] = invTransforms[bone];
        }
    }

    MemoryArenaFree(arena, invTransforms);
}
#endif

internal void CreateSkeleton(
    Skeleton *skeleton, SceneImportData *data, MemoryArena *arena)
{
    // TODO: Currently we convert all nodes to bones, in the future we should
    // filter
    Assert(data->nodeCount < MAX_BONES);
    skeleton->boneCount = data->nodeCount;
    skeleton->invBindPose = AllocateArray(arena, mat4, skeleton->boneCount);
    skeleton->names = AllocateArray(arena, const char *, skeleton->boneCount);
    skeleton->parents = AllocateArray(arena, u8, skeleton->boneCount);
    skeleton->bindPose = AllocateArray(arena, BonePose, skeleton->boneCount);

    for (u32 bone = 0; bone < skeleton->boneCount; ++bone)
    {
        SceneNode *node = data->nodes + bone;

        skeleton->names[bone] = data->nodeNames[bone];
        skeleton->parents[bone] = node->parent >= 0
                                      ? SafeTruncateU32ToU8(node->parent)
                                      : INVALID_BONE;

        BonePose *bonePose = skeleton->bindPose + bone;
        bonePose->rotation = node->rotation;
        bonePose->translation = node->position;
        bonePose->scaling = node->scale.x;
    }

    //CalculatePoseWorldInvTransforms(
        //skeleton->invBindPose, skeleton->bindPose, skeleton, arena);
}

internal void CreateAnimationClip(AnimationClip *clip, AnimationImportData *data, MemoryArena *arena,
        Skeleton *skeleton, b32 isLooping, const char *name = NULL)
{
    Assert(data->clipCount > 0);
    AnimationClipData *clipData = NULL;
    if (name != NULL)
    {
        for (u32 i = 0; i < data->clipCount; ++i)
        {
            if (StringCompare(data->clips[i].name, name))
            {
                clipData = &data->clips[i];
                break;
            }
        }
        if (clipData == NULL)
        {
            LogMessage("Could not find animation clip: '%s'", name);
        }
    }

    if (clipData == NULL)
    {
        clipData = &data->clips[0];
    }

    clip->skeleton = skeleton;
    clip->samplesPerSecond = clipData->framesPerSecond;
    clip->sampleCount = clipData->frameCount;

    i32 channelIndices[MAX_BONES];
    // Map channel to bone index
    for (u32 bone = 0; bone < skeleton->boneCount; ++bone)
    {
        channelIndices[bone] = -1;
        const char *boneName = skeleton->names[bone];
        for (u32 channelIndex = 0; channelIndex < clipData->channelCount; ++channelIndex)
        {
            AnimationChannelData *channel = clipData->channels + channelIndex;
            if (StringCompare(boneName, channel->name))
            {
                channelIndices[bone] = channelIndex;
                break;
            }
        }
    }

    u32 totalSamples = skeleton->boneCount * clip->sampleCount;
    clip->samples = AllocateArray(arena, BonePose, totalSamples);
    for (u32 sampleIndex = 0; sampleIndex < clip->sampleCount; ++sampleIndex)
    {
        for (u32 bone = 0; bone < skeleton->boneCount; ++bone)
        {
            BonePose *bonePose = &clip->samples[sampleIndex * skeleton->boneCount + bone];
            if (channelIndices[bone] != -1)
            {
                // FIXME: Inefficient!
                AnimationChannelData *channel = clipData->channels + channelIndices[bone];
                bonePose->rotation = channel->frames[sampleIndex].rotation;
                bonePose->translation = channel->frames[sampleIndex].position;
                bonePose->scaling = channel->frames[sampleIndex].scale.x;
            }
            else
            {
                bonePose->rotation = Quat();
                bonePose->translation = Vec3(0.0f);
                bonePose->scaling = 1.0f;
            }
        }
    }
    clip->isLooping = isLooping;
}

internal void InitializePlaybackState(AnimationPlaybackState *state,
    AnimationClip *clip, MemoryArena *arena)
{
    Assert(clip);
    state->clip = clip;
    state->rate = 1.0f;

    u32 boneCount = clip->skeleton->boneCount;
    state->currentPose = AllocateArray(arena, BonePose, boneCount);
}

internal void LerpPose(BonePose *resultPose, u32 boneCount, BonePose *pose0,
    BonePose *pose1, f32 t)
{
    for (u32 boneIndex = 0; boneIndex < boneCount; ++boneIndex)
    {
        BonePose *bone0 = pose0 + boneIndex;
        BonePose *bone1 = pose1 + boneIndex;
        BonePose *resultBone = resultPose + boneIndex;

        resultBone->translation =
            Lerp(bone0->translation, bone1->translation, t);
        resultBone->rotation = Lerp(bone0->rotation, bone1->rotation, t);
        resultBone->scaling = Lerp(bone0->scaling, bone1->scaling, t);
    }
}

internal void UpdatePlaybackState(AnimationPlaybackState *state, f32 globalT)
{
    AnimationClip *clip = state->clip;
    f32 t = state->rate * (globalT - state->startT); // Map global clock to local clock
    // TODO: handle precision issues

    i32 sampleIndex0 = (i32)Floor(t * clip->samplesPerSecond);
    i32 sampleIndex1 = sampleIndex0 + 1; // Avoid chance of sampleIndices being the same

    f32 secondsPerSample = 1.0f / clip->samplesPerSecond;
    f32 start = sampleIndex0 * secondsPerSample;
    f32 lerpT = (t - start) / secondsPerSample;

    i32 s0 = sampleIndex0 % (i32)clip->sampleCount;
    i32 s1 = sampleIndex1 % (i32)clip->sampleCount;
    if (t < 0.0f)
    {
        s0 = ((i32)clip->sampleCount + s0) % (i32)clip->sampleCount;
        s1 = ((i32)clip->sampleCount + s1) % (i32)clip->sampleCount;
    }

    u32 boneCount = clip->skeleton->boneCount;
    u32 poseIndex0 = s0 * boneCount;
    u32 poseIndex1 = s1 * boneCount;

    LerpPose(state->currentPose, boneCount, clip->samples + poseIndex0,
                clip->samples + poseIndex1, lerpT);
}
