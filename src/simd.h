#pragma once

#ifndef SIMD_LANE_WIDTH
#define SIMD_LANE_WIDTH 1
#endif

#if (SIMD_LANE_WIDTH == 1)

typedef u32 simd_u32;
typedef f32 simd_f32;
typedef vec3 simd_vec3;

inline simd_f32 SIMD_F32(f32 a)
{
    simd_f32 result = a;
    return result;
}

inline simd_f32 SIMD_F32(simd_u32 a)
{
    simd_f32 result = (simd_f32)a;
    return result;
}

inline simd_u32 SIMD_U32(u32 a)
{
    simd_u32 result = a;
    return result;
}

inline simd_u32 SIMD_U32(u32 a, u32 b, u32 c, u32 d, u32 e, u32 f, u32 g, u32 h)
{
    simd_u32 result = a;
    return result;
}

inline void ConditionalAssign(simd_u32 *a, simd_u32 b, simd_u32 mask)
{
    *a = mask ? b : *a;
}

inline void ConditionalAssign(simd_f32 *a, simd_f32 b, simd_u32 mask)
{
    *a = mask ? b : *a;
}

inline void ConditionalAssign(simd_vec3 *a, simd_vec3 b, simd_u32 mask)
{
    *a = mask ? b : *a;
}

inline u32 HorizontalAdd(simd_u32 a)
{
    u32 result = a;
    return result;
}

inline vec3 HorizontalAdd(simd_vec3 a)
{
    vec3 result = a;
    return result;
}

inline b32 AllZero(simd_u32 a)
{
    b32 result = (a == 0);
    return result;
}

inline simd_vec3 GatherVec3_(void *base, size_t offset, size_t stride, simd_u32 index)
{
    simd_vec3 result = *(vec3*)((u8 *)base + offset + stride * index);
    return result;
}

inline simd_f32 GatherF32_(void *base, size_t offset, size_t stride, simd_u32 index)
{
    simd_f32 result = *(f32*)((u8 *)base + offset + stride * index);
    return result;
}

#elif (SIMD_LANE_WIDTH == 4)

#include "simd_sse.h"

#elif (SIMD_LANE_WIDTH == 8)

#include "simd_avx.h"

#endif

#ifdef SIMD_RNG
// TODO: Move this somewhere else
struct RandomNumberGenerator
{
    simd_u32 state;
};

// Reference implementation from https://en.wikipedia.org/wiki/Xorshift
inline simd_u32 XorShift32(RandomNumberGenerator *rng)
{
    simd_u32 x = rng->state;
	x ^= x << 13;
	x ^= x >> 17;
	x ^= x << 5;
    rng->state = x;
	return x;
}

inline simd_f32 RandomUnilateral(RandomNumberGenerator *rng)
{
    // Clear the sign bit as SSE interprets integers as signed when converting
    // to packed single
    simd_f32 numerator = SIMD_F32(XorShift32(rng) >> 1);
    simd_f32 denom = SIMD_F32(U32_MAX >> 1);
    simd_f32 result = numerator / denom;
    return result;
}

inline simd_f32 RandomBilateral(RandomNumberGenerator *rng)
{
    simd_f32 result = SIMD_F32(-1.0f) + SIMD_F32(2.0f) * RandomUnilateral(rng);
    return result;
}
#endif

inline simd_vec3 SIMD_VEC3(f32 x, f32 y, f32 z)
{
    simd_vec3 result;
    result.x = SIMD_F32(x);
    result.y = SIMD_F32(y);
    result.z = SIMD_F32(z);
    return result;
}

inline simd_vec3 SIMD_VEC3(vec3 a)
{
    simd_vec3 result;
    result.x = SIMD_F32(a.x);
    result.y = SIMD_F32(a.y);
    result.z = SIMD_F32(a.z);
    return result;
}


#if (SIMD_LANE_WIDTH != 1)
inline simd_vec3 SIMD_VEC3(simd_f32 x, simd_f32 y, simd_f32 z)
{
    simd_vec3 result;
    result.x = x;
    result.y = y;
    result.z = z;
    return result;
}

inline simd_f32 Dot(simd_vec3 a, simd_vec3 b)
{
    simd_f32 result = a.x * b.x + a.y * b.y + a.z * b.z;
    return result;
}

inline simd_f32 LengthSq(simd_vec3 a)
{
    simd_f32 result = Dot(a, a);
    return result;
}

inline simd_f32 Length(simd_vec3 v)
{
    simd_f32 result = LengthSq(v);
    result = Sqrt(result);
    return result;
}

inline simd_vec3 operator-(simd_vec3 a)
{
    simd_vec3 result;
    result.x = -a.x;
    result.y = -a.y;
    result.z = -a.z;

    return result;
}

inline simd_vec3 operator+(simd_vec3 a, simd_vec3 b)
{
    simd_vec3 result;
    result.x = a.x + b.x;
    result.y = a.y + b.y;
    result.z = a.z + b.z;
    return result;
}

inline simd_vec3 operator-(simd_vec3 a, simd_vec3 b)
{
    simd_vec3 result = a + -b;
    return result;
}

inline simd_vec3 operator*(simd_vec3 a, simd_f32 b)
{
    simd_vec3 result;
    result.x = a.x * b;
    result.y = a.y * b;
    result.z = a.z * b;

    return result;
}

inline simd_vec3 operator*(simd_f32 a, simd_vec3 b)
{
    simd_vec3 result = b * a;
    return result;
}

inline simd_vec3 Normalize(simd_vec3 v)
{
    simd_f32 invLength = SIMD_F32(1.0f) / Length(v);
    simd_vec3 result = v * invLength;
    return result;
}

inline simd_vec3 Cross(simd_vec3 a, simd_vec3 b)
{
    simd_vec3 result;

    result.x = (a.y * b.z) - (a.z * b.y);
    result.y = (a.z * b.x) - (a.x * b.z);
    result.z = (a.x * b.y) - (a.y * b.x);

    return result;
}

inline void ConditionalAssign(simd_vec3 *a, simd_vec3 b, simd_u32 mask)
{
    ConditionalAssign(&a->x, b.x, mask);
    ConditionalAssign(&a->y, b.y, mask);
    ConditionalAssign(&a->z, b.z, mask);
}

inline simd_vec3 Hadamard(simd_vec3 a, simd_vec3 b)
{
    simd_vec3 result;
    result.x = a.x * b.x;
    result.y = a.y * b.y;
    result.z = a.z * b.z;

    return result;
}

inline simd_vec3 Lerp(simd_vec3 a, simd_vec3 b, simd_f32 t)
{
    simd_vec3 result = a * (SIMD_F32(1.0f) - t) + b * t;
    return result;
}

inline b32 AllZero(simd_u32 a)
{
    b32 result = (HorizontalAdd(a) == 0) ? true : false;
    return result;
}

#endif

#define GatherVec3(BASE, INDEX, MEMBER) \
    GatherVec3_(BASE, (u8 *)&BASE->MEMBER - (u8 *)BASE, sizeof(BASE[0]), INDEX)

#define GatherF32(BASE, INDEX, MEMBER) \
    GatherF32_(BASE, (u8 *)&BASE->MEMBER - (u8 *)BASE, sizeof(BASE[0]), INDEX)
