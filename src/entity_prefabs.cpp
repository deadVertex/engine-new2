// Entity creation related functions
inline EntityId SpawnEntityAtPosition(
    ecs_EntityWorld *world, EntityPrefab *prefab, vec3 position)
{
    Assert(prefab);
    EntityId entity = CreateEntityFromPrefab(world, prefab);

    ecs_SetComponent(
        world, PositionComponentId, &entity, 1, &position, sizeof(position));

    return entity;
}

inline EntityId SpawnEntityAtPosition(
    ecs_EntityWorld *world, u32 prefabId, vec3 position)
{
    EntityPrefab *prefab = GetEntityPrefab(world, prefabId);
    Assert(prefab);
    EntityId entity = SpawnEntityAtPosition(world, prefab, position);
    return entity;
}

inline EntityId SpawnEntityAtPositionWithScale(
    ecs_EntityWorld *world, EntityPrefab *prefab, vec3 position, vec3 scale)
{
    EntityId entity = SpawnEntityAtPosition(world, prefab, position);

    ecs_SetComponent(
        world, ScaleComponentId, &entity, 1, &scale, sizeof(scale));

    return entity;
}

inline EntityId SpawnEntityAtPositionWithScale(
    ecs_EntityWorld *world, u32 prefabId, vec3 position, vec3 scale)
{
    EntityPrefab *prefab = GetEntityPrefab(world, prefabId);
    Assert(prefab);
    EntityId entity =
        SpawnEntityAtPositionWithScale(world, prefab, position, scale);
    return entity;
}

inline EntityId SpawnEntityAtPositionWithRotation(
        ecs_EntityWorld *world, u32 prefabId, vec3 position, quat rotation)
{
    EntityPrefab *prefab = GetEntityPrefab(world, prefabId);
    Assert(prefab);
    EntityId entity = SpawnEntityAtPosition(world, prefab, position);

    ecs_SetComponent(
        world, RotationComponentId, &entity, 1, &rotation, sizeof(rotation));

    return entity;
}

inline EntityId SpawnEntityWithTransform(ecs_EntityWorld *world, u32 prefabId, vec3 position,
        quat rotation, vec3 scale)
{
    EntityPrefab *prefab = GetEntityPrefab(world, prefabId);
    Assert(prefab);
    EntityId entity = SpawnEntityAtPosition(world, prefab, position);

    ecs_SetComponent(
        world, ScaleComponentId, &entity, 1, &scale, sizeof(scale));

    ecs_SetComponent(
        world, RotationComponentId, &entity, 1, &rotation, sizeof(rotation));

    return entity;
}

internal void RegisterEntityPrefabs(ecs_EntityWorld *world)
{
    // Delete all previous prefab definitions in the case of a code reload
    ClearToZero(world->prefabs, sizeof(world->prefabs));

    {
        EntityPrefab boxGeometry = {};
        AddComponent(&boxGeometry, PositionComponentId);
        AddComponent(&boxGeometry, ShadowCasterComponentId);
        AddComponent(&boxGeometry, WorldTransformComponentId);

        AddComponentQuat(&boxGeometry, RotationComponentId, Quat());
        AddComponentVec3(&boxGeometry, ScaleComponentId, Vec3(1.0f));
        AddComponentU32(&boxGeometry, IsVisibleComponentId, true);
        AddComponentU32(&boxGeometry, RenderedMeshComponentId, Mesh_Cube);
        AddComponentU32(&boxGeometry, RenderedMaterialComponentId,
            Material_WorldDevGrid512);
        AddComponentVec3(
            &boxGeometry, BoxHalfDimensionsComponentId, Vec3(0.5f));
        AddComponentU32(
            &boxGeometry, CollisionShapeComponentId, CollisionShape_Box);
        AddComponentU32(&boxGeometry, IsWorldTransformDirtyComponentId, true);

        AddEntityPrefab(world, EntityPrefab_BoxGeometry, &boxGeometry);
    }

    {
        EntityPrefab storageBox = {};

        AddComponent(&storageBox, PositionComponentId);
        AddComponent(&storageBox, ShadowCasterComponentId);
        AddComponent(&storageBox, InventoryComponentId);
        AddComponent(&storageBox, NetEntityIdComponentId);
        AddComponent(&storageBox, InteractableComponentId);
        AddComponent(&storageBox, WorldTransformComponentId);

        AddComponentQuat(&storageBox, RotationComponentId, Quat());
        AddComponentU32(&storageBox, IsVisibleComponentId, true);
        AddComponentVec3(&storageBox, ScaleComponentId, Vec3(1.6f, 0.8f, 1.2f));
        AddComponentU32(&storageBox, RenderedMeshComponentId, Mesh_Cube);
        AddComponentU32(
            &storageBox, RenderedMaterialComponentId, Material_StorageBox);
        AddComponentU32(
            &storageBox, DeployableIdComponentId, DeployableID_StorageBox);
        AddComponentU32(&storageBox, IsWorldTransformDirtyComponentId, true);

        AddComponentU32(
            &storageBox, EntityPrefabIdComponentId, EntityType_StorageBox);
        AddComponentVec3(
            &storageBox, BoxHalfDimensionsComponentId, Vec3(0.5f));
        AddComponentU32(
            &storageBox, CollisionShapeComponentId, CollisionShape_Box);
        AddComponentF32(&storageBox, HealthComponentId, 80.0f);

        AddEntityPrefab(world, EntityPrefab_StorageBox, &storageBox);
    }

    {
        EntityPrefab storageBoxPreview = {};

        AddComponent(&storageBoxPreview, PositionComponentId);
        AddComponent(&storageBoxPreview, WorldTransformComponentId);

        AddComponentU32(&storageBoxPreview, IsVisibleComponentId, true);
        AddComponentU32(&storageBoxPreview, IsWorldTransformDirtyComponentId, true);
        AddComponentQuat(&storageBoxPreview, RotationComponentId, Quat());
        AddComponentVec3(
            &storageBoxPreview, ScaleComponentId, Vec3(1.6f, 0.8f, 1.2f));
        AddComponentU32(&storageBoxPreview, RenderedMeshComponentId, Mesh_Cube);
        AddComponentU32(&storageBoxPreview, RenderedMaterialComponentId,
            Material_BuildingPreview);
        AddComponent(&storageBoxPreview, BuildablePreviewComponentId);
        AddComponentU32(&storageBoxPreview, DeployableIdComponentId,
            DeployableID_StorageBox);

        AddEntityPrefab(
            world, EntityPrefab_StorageBoxPreview, &storageBoxPreview);
    }

    {
        EntityPrefab camera = {};
        AddComponent(&camera, PositionComponentId);
        AddComponent(&camera, EulerAnglesComponentId);
        AddComponent(&camera, VelocityComponentId);
        AddComponent(&camera, DebugCameraComponentId);

        AddEntityPrefab(world, EntityPrefab_Camera, &camera);
    }

    {
        EntityPrefab camera = {};
        AddComponent(&camera, PositionComponentId);
        AddComponent(&camera, EulerAnglesComponentId);
        AddComponent(&camera, VelocityComponentId);
        AddComponent(&camera, DebugCameraComponentId);
        AddComponent(&camera, ActiveCameraComponentId);

        AddEntityPrefab(world, EntityPrefab_ActiveDebugCamera, &camera);
    }

    {
        EntityPrefab player = {};
        AddComponent(&player, PositionComponentId);
        AddComponent(&player, EulerAnglesComponentId);
        AddComponent(&player, VelocityComponentId);
        AddComponent(&player, NetEntityIdComponentId);
        AddComponent(&player, InventoryComponentId);
        AddComponent(&player, OpenContainerComponentId);
        AddComponent(&player, ShadowCasterComponentId);
        AddComponent(&player, WorldTransformComponentId);

        AddComponentU32(&player, IsWorldTransformDirtyComponentId, true);
        AddComponentU32(&player, IsVisibleComponentId, true);
        AddComponentQuat(&player, RotationComponentId, Quat());
        AddComponentVec3(&player, ScaleComponentId, Vec3(1.0f, 1.8f, 1.0f));
        AddComponentU32(&player, RenderedMeshComponentId, Mesh_Cube);
        AddComponentU32(
            &player, RenderedMaterialComponentId, Material_GreenDiffuse);
        AddComponentU32(&player, EntityPrefabIdComponentId, EntityType_Player);
        AddComponentF32(&player, HealthComponentId, MAX_PLAYER_HEALTH);
        AddComponentVec3(
            &player, BoxHalfDimensionsComponentId, Vec3(0.5f));
        AddComponentU32(&player, CollisionShapeComponentId, CollisionShape_Box);
        AddComponentU32(&player, ActionBarActiveSlotComponentId, 0);

        AddEntityPrefab(world, EntityPrefab_Player, &player);
    }

    {
        EntityPrefab directionalLight = {};
        AddComponent(&directionalLight, PositionComponentId);
        AddComponent(&directionalLight, DirectionalLightComponentId);
        AddComponentVec3(
            &directionalLight, LightColorComponentId, Vec3(1, 0.9, 0.7));
        AddComponentQuat(&directionalLight, RotationComponentId,
            FromEulerAngles(Vec3(0.6, 0.5, 0)));

        AddEntityPrefab(
            world, EntityPrefab_DirectionalLight, &directionalLight);
    }

    {
        EntityPrefab ambientLight = {};
        AddComponent(&ambientLight, PositionComponentId);
        AddComponent(&ambientLight, AmbientLightComponentId);
        AddComponentVec3(
            &ambientLight, LightColorComponentId, Vec3(0.05, 0.1, 0.3) * 0.25f);

        AddEntityPrefab(world, EntityPrefab_AmbientLight, &ambientLight);
    }

    {
        EntityPrefab playerSpawnPoint = {};
        AddComponent(&playerSpawnPoint, PositionComponentId);
        AddComponent(&playerSpawnPoint, PlayerSpawnPointComponentId);

        AddEntityPrefab(
            world, EntityPrefab_PlayerSpawnPoint, &playerSpawnPoint);
    }

    {
        EntityPrefab enemySpawnPoint = {};
        AddComponent(&enemySpawnPoint, PositionComponentId);
        AddComponent(&enemySpawnPoint, EnemySpawnPointComponentId);

        AddEntityPrefab(world, EntityPrefab_EnemySpawnPoint, &enemySpawnPoint);
    }

    {
        EntityPrefab enemy = {};
        AddComponent(&enemy, PositionComponentId);
        AddComponent(&enemy, EulerAnglesComponentId);
        AddComponent(&enemy, VelocityComponentId);
        AddComponent(&enemy, NetEntityIdComponentId);
        // AddComponent(enemy, InventoryComponentId);
        // AddComponent(enemy, OpenContainerComponentId);
        AddComponent(&enemy, OnGroundComponentId);
        AddComponent(&enemy, ShadowCasterComponentId);
        AddComponent(&enemy, AiBrainComponentId);
        AddComponent(&enemy, AiTargetComponentId);
        AddComponent(&enemy, TimeUntilNextAttackComponentId);
        AddComponent(&enemy, WorldTransformComponentId);

        AddComponentU32(&enemy, IsWorldTransformDirtyComponentId, true);
        AddComponentU32(&enemy, IsVisibleComponentId, true);
        AddComponentQuat(&enemy, RotationComponentId, Quat());
        AddComponentVec3(&enemy, ScaleComponentId, Vec3(1.0f, 1.8f, 1.0f));
        AddComponentU32(&enemy, RenderedMeshComponentId, Mesh_Cube);
        AddComponentU32(&enemy, RenderedMaterialComponentId, Material_Red);
        AddComponentU32(&enemy, EntityPrefabIdComponentId, EntityType_Enemy);
        AddComponentF32(&enemy, HealthComponentId, MAX_PLAYER_HEALTH);
        AddComponentVec3(
            &enemy, BoxHalfDimensionsComponentId, Vec3(0.5f));
        AddComponentU32(&enemy, CollisionShapeComponentId, CollisionShape_Box);

        AddEntityPrefab(world, EntityPrefab_Enemy, &enemy);
    }

    {
        EntityPrefab skybox = {};
        AddComponent(&skybox, SkyboxComponentId);
        AddComponentU32(&skybox, IsVisibleComponentId, true);
        AddComponentU32(&skybox, RenderedMeshComponentId, Mesh_Cube);
        AddComponentU32(&skybox, RenderedMaterialComponentId, Material_Skybox);

        AddEntityPrefab(world, EntityPrefab_Skybox, &skybox);
    }

    {
        EntityPrefab bullet = {};
        AddComponent(&bullet, PositionComponentId);
        AddComponent(&bullet, VelocityComponentId);
        AddComponent(&bullet, BulletComponentId);
        AddComponent(&bullet, ProjectileOwnerComponentId);

        AddComponentF32(&bullet, TimeToLiveComponentId, 10.0f);

        AddEntityPrefab(world, EntityPrefab_Bullet, &bullet);
    }

    {
        EntityPrefab pistol = {};
        AddComponent(&pistol, PositionComponentId);
        AddComponent(&pistol, WorldTransformComponentId);
        AddComponentU32(&pistol, IsWorldTransformDirtyComponentId, true);
        AddComponentQuat(&pistol, RotationComponentId, Quat());
        AddComponentVec3(&pistol, ScaleComponentId, Vec3(10));
        AddComponentU32(&pistol, IsVisibleComponentId, true);
        AddComponentU32(&pistol, RenderedMeshComponentId, Mesh_Pistol);
        AddComponentU32(
            &pistol, RenderedMaterialComponentId, Material_StorageBox);

        AddEntityPrefab(world, EntityPrefab_Pistol, &pistol);
    }

    {
        EntityPrefab foundationPreview = {};
        AddComponent(&foundationPreview, PositionComponentId);
        AddComponent(&foundationPreview, BuildablePreviewComponentId);
        AddComponent(&foundationPreview, WorldTransformComponentId);
        AddComponentU32(&foundationPreview, IsWorldTransformDirtyComponentId, true);
        AddComponentU32(&foundationPreview, IsVisibleComponentId, true);
        AddComponentQuat(&foundationPreview, RotationComponentId, Quat());
        AddComponentVec3(&foundationPreview, ScaleComponentId, Vec3(1));
        AddComponentU32(&foundationPreview, RenderedMeshComponentId, Mesh_Foundation);
        AddComponentU32(&foundationPreview, RenderedMaterialComponentId,
            Material_BuildingPreview);
        AddComponentU32(&foundationPreview, DeployableIdComponentId,
            DeployableID_Foundation);

        AddEntityPrefab(world, EntityPrefab_FoundationPreview, &foundationPreview);
    }

    {
        EntityPrefab foundation = {};
        AddComponent(&foundation, PositionComponentId);
        AddComponent(&foundation, NetEntityIdComponentId);
        AddComponent(&foundation, ShadowCasterComponentId);
        AddComponent(&foundation, StaticComponentId);
        AddComponent(&foundation, WorldTransformComponentId);

        AddComponentU32(&foundation, IsWorldTransformDirtyComponentId, true);
        AddComponentU32(&foundation, IsVisibleComponentId, true);
        AddComponentQuat(&foundation, RotationComponentId, Quat());
        AddComponentVec3(&foundation, ScaleComponentId, Vec3(1));
        AddComponentU32(&foundation, RenderedMeshComponentId, Mesh_Foundation);
        AddComponentU32(
            &foundation, RenderedMaterialComponentId, Material_WorldDevGrid512);
        AddComponentU32(
            &foundation, TriangleMeshComponentId, TriangleMesh_Foundation);
        AddComponentU32(
            &foundation, CollisionShapeComponentId, CollisionShape_TriangleMesh);
        AddComponentU32(
            &foundation, EntityPrefabIdComponentId, EntityType_Foundation);
        AddComponentU32(
            &foundation, DeployableIdComponentId, DeployableID_Foundation);

        AddEntityPrefab(world, EntityPrefab_Foundation, &foundation);
    }

    {
        EntityPrefab wallPreview = {};
        AddComponent(&wallPreview, PositionComponentId);
        AddComponent(&wallPreview, BuildablePreviewComponentId);
        AddComponent(&wallPreview, WorldTransformComponentId);
        AddComponentU32(&wallPreview, IsWorldTransformDirtyComponentId, true);
        AddComponentU32(&wallPreview, IsVisibleComponentId, true);
        AddComponentQuat(&wallPreview, RotationComponentId, Quat());
        AddComponentVec3(&wallPreview, ScaleComponentId, Vec3(1));
        AddComponentU32(&wallPreview, RenderedMeshComponentId, Mesh_Wall);
        AddComponentU32(&wallPreview, RenderedMaterialComponentId,
            Material_BuildingPreview);
        AddComponentU32(&wallPreview, DeployableIdComponentId,
            DeployableID_Wall);

        AddEntityPrefab(world, EntityPrefab_WallPreview, &wallPreview);
    }

    {
        EntityPrefab wall = {};
        AddComponent(&wall, PositionComponentId);
        AddComponent(&wall, NetEntityIdComponentId);
        AddComponent(&wall, ShadowCasterComponentId);
        AddComponent(&wall, StaticComponentId);
        AddComponent(&wall, WorldTransformComponentId);

        AddComponentU32(&wall, IsWorldTransformDirtyComponentId, true);
        AddComponentU32(&wall, IsVisibleComponentId, true);
        AddComponentQuat(&wall, RotationComponentId, Quat());
        AddComponentVec3(&wall, ScaleComponentId, Vec3(1));
        AddComponentU32(&wall, RenderedMeshComponentId, Mesh_Wall);
        AddComponentU32(&wall, RenderedMaterialComponentId, Material_WorldDevGrid512);
        AddComponentU32(&wall, TriangleMeshComponentId, TriangleMesh_Wall);
        AddComponentU32(&wall, CollisionShapeComponentId, CollisionShape_TriangleMesh);
        AddComponentU32(
            &wall, EntityPrefabIdComponentId, EntityType_Wall);
        AddComponentU32(&wall, DeployableIdComponentId, DeployableID_Wall);

        AddEntityPrefab(world, EntityPrefab_Wall, &wall);
    }

    {
        EntityPrefab doorwayPreview = {};
        AddComponent(&doorwayPreview, PositionComponentId);
        AddComponent(&doorwayPreview, BuildablePreviewComponentId);
        AddComponent(&doorwayPreview, WorldTransformComponentId);
        AddComponentU32(&doorwayPreview, IsWorldTransformDirtyComponentId, true);
        AddComponentU32(&doorwayPreview, IsVisibleComponentId, true);
        AddComponentQuat(&doorwayPreview, RotationComponentId, Quat());
        AddComponentVec3(&doorwayPreview, ScaleComponentId, Vec3(1));
        AddComponentU32(&doorwayPreview, RenderedMeshComponentId, Mesh_Doorway);
        AddComponentU32(&doorwayPreview, RenderedMaterialComponentId,
            Material_BuildingPreview);
        AddComponentU32(&doorwayPreview, DeployableIdComponentId,
            DeployableID_Doorway);

        AddEntityPrefab(world, EntityPrefab_DoorwayPreview, &doorwayPreview);
    }

    {
        EntityPrefab doorway = {};
        AddComponent(&doorway, PositionComponentId);
        AddComponent(&doorway, NetEntityIdComponentId);
        AddComponent(&doorway, ShadowCasterComponentId);
        AddComponent(&doorway, StaticComponentId);
        AddComponent(&doorway, WorldTransformComponentId);

        AddComponentU32(&doorway, IsWorldTransformDirtyComponentId, true);
        AddComponentU32(&doorway, IsVisibleComponentId, true);
        AddComponentQuat(&doorway, RotationComponentId, Quat());
        AddComponentVec3(&doorway, ScaleComponentId, Vec3(1));
        AddComponentU32(&doorway, RenderedMeshComponentId, Mesh_Doorway);
        AddComponentU32(&doorway, RenderedMaterialComponentId, Material_WorldDevGrid512);
        AddComponentU32(&doorway, TriangleMeshComponentId, TriangleMesh_Doorway);
        AddComponentU32(&doorway, CollisionShapeComponentId, CollisionShape_TriangleMesh);
        AddComponentU32(
            &doorway, EntityPrefabIdComponentId, EntityType_Doorway);
        AddComponentU32(
            &doorway, DeployableIdComponentId, DeployableID_Doorway);

        AddEntityPrefab(world, EntityPrefab_Doorway, &doorway);
    }

    {
        EntityPrefab ceilingPreview = {};
        AddComponent(&ceilingPreview, PositionComponentId);
        AddComponent(&ceilingPreview, BuildablePreviewComponentId);
        AddComponent(&ceilingPreview, WorldTransformComponentId);
        AddComponentU32(&ceilingPreview, IsWorldTransformDirtyComponentId, true);
        AddComponentU32(&ceilingPreview, IsVisibleComponentId, true);
        AddComponentQuat(&ceilingPreview, RotationComponentId, Quat());
        AddComponentVec3(&ceilingPreview, ScaleComponentId, Vec3(1, 0.5, 1));
        AddComponentU32(&ceilingPreview, RenderedMeshComponentId, Mesh_Foundation);
        AddComponentU32(&ceilingPreview, RenderedMaterialComponentId,
            Material_BuildingPreview);
        AddComponentU32(&ceilingPreview, DeployableIdComponentId,
            DeployableID_Ceiling);

        AddEntityPrefab(world, EntityPrefab_CeilingPreview, &ceilingPreview);
    }

    {
        EntityPrefab ceiling = {};
        AddComponent(&ceiling, PositionComponentId);
        AddComponent(&ceiling, NetEntityIdComponentId);
        AddComponent(&ceiling, ShadowCasterComponentId);
        AddComponent(&ceiling, StaticComponentId);
        AddComponent(&ceiling, WorldTransformComponentId);

        AddComponentU32(&ceiling, IsWorldTransformDirtyComponentId, true);
        AddComponentU32(&ceiling, IsVisibleComponentId, true);
        AddComponentQuat(&ceiling, RotationComponentId, Quat());
        AddComponentVec3(&ceiling, ScaleComponentId, Vec3(1, 0.5, 1));
        AddComponentU32(&ceiling, RenderedMeshComponentId, Mesh_Foundation);
        AddComponentU32(&ceiling, RenderedMaterialComponentId, Material_WorldDevGrid512);
        AddComponentU32(&ceiling, TriangleMeshComponentId, TriangleMesh_Foundation);
        AddComponentU32(&ceiling, CollisionShapeComponentId, CollisionShape_TriangleMesh);
        AddComponentU32(
            &ceiling, EntityPrefabIdComponentId, EntityType_Ceiling);
        AddComponentU32(
            &ceiling, DeployableIdComponentId, DeployableID_Ceiling);

        AddEntityPrefab(world, EntityPrefab_Ceiling, &ceiling);
    }

    {
        EntityPrefab terrain = {};
        AddComponent(&terrain, PositionComponentId);
        AddComponent(&terrain, TerrainComponentId);
        AddComponent(&terrain, StaticComponentId);

        AddComponentU32(&terrain, IsVisibleComponentId, true);
        AddComponentVec3(&terrain, ScaleComponentId,
            Vec3(TERRAIN_PATCH_SCALE, TERRAIN_HEIGHT_SCALE, TERRAIN_PATCH_SCALE));
        AddComponentQuat(&terrain, RotationComponentId, Quat());
        AddComponentU32(&terrain, CollisionShapeComponentId, CollisionShape_HeightMap);

        AddEntityPrefab(world, EntityPrefab_Terrain, &terrain);
    }

    {
        EntityPrefab baseRock = {};
        AddComponent(&baseRock, PositionComponentId);
        AddComponent(&baseRock, ShadowCasterComponentId);
        AddComponent(&baseRock, StaticComponentId);
        AddComponent(&baseRock, WorldTransformComponentId);

        AddComponentU32(&baseRock, IsWorldTransformDirtyComponentId, true);
        AddComponentU32(&baseRock, IsVisibleComponentId, true);
        AddComponentQuat(&baseRock, RotationComponentId, Quat());
        AddComponentVec3(&baseRock, ScaleComponentId, Vec3(1));
        AddComponentU32(&baseRock, RenderedMaterialComponentId, Material_StorageBox);
        AddComponentU32(&baseRock, CollisionShapeComponentId, CollisionShape_TriangleMesh);

        EntityPrefab rocks[4];
        CopyMemory(&rocks[0], &baseRock, sizeof(baseRock));
        CopyMemory(&rocks[1], &baseRock, sizeof(baseRock));
        CopyMemory(&rocks[2], &baseRock, sizeof(baseRock));
        CopyMemory(&rocks[3], &baseRock, sizeof(baseRock));

        AddComponentU32(&rocks[0], RenderedMeshComponentId, Mesh_Rock01);
        AddComponentU32(&rocks[0], TriangleMeshComponentId, TriangleMesh_Rock01);

        AddComponentU32(&rocks[1], RenderedMeshComponentId, Mesh_Rock02);
        AddComponentU32(&rocks[1], TriangleMeshComponentId, TriangleMesh_Rock02);

        AddComponentU32(&rocks[2], RenderedMeshComponentId, Mesh_Rock03);
        AddComponentU32(&rocks[2], TriangleMeshComponentId, TriangleMesh_Rock03);

        AddComponentU32(&rocks[3], RenderedMeshComponentId, Mesh_Rock04);
        AddComponentU32(&rocks[3], TriangleMeshComponentId, TriangleMesh_Rock04);

        AddEntityPrefab(world, EntityPrefab_Rock01, &rocks[0]);
        AddEntityPrefab(world, EntityPrefab_Rock02, &rocks[1]);
        AddEntityPrefab(world, EntityPrefab_Rock03, &rocks[2]);
        AddEntityPrefab(world, EntityPrefab_Rock04, &rocks[3]);
    }

    {
        EntityPrefab bulletImpact = {};
        AddComponent(&bulletImpact, PositionComponentId);

        AddComponentU32(&bulletImpact, ParticleSystemComponentId, ParticleSystem_Sparks);
        AddComponentQuat(&bulletImpact, RotationComponentId, Quat());
        AddComponentVec3(&bulletImpact, ScaleComponentId, Vec3(0.25));
        AddComponentF32(&bulletImpact, TimeToLiveComponentId, 0.3f);

        AddEntityPrefab(world, EntityPrefab_BulletImpact, &bulletImpact);
    }

    {
        EntityPrefab lootTableTest = {};

        AddComponent(&lootTableTest, PositionComponentId);
        AddComponent(&lootTableTest, ShadowCasterComponentId);
        AddComponent(&lootTableTest, InventoryComponentId);
        AddComponent(&lootTableTest, NetEntityIdComponentId);
        AddComponent(&lootTableTest, InteractableComponentId);
        AddComponent(&lootTableTest, WorldTransformComponentId);

        AddComponentU32(&lootTableTest, IsWorldTransformDirtyComponentId, true);
        AddComponentQuat(&lootTableTest, RotationComponentId, Quat());
        AddComponentU32(&lootTableTest, IsVisibleComponentId, true);
        AddComponentVec3(&lootTableTest, ScaleComponentId, Vec3(1.6f, 0.8f, 1.2f));
        AddComponentU32(&lootTableTest, RenderedMeshComponentId, Mesh_Cube);
        AddComponentU32(
            &lootTableTest, RenderedMaterialComponentId, Material_StorageBox);
        AddComponentU32(
            &lootTableTest, DeployableIdComponentId, DeployableID_StorageBox);

        AddComponentU32(
            &lootTableTest, EntityPrefabIdComponentId, EntityType_StorageBox);
        AddComponentVec3(
            &lootTableTest, BoxHalfDimensionsComponentId, Vec3(0.5f));
        AddComponentU32(
            &lootTableTest, CollisionShapeComponentId, CollisionShape_Box);
        AddComponentF32(&lootTableTest, HealthComponentId, 80.0f);
        AddComponentU32(&lootTableTest, LootTableComponentId, LootTable_Test);

        AddEntityPrefab(world, EntityPrefab_LootTableTest, &lootTableTest);
    }
}
