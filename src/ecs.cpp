internal void ecs_QueueEntitiesForDeletion(
    ecs_EntityWorld *world, EntityId *entities, u32 count)
{
    Assert(
        count + world->deletionQueueLength < ArrayCount(world->deletionQueue));
    CopyMemory(world->deletionQueue + world->deletionQueueLength, entities,
        sizeof(EntityId) * count);
    world->deletionQueueLength += count;
}

internal void InitializeComponentTable(ecs_ComponentTable *table,
    MemoryArena *arena, u32 objectSize, u32 capacity, b32 notifyOnRemoveEntry)
{
    table->count = 0;
    table->capacity = capacity;
    table->objectSize = objectSize;
    table->notifyOnRemoveEntry = notifyOnRemoveEntry;

    table->keys = AllocateArray(arena, EntityId, capacity);

    if (objectSize > 0)
    {
        table->values = MemoryArena_Allocate(arena, objectSize * capacity);
    }
}

inline b32 HasEntity(ecs_ComponentTable *table, EntityId entity)
{
    b32 result = false;

    for (u32 i = 0; i < table->count; ++i)
    {
        if (table->keys[i] == entity)
        {
            result = true;
            break;
        }
    }

    return result;
}

inline b32 BinarySearchTable(ecs_ComponentTable *table, EntityId entity, u32 *index)
{
    b32 result = false;
    if (table->count > 0)
    {
        u32 l = 0;
        u32 h = table->count - 1;
        u32 m = (l+h) / 2;
        while (l < h)
        {
            if (entity < table->keys[m])
            {
                // Make sure that h doesnt wrap up to 2^32 - 1
                h = (m > 0) ? m-1 : 0;
            }
            else
            {
                l = m;
            }
            m = (l+h+1) / 2;
        }

        if (table->keys[m] == entity)
        {
            *index = m;
            result = true;
        }
    }

    return result;
}

inline void GetValuesBinarySearch(ecs_ComponentTable *table, EntityId *entities, u32 count,
        void *values, u32 objectSize)
{
    Assert(objectSize == table->objectSize);

    for (u32 entityIdx = 0; entityIdx < count; ++entityIdx)
    {
        EntityId entity = entities[entityIdx];
        u32 index = 0;
        if (!BinarySearchTable(table, entity, &index))
        {
            InvalidCodePath();
        }

        void *dst = (u8 *)values + entityIdx * objectSize;
        void *src = (u8 *)table->values + index * objectSize;
        CopyMemory(dst, src, objectSize);
    }
}

inline void GetValues(ecs_ComponentTable *table, EntityId *entities, u32 count,
    void *values, u32 objectSize)
{
    Assert(objectSize == table->objectSize);

    for (u32 entityIdx = 0; entityIdx < count; ++entityIdx)
    {
        EntityId entity = entities[entityIdx];

        b32 found = false;
        for (u32 keyIdx = 0; keyIdx < table->count; ++keyIdx)
        {
            if (table->keys[keyIdx] == entity)
            {
                void *dst = (u8 *)values + entityIdx * objectSize;
                void *src = (u8 *)table->values + keyIdx * objectSize;
                CopyMemory(dst, src, objectSize);
                found = true;
                break;
            }
        }
        Assert(found);
    }
}

inline void HasComponent(
    ecs_ComponentTable *table, EntityId *entities, u32 count, b32 *results)
{
    for (u32 entityIdx = 0; entityIdx < count; ++entityIdx)
    {
        EntityId entity = entities[entityIdx];
        u32 index = 0;
        results[entityIdx] = BinarySearchTable(table, entity, &index);
    }
}

// FIXME: Rename and refactor this
internal b32 HasComponentsSlow(ecs_EntityWorld *world, EntityId entityId,
        u32 *components, u32 componentCount)
{
    b32 result = true;
    u32 ignored = 0;
    for (u32 componentIdx = 0; componentIdx < componentCount; ++componentIdx)
    {
        u32 component = components[componentIdx];
        Assert(component < ArrayCount(world->tables));

        ecs_ComponentTable *table = &world->tables[component];
        if (!BinarySearchTable(table, entityId, &ignored))
        {
            result = false;
            break;
        }
    }
    return result;
}

internal u32 CreateEntities(ecs_EntityWorld *world, EntityId *entities, u32 count)
{
    u32 createCount = 0;
    for (u32 i = 0; i < count; ++i)
    {
        if (world->entityCount < ArrayCount(world->entityIds))
        {
            EntityId entityId = ++world->lastEntityId;
            world->entityIds[world->entityCount++] = entityId;
            entities[i] = entityId;
            createCount++;
        }
        else
        {
            break;
        }
    }

    return createCount;
}

inline u32 AddComponent(ecs_ComponentTable *table, EntityId *entities, u32 entityCount)
{
    u32 count = 0;
    for (u32 entityIdx = 0; entityIdx < entityCount; ++entityIdx)
    {
       Assert(table->count < table->capacity);
       u32 idx = table->count++;
       table->keys[idx] = entities[entityIdx];
       ClearToZero((u8 *)table->values + idx * table->objectSize, table->objectSize);
       count++;
    }

    return count;
}

// TODO: Figure out what to do on error
internal u32 AddComponents(ecs_EntityWorld *world, EntityId *entities, u32 entityCount,
        u32 *components, u32 componentCount)
{
    for (u32 componentIdx = 0; componentIdx < componentCount; ++componentIdx)
    {
        u32 component = components[componentIdx];
        Assert(component < ArrayCount(world->tables));

        ecs_ComponentTable *table = world->tables + component;

        u32 count = AddComponent(table, entities, entityCount);
        Assert(count == entityCount);
    }
    return 0;
}

inline void SetComponent(ecs_ComponentTable *table, EntityId *entities, u32 entityCount,
        void *values, u32 objectSize)
{
    Assert(table->objectSize == objectSize);

    for (u32 entityIdx = 0; entityIdx < entityCount; ++entityIdx)
    {
        EntityId entity = entities[entityIdx];

        b32 found = false;
        for (u32 i = 0; i < table->count; ++i)
        {
            if (table->keys[i] == entity)
            {
                void *dst = (u8 *)table->values + i * objectSize;
                void *src = (u8 *)values + entityIdx * objectSize;
                CopyMemory(dst, src, objectSize);

                found = true;
                break;
            }
        }
        Assert(found);
    }
}

inline b32 RemoveComponent(ecs_ComponentTable *table, EntityId entity)
{
    b32 result = false;
    u32 index = 0;
    if (BinarySearchTable(table, entity, &index))
    {
        Assert(table->count > 0);
        if (index < table->count - 1)
        {
            u32 *keyDst = table->keys + index;
            u32 *keySrc = table->keys + (index + 1);
            u32 count = table->count - (index + 1);
            CopyMemory(keyDst, keySrc, count * sizeof(EntityId));

            if (table->objectSize > 0)
            {
                void *dst = (u8 *)table->values + index * table->objectSize;
                void *src = (u8 *)table->values + (index + 1) * table->objectSize;

                CopyMemory(dst, src, count * table->objectSize);
            }
        }
        table->count -= 1;
        result = true;
    }

    return (result && table->notifyOnRemoveEntry);
}


inline void ecs_AddComponent(ecs_EntityWorld *world, u32 componentId,
    EntityId *entities, u32 entityCount)
{
    Assert(componentId < ArrayCount(world->tables));
    ecs_ComponentTable *table = world->tables + componentId;
    AddComponent(table, entities, entityCount);
}

inline void ecs_GetComponent(ecs_EntityWorld *world, u32 componentId,
    EntityId *entities, u32 entityCount, void *values, u32 objectSize)
{
    Assert(componentId < ArrayCount(world->tables));
    ecs_ComponentTable *table = world->tables + componentId;
    GetValuesBinarySearch(table, entities, entityCount, values, objectSize);
}

inline void ecs_SetComponent(ecs_EntityWorld *world, u32 componentId,
        EntityId *entities, u32 entityCount, void *values, u32 objectSize)
{
    Assert(componentId < ArrayCount(world->tables));
    ecs_ComponentTable *table = world->tables + componentId;
    SetComponent(table, entities, entityCount, values, objectSize);
}

inline void ecs_HasComponent(ecs_EntityWorld *world, u32 componentId,
        EntityId *entities, u32 entityCount, b32 *result)
{
    Assert(componentId < ArrayCount(world->tables));
    ecs_ComponentTable *table = world->tables + componentId;
    HasComponent(table, entities, entityCount, result);
}

inline b32 ecs_HasComponent(ecs_EntityWorld *world, u32 componentId,
        EntityId entity)
{
    b32 result = false;
    ecs_HasComponent(world, componentId, &entity, 1, &result);
    return result;
}

inline void ecs_RemoveComponent(ecs_EntityWorld *world, u32 componentId,
        EntityId *entities, u32 entityCount)
{
    Assert(componentId < ArrayCount(world->tables));
    ecs_ComponentTable *table = world->tables + componentId;
    for (u32 i = 0; i < entityCount; ++i)
    {
        if (RemoveComponent(table, entities[i]))
        {
            Assert(world->componentRemovalEventCount <
                   ArrayCount(world->componentRemovedEvents));
            ecs_ComponentRemovedEvent *event =
                world->componentRemovedEvents +
                world->componentRemovalEventCount++;

            event->entityId = entities[i];
            event->componentId = componentId;
        }
    }
}

inline u32 ecs_GetEntitiesWithComponent(ecs_EntityWorld *world, u32 componentId,
    EntityId *entities, u32 maxEntities)
{
    ecs_ComponentTable *table = world->tables + componentId;

    u32 count = MinU(table->count, maxEntities);
    CopyMemory(entities, table->keys, count * sizeof(EntityId));

    return count;
}

inline u32 ecs_Join(ecs_EntityWorld *world, u32 *components, u32 componentCount,
        EntityId *entities, u32 maxEntities)
{
    Assert(componentCount > 1);
    Assert(components[0] < ArrayCount(world->tables));
    ecs_ComponentTable *baseTable = world->tables + components[0];

    u32 *join = components + 1;
    u32 joinLength = componentCount - 1;

    u32 count = 0;
    for (u32 i = 0; i < baseTable->count; ++i)
    {
        EntityId entityId = baseTable->keys[i];
        if (HasComponentsSlow(world, entityId, join, joinLength))
        {
            Assert(count < maxEntities); // FIXME: Handle this better
            entities[count++] = entityId;
        }
    }

    return count;
}

inline void ecs_RegisterComponent(ecs_EntityWorld *world, u32 componentId,
    MemoryArena *arena, u32 componentSize, u32 tableCapacity, const char *name,
    u32 dataType, b32 notifyOnRemoveEntry)
{
    Assert(componentId < ArrayCount(world->tables));
    Assert(componentId < ArrayCount(world->componentMetaData));
    Assert(tableCapacity <= MAX_ENTITIES);

    ecs_ComponentTable *table = world->tables + componentId;
    InitializeComponentTable(table, arena, componentSize, tableCapacity, notifyOnRemoveEntry);

    ComponentMetaData *metaData = world->componentMetaData + componentId;
    metaData->id = componentId;
    metaData->name = name;
    metaData->dataType = dataType;
}

internal b32 GetFirstEntity(
    ecs_EntityWorld *world, u32 componentId, EntityId *entity)
{
    b32 result = false;
    Assert(componentId < ArrayCount(world->tables));
    ecs_ComponentTable *table = world->tables + componentId;
    if(table->count > 0)
    {
        *entity = table->keys[0];
        result = true;
    }

    return result;
}

inline void ClearTable(ecs_ComponentTable *table)
{
    ClearToZero(table->keys, table->count * sizeof(EntityId));

    if (table->objectSize > 0)
    {
        ClearToZero(table->values, table->count * table->objectSize);
    }

    table->count = 0;
}

// NOTE: This does not remove the components that belong to that entity
inline void RemoveEntity(ecs_EntityWorld *world, EntityId entity)
{
    for (u32 i = 0; i < world->entityCount; ++i)
    {
        if (world->entityIds[i] == entity)
        {
            u32 last = world->entityCount - 1;
            world->entityIds[i] = world->entityIds[last];
            world->entityIds[last] = 0;
            world->entityCount -= 1;
            break;
        }
    }
}

inline void ecs_DeleteAllEntities(ecs_EntityWorld *world)
{
    world->entityCount = 0;
    world->deletionQueueLength = 0;
    for (u32 i = 0; i < ArrayCount(world->tables); ++i)
    {
        ClearTable(world->tables + i);
    }
}

internal void InitializeEntityWorld(ecs_EntityWorld *world, MemoryArena *arena,
    ComponentInitializationData *entries, u32 length)
{
    for (u32 i = 0; i < length; ++i)
    {
        u32 componentId = entries[i].componentId;
        u32 maxEntities = entries[i].maxEntities;
        const char *name = entries[i].name;
        u32 dataType = entries[i].dataType;
        b32 notifyOnRemoveEntry = entries[i].notifyOnRemoveEntry;

        u32 size = GetDataTypeSize(dataType);

        ecs_RegisterComponent(world, componentId, arena, size, maxEntities,
            name, dataType, notifyOnRemoveEntry);
    }
}

internal void ProcessEntityDeletionQueue(ecs_EntityWorld *world)
{
    for (u32 i = 0; i < world->deletionQueueLength; ++i)
    {
        EntityId entity = world->deletionQueue[i];

        // Remove entity from each component table
        for (u32 component = 0; component < ArrayCount(world->tables); ++component)
        {
            ecs_ComponentTable *table = world->tables + component;
            if (RemoveComponent(table, entity))
            {
                Assert(world->componentRemovalEventCount <
                       ArrayCount(world->componentRemovedEvents));
                ecs_ComponentRemovedEvent *event =
                    world->componentRemovedEvents +
                    world->componentRemovalEventCount++;

                event->entityId = entity;
                event->componentId = component;
            }
        }

        // Remove entity from world entities list
        RemoveEntity(world, entity);
    }

    world->deletionQueueLength = 0;
}

inline ComponentMetaData *FindComponentMetaData(
    ecs_EntityWorld *world, u32 componentId)
{
    ComponentMetaData *result = NULL;
    for (u32 i = 0; i < ArrayCount(world->componentMetaData); ++i)
    {
        if (world->componentMetaData[i].id == componentId)
        {
            result = world->componentMetaData + i;
            break;
        }
    }

    return result;
}

inline ComponentMetaData *FindComponentMetaDataByName(
    ecs_EntityWorld *world, const char *name)
{
    ComponentMetaData *result = NULL;
    for (u32 i = 0; i < ArrayCount(world->componentMetaData); ++i)
    {
        if (StringCompare(world->componentMetaData[i].name, name))
        {
            result = world->componentMetaData + i;
            break;
        }
    }

    return result;
}

internal u32 ecs_SerializeWorld(ecs_EntityWorld *world, void *buffer, u32 capacity)
{
    Assert(world->deletionQueueLength == 0);
    DataBuffer dataBuffer = CreateDataBuffer(buffer, capacity);

    u32 version = ECS_FILE_DATA_VERSION;
    WriteValue(&dataBuffer, version);

    WriteValue(&dataBuffer, world->entityCount);
    WriteArray(&dataBuffer, world->entityIds, world->entityCount);
    WriteValue(&dataBuffer, world->lastEntityId);

    u32 tableCount = ArrayCount(world->tables);
    WriteValue(&dataBuffer, tableCount);
    for (u32 componentId = 0; componentId < tableCount; ++componentId)
    {
        ecs_ComponentTable *table = world->tables + componentId;
        WriteValue(&dataBuffer, componentId);
        WriteValue(&dataBuffer, table->objectSize);

        ComponentMetaData *metaData = FindComponentMetaData(world, componentId);
        Assert(metaData);

        WriteValue(&dataBuffer, metaData->dataType);
        WriteValue(&dataBuffer, table->count);

        WriteArray(&dataBuffer, table->keys, table->count);
        WriteData(&dataBuffer, table->values, table->objectSize * table->count);
    }

    return dataBuffer.cursor;
}

// NOTE: Entity world must be cleared before calling this
internal b32 ecs_DeserializeWorld(ecs_EntityWorld *world, void *buffer, u32 length)
{
    Assert(world->entityCount == 0);

    DataBuffer dataBuffer = CreateDataBuffer(buffer, length);

    u32 version = 0;
    ReadValue(&dataBuffer, &version);
    Assert(version <= ECS_FILE_DATA_VERSION);
    b32 upgradeData = (version != ECS_FILE_DATA_VERSION);

    ReadValue(&dataBuffer, &world->entityCount);
    Assert(world->entityCount < ArrayCount(world->entityIds));

    ReadArray(&dataBuffer, world->entityIds, world->entityCount);
    ReadValue(&dataBuffer, &world->lastEntityId);

    u32 tableCount = 0;
    ReadValue(&dataBuffer, &tableCount);
    Assert(upgradeData || tableCount == ArrayCount(world->tables));

    for (u32 i = 0; i < tableCount; ++i)
    {
        u32 componentId = 0;
        ReadValue(&dataBuffer, &componentId);

        u32 count;
        u32 capacity = 0;
        u32 objectSize = 0;
        u32 dataType = 0;
        ReadValue(&dataBuffer, &objectSize);
        ReadValue(&dataBuffer, &dataType);
        ReadValue(&dataBuffer, &count);

        b32 skipTableData = true;
        if (componentId < ArrayCount(world->tables))
        {
            ecs_ComponentTable *table = world->tables + componentId;

            ComponentMetaData *metaData = FindComponentMetaData(world, componentId);
            Assert(metaData);

            if (table->objectSize == objectSize && metaData->dataType == dataType)
            {
                if (count <= table->capacity)
                {
                    table->count = count;

                    ReadArray(&dataBuffer, table->keys, table->count);
                    ReadData(&dataBuffer, table->values,
                        table->objectSize * table->count);

                    skipTableData = false;
                }
                else
                {
                    LogMessage(
                        "ERROR: Too many entries to load into table %s, %u "
                        "entries available but only enough space to start %u."
                        "skipping table",
                        metaData->name, count, table->capacity);
                    Assert(upgradeData);
                }
            }
            else
            {
                LogMessage(
                    "ERROR: Data type mismatch for table %s, skipping table",
                    metaData->name);
                Assert(upgradeData);
            }
        }
        else
        {
            LogMessage("ERROR: Invalid component ID %u", componentId);
            InvalidCodePath();
        }

        if (skipTableData)
        {
            dataBuffer.cursor += count * sizeof(EntityId); // Skip keys
            dataBuffer.cursor += count * objectSize; // Skip values
        }

    }

    return true;
}

inline EntityPrefab *GetEntityPrefab(ecs_EntityWorld *world, u32 prefabId)
{
    Assert(prefabId < ArrayCount(world->prefabs));
    return world->prefabs + prefabId;
}

#define AddEntityPrefab(WORLD, NAME, PREFAB) \
    AddEntityPrefab_(WORLD, NAME, #NAME, PREFAB)

inline void AddEntityPrefab_(
    ecs_EntityWorld *world, u32 prefabId, const char *name, EntityPrefab *prefab)
{
    Assert(prefabId < ArrayCount(world->prefabs));
    world->prefabs[prefabId] = *prefab;
    world->prefabs[prefabId].name = name;
}

internal EntityId CreateEntityFromPrefab(
    ecs_EntityWorld *world, EntityPrefab *prefab)
{
    EntityId entity = NULL_ENTITY;
    CreateEntities(world, &entity, 1);

    AddComponents(world, &entity, 1, prefab->componentIds, prefab->count);

    for (u32 i = 0; i < prefab->count; ++i)
    {
        u32 componentId = prefab->componentIds[i];
        ComponentValue *componentValue = prefab->componentValues + i;

        if (componentValue->dataType != DataType_None)
        {
            u32 dataLength = GetDataTypeSize(componentValue->dataType);
            Assert(dataLength != 0);

            ecs_SetComponent(world, componentId, &entity, 1,
                &componentValue->value, dataLength);
        }
    }

    return entity;
}

internal EntityId CreateEntityFromPrefab(ecs_EntityWorld *world, u32 prefabId)
{
    EntityPrefab *prefab = GetEntityPrefab(world, prefabId);
    Assert(prefab);
    EntityId entity = CreateEntityFromPrefab(world, prefab);
    return entity;
}

inline ComponentValue *AllocateComponentDescription(
    EntityPrefab *desc, u32 componentId)
{
    Assert(desc->count < ArrayCount(desc->componentIds));
    u32 index = desc->count;
    desc->componentIds[index] = componentId;
    desc->count++;

    return desc->componentValues + index;
}

inline void AddComponent(EntityPrefab *desc, u32 componentId)
{
    AllocateComponentDescription(desc, componentId);
}

inline void AddComponents(EntityPrefab *desc, u32 *componentIds, u32 count)
{
    for (u32 i = 0; i < count; ++i)
    {
        AddComponent(desc, componentIds[i]);
    }
}

inline void AddComponentVec3(EntityPrefab *desc, u32 componentId, vec3 value)
{
    ComponentValue *componentValue =
        AllocateComponentDescription(desc, componentId);
    componentValue->dataType = DataType_Vec3;
    componentValue->value.v3 = value;
}

inline void AddComponentU32(EntityPrefab *desc, u32 componentId, u32 value)
{
    ComponentValue *componentValue =
        AllocateComponentDescription(desc, componentId);
    componentValue->dataType = DataType_U32;
    componentValue->value.u = value;
}

inline void AddComponentF32(EntityPrefab *desc, u32 componentId, f32 value)
{
    ComponentValue *componentValue =
        AllocateComponentDescription(desc, componentId);
    componentValue->dataType = DataType_F32;
    componentValue->value.f = value;
}

inline void AddComponentQuat(EntityPrefab *desc, u32 componentId, quat value)
{
    ComponentValue *componentValue =
        AllocateComponentDescription(desc, componentId);
    componentValue->dataType = DataType_Quat;
    componentValue->value.q = value;
}

inline b32 HasComponent(EntityPrefab *desc, u32 componentId)
{
    b32 result = false;
    for (u32 i = 0; i < desc->count; ++i)
    {
        if (desc->componentIds[i] == componentId)
        {
            result = true;
            break;
        }
    }

    return result;
}

inline EntityPrefab *GetEntityPrefabByName(ecs_EntityWorld *world, const char *name)
{
    EntityPrefab *result = NULL;
    for (u32 i = 0; i < ArrayCount(world->prefabs); ++i)
    {
        if (world->prefabs[i].name != NULL)
        {
            if (StringCompare(world->prefabs[i].name, name))
            {
                result = world->prefabs + i;
                break;
            }
        }
    }

    return result;
}

inline u32 GetEntityPrefabId(ecs_EntityWorld *world, const char *name)
{
    u32 result = ArrayCount(world->prefabs); // TODO: Need invalid entity prefab id
    for (u32 i = 0; i < ArrayCount(world->prefabs); ++i)
    {
        if (world->prefabs[i].name != NULL)
        {
            if (StringCompare(world->prefabs[i].name, name))
            {
                result = i;
                break;
            }
        }
    }
    Assert(result != ArrayCount(world->prefabs));

    return result;
}
