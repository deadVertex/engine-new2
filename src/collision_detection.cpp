#include "collision_detection.h"

inline void Swap(RaycastResult *a, RaycastResult *b)
{
    RaycastResult temp = *a;
    *a = *b;
    *b = temp;
}

inline void Swap(u32 *a, u32 *b)
{
    u32 temp = *a;
    *a = *b;
    *b = temp;
}

// Returns true if b is closer than a
inline b32 CompareRaycastResults(RaycastResult *a, RaycastResult *b)
{
    if (a->isValid && b->isValid)
    {
        if (b->tmin < a->tmin)
        {
            return true;
        }
        return false;
    }
    else if (a->isValid)
    {
        return false;
    }
    else if (b->isValid)
    {
        return true;
    }
    else
    {
        return false;
    }
}

// FIXME: Should probably put a bit more effort and testing into sorting
inline void SortRaycastResults(RaycastResult *results, u32 *indices, u32 count)
{
    if (count > 1)
    {
        for (u32 i = 0; i < count - 1; ++i)
        {
            for (u32 j = 0; j < count - 1; ++j)
            {
                RaycastResult *a = results + j;
                RaycastResult *b = results + j + 1;
                if (CompareRaycastResults(a, b))
                {
                    Swap(a, b);
                    Swap(indices + j, indices + j + 1);
                }
            }
        }
    }
}

// Caller is expected to calculate index via pointer arithmetic
// Result is NULL if none of the raycast results are valid
RaycastResult *FindClosestRaycastResult(RaycastResult *results, u32 count)
{
    RaycastResult *minResult = NULL;
    for (u32 i = 0; i < count; ++i)
    {
        if (results[i].isValid)
        {
            if (minResult == NULL)
            {
                minResult = results + i;
            }
            else if (results[i].tmin < minResult->tmin)
            {
                minResult = results + i;
            }
        }
    }

    return minResult;
}

inline vec3 ClosestPointOnLineSegment(vec3 p, vec3 a, vec3 b)
{
    vec3 ab = b - a;
    f32 t = Dot(p - a, ab) / (Dot(ab, ab));
    t = Clamp(t, 0.0f, 1.0f);

    vec3 result = a + t * ab;
    return result;
}

inline f32 RayPlaneIntersectT(vec3 normal, f32 distance, vec3 start, vec3 end)
{
    vec3 direction = end - start;
    f32 denom = Dot(normal, direction);

    // NOTE: May divide by 0 but should still give correct result.
    f32 t = (distance - Dot(normal, start)) / denom;

    return t;
}

inline RaycastResult RayIntersectPlane(
    vec3 planeNormal, f32 planeDistance, vec3 start, vec3 end)
{
    RaycastResult result = {};

    vec3 dir = end - start;
    f32 denom = Dot(planeNormal, dir);

    // NOTE: May divide by 0 but should still give correct result.
    f32 t = (planeDistance - Dot(planeNormal, start)) / denom;

    if (t >= 0.0f && t < 1.0f)
    {
        result.hitPoint = start + dir * t;
        result.hitNormal = planeNormal;
        result.tmin = t;
        result.isValid = true;
    }

    return result;
}

inline RaycastResult SphereSweepPlane(
    vec3 planeNormal, f32 planeDistance, vec3 start, vec3 end, f32 sweepRadius)
{
    RaycastResult result = {};
    vec3 v = end - start;
    f32 dist = Dot(planeNormal, start) - planeDistance;
    if (Abs(dist) <= sweepRadius)
    {
        // Sphere is already overlapping plane
        result.tmin = 0.0f;
        result.hitPoint = start;
        result.hitNormal = planeNormal;
        result.isValid = true;
    }
    else
    {
        f32 denom = Dot(planeNormal, v);
        if (denom * dist >= 0.0f)
        {
            // No intersection, sphere is moving parallel to or away from plane
        }
        else
        {
            // Sphere is moving towards plane
            f32 r = dist > 0.0f ? sweepRadius : -sweepRadius;
            f32 t = (r - dist) / denom;
            if (t >= 0.0f && t < 1.0f)
            {
                result.tmin = t;
                // TODO: Return point of intersection rather than just position
                // sphere center on intersection
                // intersectionPoint = start + t * v - r * planeNormal
                result.hitPoint = start + v * t;
                result.hitNormal = planeNormal;
                result.isValid = true;
            }
        }
    }

    return result;
}

inline void CollisionDebug_AddCall(
    CollisionDebugState *state, CollisionDebugCall call)
{
    if (state->count < ArrayCount(state->calls))
    {
        state->calls[state->count++] = call;
    }
}

#include "collision_detection/sphere.cpp"
#include "collision_detection/aabb.cpp"
#include "collision_detection/capsule.cpp"
#include "collision_detection/triangle.cpp"

internal void CollisionDebug_ReplayCall(CollisionDebugCall *call,
    DebugDrawingSystem *debugDrawingSystem, MemoryArena *tempArena)
{
    b32 isSweepTest = call->type >= CollisionDebugCall_SphereSweepSpheres;

    if (isSweepTest)
    {
#if 1
        DrawSphere(debugDrawingSystem, call->start, call->sweepRadius,
            COLLISION_DEBUG_RAY_COLOR);
        DrawSphere(debugDrawingSystem, call->end, call->sweepRadius,
            COLLISION_DEBUG_RAY_COLOR);
        DrawLine(debugDrawingSystem, call->start, call->end,
            COLLISION_DEBUG_RAY_COLOR);
#else
        DrawCapsule(debugDrawingSystem, params->start, params->end,
            params->sweepRadius, 24, COLLISION_DEBUG_RAY_COLOR);
#endif
    }
    else
    {
        DrawLine(debugDrawingSystem, call->start, call->end,
            COLLISION_DEBUG_RAY_COLOR);
    }

    RaycastResult *results =
        AllocateArray(tempArena, RaycastResult, call->count);

    switch (call->type)
    {
    case CollisionDebugCall_RayIntersectAabb:
        RayIntersectAabb_(call->aabb.minPoints, call->aabb.maxPoints,
            call->count, call->start, call->end, results, NULL);
        break;
    case CollisionDebugCall_RayIntersectSphere:
        RayIntersectSphere(call->sphere.centers, call->sphere.radii,
            call->count, call->start, call->end, results, NULL);
        break;
    case CollisionDebugCall_RayIntersectTriangle:
        RayIntersectTriangle(call->triangle.vertices, call->count, call->start,
            call->end, results, NULL);
        break;
    case CollisionDebugCall_RayIntersectCapsule:
        RayIntersectCapsule(call->capsule.endPointsA, call->capsule.endPointsB,
            call->capsule.radii, call->count, call->start, call->end, results,
            NULL);
        break;
    case CollisionDebugCall_SphereSweepSpheres:
        SphereSweepSpheres(call->sphere.centers, call->sphere.radii,
            call->count, call->start, call->end, call->sweepRadius, results,
            NULL);
        break;
    case CollisionDebugCall_SphereSweepCapsules:
        SphereSweepCapsules(call->capsule.endPointsA, call->capsule.endPointsB,
            call->capsule.radii, call->count, call->start, call->end,
            call->sweepRadius, results, NULL);
        break;
    case CollisionDebugCall_SphereSweepTriangles:
        SphereSweepTriangles(call->triangle.vertices, call->count, call->start,
            call->end, call->sweepRadius, results, NULL);
        break;
    default:
        InvalidCodePath();
        break;
    }

    for (u32 i = 0; i < call->count; ++i)
    {
        RaycastResult result = results[i];
        vec3 color = result.isValid ? COLLISION_DEBUG_GEOMETRY_HIT_COLOR
            : COLLISION_DEBUG_GEOMETRY_COLOR;
        if (result.isValid)
        {
            if (isSweepTest)
            {
                DrawSphere(debugDrawingSystem, result.hitPoint,
                    call->sweepRadius, COLLISION_DEBUG_RAY_INTERSECT_COLOR);
            }
            else
            {
                DrawLine(debugDrawingSystem, call->start, result.hitPoint,
                    COLLISION_DEBUG_RAY_INTERSECT_COLOR);
                DrawLine(debugDrawingSystem, result.hitPoint,
                    result.hitPoint + result.hitNormal,
                    COLLISION_DEBUG_NORMAL_COLOR);
            }
        }

        switch (call->type)
        {
        case CollisionDebugCall_RayIntersectAabb:
            DrawBox(debugDrawingSystem, call->aabb.minPoints[i],
                call->aabb.maxPoints[i], color);
            break;
        case CollisionDebugCall_RayIntersectSphere:
        case CollisionDebugCall_SphereSweepSpheres:
            DrawSphere(debugDrawingSystem, call->sphere.centers[i],
                call->sphere.radii[i], color);
            break;
        case CollisionDebugCall_RayIntersectTriangle:
        case CollisionDebugCall_SphereSweepTriangles:
            DrawTriangles(debugDrawingSystem, call->triangle.vertices + i * 3,
                1, color, 0.5f, COLLISION_DEBUG_NORMAL_COLOR);
            break;
        case CollisionDebugCall_RayIntersectCapsule:
        case CollisionDebugCall_SphereSweepCapsules:
            DrawCapsule(debugDrawingSystem, call->capsule.endPointsA[i],
                call->capsule.endPointsB[i], call->capsule.radii[i], 24, color);
            break;
        }
    }
}
