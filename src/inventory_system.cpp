struct ItemIconEntry
{
    u32 itemId;
    u32 icon;
};

global ItemIconEntry g_ItemIcons[] = {
    {ItemID_Pistol, Texture_PistolIcon},
    {ItemID_PistolAmmo, Texture_PistolAmmoIcon},
    {ItemID_Hatchet, Texture_HatchetIcon},
    {ItemID_StorageBox, Texture_StorageBoxIcon},
    {ItemID_Foundation, Texture_FoundationIcon},
    {ItemID_Wall, Texture_WallIcon},
    {ItemID_Doorway, Texture_DoorwayIcon},
};

enum
{
    DragState_None,
    DragState_InProgress,
    DragState_Complete,
};

struct InventoryState
{
    u32 dragState;
    u32 dragStart;
    u32 dragEnd;
    EntityId dragStartEntity;
    EntityId dragEndEntity;
};

internal void GetInventorySlots(
    ecs_EntityWorld *world, EntityId owner, EntityId *slots, u32 maxSlots)
{
    ClearToZero(slots, maxSlots * sizeof(EntityId));

    // TODO: Probably worth checking that the owner entity has an inventory
    // component

    u32 join[] = {InventorySlotOwnerComponentId, InventorySlotIndexComponentId};

    EntityId entities[MAX_ENTITIES];
    u32 count = ecs_Join(world, join, ArrayCount(join), entities, ArrayCount(entities));

    EntityId owners[MAX_ENTITIES];
    u32 slotIndices[MAX_ENTITIES];

    ecs_GetComponent(world, InventorySlotOwnerComponentId, entities, count,
        owners, sizeof(owners[0]));
    ecs_GetComponent(world, InventorySlotIndexComponentId, entities, count,
        slotIndices, sizeof(slotIndices[0]));

    for (u32 i = 0; i < count; ++i)
    {
        if (owners[i] == owner)
        {
            u32 slotIndex = slotIndices[i];

            Assert(slotIndex < maxSlots);
            slots[slotIndex] = entities[i];
        }
    }
}

internal void GetActionBarSlots(
    ecs_EntityWorld *world, EntityId owner, EntityId *slots, u32 maxSlots)
{
    if (ecs_HasComponent(world, ActionBarActiveSlotComponentId, owner))
    {
        EntityId inventory[INVENTORY_SLOT_COUNT];
        GetInventorySlots(world, owner, inventory, ArrayCount(inventory));

        // Action bar is just the first N inventory slots
        CopyMemory(slots, inventory, maxSlots * sizeof(u32));
    }
}

internal b32 FindEmptySlot(EntityId *slots, u32 count, u32 *emptySlotIdx)
{
    b32 result = false;
    for (u32 i = 0; i < count; ++i)
    {
        if (slots[i] == NULL_ENTITY)
        {
            // Empty slot found
            *emptySlotIdx = i;
            result = true;
            break;
        }
    }

    return result;
}

internal EntityId CreateItemEntity(ecs_EntityWorld *world, u32 itemId,
    u32 itemQuantity, u32 slotIdx, EntityId owner)
{
    EntityId item = NULL_ENTITY;
    CreateEntities(world, &item, 1);
    u32 components[] = {ItemIdComponentId, ItemQuantityComponentId,
        InventorySlotOwnerComponentId, InventorySlotIndexComponentId,
        NetEntityIdComponentId, EntityPrefabIdComponentId};
    AddComponents(world, &item, 1, components, ArrayCount(components));

    ecs_SetComponent(
            world, ItemIdComponentId, &item, 1, &itemId, sizeof(itemId));
    ecs_SetComponent(world, ItemQuantityComponentId, &item, 1, &itemQuantity,
        sizeof(itemQuantity));

    ecs_SetComponent(
        world, InventorySlotOwnerComponentId, &item, 1, &owner, sizeof(owner));
    ecs_SetComponent(world, InventorySlotIndexComponentId, &item, 1, &slotIdx,
        sizeof(slotIdx));

    u32 prefabId = EntityType_Item;
    ecs_SetComponent(world, EntityPrefabIdComponentId, &item, 1, &prefabId,
        sizeof(prefabId));

    return item;
}

internal EntityId AddItemToInventory(
    ecs_EntityWorld *world, EntityId owner, u32 itemId, u32 itemQuantity = 1)
{
    EntityId result = NULL_ENTITY;

    // Find free slot in inventory
    EntityId slots[INVENTORY_SLOT_COUNT] = {};
    GetInventorySlots(world, owner, slots, ArrayCount(slots));

    // FIXME: Why are we checking this here? Can't we assume this by now
    u32 components[] = { NetEntityIdComponentId };
    Assert(HasComponentsSlow(world, owner, components, ArrayCount(components)));

    u32 emptySlotIdx = 0;
    if (FindEmptySlot(slots, ArrayCount(slots), &emptySlotIdx))
    {
        // Create new item entity
        result =
            CreateItemEntity(world, itemId, itemQuantity, emptySlotIdx, owner);
    }
    else
    {
        LogMessage("No free inventory slot found");
    }

    return result;
}

inline b32 GetItemInInventorySlot(ecs_EntityWorld *world, EntityId owner,
    u32 slotIdx, EntityId *item)
{
    b32 result = false;

    u32 join[] = {InventorySlotOwnerComponentId, InventorySlotIndexComponentId};

    EntityId entities[MAX_ENTITIES];
    u32 count = ecs_Join(world, join, ArrayCount(join), entities, ArrayCount(entities));

    EntityId owners[MAX_ENTITIES];
    u32 slotIndices[MAX_ENTITIES];
    ecs_GetComponent(world, InventorySlotOwnerComponentId, entities, count,
        owners, sizeof(owners[0]));
    ecs_GetComponent(world, InventorySlotIndexComponentId, entities, count,
        slotIndices, sizeof(slotIndices[0]));

    for (u32 i = 0; i < count; ++i)
    {
        if ((owners[i] == owner) && (slotIndices[i] == slotIdx))
        {
            *item = entities[i];
            result = true;
            break;
        }
    }

    return result;
}

internal void UpdateInventory(
    ecs_EntityWorld *world, EntityId owner, PlayerInventoryCommand command)
{
    if (command.action == PlayerInventoryAction_ChangeSlot)
    {
        Assert(command.changeSlot.to < INVENTORY_SLOT_COUNT);
        Assert(command.changeSlot.from < INVENTORY_SLOT_COUNT);

        EntityId fromEntity = NULL_ENTITY;
        EntityId toEntity = NULL_ENTITY;

        MapNetEntityIdToEntityId(world, &command.changeSlot.fromEntity,
            1, &fromEntity);
        MapNetEntityIdToEntityId(world, &command.changeSlot.toEntity,
            1, &toEntity);

        // TODO: Verify that toEntity and fromEntity are valid for this player

        EntityId from = NULL_ENTITY;
        if (GetItemInInventorySlot(
                world, fromEntity, command.changeSlot.from, &from))
        {
            EntityId to = NULL_ENTITY;
            if (GetItemInInventorySlot(
                    world, toEntity, command.changeSlot.to, &to))
            {
                ecs_SetComponent(world, InventorySlotOwnerComponentId,
                        &to, 1, &fromEntity, sizeof(fromEntity));
                ecs_SetComponent(world, InventorySlotIndexComponentId, &to, 1,
                    &command.changeSlot.from, sizeof(command.changeSlot.from));
            }

            ecs_SetComponent(world, InventorySlotOwnerComponentId,
                    &from, 1, &toEntity, sizeof(toEntity));
            ecs_SetComponent(world, InventorySlotIndexComponentId, &from, 1,
                    &command.changeSlot.to, sizeof(command.changeSlot.to));
        }
    }
    else if (command.action == PlayerInventoryAction_Close)
    {
        EntityId subject = NULL_ENTITY;
        ecs_SetComponent(world, OpenContainerComponentId, &owner, 1,
            &subject, sizeof(subject));
    }
}

inline u32 GetItemIcon(u32 itemID)
{
    u32 result = MAX_TEXTURES;
    for (u32 i = 0; i < ArrayCount(g_ItemIcons); ++i)
    {
        if (g_ItemIcons[i].itemId == itemID)
        {
            result = g_ItemIcons[i].icon;
            break;
        }
    }

    return result;
}

internal void DrawInventorySlot(ecs_EntityWorld *world, GameMemory *memory,
    mat4 orthographic, vec2 position, vec2 dimensions, GameInput *input,
    InventoryState *inventoryState, u32 slotIdx, EntityId item,
    vec2 mousePosition, EntityId owner, VertexBuffer *textBuffer, Font *font,
    b32 enableHightlight = false, b32 enableMouseOverHighlight = true,
    b32 drawSlotIdx = false)
{
    f32 rectPadding = 2.0f;

    ui_DrawQuadArguments drawTile = {};
    drawTile.memory = memory;
    drawTile.orthographicProjection = orthographic;
    drawTile.position = position;
    drawTile.width = dimensions.x;
    drawTile.height = dimensions.y;
    drawTile.color = Vec4(0.2, 0.2, 0.2, 0.5);
    drawTile.horizontalAlignment = HorizontalAlign_Left;
    drawTile.verticalAlignment = VerticalAlign_Top;

    // Top left alignment
    rect2 rect = {};
    rect.min.x = drawTile.position.x - rectPadding;
    rect.min.y = drawTile.position.y - drawTile.height - rectPadding;
    rect.max.x = drawTile.position.x + drawTile.width + rectPadding;
    rect.max.y = drawTile.position.y + rectPadding;
    if (ContainsPoint(rect, mousePosition))
    {
        if (enableMouseOverHighlight)
        {
            drawTile.color = Vec4(0.6, 0.6, 0.6, 0.5);
        }

        if (input->buttonStates[KEY_MOUSE_BUTTON_LEFT].isDown &&
            !input->buttonStates[KEY_MOUSE_BUTTON_LEFT].wasDown)
        {
            // Mouse press
            inventoryState->dragStart = slotIdx;
            inventoryState->dragStartEntity = owner;
            inventoryState->dragState = DragState_InProgress;
        }
        else if (!input->buttonStates[KEY_MOUSE_BUTTON_LEFT].isDown &&
                 input->buttonStates[KEY_MOUSE_BUTTON_LEFT].wasDown)
        {
            // Mouse release
            if (inventoryState->dragState == DragState_InProgress)
            {
                inventoryState->dragEnd = slotIdx;
                inventoryState->dragEndEntity = owner;
                inventoryState->dragState = DragState_Complete;
            }
        }
    }

    if (enableHightlight)
    {
        drawTile.color = Vec4(0.6, 0.6, 0.6, 0.5);
    }

    ui_DrawQuad(&drawTile);

    char buffer[8];
    if (item != NULL_ENTITY)
    {
        u32 itemId = 0;
        ecs_GetComponent(
            world, ItemIdComponentId, &item, 1, &itemId, sizeof(itemId));

        u32 itemQuantity = 0;
        ecs_GetComponent(world, ItemQuantityComponentId, &item, 1,
            &itemQuantity, sizeof(itemQuantity));

        // Map item ID to icon
        u32 icon = GetItemIcon(itemId);

        // Draw Icon
        ui_DrawQuadArguments drawIcon = {};
        drawIcon.memory = memory;
        drawIcon.orthographicProjection = orthographic;
        drawIcon.position.x = position.x + dimensions.x * 0.5f;
        drawIcon.position.y = position.y - dimensions.y * 0.5f;
        drawIcon.width = dimensions.x * 0.9f;
        drawIcon.height = dimensions.y * 0.9f;
        drawIcon.color = Vec4(0, 1, 0, 1);
        drawIcon.texture = (icon == MAX_TEXTURES) ? 0 : icon;
        drawIcon.horizontalAlignment = HorizontalAlign_Center;
        drawIcon.verticalAlignment = VerticalAlign_Center;
        ui_DrawQuad(&drawIcon);

        snprintf(buffer, sizeof(buffer), "%u", itemQuantity);

        ui_DrawStringArguments drawString = {};
        drawString.memory = memory;
        drawString.vertexBuffer = textBuffer;
        drawString.text = buffer;
        drawString.position.x = position.x + dimensions.x - 2.0f;
        drawString.position.y = position.y - 2.0f;
        drawString.color = Vec4(1);
        drawString.drawShadow = true;
        drawString.shadowColor = Vec4(0, 0, 0, 1);
        drawString.shadowOffset = Vec2(1,-1);
        drawString.font = font;
        drawString.orthographicProjection = orthographic;
        drawString.horizontalAlignment = HorizontalAlign_Right;
        drawString.verticalAlignment = VerticalAlign_Top;
        ui_DrawString(&drawString);

    }

    if (drawSlotIdx)
    {
        snprintf(buffer, sizeof(buffer), "%u", slotIdx + 1);

        ui_DrawStringArguments drawString = {};
        drawString.memory = memory;
        drawString.vertexBuffer = textBuffer;
        drawString.text = buffer;
        drawString.position.x = position.x + dimensions.x * 0.5f;
        drawString.position.y = position.y - dimensions.y + 2.0f;
        drawString.color = Vec4(0.6);
        drawString.drawShadow = true;
        drawString.shadowColor = Vec4(0, 0, 0, 1);
        drawString.shadowOffset = Vec2(1, -1);
        drawString.font = font;
        drawString.orthographicProjection = orthographic;
        drawString.horizontalAlignment = HorizontalAlign_Center;
        drawString.verticalAlignment = VerticalAlign_Bottom;

        ui_DrawString(&drawString);
    }
}

// TODO: Need a better system for positioning UI elements relative to each other
internal void DrawInventory(ecs_EntityWorld *world, EntityId *inventory,
    u32 inventorySize, GameMemory *memory, GameInput *input, mat4 orthographic,
    InventoryState *inventoryState, EntityId player, EntityId container,
    EntityId *containerInventory, u32 containerInventorySize,
    VertexBuffer *textBuffer, Font *font)
{
    f32 panelWidth = 400.0f;
    f32 panelHeight = 300.0f;
    f32 panelPositionX = memory->frameBufferWidth * 0.5f;
    f32 panelPositionY = memory->frameBufferHeight * 0.5f;

    vec2 mousePosition = Vec2((f32)input->mousePosX,
        (f32)memory->frameBufferHeight - (f32)input->mousePosY);

    ui_DrawQuadArguments drawPanel = {};
    drawPanel.memory = memory;
    drawPanel.orthographicProjection = orthographic;
    drawPanel.position = Vec2(panelPositionX, panelPositionY);
    drawPanel.width = panelWidth;
    drawPanel.height = panelHeight;
    drawPanel.color = Vec4(0.02, 0.02, 0.02, 1.0);
    drawPanel.horizontalAlignment = HorizontalAlign_Center;
    drawPanel.verticalAlignment = VerticalAlign_Center;
    ui_DrawQuad(&drawPanel);

    f32 tileW = 60.0f;
    f32 tileH = 60.0f;
    f32 border = 4.0f;
    f32 startX = drawPanel.position.x - drawPanel.width * 0.5f + border;
    f32 startY = drawPanel.position.y + drawPanel.height * 0.5f - border;

    u32 startingRow = 1; // Skip first row for the player as that is considered
                         // the action bar and is drawn separately.

    // Draw grid
    // FIXME: row and col numbers should be based off number of inventory slots
    // Assert(inventorySize == 4 * 6); // 5th row is for action bar
    for (u32 row = startingRow; row < 4; ++row)
    {
        for (u32 col = 0; col < 6; ++col)
        {
            u32 slotIdx = row * 6 + col;
            Assert(slotIdx < inventorySize);

            vec2 position = Vec2(
                startX + col * (tileW + 2.0f), startY - row * (tileH + 2.0f));
            vec2 dimensions = Vec2(tileW, tileH);
            EntityId item = inventory[slotIdx];

            DrawInventorySlot(world, memory, orthographic, position, dimensions, input,
                inventoryState, slotIdx, item, mousePosition, player, textBuffer, font);
        }
    }


    if (container != NULL_ENTITY)
    {
        // Draw container
        ui_DrawQuadArguments drawContainerPanel = {};
        drawContainerPanel.memory = memory;
        drawContainerPanel.orthographicProjection = orthographic;
        drawContainerPanel.position.x =
            panelPositionX + panelWidth * 0.5f + 2.0f;
        drawContainerPanel.position.y =
            panelPositionY + panelHeight * 0.5f;
        drawContainerPanel.width = 300.0f;
        drawContainerPanel.height = 400.0f;
        drawContainerPanel.color = Vec4(0.02, 0.02, 0.02, 1.0);
        drawContainerPanel.horizontalAlignment = HorizontalAlign_Left;
        drawContainerPanel.verticalAlignment = VerticalAlign_Top;
        ui_DrawQuad(&drawContainerPanel);

        f32 drawContainerStartX = drawContainerPanel.position.x + 2.0f;
        f32 drawContainerStartY = drawContainerPanel.position.y - 2.0f;

        u32 containerStartingRow = 0; // Assuming for the container we will want
                                      // to just display any action bar items as
                                      // regular inventory items.

        u32 rowCount = containerInventorySize / 4;
        for (u32 row = containerStartingRow; row < rowCount; ++row)
        {
            for (u32 col = 0; col < 4; ++col)
            {
                u32 slotIdx = row * 4 + col;
                Assert(slotIdx < containerInventorySize);

                vec2 position;
                position.x = drawContainerStartX + col * (tileW + 2.0f);
                position.y = drawContainerStartY - row * (tileH + 2.0f);
                vec2 dimensions = Vec2(tileW, tileH);
                EntityId item = containerInventory[slotIdx];

                // FIXME: Need to include owner in drag events
                DrawInventorySlot(world, memory, orthographic, position, dimensions,
                    input, inventoryState, slotIdx, item, mousePosition,
                    container, textBuffer, font);
            }
        }
    }
}

internal void DrawActionBar(ecs_EntityWorld *world, EntityId *actionBar,
    u32 actionBarSize, GameMemory *memory, GameInput *input, mat4 orthographic,
    InventoryState *inventoryState, EntityId player, u32 selectedActionBarSlot,
    b32 enableMouseOverHighlight, VertexBuffer *textBuffer, Font *font)
{
    f32 panelWidth = 400.0f;
    f32 panelHeight = 64.0f;
    f32 panelPositionX = memory->frameBufferWidth * 0.5f;
    f32 panelPositionY = 50.0f;

    vec2 mousePosition = Vec2((f32)input->mousePosX,
        (f32)memory->frameBufferHeight - (f32)input->mousePosY);

    ui_DrawQuadArguments drawPanel = {};
    drawPanel.memory = memory;
    drawPanel.orthographicProjection = orthographic;
    drawPanel.position = Vec2(panelPositionX, panelPositionY);
    drawPanel.width = panelWidth;
    drawPanel.height = panelHeight;
    drawPanel.color = Vec4(0.02, 0.02, 0.02, 1.0);
    drawPanel.horizontalAlignment = HorizontalAlign_Center;
    drawPanel.verticalAlignment = VerticalAlign_Center;
    ui_DrawQuad(&drawPanel);

    f32 tileW = 60.0f;
    f32 tileH = 60.0f;
    f32 border = 4.0f;
    f32 startX = drawPanel.position.x - drawPanel.width * 0.5f + border;
    f32 startY = drawPanel.position.y + drawPanel.height * 0.5f - border;

    // FIXME: row and col numbers should be based off number of inventory slots
    // Assert(inventorySize == 4 * 6); // 5th row is for action bar
    for (u32 slotIdx = 0; slotIdx < actionBarSize; ++slotIdx)
    {
        vec2 position = Vec2(startX + slotIdx * (tileW + 2.0f), startY);
        vec2 dimensions = Vec2(tileW, tileH);
        EntityId item = actionBar[slotIdx];

        DrawInventorySlot(world, memory, orthographic, position, dimensions,
            input, inventoryState, slotIdx, item, mousePosition, player,
            textBuffer, font, selectedActionBarSlot == slotIdx,
            enableMouseOverHighlight, true);
    }
}

internal void RemoveAllItemsOwnedByEntity(ecs_EntityWorld *world, EntityId owner)
{
    EntityId entities[MAX_ENTITIES];
    u32 entityCount = ecs_GetEntitiesWithComponent(
        world, InventorySlotOwnerComponentId, entities, ArrayCount(entities));

    EntityId owners[MAX_ENTITIES];
    ecs_GetComponent(world, InventorySlotOwnerComponentId, entities,
        entityCount, owners, sizeof(owners[0]));

    for (u32 entityIdx = 0; entityIdx < entityCount; ++entityIdx)
    {
        if (owners[entityIdx] == owner)
        {
            ecs_QueueEntitiesForDeletion(world, entities + entityIdx, 1);
        }
    }
}

internal void UpdateActionBar(
    ecs_EntityWorld *world, EntityId owner, PlayerCommand cmd)
{
    if (cmd.switchToActionSlot > 0)
    {
        if (cmd.switchToActionSlot <= ACTION_BAR_SLOT_COUNT)
        {
            u32 newSlot = cmd.switchToActionSlot - 1;
            ecs_SetComponent(world, ActionBarActiveSlotComponentId, &owner, 1,
                &newSlot, sizeof(newSlot));
        }
    }
}

internal u32 GetAllInstancesOfItemInInventory(ecs_EntityWorld *world, EntityId owner,
        u32 itemId, EntityId *instances, u32 maxInstances)
{
    u32 join[] = {InventorySlotOwnerComponentId, ItemIdComponentId, ItemQuantityComponentId};

    EntityId entities[MAX_ENTITIES];
    u32 count = ecs_Join(world, join, ArrayCount(join), entities, ArrayCount(entities));

    EntityId owners[MAX_ENTITIES];
    ecs_GetComponent(world, InventorySlotOwnerComponentId, entities, count,
        owners, sizeof(owners[0]));

    u32 itemIds[MAX_ENTITIES];
    ecs_GetComponent(world, ItemIdComponentId, entities, count,
        itemIds, sizeof(itemIds[0]));

    u32 instanceCount = 0;
    for (u32 i = 0; i < count; ++i)
    {
        if (owners[i] == owner && itemIds[i] == itemId)
        {
            Assert(instanceCount < maxInstances);
            instances[instanceCount++] = entities[i];
        }
    }

    return instanceCount;
}

internal void DecrementQuantityOfItemInInventory(ecs_EntityWorld *world,
    EntityId *inventory, u32 inventoryLength, u32 decrement)
{
    u32 quantities[MAX_ENTITIES];
    ecs_GetComponent(world, ItemQuantityComponentId, inventory, inventoryLength,
        quantities, sizeof(quantities[0]));

    u32 removeCount = 0;
    EntityId entitiesToRemove[MAX_ENTITIES];

    u32 writeLength = 0;
    for (u32 i = 0; i < inventoryLength; ++i)
    {
        if (decrement > 0)
        {
            if (quantities[i] > decrement)
            {
                quantities[i] -= decrement;
                writeLength = i + 1;
                break;
            }
            else
            {
                Assert(decrement >= quantities[i]);
                decrement -= quantities[i];
                Assert(removeCount < ArrayCount(entitiesToRemove));
                entitiesToRemove[removeCount++] = inventory[i];
            }
        }
        else
        {
            break;
        }
    }

    if (writeLength > 0)
    {
        ecs_SetComponent(world, ItemQuantityComponentId, inventory, writeLength,
            quantities, sizeof(quantities[0]));
    }

    ecs_QueueEntitiesForDeletion(world, entitiesToRemove, removeCount);
}

inline u32 GetActiveItemId(ecs_EntityWorld *world, EntityId playerEntity)
{
    u32 itemId = ItemID_None;
    u32 activeActionBarSlot = 0;
    ecs_GetComponent(world, ActionBarActiveSlotComponentId, &playerEntity, 1,
        &activeActionBarSlot, sizeof(activeActionBarSlot));

    EntityId actionBar[ACTION_BAR_SLOT_COUNT];
    GetActionBarSlots(world, playerEntity, actionBar, ArrayCount(actionBar));

    Assert(activeActionBarSlot < ArrayCount(actionBar));

    EntityId activeItemEntity = actionBar[activeActionBarSlot];
    if (activeItemEntity != NULL_ENTITY)
    {
        ecs_GetComponent(world, ItemIdComponentId, &activeItemEntity, 1,
            &itemId, sizeof(itemId));
    }

    return itemId;
}
