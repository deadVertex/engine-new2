struct Cmd_Entry
{
    const char *name;
    u32 commandId;
};

#define CMD_MAX_ARGS 8
struct CommandExecutionEvent
{
    const char *argv[CMD_MAX_ARGS];
    u32 argc;
    u32 commandId;
};

#define MAX_CMDS 0x1000
struct Cmd_System
{
    Cmd_Entry commands[MAX_CMDS];
    u32 count;

    char workingBuffer[4096];
    u32 workingBufferLength;
};

internal void Cmd_Register(
    Cmd_System *system, const char *name, u32 commandId)
{
    Assert(system->count < ArrayCount(system->commands));
    Cmd_Entry *entry = system->commands + system->count++;
    entry->name = name;
    entry->commandId = commandId;
}

internal void Cmd_Exec(Cmd_System *system, const char *str)
{
    u32 length = StringLength(str);
    Assert(system->workingBufferLength + length <
           ArrayCount(system->workingBuffer));

    char *dst = system->workingBuffer + system->workingBufferLength;
    CopyMemory(dst, str, length);
    dst[length] = ';';

    system->workingBufferLength += length + 1;
}

internal u32 Cmd_ExecBuffer(Cmd_System *system, char *buffer,
    u32 bufferLength, CommandExecutionEvent *events, u32 maxEvents)
{
    u32 eventCount = 0;
    char *cmdline = buffer;
    u32 length = 0;
    for (u32 bufferIdx = 0; bufferIdx < bufferLength; ++bufferIdx)
    {
        if (buffer[bufferIdx] == ';')
        {
            if (bufferIdx > 0)
            {
                length = bufferIdx - SafeTruncateU64ToU32(cmdline - buffer);
                cmdline[length] = '\0';

                const char *argv[CMD_MAX_ARGS] = {};
                u32 argc = 1;
                argv[0] = cmdline;

                for (u32 i = 0; i < length - 1; ++i)
                {
                    if (cmdline[i] == ' ')
                    {
                        cmdline[i] = '\0';

                        // TODO: Use iswhitespace
                        if (cmdline[i + 1] != ' ')
                        {
                            Assert(argc < ArrayCount(argv));
                            argv[argc++] = &cmdline[i + 1];
                        }
                    }
                }

                for (u32 i = 0; i < system->count; ++i)
                {
                    if (StringCompare(system->commands[i].name, argv[0]))
                    {
                        Assert(eventCount < maxEvents);
                        CommandExecutionEvent *event = events + eventCount++;

                        event->commandId = system->commands[i].commandId;
                        event->argc = argc;
                        CopyMemory(
                            event->argv, argv, event->argc * sizeof(char *));
                        break;
                    }
                }
            }

            cmdline = buffer + bufferIdx + 1; // Skip ; character
            length = 0;
            bufferIdx++;
        }
    }

    return eventCount;
}

internal const char *Cmd_Completion(
    Cmd_System *system, const char *partialCommand, u32 length, u32 matchOffset)
{
    u32 matches[64];
    u32 matchCount = 0;
    for (u32 i = 0; i < system->count; ++i)
    {
        if (StringCompare(partialCommand, system->commands[i].name, length))
        {
            if (matchCount < ArrayCount(matches))
            {
                matches[matchCount++] = i;
            }
        }
    }

    const char *result = NULL;
    if (matchCount > 0)
    {
        u32 matchIdx = matchOffset % matchCount;
        u32 idx = matches[matchIdx];
        result = system->commands[idx].name;
    }

    return result;
}
