struct CollisionDetectionTestbench
{
    CameraState cameraState;
};

struct LineSegment
{
    vec3 start;
    vec3 end;
};

internal LineSegment GenerateLineSegment(f32 x, f32 y,
    f32 frameBufferWidth, f32 frameBufferHeight, f32 fov, f32 nearClip,
    f32 farClip, vec3 cameraPosition, vec3 cameraRotation)
{
  y = frameBufferHeight - y;

  f32 aspect = frameBufferWidth / frameBufferHeight;
  f32 rad = Radians(fov);
  f32 vLen = Tan(rad / 2.0f) * nearClip;
  f32 hLen = vLen * aspect;

  mat4 rotation = RotateX(-cameraRotation.x) * RotateY(-cameraRotation.y);
  rotation = Transpose(rotation); // Why do this, just don't negate rotation

  mat4 transform = Translate(cameraPosition) * rotation;
  //Debug_DrawAxis(transform, 20.0f);

  vec3 right = Normalize(rotation.columns[0].xyz);
  vec3 up = Normalize(rotation.columns[1].xyz);
  vec3 forward = -Normalize(rotation.columns[2].xyz);

  vec3 v = up * vLen;
  vec3 h = right * hLen;

  x -= frameBufferWidth * 0.5f;
  y -= frameBufferHeight * 0.5f;

  x /= frameBufferWidth * 0.5f;
  y /= frameBufferHeight * 0.5f;

  vec3 end = cameraPosition + forward * nearClip + h * x + v * y;

  vec3 dir = Normalize(end - cameraPosition);

  LineSegment result = {};
  result.start = cameraPosition;
  result.end = result.start + dir * farClip;

  return result;
}

struct CameraProps
{
    mat4 projection;
    mat4 view;
    f32 fov;
    f32 nearClip;
    f32 farClip;
    f32 frameBufferWidth;
    f32 frameBufferHeight;
    vec3 position;
    vec3 rotation;
};

// NOTE: This function assumes that the vertices from meshData are specified in
// counter-clockwise winding order
internal TriangleMesh CreateTriangleMesh(MeshData meshData, MemoryArena *tempArena)
{
    TriangleMesh result = {};

    Assert(meshData.indexCount % 3 == 0);

    vec3 *vertices =
        AllocateArray(tempArena, vec3, meshData.indexCount);
    for (u32 i = 0; i < meshData.indexCount; ++i)
    {
        u32 index = meshData.indices[i];
        Vertex *src = meshData.vertices + index;
        vertices[i] = src->position;
    }

    // Swap winding order
    for (u32 i = 0; i < meshData.indexCount / 3; ++i)
    {
        vec3 temp = vertices[i * 3];
        vertices[i * 3] = vertices[i * 3 + 2];
        vertices[i * 3 + 2] = temp;
    }

    result.vertices = vertices;
    result.count = meshData.indexCount;

    return result;
}

internal void UpdateRaycastScene(CollisionDetectionTestbench *testbench,
    GameInput *input, GameMemory *memory, MemoryArena *tempArena,
    DebugDrawingSystem *debugDrawingSystem, f32 dt, CameraProps cameraProps)
{
    mat4 transform = WorldTransform(Vec3(0, 0, -5), Vec3(1));
    vec4 color = Vec4(1, 0.6, 0.2, 0.8f);

    {
        RenderCommand_DrawMesh *drawCube =
            AllocateRenderCommand(memory, RenderCommand_DrawMesh);

        drawCube->mesh = Mesh_Cube;
        drawCube->shader = Shader_Color;

        AddShaderUniformValueMat4(drawCube, UniformValue_MvpMatrix,
            cameraProps.projection * cameraProps.view * transform);

        AddShaderUniformValueVec4(drawCube, UniformValue_Color, color);
    }

    DrawBox(debugDrawingSystem, Vec3(-0.5) + Vec3(0, 0, -5),
        Vec3(0.5) + Vec3(0, 0, -5), color.xyz);

    vec3 sphereCenter = Vec3(4, 0, 0);
    f32 sphereRadius = 0.5f;
    DrawSphere(debugDrawingSystem, sphereCenter, sphereRadius, color.xyz);

    vec3 capsuleEndPointA = Vec3(6, -1, -2);
    vec3 capsuleEndPointB = Vec3(6, 1, -2);
    f32 capsuleRadius = 0.5f;
    DrawCapsule(debugDrawingSystem, capsuleEndPointA, capsuleEndPointB,
        capsuleRadius, 23, color.xyz);

    vec3 triangleVertices[3] = {
        Vec3(-1, -0.5, 2),
        Vec3(1, -0.5, 2),
        Vec3(0, 1.5, 2)
    };

    DrawTriangles(debugDrawingSystem, triangleVertices, 1, color.xyz, 0.5f,
        Vec3(1, 0, 1), 0.0f);

    // clang-format off
    // Clockwise winding order
    vec3 meshVertices[] = {
        Vec3(0.5f, -0.5f, 0.5f),
        Vec3(0.0f, -0.5f, -0.5f),
        Vec3(-0.5f, -0.5f, 0.5f),

        Vec3(-0.5f, -0.5f, 0.5f),
        Vec3(0.0f, 0.5f, 0.0f),
        Vec3(0.5f, -0.5f, 0.5f),

        Vec3(0.5f, -0.5f, 0.5f),
        Vec3(0.0f, 0.5f, 0.0f),
        Vec3(0.0f, -0.5f, -0.5f),

        Vec3(0.0f, -0.5f, -0.5f),
        Vec3(0.0f, 0.5f, 0.0f),
        Vec3(-0.5f, -0.5f, 0.5f)
    };
    // clang-format on

    u32 meshVertexCount = ArrayCount(meshVertices);
    DrawTriangles(debugDrawingSystem, meshVertices, meshVertexCount / 3,
        color.xyz, 0.5f, Vec3(1, 0, 1), 0.0f);

    u32 totalTriangleVertices = meshVertexCount + 3; // Tetrahedron vertices + triangle vertices
    {
        Vertex *vertices = AllocateArray(tempArena, Vertex, totalTriangleVertices);
        vertices[0].position = triangleVertices[0];
        vertices[1].position = triangleVertices[1];
        vertices[2].position = triangleVertices[2];

        for (u32 i = 0; i < meshVertexCount; ++i)
        {
            vertices[i+3].position = meshVertices[i];
        }

        RenderCommand_UpdateVertexBuffer* updateVertexBuffer =
            AllocateRenderCommand(memory, RenderCommand_UpdateVertexBuffer);
        updateVertexBuffer->id = VertexBuffer_CollisionGeometry;
        updateVertexBuffer->vertexLayout = VertexLayout_Standard;
        updateVertexBuffer->vertices = vertices;
        updateVertexBuffer->vertexCount = totalTriangleVertices;
    }

    {
        RenderCommand_DrawVertexBuffer *drawVertexBuffer =
            AllocateRenderCommand(memory, RenderCommand_DrawVertexBuffer);
        drawVertexBuffer->id = VertexBuffer_CollisionGeometry;
        drawVertexBuffer->shader = Shader_Color;
        drawVertexBuffer->primitive = Primitive_Triangles;
        drawVertexBuffer->firstVertex = 0;
        drawVertexBuffer->vertexCount = totalTriangleVertices;

        AddShaderUniformValueMat4(drawVertexBuffer, UniformValue_MvpMatrix,
            cameraProps.projection * cameraProps.view);

        AddShaderUniformValueVec4(drawVertexBuffer, UniformValue_Color, color);
    }

    if (WasPressed(input->buttonStates + KEY_MOUSE_BUTTON_LEFT))
    {
        LineSegment lineSegment =
            GenerateLineSegment((f32)input->mousePosX, (f32)input->mousePosY,
                cameraProps.frameBufferWidth, cameraProps.frameBufferHeight,
                cameraProps.fov, cameraProps.nearClip, cameraProps.farClip,
                cameraProps.position, cameraProps.rotation);

        DrawLine(debugDrawingSystem, lineSegment.start, lineSegment.end,
                Vec3(0.8, 0.8, 0.8), 30.0f);

        LogMessage("start = (%g, %g, %g) end = (%g, %g, %g)",
            lineSegment.start.x, lineSegment.start.y, lineSegment.start.z,
            lineSegment.end.x, lineSegment.end.y, lineSegment.end.z);

        b32 sweepTest = true;
        if (sweepTest)
        {
            f32 sweepRadius = 0.25f;
            RaycastResult sphereRaycastResult = SphereSweepSphere_(
                sphereCenter, sphereRadius, lineSegment.start, lineSegment.end, sweepRadius);

            RaycastResult capsuleRaycastResult = SphereSweepCapsule_(
                capsuleEndPointA, capsuleEndPointB, capsuleRadius,
                lineSegment.start, lineSegment.end, sweepRadius,
                debugDrawingSystem);

            RaycastResult triangleRaycastResult =
                SphereSweepTriangle_(triangleVertices[2], triangleVertices[1],
                    triangleVertices[0], lineSegment.start, lineSegment.end,
                    sweepRadius, debugDrawingSystem);

            u32 triangleCount = meshVertexCount / 3;
            RaycastResult meshRaycastResults[256];
            SphereSweepTriangles(meshVertices, triangleCount, lineSegment.start,
                lineSegment.end, sweepRadius, meshRaycastResults, NULL);

            RaycastResult meshRaycastResult = {};
            RaycastResult *closestRaycastResult =
                FindClosestRaycastResult(meshRaycastResults, triangleCount);
            if (closestRaycastResult != NULL)
            {
                meshRaycastResult = *closestRaycastResult;
            }

            RaycastResult raycastResult = sphereRaycastResult;
            if (capsuleRaycastResult.isValid)
            {
                if (capsuleRaycastResult.tmin < raycastResult.tmin ||
                    !raycastResult.isValid)
                {
                    raycastResult = capsuleRaycastResult;
                }
            }

            if (triangleRaycastResult.isValid)
            {
                if (triangleRaycastResult.tmin < raycastResult.tmin ||
                    !raycastResult.isValid)
                {
                    raycastResult = triangleRaycastResult;
                }
            }

            if (meshRaycastResult.isValid)
            {
                if (meshRaycastResult.tmin < raycastResult.tmin ||
                    !raycastResult.isValid)
                {
                    raycastResult = meshRaycastResult;
                }
            }

            vec3 rayColor = Vec3(0.8);
            vec3 end = lineSegment.end;
            if (raycastResult.isValid)
            {
                DrawLine(debugDrawingSystem, raycastResult.hitPoint,
                    raycastResult.hitPoint + raycastResult.hitNormal * 0.5f,
                    Vec3(1, 0, 1), 30.0f);

                end = raycastResult.hitPoint;
                rayColor = Vec3(0, 0, 1);
            }
            DrawCapsule(debugDrawingSystem, lineSegment.start, end, sweepRadius,
                24, rayColor, 30.0f);
        }
        else
        {
            RaycastResult boxRaycastResult = RayIntersectAabb_(
                Vec3(-0.5) + Vec3(0, 0, -5), Vec3(0.5) + Vec3(0, 0, -5),
                lineSegment.start, lineSegment.end);

            RaycastResult sphereRaycastResult = RayIntersectSphere_(
                sphereCenter, sphereRadius, lineSegment.start, lineSegment.end);

            RaycastResult capsuleRaycastResult =
                RayIntersectCapsule_(capsuleEndPointA, capsuleEndPointB,
                    capsuleRadius, lineSegment.start, lineSegment.end);

            // Collision system uses clockwise winding order for triangles
            RaycastResult triangleRaycastResult =
                RayIntersectTriangle_(triangleVertices[2], triangleVertices[1],
                    triangleVertices[0], lineSegment.start, lineSegment.end);

            u32 triangleCount = meshVertexCount / 3;
            RaycastResult meshRaycastResults[256];
            RayIntersectTriangle(meshVertices, triangleCount, lineSegment.start,
                lineSegment.end, meshRaycastResults, NULL);

            RaycastResult meshRaycastResult = {};
            RaycastResult *closestRaycastResult =
                FindClosestRaycastResult(meshRaycastResults, triangleCount);
            u32 hitTriangleIndex = 0;
            if (closestRaycastResult != NULL)
            {
                meshRaycastResult = *closestRaycastResult;
                hitTriangleIndex =
                    (u32)(closestRaycastResult - meshRaycastResults);
            }

            RaycastResult raycastResult = boxRaycastResult;
            if (sphereRaycastResult.isValid)
            {
                if (sphereRaycastResult.tmin < raycastResult.tmin ||
                    !raycastResult.isValid)
                {
                    raycastResult = sphereRaycastResult;
                }
            }

            if (capsuleRaycastResult.isValid)
            {
                if (capsuleRaycastResult.tmin < raycastResult.tmin ||
                    !raycastResult.isValid)
                {
                    raycastResult = capsuleRaycastResult;
                }
            }

            if (triangleRaycastResult.isValid)
            {
                if (triangleRaycastResult.tmin < raycastResult.tmin ||
                    !raycastResult.isValid)
                {
                    raycastResult = triangleRaycastResult;
                }
            }

            if (meshRaycastResult.isValid)
            {
                if (meshRaycastResult.tmin < raycastResult.tmin ||
                    !raycastResult.isValid)
                {
                    raycastResult = meshRaycastResult;
                }
            }

            if (raycastResult.isValid)
            {
                DrawLine(debugDrawingSystem, raycastResult.hitPoint,
                    raycastResult.hitPoint + raycastResult.hitNormal * 0.5f,
                    Vec3(0, 0, 1), 30.0f);
            }
        }
    }
}

internal void UpdateCollisionDetectionTestbench(
    CollisionDetectionTestbench *testbench, GameInput *input,
    GameMemory *memory, DebugDrawingSystem *debugDrawingSystem,
    MemoryArena *tempArena, CVarTable *cvarTable, f32 dt)
{
    if (!CVar_GetValueU32(cvarTable, CVAR(show_console)))
    {
        if (input->buttonStates[KEY_MOUSE_BUTTON_RIGHT].isDown)
        {
            memory->setCursorMode(CursorMode_Locked);
        }
        else
        {
            memory->setCursorMode(CursorMode_Unlocked);
        }

        MoveCameraParameters moveCameraParams;
        moveCameraParams.currentState = testbench->cameraState;
        moveCameraParams.input = input;
        moveCameraParams.frameBufferWidth = (f32)memory->frameBufferWidth;
        moveCameraParams.frameBufferHeight = (f32)memory->frameBufferHeight;
        moveCameraParams.speed = 200.0f; // TODO: Use cvars
        moveCameraParams.friction = 7.0f;
        moveCameraParams.dt = dt;
        testbench->cameraState = MoveCamera(&moveCameraParams);
    }

    // TODO: Store somewhere
    f32 fovy = 60.0f;
    f32 aspect = (f32)memory->frameBufferWidth / (f32)memory->frameBufferHeight;
    f32 far = 512.0f;
    f32 near = 0.1f;
    mat4 projection = Perspective(fovy, aspect, near, far);
    mat4 view = RotateX(-testbench->cameraState.rotation.x) *
                RotateY(-testbench->cameraState.rotation.y) *
                Translate(-testbench->cameraState.position);

    mat4 orthographic = Orthographic(0.0f, (f32)memory->frameBufferWidth, 0.0f,
        (f32)memory->frameBufferHeight);

    // FIXME: Copied from game.cpp
    // Render to HDR buffer for tone mapping
    {
        RenderCommand_BindRenderTarget *bindRenderTarget =
            AllocateRenderCommand(memory, RenderCommand_BindRenderTarget);
        bindRenderTarget->id = RenderTarget_HdrBuffer;
    }

    {
        RenderCommand_Clear *clear =
            AllocateRenderCommand(memory, RenderCommand_Clear);
        clear->color = Vec4(0.04, 0.04, 0.04, 1.0);
    }

    CameraProps cameraProps;
    cameraProps.projection = projection;
    cameraProps.view = view;
    cameraProps.fov = fovy;
    cameraProps.nearClip = near;
    cameraProps.farClip = far;
    cameraProps.frameBufferWidth = (f32)memory->frameBufferWidth;
    cameraProps.frameBufferHeight = (f32)memory->frameBufferHeight;
    cameraProps.position = testbench->cameraState.position;
    cameraProps.rotation = testbench->cameraState.rotation;
    UpdateRaycastScene(testbench, input, memory, tempArena, debugDrawingSystem,
        dt, cameraProps);

    VertexPC *debugLineVertices =
        AllocateArray(tempArena, VertexPC, MAX_DEBUG_LINES * 2);
    u32 debugLineVertexCount = Debug_GetVertices(
        debugDrawingSystem, debugLineVertices, MAX_DEBUG_LINES * 2, true);

    {
        RenderCommand_UpdateVertexBuffer *updateVertexBuffer =
            AllocateRenderCommand(memory, RenderCommand_UpdateVertexBuffer);
        updateVertexBuffer->id = VertexBuffer_DebugLines;
        updateVertexBuffer->vertexLayout = VertexLayout_PositionAndColor;
        updateVertexBuffer->vertices = debugLineVertices;
        updateVertexBuffer->vertexCount = debugLineVertexCount;
    }

    {
        RenderCommand_DrawVertexBuffer *drawLines =
            AllocateRenderCommand(memory, RenderCommand_DrawVertexBuffer);
        drawLines->id = VertexBuffer_DebugLines;
        drawLines->shader = Shader_VertexColor;
        drawLines->primitive = Primitive_Lines;
        drawLines->firstVertex = 0;
        drawLines->vertexCount = debugLineVertexCount;

        AddShaderUniformValueMat4(
            drawLines, UniformValue_MvpMatrix, projection * view);
    }

    // FIXME: Copied from game.cpp
    // Bind default frame buffer
    {
        RenderCommand_BindRenderTarget *bindBackBuffer =
            AllocateRenderCommand(memory, RenderCommand_BindRenderTarget);
        bindBackBuffer->id = RenderTarget_BackBuffer;
    }

    {
        RenderCommand_Clear *clear =
            AllocateRenderCommand(memory, RenderCommand_Clear);
        clear->color = Vec4(0, 0, 0, 1);
    }

    // Draw tone mapped HDR buffer
    {
        RenderCommand_DrawMesh *drawHdrBuffer =
            AllocateRenderCommand(memory, RenderCommand_DrawMesh);
        drawHdrBuffer->mesh = Mesh_Quad;
        drawHdrBuffer->shader = Shader_Tonemapping;

        AddShaderUniformValueRenderTarget(
            drawHdrBuffer, UniformValue_HdrBuffer, RenderTarget_HdrBuffer);
        AddShaderUniformValueMat4(drawHdrBuffer, UniformValue_MvpMatrix,
            orthographic *
                WorldTransform(Vec3(memory->frameBufferWidth * 0.5f,
                                   memory->frameBufferHeight * 0.5f, 0.0f),
                    Vec3((f32)memory->frameBufferWidth,
                        -(f32)memory->frameBufferHeight, 1.0f)));
    }
}
