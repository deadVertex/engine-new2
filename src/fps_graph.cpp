struct FpsGraph
{
    f32 frameTimeSamples[64];
    u32 frameTimeIndex;
    f32 frameTimeToDisplay;
    f32 timeOfLastDisplay;
    f32 displayTime;
    f32 maxFrameTime;
    f32 minFrameTime;
    f32 avgFrameTime;
    f32 varFrameTime;
};

internal void FpsGraph_OnFrameState(FpsGraph *fpsGraph, float frameTime, float dt)
{
    fpsGraph->displayTime += dt;
    fpsGraph->frameTimeSamples[fpsGraph->frameTimeIndex] = frameTime;
    fpsGraph->frameTimeIndex =
        (fpsGraph->frameTimeIndex + 1) % ArrayCount(fpsGraph->frameTimeSamples);
}

internal void FpsGraph_Draw(FpsGraph *fpsGraph, GameMemory *memory,
    VertexBuffer *textBuffer, Font *font, b32 minimalDisplay)
{
    ui_Builder builder = {};
    ui_InitializeBuilder(&builder, memory);

    if (fpsGraph->displayTime > fpsGraph->timeOfLastDisplay + 0.2f)
    {
        fpsGraph->frameTimeToDisplay = memory->frameTime;
        f32 total = 0.0f;
        f32 max = 0.0f;
        f32 min = fpsGraph->frameTimeSamples[0];
        for (u32 i = 0; i < ArrayCount(fpsGraph->frameTimeSamples); ++i)
        {
            total += fpsGraph->frameTimeSamples[i];
            max = Max(max, fpsGraph->frameTimeSamples[i]);
            min = Min(min, fpsGraph->frameTimeSamples[i]);
        }
        fpsGraph->maxFrameTime = max;
        fpsGraph->minFrameTime = min;
        fpsGraph->avgFrameTime =
            total / (f32)ArrayCount(fpsGraph->frameTimeSamples);

        f32 variance = 0.0f;
        for (u32 i = 0; i < ArrayCount(fpsGraph->frameTimeSamples); ++i)
        {
            f32 delta =
                fpsGraph->frameTimeSamples[i] - fpsGraph->avgFrameTime;
            variance += delta * delta;
        }
        fpsGraph->varFrameTime =
            variance / (f32)ArrayCount(fpsGraph->frameTimeSamples);

        fpsGraph->timeOfLastDisplay = fpsGraph->displayTime;
    }

    char fpsString[100];
    if (!minimalDisplay)
    {
        ui_DrawBarGraphArguments drawBarGraph = {};
        drawBarGraph.color = Vec4(1.0, 0.6, 0.2, 1.0);
        drawBarGraph.axisColor = Vec4(1);

        drawBarGraph.width = 800.0f;
        drawBarGraph.height = 150.0f;
        drawBarGraph.position =
            Vec2(memory->frameBufferWidth - drawBarGraph.width,
                memory->frameBufferHeight - 150.0f);
        drawBarGraph.axisWidth = 1.0f;
        drawBarGraph.maxValue = 1.0f / 60.0f;
        drawBarGraph.sampleCount = ArrayCount(fpsGraph->frameTimeSamples);
        drawBarGraph.samples = fpsGraph->frameTimeSamples;

        ui_DrawBarGraph(&builder, &drawBarGraph);

        snprintf(fpsString, sizeof(fpsString),
            "Min: %.0f (%.02f ms)\nMax: %.0f (%.02f ms)\nAvg: %.0f (%.02f "
            "ms)\nVar:     (%.02f ms)\nDev:     (%.02f ms)",
            1.0f / fpsGraph->minFrameTime, fpsGraph->minFrameTime * 1000.0f,
            1.0f / fpsGraph->maxFrameTime, fpsGraph->maxFrameTime * 1000.0f,
            1.0f / fpsGraph->avgFrameTime, fpsGraph->avgFrameTime * 1000.0f,
            fpsGraph->varFrameTime * 1000.0f,
            Sqrt(fpsGraph->varFrameTime) * 1000.0f);
    }
    else
    {
        snprintf(fpsString, sizeof(fpsString), "FPS: %.0f (%.02f ms)",
            1.0f / fpsGraph->avgFrameTime, fpsGraph->avgFrameTime * 1000.0f);
    }

    ui_DrawStringArguments drawFps = {};
    drawFps.memory = memory;
    drawFps.vertexBuffer = textBuffer;
    drawFps.text = fpsString;
    drawFps.position = Vec2(
            memory->frameBufferWidth - 10.0f, memory->frameBufferHeight - 60.0f);
    drawFps.color = Vec4(1, 1, 1, 1);
    drawFps.font = font;
    drawFps.orthographicProjection =
        Orthographic(0.0f, (f32)memory->frameBufferWidth, 0.0f,
                (f32)memory->frameBufferHeight);
    drawFps.horizontalAlignment = HorizontalAlign_Right;
    drawFps.verticalAlignment = VerticalAlign_Center;
    ui_DrawString(&drawFps);
}
