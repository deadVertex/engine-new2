global f32 g_debug_camera_speed = 80.0f;
global f32 g_debug_camera_friction = 7.0f;

inline u32 GetEntityType(ecs_EntityWorld *world, EntityId entity)
{
    u32 prefabId;
    ecs_GetComponent(world, EntityPrefabIdComponentId, &entity, 1,
            &prefabId, sizeof(prefabId));

    return prefabId;
}

internal void RemoveDeadEntitiesSystem(ecs_EntityWorld *world)
{
    EntityId entities[MAX_ENTITIES];
    u32 entityCount = ecs_GetEntitiesWithComponent(
        world, HealthComponentId, entities, ArrayCount(entities));

    f32 health[MAX_ENTITIES];
    ecs_GetComponent(world, HealthComponentId, entities, entityCount, health,
        sizeof(health[0]));

    for (u32 entityIdx = 0; entityIdx < entityCount; ++entityIdx)
    {
        if (health[entityIdx] <= 0.0f)
        {
            EntityId entity = entities[entityIdx];

            LogMessage("Removing dead entity %u", entity);
            ecs_QueueEntitiesForDeletion(world, &entity, 1);

            // TODO: Only temporary, in future we don't want to delete items if
            // the container is killed.
            if (ecs_HasComponent(world, InventoryComponentId, entity))
            {
                RemoveAllItemsOwnedByEntity(world, entity);
            }
        }
    }
}

internal void MoveCamera(ecs_EntityWorld *world, GameInput *input, float dt,
    f32 frameBufferWidth, f32 frameBufferHeight)
{
    EntityId camera = NULL_ENTITY;
    if (GetFirstEntity(world, DebugCameraComponentId, &camera))
    {
        vec3 eulerAngles;
        ecs_GetComponent(world, EulerAnglesComponentId, &camera, 1,
            &eulerAngles, sizeof(eulerAngles));

        vec3 position;
        ecs_GetComponent(world, PositionComponentId, &camera, 1, &position,
            sizeof(position));

        vec3 velocity;
        ecs_GetComponent(world, VelocityComponentId, &camera, 1, &velocity,
            sizeof(velocity));

        MoveCameraParameters params = {};
        params.currentState.position = position;
        params.currentState.rotation = eulerAngles;
        params.currentState.velocity = velocity;
        params.input = input;
        params.frameBufferWidth = frameBufferWidth;
        params.frameBufferHeight = frameBufferHeight;
        params.speed = g_debug_camera_speed;
        params.friction = g_debug_camera_friction;
        params.dt = dt;
        CameraState newState = MoveCamera(&params);

        ecs_SetComponent(world, EulerAnglesComponentId, &camera, 1,
            &newState.rotation, sizeof(newState.rotation));
        ecs_SetComponent(world, PositionComponentId, &camera, 1,
            &newState.position, sizeof(newState.position));
        ecs_SetComponent(world, VelocityComponentId, &camera, 1,
            &newState.velocity, sizeof(newState.velocity));
    }
}

struct CameraProperties
{
    vec3 position;
    vec3 rotation; // FIXME: Rename to eulerAngles
    mat4 view;
    mat4 projection;
    f32 fov;
    f32 nearClip;
    f32 farClip;
};

// FIXME: Update all users of this code to use the quaternion rotation
internal CameraProperties GetActiveCameraProperties(
    ecs_EntityWorld *world, f32 frameBufferWidth, f32 frameBufferHeight)
{
    CameraProperties result = {};
    EntityId camera = NULL_ENTITY;
    if (GetFirstEntity(world, ActiveCameraComponentId, &camera))
    {
        vec3 cameraAngles;
        ecs_GetComponent(world, EulerAnglesComponentId, &camera, 1,
            &cameraAngles, sizeof(cameraAngles));

        vec3 cameraPosition;
        ecs_GetComponent(world, PositionComponentId, &camera, 1,
            &cameraPosition, sizeof(cameraPosition));

        b32 isPlayer =
            ecs_HasComponent(world, ActionBarActiveSlotComponentId, camera);

        cameraPosition =
            isPlayer ? cameraPosition + PLAYER_CAMERA_OFFSET : cameraPosition;

        result.position = cameraPosition;
        result.rotation = cameraAngles;

        // TODO: Store in component
        f32 fovy = 60.0f;
        f32 aspect = frameBufferWidth / frameBufferHeight;
        f32 far = 2000.0f;
        f32 near = 0.10f;
        result.projection = Perspective(fovy, aspect, near, far);

        // Inverse of the camera world transform
        result.view = RotateX(-cameraAngles.x) * RotateY(-cameraAngles.y) *
                      Translate(-cameraPosition);
        result.fov = fovy;
        result.nearClip = near;
        result.farClip = far;
    }
    else
    {
        result.view = Identity();
        result.projection = Identity();
    }

    return result;
}

internal void SpawnRocksOnTerrainSystem(
    ecs_EntityWorld *world, GameAssets *assets, MemoryArena *tempArena)
{
    u32 join[] = {TerrainComponentId, PositionComponentId,
        ScaleComponentId};

    EntityId entities[MAX_ENTITIES];
    u32 count =
        ecs_Join(world, join, ArrayCount(join), entities, ArrayCount(entities));

    vec3 positions[MAX_ENTITIES];
    vec3 scales[MAX_ENTITIES];

    ecs_GetComponent(world, PositionComponentId, entities, count, positions,
        sizeof(positions[0]));
    ecs_GetComponent(
        world, ScaleComponentId, entities, count, scales, sizeof(scales[0]));

    Assert(count <= 1);
    for (u32 i = 0; i < count; ++i)
    {
        SpawnRocks(world, positions[i], scales[i], assets, tempArena);
    }
}

internal void UpdateTimeToLiveSystem(ecs_EntityWorld *world, f32 dt)
{
    EntityId entities[MAX_ENTITIES];
    u32 entityCount = ecs_GetEntitiesWithComponent(
        world, TimeToLiveComponentId, entities, ArrayCount(entities));

    f32 timeToLive[MAX_ENTITIES];
    ecs_GetComponent(world, TimeToLiveComponentId, entities, entityCount,
        timeToLive, sizeof(timeToLive[0]));

    for (u32 entityIdx = 0; entityIdx < entityCount; ++entityIdx)
    {
        if (timeToLive[entityIdx] <= 0.0f)
        {
            ecs_QueueEntitiesForDeletion(world, &entities[entityIdx], 1);
        }
        else
        {
            timeToLive[entityIdx] -= dt;
        }
    }

    ecs_SetComponent(world, TimeToLiveComponentId, entities, entityCount,
        timeToLive, sizeof(timeToLive[0]));
}

internal void ProcessLootTablesSystem(
    ecs_EntityWorld *world, GameAssets *assets, RandomNumberGenerator *rng)
{
    EntityId entities[MAX_ENTITIES];
    u32 entityCount = ecs_GetEntitiesWithComponent(
        world, LootTableComponentId, entities, ArrayCount(entities));

    u32 lootTableIds[MAX_ENTITIES];
    ecs_GetComponent(world, LootTableComponentId, entities, entityCount,
        lootTableIds, sizeof(lootTableIds[0]));

    for (u32 entityIdx = 0; entityIdx < entityCount; ++entityIdx)
    {
        u32 lootTableIdx = lootTableIds[entityIdx];
        Assert(lootTableIdx < ArrayCount(assets->lootTables));
        LootTable *lootTable = assets->lootTables + lootTableIdx;

        for (u32 rowIdx = 0; rowIdx < lootTable->count; ++rowIdx)
        {
            LootTableEntry *row = lootTable->entries + rowIdx;
            Assert(row->probability > 0.0f && row->probability < 1.0f);

            if (row->probability >= RandomUnilateral(rng))
            {
                AddItemToInventory(
                    world, entities[entityIdx], row->itemId, row->quantity);
            }
        }
    }

    // Not sure if I like the idea of removing a component to indicate that no
    // further processing is required, but it's easy and it works.
    ClearTable(world->tables + LootTableComponentId);
}
