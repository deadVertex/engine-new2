#pragma once

#include "platform.h"
#include "math_lib.h"
#include "logging.h"

enum
{
    WindowMode_Windowed,
    WindowMode_WindowedBorderless,
    WindowMode_Fullscreen,
};

struct PlatformData
{
    b32 isRunning; // RW
    b32 isClientRunning; // R0
    b32 isServerRunning; // R0
    u32 windowMode; // Unsupported
    b32 vSyncEnabled; // RW
    u32 frameRateCap; // RW
    u32 frameBufferWidth; // RW
    u32 frameBufferHeight; // RW
};

struct DebugReadFileResult
{
    void *contents;
    u32 size;
};

#define DebugReadEntireFile(NAME) DebugReadFileResult NAME(const char *path)
typedef DebugReadEntireFile(DebugReadEntireFileFunction);

#define DebugWriteEntireFile(NAME) b32 NAME(const char *path, void *data, u32 length)
typedef DebugWriteEntireFile(DebugWriteEntireFileFunction);

#define DebugFreeFileMemory(NAME) void NAME(DebugReadFileResult fileResult)
typedef DebugFreeFileMemory(DebugFreeFileMemoryFunction);

enum
{
    CursorMode_Unlocked,
    CursorMode_Locked,
};
#define DebugSetCursorMode(NAME) void NAME(u32 mode)
typedef DebugSetCursorMode(DebugSetCursorModeFunction);

#define DebugStartServer(NAME) void NAME(void)
typedef DebugStartServer(DebugStartServerFunction);

#define DebugStartClient(NAME) b32 NAME(const char *address)
typedef DebugStartClient(DebugStartClientFunction);

#define DebugGetPlatformData(NAME) void NAME(PlatformData *data)
typedef DebugGetPlatformData(DebugGetPlatformDataFunction);

#define DebugSetPlatformData(NAME) void NAME(PlatformData *data)
typedef DebugSetPlatformData(DebugSetPlatformDataFunction);

struct DebugRecord
{
    u64 guid;
    const char *fileName;
    const char *name;
    u32 lineNumber;
    DebugRecord *next;
};

enum
{
    DebugEventType_BeginBlock,
    DebugEventType_EndBlock,
};

struct DebugEvent
{
    u32 recordIndex;
    u32 type;
    u64 cycleCount;
};

#define MAX_DEBUG_EVENTS 0x10000
#define MAX_DEBUG_RECORDS 0x1000
struct DebugEventTable
{
    DebugEvent events[2][MAX_DEBUG_EVENTS];
    u32 count[2];
    u32 writeCursor;
    u32 readCursor;

    DebugRecord records[MAX_DEBUG_RECORDS];
    u32 recordCount;
    DebugRecord *recordsHashTable[MAX_DEBUG_RECORDS];
};

global DebugEventTable *g_DebugEventTable;

#define ENABLE_PERFORMANCE_COUNTERS
#ifdef ENABLE_PERFORMANCE_COUNTERS
#ifdef _MSC_VER
#include <intrin.h>
#elif !defined(__clang__)
#include <x86intrin.h>
#endif

inline u32 UpdateDebugRecord(
    u64 guid, const char *fileName, u32 lineNumber, const char *name)
{
    u32 index = guid % ArrayCount(g_DebugEventTable->recordsHashTable);
    DebugRecord *record = g_DebugEventTable->recordsHashTable[index];
    while (record != NULL && record->guid != guid)
    {
        record = record->next;
    }
    if (record == NULL)
    {
        Assert(g_DebugEventTable->recordCount <
               ArrayCount(g_DebugEventTable->records));
        record = g_DebugEventTable->records + g_DebugEventTable->recordCount++;
        record->next = g_DebugEventTable->recordsHashTable[index];
        g_DebugEventTable->recordsHashTable[index] = record;
    }

    record->fileName = fileName;
    record->lineNumber = lineNumber;
    record->name = name;
    record->guid = guid;

    u32 recordIndex = (u32)(record - g_DebugEventTable->records);
    return recordIndex;
}

inline void RecordDebugEvent(u32 type, u32 recordIndex)
{
    u32 writeCursor = g_DebugEventTable->writeCursor;
    Assert(g_DebugEventTable->count[writeCursor] < MAX_DEBUG_EVENTS);
    DebugEvent *event =
        g_DebugEventTable->events[writeCursor] + g_DebugEventTable->count[writeCursor]++;
    event->recordIndex = recordIndex;
    event->type = type;
    event->cycleCount = __rdtsc();
}

#define DEBUG_GUID(FILE, LINE) (u64)(FILE#LINE)

#define BEGIN_TIMED_BLOCK(NAME)                                                \
    u32 __recordIndex##NAME = UpdateDebugRecord(                               \
        DEBUG_GUID(__FILE__, __LINE__), __FILE__, __LINE__, #NAME);            \
    RecordDebugEvent(DebugEventType_BeginBlock, __recordIndex##NAME);

#define END_TIMED_BLOCK(NAME) \
    RecordDebugEvent(DebugEventType_EndBlock, __recordIndex##NAME);

struct TimedBlock
{
    u32 recordIndex;

    TimedBlock(u64 guid, const char *fileName, u32 lineNumber, const char *function)
    {
        recordIndex = UpdateDebugRecord(guid, fileName, lineNumber, function);
        RecordDebugEvent(DebugEventType_BeginBlock, recordIndex);
    }

    ~TimedBlock()
    {
        RecordDebugEvent(DebugEventType_EndBlock, recordIndex);
    }
};

#define TIMED_BLOCK()                                                          \
    TimedBlock _timedBlock##__LINE__ = TimedBlock(                             \
        DEBUG_GUID(__FILE__, __LINE__), __FILE__, __LINE__, __FUNCTION__)
#else
#define TIMED_BLOCK() {}
#define BEGIN_TIMED_BLOCK(NAME)
#define BEGIN_TIMED_BLOCK(NAME)
#endif

enum
{
  KEY_UNKNOWN = 0,
  KEY_MOUSE_BUTTON_LEFT = 1,
  KEY_MOUSE_BUTTON_MIDDLE = 2,
  KEY_MOUSE_BUTTON_RIGHT = 3,
  KEY_MOUSE_BUTTON4 = 4,
  KEY_MOUSE_BUTTON5 = 5,

  KEY_BACKSPACE = 8,
  KEY_TAB = 9,
  KEY_INSERT = 10,
  KEY_HOME = 11,
  KEY_PAGE_UP = 12,
  KEY_DELETE = 13,
  KEY_END = 14,
  KEY_PAGE_DOWN = 15,
  KEY_ENTER = 16,

  KEY_LEFT_SHIFT = 17,
  KEY_LEFT_CTRL = 18,
  KEY_LEFT_ALT = 19,
  KEY_RIGHT_SHIFT = 20,
  KEY_RIGHT_CTRL = 21,
  KEY_RIGHT_ALT = 22,

  KEY_LEFT = 23,
  KEY_RIGHT = 24,
  KEY_UP = 25,
  KEY_DOWN = 26,

  KEY_ESCAPE = 27,
  KEY_SPACE = 32,

  KEY_APOSTROPHE = 39,
  KEY_COMMA = 44,
  KEY_MINUS = 45,
  KEY_PERIOD = 46,
  KEY_SLASH = 47,
  KEY_0 = 48,
  KEY_1 = 49,
  KEY_2 = 50,
  KEY_3 = 51,
  KEY_4 = 52,
  KEY_5 = 53,
  KEY_6 = 54,
  KEY_7 = 55,
  KEY_8 = 56,
  KEY_9 = 57,
  KEY_SEMI_COLON = 59,
  KEY_EQUAL = 61,

  KEY_A = 65,
  KEY_B = 66,
  KEY_C = 67,
  KEY_D = 68,
  KEY_E = 69,
  KEY_F = 70,
  KEY_G = 71,
  KEY_H = 72,
  KEY_I = 73,
  KEY_J = 74,
  KEY_K = 75,
  KEY_L = 76,
  KEY_M = 77,
  KEY_N = 78,
  KEY_O = 79,
  KEY_P = 80,
  KEY_Q = 81,
  KEY_R = 82,
  KEY_S = 83,
  KEY_T = 84,
  KEY_U = 85,
  KEY_V = 86,
  KEY_W = 87,
  KEY_X = 88,
  KEY_Y = 89,
  KEY_Z = 90,
  KEY_LEFT_BRACKET = 91,
  KEY_BACKSLASH = 92,
  KEY_RIGHT_BRACKET = 93,
  KEY_GRAVE_ACCENT = 96,

  KEY_F1 = 128,
  KEY_F2 = 129,
  KEY_F3 = 130,
  KEY_F4 = 131,
  KEY_F5 = 132,
  KEY_F6 = 133,
  KEY_F7 = 134,
  KEY_F8 = 135,
  KEY_F9 = 136,
  KEY_F10 = 137,
  KEY_F11 = 138,
  KEY_F12 = 139,

  KEY_NUM0 = 140,
  KEY_NUM1 = 141,
  KEY_NUM2 = 142,
  KEY_NUM3 = 143,
  KEY_NUM4 = 144,
  KEY_NUM5 = 145,
  KEY_NUM6 = 146,
  KEY_NUM7 = 147,
  KEY_NUM8 = 148,
  KEY_NUM9 = 149,
  KEY_NUM_DECIMAL = 150,
  KEY_NUM_ENTER = 151,
  KEY_NUM_PLUS = 152,
  KEY_NUM_MINUS = 153,
  KEY_NUM_MULTIPLY = 154,
  KEY_NUM_DIVIDE = 155,
  MAX_KEYS,
};

struct ButtonState
{
    b32 isDown;
    b32 wasDown;
};

inline b32 WasPressed(ButtonState *buttonState)
{
    return (buttonState->isDown && !buttonState->wasDown);
}

inline b32 WasReleased(ButtonState *buttonState)
{
    return (!buttonState->isDown && buttonState->wasDown);
}

struct GameInput
{
    i32 mouseRelPosX;
    i32 mouseRelPosY;
    i32 mousePosX;
    i32 mousePosY;
    ButtonState buttonStates[MAX_KEYS];
    u32 characters[4];
    u32 characterCount;
};

enum
{
    PlayerInventoryAction_None,
    PlayerInventoryAction_ChangeSlot,
    PlayerInventoryAction_Close,
};

typedef u32 NetEntityId;

struct PlayerInventoryCommand
{
    u32 action;
    union
    {
        struct
        {
            u32 from;
            u32 to;
            NetEntityId fromEntity;
            NetEntityId toEntity;
        } changeSlot;
    };
};

enum
{
    PlayerAction_MoveForward = Bit(0),
    PlayerAction_MoveBackward = Bit(1),
    PlayerAction_MoveLeft = Bit(2),
    PlayerAction_MoveRight = Bit(3),
    PlayerAction_Interact = Bit(4),
    PlayerAction_PrimaryFire = Bit(5),
    PlayerAction_Jump = Bit(6),
    PlayerAction_Sprint = Bit(7),
};

struct PlayerCommand
{
    u32 actions;
    u32 prevActions;
    float mouseX;
    float mouseY;
    u32 switchToActionSlot;

    PlayerInventoryCommand inventoryCommand;
};

struct GameSoundOutputBuffer
{
    i16 *samples;
    u32 samplesPerSecond;
    u32 sampleCount;
};

enum
{
    GameState_MainMenu,
    GameState_Playing,
    GameState_CollisionDetectionTestBench,
    GameState_RenderTest,
    GameState_Experimental,
};

enum
{
    Mesh_Quad,
    Mesh_Cube,
    Mesh_Monkey,
    Mesh_Pistol,
    Mesh_Hatchet,
    Mesh_Foundation,
    Mesh_Wall,
    Mesh_Doorway,
    Mesh_Sphere,
    Mesh_TerrainPatch,
    Mesh_Rock01,
    Mesh_Rock02,
    Mesh_Rock03,
    Mesh_Rock04,
    Mesh_SkinningTest,
    MAX_MESHES,
};

enum
{
    Shader_Color,
    Shader_Texture,
    Shader_TextureAlpha,
    Shader_DiffuseColor,
    Shader_DiffuseColorInstanced,
    Shader_DiffuseTexture,
    Shader_Tonemapping,
    Shader_Terrain,
    Shader_CubeMap,
    Shader_AlphaMapping,
    Shader_ColorUI,
    Shader_FullscreenVignette,
    Shader_MicrofacetPBR,
    Shader_VertexColor,
    Shader_VertexColorWithDepth,
    Shader_WorldDiffuseTexture,
    Shader_VisualizeNormals,
    Shader_DiffuseNormalMapping,
    Shader_TerrainDebug,
    Shader_ParticleTexture,
    Shader_Skinning,
    MAX_SHADERS,
};

enum
{
    Texture_DevGrid512,
    Texture_Grass,
    Texture_Noise,
    Texture_Gradient,
    Texture_Rock,
    Texture_Skybox,
    Texture_TestFont,
    Texture_MainMenuTitleFont,
    Texture_Sand,
    Texture_DebugFont,
    Texture_PistolIcon,
    Texture_PistolAmmoIcon,
    Texture_HatchetIcon,
    Texture_HatchetBaseColor,
    Texture_StorageBoxIcon,
    Texture_FoundationIcon,
    Texture_WallIcon,
    Texture_DoorwayIcon,
    Texture_TerrainNormalMap,
    Texture_TestNormalMap,
    Texture_UVGrid,
    Texture_TestNormalMap2,
    Texture_SmokeParticle,
    Texture_SunIcon,
    Texture_CloudIcon,
    MAX_TEXTURES,
};

enum
{
    Material_GreenDiffuse,
    Material_DevGrid512,
    Material_StorageBox,
    Material_BuildingPreview,
    Material_Red,
    Material_WorldDevGrid512,
    Material_Skybox,
    Material_Hatchet,
    Material_VisualizeNormals,
    Material_NormalMapTest,
    Material_UVTest,
    Material_Skinning,
    MAX_MATERIALS,
};

#define COLLISION_GEOMETRY_MAX_VERTICES 0x1000
enum
{
    VertexBuffer_Text,
    VertexBuffer_DebugLines,
    VertexBuffer_CollisionGeometry,
    VertexBuffer_DebugLinesWithDepth,
    MAX_VERTEX_BUFFERS,
};

enum
{
    Primitive_Triangles,
    Primitive_Lines,
};

enum
{
    FaceCulling_None,
    FaceCulling_BackFace,
    FaceCulling_FrontFace,
};

enum
{
    RenderCommand_CreateMeshTypeId,
    RenderCommand_CreateShaderTypeId,
    RenderCommand_CreateTextureTypeId,
    RenderCommand_DrawMeshTypeId,
    RenderCommand_CreateUniformBufferTypeId,
    RenderCommand_UpdateUniformBufferTypeId,
    RenderCommand_CreateRenderTargetTypeId,
    RenderCommand_BindRenderTargetTypeId,
    RenderCommand_ClearTypeId,
    RenderCommand_CreateCubeMapTypeId,
    RenderCommand_CreateVertexBufferTypeId,
    RenderCommand_UpdateVertexBufferTypeId,
    RenderCommand_DrawVertexBufferTypeId,
    RenderCommand_CreateInstancedMeshTypeId,
    RenderCommand_DrawInstancedMeshTypeId,
};

//#define TERRAIN_PATCH_GRID_DIM 512
#define TERRAIN_PATCH_SCALE 2048.0f
#define TERRAIN_HEIGHTMAP_SIZE 512
#define TERRAIN_HEIGHT_SCALE 256.0f
#define TERRAIN_COLLISION_GRID_DIM 512 // May need to adjust this to match
                                       // TERRAIN_PATCH_SCALE depending on
                                       // how steep the terrain is

#define TERRAIN_COLLISION_MAX_NODES 0x10000
#define SAMPLES_PER_TREE_NODE 4 // Collision
#define TERRAIN_VERTICES_PER_NODE 16 // Rendering
#define TERRAIN_RENDERING_MAX_NODES 0x10000



#define TEXT_BUFFER_MAX_VERTICES 0x8000
#define MAX_DEBUG_LINES 0xA0000

#define MAX_GAME_PERSISTENT_MEMORY Megabytes(512)
#define MAX_GAME_TEMPORARY_MEMORY Megabytes(256)

// NOTE: Must match values defined in shader
#define MAX_DIRECTIONAL_LIGHTS 4
#define MAX_POINT_LIGHTS 32
#define MAX_SPOT_LIGHTS 32

struct DirectionalLight
{
    vec4 direction;
    vec4 color;
};

struct PointLight
{
    vec4 position;
    vec4 color;
    vec4 attenuation;
};

struct SpotLight
{
    vec4 position;
    vec4 direction;
    vec4 color;
    vec4 attenuation[2];
};

// Refactor to SoA to match entity data layout
struct LightingDataUniformBuffer
{
    DirectionalLight directionalLights[MAX_DIRECTIONAL_LIGHTS];
    PointLight pointLights[MAX_POINT_LIGHTS];
    SpotLight spotLights[MAX_SPOT_LIGHTS];
    vec4 ambientLightColor;
    u32 directionalLightCount;
    u32 pointLightCount;
    u32 spotLightCount;
};

#define MAX_BONES 255
struct BoneTransformsUniformBuffer
{
    mat4 transforms[MAX_BONES];
    u32 length;
};

enum
{
    RenderTarget_BackBuffer,
    RenderTarget_HdrBuffer,
    RenderTarget_ShadowBuffer,
    MAX_RENDER_TARGETS,
};

enum
{
    CUBE_MAP_FACE_Z_POS,
    CUBE_MAP_FACE_Z_NEG,
    CUBE_MAP_FACE_Y_POS,
    CUBE_MAP_FACE_Y_NEG,
    CUBE_MAP_FACE_X_POS,
    CUBE_MAP_FACE_X_NEG,
    CUBE_MAP_MAX_FACES,
};

enum
{
    ColorFormat_RGBA8,
    ColorFormat_R8,
    ColorFormat_R16,
};

struct Vertex
{
    vec3 position;
    vec3 normal;
    vec3 tangent;
    vec2 texCoord;
};

struct VertexPC
{
    vec3 position;
    vec3 color;
};

struct VertexSkinning
{
    vec3 position;
    vec3 normal;
    vec3 tangent;
    vec2 texCoord;
    u32 boneId;
};

enum
{
    UniformValue_Color,
    UniformValue_MvpMatrix,
    UniformValue_AlbedoTexture,
    UniformValue_ShadowMap,
    UniformValue_LightViewProjection,
    UniformValue_ModelMatrix,
    UniformValue_LightingData,
    UniformValue_HdrBuffer,
    UniformValue_Heightmap,
    UniformValue_SplatMap,
    UniformValue_GrassTexture,
    UniformValue_RockTexture,
    UniformValue_CubeMap,
    UniformValue_AlphaMap,
    UniformValue_SandTexture,
    UniformValue_MaxDistance,
    UniformValue_CameraPosition,
    UniformValue_NormalMap,
    UniformValue_UVTransform,
    UniformValue_ViewProjection,
    UniformValue_BoneTransformsBuffer,
    MAX_UNIFORMS = 24,
};

enum
{
    UniformBuffer_LightingData,
    UniformBuffer_BoneTransforms,
    MAX_UNIFORM_BUFFERS,
};

enum
{
    UniformType_Vec3,
    UniformType_Vec4,
    UniformType_Mat4,
    UniformType_Texture,
    UniformType_Buffer,
    UniformType_RenderTarget,
    UniformType_F32,
};

struct ShaderUniformValue
{
    u32 id;
    u32 type;
    union
    {
        vec3 v3;
        vec4 v4;
        mat4 m4;
        u32 texture;
        u32 buffer;
        u32 renderTarget;
        f32 f;
    };
};

struct ShaderUniformDefinitions
{
    u32 id;
    u32 type;
    const char *name;
};

struct VertexBuffer
{
    Vertex *vertices;
    u32 count;
    u32 capacity;
};

enum
{
    TextureSamplerMode_Nearest,
    TextureSamplerMode_Linear,
    TextureSamplerMode_LinearMipMapping,
};

enum
{
    VertexLayout_Standard,
    VertexLayout_PositionAndColor,
    VertexLayout_Skinning,
};

#define SHADER_MAX_INCLUDES 8

struct RenderCommand_DrawMesh
{
    u32 mesh;
    u32 shader;
    ShaderUniformValue uniformValues[MAX_UNIFORMS];
    u32 uniformValueCount;
};

struct RenderCommand_DrawInstancedMesh
{
    u32 mesh;
    u32 shader;
    ShaderUniformValue uniformValues[MAX_UNIFORMS];
    u32 uniformValueCount;
    u32 instanceCount;
    mat4 *modelMatrices;
};

struct RenderCommand_CreateMesh
{
    u32 id;
    void *vertices;
    u32 vertexCount;
    u32 vertexLayout;

    u32 *indices;
    u32 indexCount;
};

struct RenderCommand_CreateInstancedMesh
{
    u32 id;
    Vertex *vertices;
    u32 vertexCount;

    u32 *indices;
    u32 indexCount;

    u32 maxInstanceCount;
};

struct RenderCommand_CreateShader
{
    u32 id;
    const char *shaderSource;
    const char *defines;
    const char *includes[SHADER_MAX_INCLUDES];
    u32 includeCount;
    ShaderUniformDefinitions uniformDefinitions[MAX_UNIFORMS];
    u32 uniformDefinitionCount;
    b32 depthTestEnabled;
    b32 alphaBlendingEnabled;
    b32 lineModeEnabled;
    u32 faceCullingMode;
};

struct RenderCommand_CreateTexture
{
    u32 id;
    void *pixels;
    u32 width;
    u32 height;
    u32 colorFormat;
    u32 samplerMode;
};

struct RenderCommand_CreateCubeMap
{
    u32 id;
    void *pixels[CUBE_MAP_MAX_FACES];
    u32 width;
    u32 colorFormat;
};

struct RenderCommand_CreateUniformBuffer
{
    u32 id;
    u32 capacity;
    // TODO: Support initializing the buffer with data
};

struct RenderCommand_UpdateUniformBuffer
{
    void *data;
    u32 id;
    u32 length;
    u32 offset;
};

struct RenderCommand_CreateRenderTarget
{
    u32 id;
    u32 width;
    u32 height;
    b32 isShadowBuffer;
};

struct RenderCommand_BindRenderTarget
{
    u32 id;
};

struct RenderCommand_Clear
{
    vec4 color;
};

struct RenderCommand_CreateVertexBuffer
{
    u32 id;
    u32 vertexCount;
    u32 vertexLayout;
};

struct RenderCommand_UpdateVertexBuffer
{
    u32 id;
    u32 vertexLayout;
    void *vertices;
    u32 vertexCount;
};

struct RenderCommand_DrawVertexBuffer
{
    u32 id;
    u32 shader;
    u32 firstVertex;
    u32 vertexCount;
    u32 primitive;
    ShaderUniformValue uniformValues[MAX_UNIFORMS];
    u32 uniformValueCount;
};

struct RenderCommand
{
    u32 type;
    union {
        RenderCommand_DrawMesh drawMesh;
        RenderCommand_DrawInstancedMesh drawInstancedMesh;

        RenderCommand_CreateMesh createMesh;
        RenderCommand_CreateInstancedMesh createInstancedMesh;

        RenderCommand_CreateShader createShader;

        RenderCommand_CreateTexture createTexture;
        RenderCommand_CreateCubeMap createCubeMap;

        RenderCommand_CreateUniformBuffer createUniformBuffer;

        RenderCommand_UpdateUniformBuffer updateUniformBuffer;

        RenderCommand_CreateRenderTarget createRenderTarget;

        RenderCommand_BindRenderTarget bindRenderTarget;

        RenderCommand_Clear clear;

        RenderCommand_CreateVertexBuffer createVertexBuffer;

        RenderCommand_UpdateVertexBuffer updateVertexBuffer;

        RenderCommand_DrawVertexBuffer drawVertexBuffer;
    };
};

inline ShaderUniformValue *AllocateShaderUniformValue(
    RenderCommand_DrawMesh *command)
{
    Assert(command->uniformValueCount < ArrayCount(command->uniformValues));
    ShaderUniformValue *value =
        command->uniformValues + command->uniformValueCount++;

    return value;
}

inline ShaderUniformValue *AllocateShaderUniformValue(
    RenderCommand_DrawInstancedMesh *command)
{
    Assert(command->uniformValueCount < ArrayCount(command->uniformValues));
    ShaderUniformValue *value =
        command->uniformValues + command->uniformValueCount++;

    return value;
}

inline ShaderUniformValue *AllocateShaderUniformValue(
    RenderCommand_DrawVertexBuffer *command)
{
    Assert(command->uniformValueCount < ArrayCount(command->uniformValues));
    ShaderUniformValue *value =
        command->uniformValues + command->uniformValueCount++;

    return value;
}

inline void SetShaderUniformValueMat4(ShaderUniformValue *value, u32 id, mat4 m4)
{
    value->id = id;
    value->type = UniformType_Mat4;
    value->m4 = m4;
}

inline void SetShaderUniformValueVec4(ShaderUniformValue *value, u32 id, vec4 v4)
{
    value->id = id;
    value->type = UniformType_Vec4;
    value->v4 = v4;
}

inline void SetShaderUniformValueVec3(ShaderUniformValue *value, u32 id, vec3 v3)
{
    value->id = id;
    value->type = UniformType_Vec3;
    value->v3 = v3;
}

inline void SetShaderUniformValueTexture(ShaderUniformValue *value, u32 id, u32 texture)
{
    value->id = id;
    value->type = UniformType_Texture;
    value->texture = texture;
}

inline void SetShaderUniformValueBuffer(ShaderUniformValue *value, u32 id, u32 buffer)
{
    value->id = id;
    value->type = UniformType_Buffer;
    value->buffer = buffer;
}

inline void SetShaderUniformValueRenderTarget(ShaderUniformValue *value, u32 id, u32 renderTarget)
{
    value->id = id;
    value->type = UniformType_RenderTarget;
    value->renderTarget = renderTarget;
}

inline void SetShaderUniformValueF32(ShaderUniformValue *value, u32 id, f32 f)
{
    value->id = id;
    value->type = UniformType_F32;
    value->f = f;
}

inline ShaderUniformValue* AllocateShaderUniformValue(RenderCommand *command, u32 id, u32 type)
{
    Assert(command->drawMesh.uniformValueCount <
           ArrayCount(command->drawMesh.uniformValues));

    ShaderUniformValue *value =
        command->drawMesh.uniformValues + command->drawMesh.uniformValueCount++;

    value->id = id;
    value->type = type;

    return value;
}

// RenderCommand_DrawMesh Uniform Management
inline ShaderUniformValue *AddShaderUniformValueMat4(
    RenderCommand_DrawMesh *command, u32 id, mat4 m4)
{
    ShaderUniformValue *result = AllocateShaderUniformValue(command);
    SetShaderUniformValueMat4(result, id, m4);

    return result;
}

inline ShaderUniformValue *AddShaderUniformValueVec4(
    RenderCommand_DrawMesh *command, u32 id, vec4 value)
{
    ShaderUniformValue *result = AllocateShaderUniformValue(command);
    SetShaderUniformValueVec4(result, id, value);

    return result;
}

inline ShaderUniformValue *AddShaderUniformValueVec3(
    RenderCommand_DrawMesh *command, u32 id, vec3 value)
{
    ShaderUniformValue *result = AllocateShaderUniformValue(command);
    SetShaderUniformValueVec3(result, id, value);

    return result;
}

inline ShaderUniformValue *AddShaderUniformValueTexture(
    RenderCommand_DrawMesh *command, u32 id, u32 value)
{
    ShaderUniformValue *result = AllocateShaderUniformValue(command);
    SetShaderUniformValueTexture(result, id, value);

    return result;
}

inline ShaderUniformValue *AddShaderUniformValueBuffer(
    RenderCommand_DrawMesh *command, u32 id, u32 value)
{
    ShaderUniformValue *result = AllocateShaderUniformValue(command);
    SetShaderUniformValueBuffer(result, id, value);

    return result;
}

inline ShaderUniformValue *AddShaderUniformValueRenderTarget(
    RenderCommand_DrawMesh *command, u32 id, u32 value)
{
    ShaderUniformValue *result = AllocateShaderUniformValue(command);
    SetShaderUniformValueRenderTarget(result, id, value);

    return result;
}

inline ShaderUniformValue *AddShaderUniformValueF32(
    RenderCommand_DrawMesh *command, u32 id, f32 value)
{
    ShaderUniformValue *result = AllocateShaderUniformValue(command);
    SetShaderUniformValueF32(result, id, value);

    return result;
}

// RenderCommand_DrawVertexBuffer Uniform Management
inline ShaderUniformValue *AddShaderUniformValueMat4(
    RenderCommand_DrawVertexBuffer *command, u32 id, mat4 m4)
{
    ShaderUniformValue *result = AllocateShaderUniformValue(command);
    SetShaderUniformValueMat4(result, id, m4);

    return result;
}

inline ShaderUniformValue *AddShaderUniformValueVec4(
    RenderCommand_DrawVertexBuffer *command, u32 id, vec4 value)
{
    ShaderUniformValue *result = AllocateShaderUniformValue(command);
    SetShaderUniformValueVec4(result, id, value);

    return result;
}

inline ShaderUniformValue *AddShaderUniformValueVec3(
    RenderCommand_DrawVertexBuffer *command, u32 id, vec3 value)
{
    ShaderUniformValue *result = AllocateShaderUniformValue(command);
    SetShaderUniformValueVec3(result, id, value);

    return result;
}

inline ShaderUniformValue *AddShaderUniformValueTexture(
    RenderCommand_DrawVertexBuffer *command, u32 id, u32 value)
{
    ShaderUniformValue *result = AllocateShaderUniformValue(command);
    SetShaderUniformValueTexture(result, id, value);

    return result;
}

inline ShaderUniformValue *AddShaderUniformValueBuffer(
    RenderCommand_DrawVertexBuffer *command, u32 id, u32 value)
{
    ShaderUniformValue *result = AllocateShaderUniformValue(command);
    SetShaderUniformValueBuffer(result, id, value);

    return result;
}

inline ShaderUniformValue *AddShaderUniformValueRenderTarget(
    RenderCommand_DrawVertexBuffer *command, u32 id, u32 value)
{
    ShaderUniformValue *result = AllocateShaderUniformValue(command);
    SetShaderUniformValueRenderTarget(result, id, value);

    return result;
}

inline ShaderUniformValue *AddShaderUniformValueF32(
    RenderCommand_DrawVertexBuffer *command, u32 id, f32 value)
{
    ShaderUniformValue *result = AllocateShaderUniformValue(command);
    SetShaderUniformValueF32(result, id, value);

    return result;
}

struct GamePacketHeader
{
    u64 timestamp;
    u64 lastReceivedTimestamp;
};

struct GamePacket
{
    u32 clientId;
    u32 length;
    GamePacketHeader header;
    u8 data[2048];
};

struct GameMemory
{
    u64 persistentStorageSize;
    void *persistentStorage;

    u64 temporaryStorageSize;
    void *temporaryStorage;

    DebugReadEntireFileFunction *readEntireFile;
    DebugWriteEntireFileFunction *writeEntireFile;
    DebugFreeFileMemoryFunction *freeFileMemory;

    DebugLogMessageFunction *logMessage;

    DebugSetCursorModeFunction *setCursorMode;

    DebugStartServerFunction *startServer;
    DebugStartClientFunction *startClient;

    DebugGetPlatformDataFunction *getPlatformData;
    DebugSetPlatformDataFunction *setPlatformData;

    RenderCommand *renderCommands;
    u32 renderCommandsCount;
    u32 maxRenderCommands;

    u32 frameBufferWidth;
    u32 frameBufferHeight;

    b32 wasCodeReloaded;

    GamePacket incomingPackets[4];
    u32 incomingPacketCount;

    GamePacket outgoingPackets[4];
    u32 outgoingPacketCount;

    u8 reliableOutgoingData[1024];
    u32 reliableOutgoingDataLength;

    u8 reliableIncomingData[1024];
    u32 reliableIncomingDataLength;

    f32 frameTime;
    f32 networkLatency;

    DebugEventTable *debugEventTable;
};

inline void* AllocateRenderCommand_(GameMemory *memory, u32 type)
{
    Assert(memory->renderCommandsCount < memory->maxRenderCommands);
    RenderCommand *command =
        memory->renderCommands + memory->renderCommandsCount++;
    command->type = type;

    switch(type)
    {
    case RenderCommand_CreateMeshTypeId:
        return &command->createMesh;
    case RenderCommand_CreateInstancedMeshTypeId:
        return &command->createInstancedMesh;
    case RenderCommand_CreateShaderTypeId:
        return &command->createShader;
    case RenderCommand_CreateTextureTypeId:
        return &command->createTexture;
    case RenderCommand_DrawMeshTypeId:
        return &command->drawMesh;
    case RenderCommand_DrawInstancedMeshTypeId:
        return &command->drawInstancedMesh;
    case RenderCommand_CreateUniformBufferTypeId:
        return &command->createUniformBuffer;
    case RenderCommand_UpdateUniformBufferTypeId:
        return &command->updateUniformBuffer;
    case RenderCommand_CreateRenderTargetTypeId:
        return &command->createRenderTarget;
    case RenderCommand_BindRenderTargetTypeId:
        return &command->bindRenderTarget;
    case RenderCommand_ClearTypeId:
        return &command->clear;
    case RenderCommand_CreateCubeMapTypeId:
        return &command->createCubeMap;
    case RenderCommand_CreateVertexBufferTypeId:
        return &command->createVertexBuffer;
    case RenderCommand_DrawVertexBufferTypeId:
        return &command->drawVertexBuffer;
    case RenderCommand_UpdateVertexBufferTypeId:
        return &command->updateVertexBuffer;
    default:
        InvalidCodePath();
        return NULL;
    }
}

#define AllocateRenderCommand(MEMORY, TYPE) \
    (TYPE *)AllocateRenderCommand_(MEMORY, TYPE##TypeId)



#define GAME_UPDATE(NAME) void NAME(GameMemory *memory, GameInput *input, f32 dt)
typedef GAME_UPDATE(GameUpdateFunction);

#define GAME_SERVER_UPDATE(NAME) void NAME(GameMemory *memory, f32 dt)
typedef GAME_SERVER_UPDATE(GameServerUpdateFunction);

#define GAME_GET_SOUND_SAMPLES(NAME)                                           \
    void NAME(GameMemory *memory, GameSoundOutputBuffer *soundOutputBuffer)
typedef GAME_GET_SOUND_SAMPLES(GameGetSoundSamplesFunction);

struct GameCode
{
    GameUpdateFunction *update;
    GameServerUpdateFunction *serverUpdate;
    GameGetSoundSamplesFunction *getSoundSamples;
};

struct BoundingBox
{
    vec3 min;
    vec3 max;
};

enum
{
    DataType_None,
    DataType_U32,
    DataType_F32,
    DataType_Vec3,
    DataType_Quat,
    DataType_Aabb,
    DataType_Mat4,
    DataType_Vec2,
    MAX_DATA_TYPES,
};

inline u32 GetDataTypeSize(u32 dataType)
{
    u32 dataTypeSizes[MAX_DATA_TYPES] = {
        0,
        sizeof(u32),
        sizeof(f32),
        sizeof(vec3),
        sizeof(quat),
        sizeof(BoundingBox),
        sizeof(mat4),
        sizeof(vec2),
    };

    Assert(dataType < ArrayCount(dataTypeSizes));
    return dataTypeSizes[dataType];
}

inline const char *GetDataTypeName(u32 dataType)
{
    const char* dataTypeNames[MAX_DATA_TYPES] = {
        "None",
        "U32",
        "F32",
        "Vec3",
        "Quat",
        "AABB",
        "Mat4",
        "Vec2",
    };

    Assert(dataType < ArrayCount(dataTypeNames));
    return dataTypeNames[dataType];
}

enum
{
    EntityPrefab_BoxGeometry,
    EntityPrefab_StorageBox,
    EntityPrefab_StorageBoxPreview,
    EntityPrefab_Camera,
    EntityPrefab_Player,
    EntityPrefab_ActiveDebugCamera,
    EntityPrefab_PointLight,
    EntityPrefab_DirectionalLight,
    EntityPrefab_AmbientLight,
    EntityPrefab_PlayerSpawnPoint,
    EntityPrefab_Enemy,
    EntityPrefab_Skybox,
    EntityPrefab_EnemySpawnPoint,
    EntityPrefab_Bullet,
    EntityPrefab_Pistol,
    EntityPrefab_FoundationPreview,
    EntityPrefab_Foundation,
    EntityPrefab_WallPreview,
    EntityPrefab_Wall,
    EntityPrefab_DoorwayPreview,
    EntityPrefab_Doorway,
    EntityPrefab_Terrain,
    EntityPrefab_CeilingPreview,
    EntityPrefab_Ceiling,
    EntityPrefab_Rock01,
    EntityPrefab_Rock02,
    EntityPrefab_Rock03,
    EntityPrefab_Rock04,
    EntityPrefab_BulletImpact,
    EntityPrefab_LootTableTest,
    MaxEntityPrefabs,
};

// FIXME: Reduce this list
enum
{
    PositionComponentId            = 0x0,
    RotationComponentId            = 0x1,
    ScaleComponentId               = 0x2,
    RenderedMeshComponentId        = 0x3,
    ShadowCasterComponentId        = 0x4,
    RenderedMaterialComponentId    = 0x5,
    ActiveCameraComponentId        = 0x6,
    LightEmitterRadiusComponentId  = 0x7,
    LightMaximumRangeComponentId   = 0x8,
    LightColorComponentId          = 0x9,
    SpotLightInnerAngleComponentId = 0xA,
    SpotLightOuterAngleComponentId = 0xB,
    DirectionalLightComponentId    = 0xC,
    AmbientLightComponentId        = 0xD,
    PointLightComponentId          = 0xE,
    SpotLightComponentId           = 0xF,
    NetEntityIdComponentId         = 0x10,
    InventoryComponentId           = 0x11,
    InventorySlotOwnerComponentId  = 0x12,
    InventorySlotIndexComponentId  = 0x13,
    ItemIdComponentId              = 0x14,
    EntityPrefabIdComponentId      = 0x15,
    InteractableComponentId        = 0x16,
    OpenContainerComponentId       = 0x17,
    BoxHalfDimensionsComponentId   = 0x18,
    BuildablePreviewComponentId    = 0x19,
    ActivePlayerComponentId        = 0x1A, // Only exists on client
    DebugCameraComponentId         = 0x1B,
    HealthComponentId              = 0x1C,
    VelocityComponentId            = 0x1D,
    EulerAnglesComponentId         = 0x1E,
    PlayerSpawnPointComponentId    = 0x1F,
    OnGroundComponentId            = 0x20,
    SkyboxComponentId              = 0x21,
    AiBrainComponentId             = 0x22,
    EnemySpawnPointComponentId     = 0x23,
    RadiusComponentId              = 0x24,
    CapsuleEndPointAComponentId    = 0x25,
    CapsuleEndPointBComponentId    = 0x26,
    TriangleMeshComponentId        = 0x27,
    CollisionShapeComponentId      = 0x28,
    ActionBarActiveSlotComponentId = 0x29,
    BulletComponentId              = 0x2A,
    ProjectileOwnerComponentId     = 0x2B,
    ItemQuantityComponentId        = 0x2C,
    DeployableIdComponentId        = 0x2D,
    TerrainComponentId             = 0x2E,
    IsVisibleComponentId           = 0x2F,
    ParticleSystemComponentId      = 0x30,
    TimeToLiveComponentId          = 0x31,
    LootTableComponentId           = 0x32,
    StaticComponentId              = 0x33,
    BroadPhaseAabbComponentId      = 0x34,
    AiTargetComponentId            = 0x35,
    TimeUntilNextAttackComponentId = 0x36,
    WorldTransformComponentId      = 0x37,
    ParentComponentId              = 0x38,
    LocalTransformComponentId      = 0x39,
    IsBoneComponentId              = 0x3A,
    IsWorldTransformDirtyComponentId = 0x3B, // Hack for now, this should be a feature of the component tables
    IsLocalTransformDirtyComponentId = 0x3C,
    BoneIdComponentId              = 0x3D,
    DrawWorldTransformComponentId  = 0x3E,
    MaxComponentsIds,
};

#define ECS_FILE_DATA_VERSION 1

enum
{
    EntityType_Player,
    EntityType_Item,
    EntityType_StorageBox,
    EntityType_Enemy,
    EntityType_Foundation,
    EntityType_Wall,
    EntityType_Doorway,
    EntityType_Ceiling,
};

struct Material
{
    u32 shader;
    b32 useTexture;
    b32 ignoreColor;
    b32 useLightingData;
    b32 useNormalMap;
    u32 texture;
    b32 useBoneTransformsBuffer;
    vec4 color;
};

#define MAX_PLAYER_HEALTH 100.0f

struct CommandRegistrationData
{
    u32 id;
    const char *string;
};

enum
{
    CommandID_Unknown,
    CommandID_Echo,
    CommandID_MemInfo,
    CommandID_ListCommands,
    CommandID_Set,
    CommandID_Print,
    CommandID_ListCvars,
    CommandID_CollisionDebugInfo,
    CommandID_CollisionTestbench,
    CommandID_RenderTest,
    CommandID_GetPlatformData,
    CommandID_Quit,
    CommandID_LoadMap,
    CommandID_ServerExec,
    CommandID_EcsDumpTable,
    CommandID_EcsSet,
    CommandID_EcsGet,
    CommandID_SaveMap,
    CommandID_CollisionDebugDumpCall,
    CommandID_AddItem,
    CommandID_DumpClientData,
    CommandID_ListComponents,
    CommandID_PlaySound,
    CommandID_Connect,
    CommandID_ListPrefabs,
    CommandID_SpawnEntityAtPosition,
};

// clang-format off
global CommandRegistrationData g_CommandRegistrationData[]{
    {CommandID_Echo,                        "echo"                           },
    {CommandID_MemInfo,                     "meminfo"                        },
    {CommandID_ListCommands,                "list_commands"                  },
    {CommandID_Set,                         "set"                            },
    {CommandID_Print,                       "print"                          },
    {CommandID_ListCvars,                   "list_cvars"                     },
    {CommandID_CollisionDebugInfo,          "collision_debug_info"           },
    {CommandID_CollisionTestbench,          "collision_testbench"            },
    {CommandID_RenderTest,                  "render_test"                    },
    {CommandID_GetPlatformData,             "get_platform_data"              },
    {CommandID_Quit,                        "quit"                           },
    {CommandID_LoadMap,                     "load_map"                       },
    {CommandID_ServerExec,                  "server_exec"                    },
    {CommandID_EcsDumpTable,                "ecs_dump_table"                 },
    {CommandID_EcsSet,                      "ecs_set"                        },
    {CommandID_EcsGet,                      "ecs_get"                        },
    {CommandID_SaveMap,                     "save_map"                       },
    {CommandID_CollisionDebugDumpCall,      "collision_debug_dump_call"      },
    {CommandID_AddItem,                     "add_item"                       },
    {CommandID_DumpClientData,              "dump_client_data"               },
    {CommandID_ListComponents,              "list_components"                },
    {CommandID_PlaySound,                   "play_sound"                     },
    {CommandID_Connect,                     "connect"                        },
    {CommandID_ListPrefabs,                 "list_prefabs"                   },
    {CommandID_SpawnEntityAtPosition,       "spawn_entity_at_position"       },
};
// clang-format on

struct CVarValue
{
    u32 dataType;
    union
    {
        u32 u;
        f32 f;
    };
};

struct CVarRegistrationData
{
    u32 key;
    const char *name;
    CVarValue value;
};

enum
{
    DrawCollisionShapeFlag_None = 0x0,
    DrawCollisionShapeFlag_Boxes = 0x1,
    DrawCollisionShapeFlag_Spheres = 0x2,
    DrawCollisionShapeFlag_Capsule = 0x4,
    DrawCollisionShapeFlag_TriangleMeshes = 0x8,
    DrawCollisionShapeFlag_HeightMap = 0x10,
    DrawCollisionShapeFlag_All = 0xFF,
};

#define CVAR(NAME) CVarID_##NAME
enum
{
    CVAR(show_fps),
    CVAR(vsync),
    CVAR(max_fps),
    CVAR(window_mode),
    CVAR(framebuffer_width),
    CVAR(framebuffer_height),
    CVAR(collision_debug_selected_call),
    CVAR(r_draw_grid),
    CVAR(r_draw_entity_positions),
    CVAR(r_draw_skybox),
    CVAR(r_draw_meshes),
    CVAR(show_console),
    CVAR(collision_recording_enabled),
    CVAR(collision_debug_enabled),
    CVAR(r_draw_collision_shapes),
    CVAR(r_draw_broadphase),
    CVAR(r_draw_particles),
    CVAR(show_debug_counters),
    CVAR(r_draw_shadows),
    CVAR(r_draw_terrain),
    CVAR(show_ping),
    CVAR(enable_networking),
    CVAR(enable_ai),
    CVAR(r_draw_origin),
};

// clang-format off
#define E(NAME, DATATYPE, VALUE) \
{ CVAR(NAME), #NAME, { DATATYPE, VALUE} }

// FIXME: Two sources of truth on application startup, should use these
// defaults on startup rather than the values specified in platform layer.
// NOTE: These defaults are overwritten on startup by whatever the platform
// layer specifies as the frame buffer dimensions
CVarRegistrationData g_CVarRegistrationData[] = {
    E(show_fps,                       DataType_U32,  1                             ),
    E(vsync,                          DataType_U32,  false                         ),
    E(max_fps,                        DataType_U32,  145                           ),
    E(window_mode,                    DataType_U32,  WindowMode_Windowed ),
    E(framebuffer_width,              DataType_U32,  1920                          ),
    E(framebuffer_height,             DataType_U32,  1080                           ),
    E(collision_debug_selected_call,  DataType_U32,  0                             ),
    E(r_draw_grid,                    DataType_U32,  false                         ),
    E(r_draw_entity_positions,        DataType_U32,  false                         ),
    E(r_draw_skybox,                  DataType_U32,  true                          ),
    E(r_draw_meshes,                  DataType_U32,  true                          ),
    E(show_console,                   DataType_U32,  false                         ),
    E(collision_recording_enabled,    DataType_U32,  false                         ),
    E(collision_debug_enabled,        DataType_U32,  false                         ),
    E(r_draw_collision_shapes,        DataType_U32,  DrawCollisionShapeFlag_None   ),
    E(r_draw_broadphase,              DataType_U32,  false                         ),
    E(r_draw_particles,               DataType_U32,  true                          ),
    E(show_debug_counters,            DataType_U32,  false                         ),
    E(r_draw_shadows,                 DataType_U32,  true                          ),
    E(r_draw_terrain,                 DataType_U32,  true                          ),
    E(show_ping,                      DataType_U32,  true                          ),
    E(enable_networking,              DataType_U32,  false                         ),
    E(enable_ai,                      DataType_U32,  false                         ),
    E(r_draw_origin,                  DataType_U32,  false                         ),
};
// clang-format on
#undef E

struct TriangleMesh
{
    vec3 *vertices;
    u32 count;
};

enum
{
    TriangleMesh_Rock01,
    TriangleMesh_Rock02,
    TriangleMesh_Rock03,
    TriangleMesh_Rock04,
    TriangleMesh_Shack01,
    TriangleMesh_Warehouse,
    TriangleMesh_Cube,
    TriangleMesh_Foundation,
    TriangleMesh_Wall,
    TriangleMesh_Doorway,
    MAX_TRIANGLE_MESHES,
};

enum
{
    CollisionShape_Box,
    CollisionShape_Sphere,
    CollisionShape_Capsule,
    CollisionShape_TriangleMesh,
    CollisionShape_HeightMap,
    MAX_COLLISION_SHAPE_TYPES,
};

enum
{
    AudioClip_Test,
    AudioClip_Test2,
    AudioClip_Pistol,
    AudioClip_Ambience,
    MAX_AUDIO_CLIPS,
};

#define MAX_PLAYING_SOUNDS 64

enum
{
    ItemID_None = 0x0,
    ItemID_Pistol = 0x1,
    ItemID_PistolAmmo = 0x2,
    ItemID_Hatchet = 0x3,
    ItemID_StorageBox = 0x4,
    ItemID_Foundation = 0x5,
    ItemID_Wall = 0x6,
    ItemID_Doorway = 0x7,
    ItemID_Ceiling = 0x8,
};

enum
{
    DeployableID_None = 0x0,
    DeployableID_StorageBox = 0x1,
    DeployableID_Foundation = 0x2,
    DeployableID_Wall = 0x3,
    DeployableID_Doorway = 0x4,
    DeployableID_Ceiling = 0x5,
};

#define PLAYER_HEIGHT 1.8f
#define PLAYER_CAMERA_OFFSET Vec3(0.0f, PLAYER_HEIGHT * 0.5f, 0.0f)
#define MAX_PLAYERS 32
#define MAX_ITEMS 64
#define INVENTORY_SLOT_COUNT 30
#define ACTION_BAR_SLOT_COUNT 6

#define MAX_NET_ENTITIES 0x1000
#define MAX_PARTICLE_SYSTEMS 64

struct AudioData
{
    i16 *samples;
    u32 sampleCount;
    u32 channelCount;
    u32 samplesPerSecond;
};

struct HeightMap
{
    f32 *values;
    u32 width;
    u32 height;
};

struct TerrainLayerMap
{
    u8 *values;
    u32 width;
    u32 height;
};

struct TerrainNormalMap
{
    vec3 *values;
    u32 width;
    u32 height;
};

struct QuadTreeNode
{
    b32 isLeaf;
    BoundingBox aabb;
    u32 row;
    u32 col;
    QuadTreeNode *children[4];
};

struct QuadTree
{
    QuadTreeNode *nodes;
    u32 maxNodes;
    u32 nodeCount;
    u32 samplesPerNode;
    QuadTreeNode *root;
};

struct LootTableEntry
{
    u32 itemId;
    u32 quantity;
    f32 probability;
};

struct LootTable
{
    LootTableEntry entries[32];
    u32 count;
};

enum
{
    LootTable_Test,
    LootTable_Food,
    LootTable_Weapons,
    MAX_LOOT_TABLES,
};

struct BoneTransform
{
    quat rotation;
    vec3 position;
    vec3 scale;
};

struct AnimationClip
{
    BoneTransform boneTransforms[24]; // boneCount = 3, frameCount = 8
    u32 boneCount;
    u32 sampleCount;
    f32 samplesPerSecond;
};
#define MAX_ANIMATION_CLIPS 1

struct Skeleton
{
    u32 boneCount;
    u8 parents[255];
    BoneTransform bindPose[255];
    mat4 invBindPose[255];
};

#define MAX_SKELETONS 1
#define INVALID_BONE_IDX 0xFF

struct GameAssets
{
    AudioData audioClips[MAX_AUDIO_CLIPS];
    TriangleMesh triangleMeshes[MAX_TRIANGLE_MESHES];
    BoundingBox triangleMeshBoundingBoxes[MAX_TRIANGLE_MESHES];

    QuadTree heightMapQuadTree;
    QuadTree renderQuadTree;

    HeightMap heightMap;
    TerrainLayerMap layerMap;
    TerrainNormalMap normalMap;

    MemoryArena meshDataArena;
    MemoryArena audioDataArena;
    MemoryArena terrainArena;
    MemoryArena terrainCollisionArena;
    MemoryArena modelDataArena;

    LootTable lootTables[MAX_LOOT_TABLES];

    AnimationClip animationClips[MAX_ANIMATION_CLIPS];
    Skeleton skeletons[MAX_SKELETONS];
};

enum
{
    ParticleSystem_SmokeImpact,
    ParticleSystem_Sparks,
};
