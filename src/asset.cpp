// TODO: Look at replacing this with our own PNG loading code
#define STB_NO_STDIO
#define STBI_ONLY_PNG
#define STBI_NO_SIMD
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

// TODO: Look at replacing this with our own Glyph rendering
#if !defined(STB_RECT_PACK_IMPLEMENTATION)
#define STB_RECT_PACK_IMPLEMENTATION
#include <stb_rect_pack.h>
#endif

#if !defined(STB_TRUETYPE_IMPLEMENTATION)
#define STB_TRUETYPE_IMPLEMENTATION
#include <stb_truetype.h>
#endif

struct MeshData
{
    Vertex *vertices;
    u32 *indices;
    u32 vertexCount;
    u32 indexCount;
};

inline vec3 CalculateBoundingBoxForMesh(MeshData meshData, mat4 transform)
{
    vec3 min = {};
    vec3 max = {};
    for (u32 i = 0; i < meshData.vertexCount; ++i)
    {
        vec3 v = meshData.vertices[i].position;
        v = (transform * Vec4(v, 1.0)).xyz;

        min = Min(min, v);
        max = Max(max, v);
    }

    vec3 result = (max - min) * 0.5f;
    return result;
}

internal void CalculateTangents(MeshData meshData)
{
    for (u32 triangleIndex = 0; triangleIndex < meshData.indexCount / 3;
         ++triangleIndex)
    {
        u32 i = meshData.indices[triangleIndex * 3];
        u32 j = meshData.indices[triangleIndex * 3 + 1];
        u32 k = meshData.indices[triangleIndex * 3 + 2];

        Vertex *a = meshData.vertices + i;
        Vertex *b = meshData.vertices + j;
        Vertex *c = meshData.vertices + k;

        vec3 deltaP1 = b->position - a->position;
        vec3 deltaP2 = c->position - a->position;

        vec2 deltaUV1 = b->texCoord - a->texCoord;
        vec2 deltaUV2 = c->texCoord - a->texCoord;

        // Calculate inverse of 2x2 matrix
        f32 r = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV1.y * deltaUV2.x);
        vec3 tangent = (deltaP1 * deltaUV2.y - deltaP2 * deltaUV1.y) * r;

        a->tangent += tangent;
        b->tangent += tangent;
        c->tangent += tangent;
    }

    for (u32 vertexIdx = 0; vertexIdx < meshData.vertexCount; ++vertexIdx)
    {
        meshData.vertices[vertexIdx].tangent =
            SafeNormalize(meshData.vertices[vertexIdx].tangent);
    }
}

internal b32 LoadTexture(void *fileData, u32 length, GameMemory *memory,
    u32 textureId, MemoryArena *tempArena, b32 flipYAxis = true)
{
    TIMED_BLOCK();
    b32 result = false;
    i32 width, height, channels;
    stbi_set_flip_vertically_on_load(flipYAxis);

    u8 *pixels = stbi_load_from_memory(
        (const u8 *)fileData, length, &width, &height, &channels, STBI_rgb_alpha);
    if (pixels != NULL)
    {
        // FIXME: Not all textures are 32BPP 
        u32 bufferSize = width * height * 4;
        void *tempBuffer = MemoryArena_Allocate(tempArena, bufferSize);
        memcpy(tempBuffer, pixels, bufferSize);

        stbi_image_free(pixels);

        RenderCommand_CreateTexture *createTexture =
            AllocateRenderCommand(memory, RenderCommand_CreateTexture);
        createTexture->pixels = tempBuffer;
        createTexture->width = width;
        createTexture->height = height;
        createTexture->id = textureId;
        createTexture->colorFormat = ColorFormat_RGBA8;
        createTexture->samplerMode = TextureSamplerMode_LinearMipMapping;

        // NOTE: Memory is not freed, must use temp allocator

        result = true;
    }

    return result;
}

internal void LoadTextureFromFile(const char *path,
    GameMemory *memory, u32 textureId, MemoryArena *tempArena, b32 flipYAxis = true)
{
    DebugReadFileResult textureFile = memory->readEntireFile(path);
    Assert(textureFile.contents != NULL);
    b32 success = LoadTexture(
        textureFile.contents, textureFile.size, memory, textureId, tempArena, flipYAxis);
    Assert(success);
    memory->freeFileMemory(textureFile);
}

internal void CopySubImage(u8 *subImageData, const u8 *imageData,
    u32 subImageWidth, u32 subImageHeight, u32 numChannels, u32 xStart,
    u32 yStart, u32 imagePitch)
{
    u32 subImagePitch = subImageWidth * numChannels;
    for (u32 y = yStart; y < yStart + subImageHeight; ++y)
    {
        u32 start = y * imagePitch + xStart * numChannels;
        CopyMemory(subImageData + (y - yStart) * subImagePitch,
            imageData + start, subImagePitch);
    }
}

internal b32 LoadCubeMap(const void *fileData, u32 length, GameMemory *memory,
    u32 textureId, MemoryArena *tempArena)
{
    TIMED_BLOCK();
    b32 result = false;
    i32 width, height, channels;
    stbi_set_flip_vertically_on_load(false);
    u8 *pixels = stbi_load_from_memory(
        (const u8 *)fileData, length, &width, &height, &channels, STBI_rgb_alpha);
    if (pixels != NULL)
    {
        u32 bytesPerPixel = 4;
        u32 faceWidth = width / 4;
        Assert(height / faceWidth == 3);
        u32 pitch = width * bytesPerPixel;
        u32 facePitch = faceWidth * bytesPerPixel;

        u32 faceSize = faceWidth * faceWidth * bytesPerPixel;
        u8 *faces[CUBE_MAP_MAX_FACES];

        for (u32 i = 0; i < CUBE_MAP_MAX_FACES; ++i)
        {
            faces[i] = (u8 *)MemoryArena_Allocate(tempArena, faceSize);
        }

        // +X
        CopySubImage(faces[CUBE_MAP_FACE_X_POS], pixels, faceWidth, faceWidth,
            bytesPerPixel, faceWidth * 2, faceWidth, pitch);

        // -X
        CopySubImage(faces[CUBE_MAP_FACE_X_NEG], pixels, faceWidth, faceWidth,
            bytesPerPixel, 0, faceWidth, pitch);

        // +Y
        CopySubImage(faces[CUBE_MAP_FACE_Y_POS], pixels, faceWidth, faceWidth,
            bytesPerPixel, faceWidth, 0, pitch);

        // -Y
        CopySubImage(faces[CUBE_MAP_FACE_Y_NEG], pixels, faceWidth, faceWidth,
            bytesPerPixel, faceWidth, faceWidth * 2, pitch);

        // +Z
        CopySubImage(faces[CUBE_MAP_FACE_Z_POS], pixels, faceWidth, faceWidth,
            bytesPerPixel, faceWidth, faceWidth, pitch);

        // -Z
        CopySubImage(faces[CUBE_MAP_FACE_Z_NEG], pixels, faceWidth, faceWidth,
            bytesPerPixel, faceWidth * 3, faceWidth, pitch);


        // Fill out render command
        RenderCommand_CreateCubeMap* createCubeMap =
            AllocateRenderCommand(memory, RenderCommand_CreateCubeMap);
        createCubeMap->id = textureId;
        for (u32 i = 0; i < CUBE_MAP_MAX_FACES; ++i)
        {
            createCubeMap->pixels[i] = faces[i];
        }
        createCubeMap->width = faceWidth;
        createCubeMap->colorFormat = ColorFormat_RGBA8;

        // NOTE: Memory is not freed, must use temp allocator

        result = true;
    }

    return result;
}

internal void LoadCubeMapFromFile(
    const char *path, GameMemory *memory, u32 textureId, MemoryArena *tempArena)
{
    DebugReadFileResult skyboxFile = memory->readEntireFile(path);
    Assert(skyboxFile.contents != NULL);
    b32 success = LoadCubeMap(
        skyboxFile.contents, skyboxFile.size, memory, textureId, tempArena);
    Assert(success);
    memory->freeFileMemory(skyboxFile);
}

// TODO: Replace atoi and atof
// TODO: Optimize vertex and index lists, remove duplicate vertices
internal MeshData LoadMeshData(char *fileData, u32 length,
    MemoryArena *meshDataArena, MemoryArena *tempArena)
{
    u32 maxVertices = 0xFFFF;
    vec3 *positions = AllocateArray(tempArena, vec3, maxVertices);
    vec3 *normals = AllocateArray(tempArena, vec3, maxVertices);
    vec2 *texCoords = AllocateArray(tempArena, vec2, maxVertices);
    u32 positionCount = 0;
    u32 normalCount = 0;
    u32 texCoordCount = 0;

    Vertex *vertices = AllocateArray(tempArena, Vertex, maxVertices);
    u32 vertexCount = 0;

    u32 maxIndices = 0xFFFF;
    u32 *indices = AllocateArray(tempArena, u32, maxIndices);
    u32 indexCount = 0;

    b32 useTexCoords = false;

    u32 cursor = 0;
    while (cursor < length)
    {
        // Consume whitespace
        while(IsWhitespace(fileData[cursor]))
        {
            // Remove any preceeding whitespace
            cursor++;
        }

        // Skip comments and objects for now
        if (fileData[cursor] == '#' ||
            fileData[cursor] == 'o')
        {
            while (fileData[cursor++] != '\n')
            {
                // Skip entire line
            }
            continue;
        }

        if (fileData[cursor] == 'v')
        {
            // Handle vertex
            cursor++;
            if (fileData[cursor] == ' ')
            {
                // vertex position

                Assert(positionCount < maxVertices);
                vec3 *position = positions + positionCount++;

                // Parse 3 floats
                // Consume non-numeric characters
                while (!IsNumeric(fileData[cursor]))
                {
                    cursor++;
                }

                position->x = (float)atof(fileData + cursor);
                while (IsNumeric(fileData[cursor]))
                {
                    // Skip over x value
                    cursor++;
                }

                while (IsWhitespace(fileData[cursor]))
                {
                    // Skip over whitespace
                    cursor++;
                }

                position->y = (float)atof(fileData + cursor);
                while (IsNumeric(fileData[cursor]))
                {
                    // Skip over y value
                    cursor++;
                }

                while (IsWhitespace(fileData[cursor]))
                {
                    // Skip over whitespace
                    cursor++;
                }

                position->z = (float)atof(fileData + cursor);
                while (IsNumeric(fileData[cursor]))
                {
                    // Skip over z value
                    cursor++;
                }
            }
            else if (fileData[cursor] == 'n')
            {
                // vertex normal
                Assert(normalCount < maxVertices);
                vec3 *normal = normals + normalCount++;

                // Parse 3 floats
                // Consume non-numeric characters
                while (!IsNumeric(fileData[cursor]))
                {
                    cursor++;
                }

                normal->x = (float)atof(fileData + cursor);
                while (IsNumeric(fileData[cursor]))
                {
                    // Skip over x value
                    cursor++;
                }

                while (IsWhitespace(fileData[cursor]))
                {
                    // Skip over whitespace
                    cursor++;
                }

                normal->y = (float)atof(fileData + cursor);
                while (IsNumeric(fileData[cursor]))
                {
                    // Skip over y value
                    cursor++;
                }

                while (IsWhitespace(fileData[cursor]))
                {
                    // Skip over whitespace
                    cursor++;
                }

                normal->z = (float)atof(fileData + cursor);
                while (IsNumeric(fileData[cursor]))
                {
                    // Skip over z value
                    cursor++;
                }
            }
            else if (fileData[cursor] == 't')
            {
                // vertex texture coordinates
                useTexCoords = true;

                Assert(texCoordCount < maxVertices);
                vec2 *texCoord = texCoords + texCoordCount++;

                // Parse 2 floats
                // Consume non-numeric characters
                while (!IsNumeric(fileData[cursor]))
                {
                    cursor++;
                }

                texCoord->x = (float)atof(fileData + cursor);
                while (IsNumeric(fileData[cursor]))
                {
                    // Skip over x value
                    cursor++;
                }

                while (IsWhitespace(fileData[cursor]))
                {
                    // Skip over whitespace
                    cursor++;
                }

                texCoord->y = (float)atof(fileData + cursor);
                while (IsNumeric(fileData[cursor]))
                {
                    // Skip over y value
                    cursor++;
                }
            }
            else
            {
                // ignore other vertex data for now
            }
        }
        else if (fileData[cursor] == 'f')
        {
            cursor++;
            while (IsWhitespace(fileData[cursor]))
            {
                // Skip over whitespace
                cursor++;
            }
            
            for (u32 index = 0; index < 3; ++index)
            {
                while (!IsNumeric(fileData[cursor]))
                {
                    cursor++;
                }

                u32 positionIndex = (u32)atoi(fileData + cursor);
                while (IsNumeric(fileData[cursor]))
                {
                    // skip over position index
                    cursor++;
                }

                if (fileData[cursor] == '/')
                {
                    cursor++;
                }

                u32 texCoordIndex = 0;
                if (useTexCoords)
                {
                    texCoordIndex = (u32)atoi(fileData + cursor);
                    while (IsNumeric(fileData[cursor]))
                    {
                        // skip over texCoord index
                        cursor++;
                    }
                }

                if (fileData[cursor] == '/')
                {
                    cursor++;
                }

                u32 normalIndex = (u32)atoi(fileData + cursor);
                while (IsNumeric(fileData[cursor]))
                {
                    // skip over normal index
                    cursor++;
                }

                // Convert to 0 base index
                Assert(positionIndex > 0);
                Assert(normalIndex > 0);

                positionIndex -= 1;
                normalIndex -= 1;

                Assert(vertexCount < maxVertices);
                Vertex *vertex = vertices + vertexCount;

                Assert(positionIndex < maxVertices);
                vertex->position = positions[positionIndex];

                if (useTexCoords)
                {
                    Assert(texCoordIndex > 0);
                    texCoordIndex -= 1;
                    Assert(texCoordIndex < maxVertices);
                    vertex->texCoord = texCoords[texCoordIndex];
                }


                Assert(normalIndex < maxVertices);
                vertex->normal = normals[normalIndex];




                Assert(indexCount < maxIndices);
                indices[indexCount++] = vertexCount++;
            }

            Assert(indexCount % 3 == 0);
        }
        else if (fileData[cursor] == 's')
        {
            // Skip to next line
            while (fileData[cursor] != '\n')
            {
                cursor++;
            }
        }
        else
        {
            // Unknown syntax ignore field
            while (fileData[cursor++] != '\n')
            {
                // Skip entire line
            }
            continue;
            //return false;
        }
    }

    MeshData result = {};
    result.vertices = AllocateArray(meshDataArena, Vertex, vertexCount);
    CopyMemory(result.vertices, vertices, sizeof(Vertex) * vertexCount);
    result.indices = AllocateArray(meshDataArena, u32, indexCount);
    CopyMemory(result.indices, indices, sizeof(u32) * indexCount);
    result.vertexCount = vertexCount;
    result.indexCount = indexCount;
    return result;
}

internal void LoadMeshFromFile(const char *path, GameMemory *memory,
    u32 meshId, MemoryArena *tempArena)
{
    DebugReadFileResult fileResult = memory->readEntireFile(path);
    Assert(fileResult.contents != NULL);

    // -1 to remove nul character
    MeshData meshData = LoadMeshData((char *)fileResult.contents,
            fileResult.size - 1, tempArena, tempArena);

    Assert(meshData.vertexCount > 0);
    Assert(meshData.indexCount > 0);

    CalculateTangents(meshData);

    RenderCommand_CreateMesh *createMesh =
        AllocateRenderCommand(memory, RenderCommand_CreateMesh);
    createMesh->vertices = meshData.vertices;
    createMesh->vertexCount = meshData.vertexCount;
    createMesh->vertexLayout = VertexLayout_Standard;
    createMesh->indices = meshData.indices;
    createMesh->indexCount = meshData.indexCount;
    createMesh->id = meshId;
    memory->freeFileMemory(fileResult);
}

internal void CreateMesh(u32 meshId, MemoryArena *tempArena, Vertex *vertices,
    u32 vertexCount, u32 *indices, u32 indexCount, u32 maxInstances,
    GameMemory *memory)
{
    // Copy input vertices and indices to temporary memory that will still be
    // valid when the render command is processed.
    Vertex *vertexBuffer = AllocateArray(tempArena, Vertex, vertexCount);
    CopyMemory(vertexBuffer, vertices, sizeof(Vertex) * vertexCount);

    u32 *indexBuffer = AllocateArray(tempArena, u32, indexCount);
    CopyMemory(indexBuffer, indices, sizeof(u32) * indexCount);

    if (maxInstances == 0)
    {
        RenderCommand_CreateMesh *createMesh =
            AllocateRenderCommand(memory, RenderCommand_CreateMesh);
        createMesh->vertices = vertexBuffer;
        createMesh->vertexCount = vertexCount;
        createMesh->vertexLayout = VertexLayout_Standard;
        createMesh->indices = indexBuffer;
        createMesh->indexCount = indexCount;
        createMesh->id = meshId;
    }
    else
    {
        RenderCommand_CreateInstancedMesh *createMesh =
            AllocateRenderCommand(memory, RenderCommand_CreateInstancedMesh);
        createMesh->vertices = vertexBuffer;
        createMesh->vertexCount = vertexCount;
        createMesh->indices = indexBuffer;
        createMesh->indexCount = indexCount;
        createMesh->id = meshId;
        createMesh->maxInstanceCount = maxInstances;
    }
}

internal MeshData GeneratePatchMesh(MemoryArena *arena, u32 gridDim)
{
    MeshData result = {};

    u32 vertDim = gridDim + 1;
    u32 totalVertices = vertDim * vertDim;
    Vertex *vertices = AllocateArray(arena, Vertex, totalVertices);

    u32 totalIndices = gridDim * gridDim * 2 * 3;
    u32 *indices = AllocateArray(arena, u32, totalIndices);

    for (u32 y = 0; y < vertDim; ++y)
    {
        for (u32 x = 0; x < vertDim; ++x)
        {
            float fx = x / (float)gridDim;
            float fy = y / (float)gridDim;

            Vertex *vertex = &vertices[y * vertDim + x];
            // Map to -0.5 to 0.5 range
            vertex->position = Vec3(fx - 0.5f, 0.0f, fy - 0.5f);
            vertex->normal = Vec3(0.0f, 1.0f, 0.0f);
            vertex->texCoord = Vec2(fx, fy);
        }
    }

    u32 index = 0;
    for (u32 y = 0; y < gridDim; y++)
    {
        for (u32 x = 0; x < gridDim; x++)
        {
            indices[index++] = (x + vertDim * y);
            indices[index++] = ((x + 1) + vertDim * y);
            indices[index++] = (x + vertDim * (y + 1));

            indices[index++] = ((x + 1) + vertDim * y);
            indices[index++] = ((x + 1) + vertDim * (y + 1));
            indices[index++] = (x + vertDim * (y + 1));
        }
    }

    result.vertices = vertices;
    result.indices = indices;
    result.vertexCount = totalVertices;
    result.indexCount = totalIndices;

    CalculateTangents(result);

    return result;
}

internal MeshData GetCubeMeshData(MemoryArena *tempArena)
{
    // clang-format off
    Vertex cubeVertices[] = {
        // Top
        {{-0.5f, 0.5f, -0.5f}, {0.0f, 1.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 1.0f}},
        {{0.5f, 0.5f, -0.5f}, {0.0f, 1.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {1.0f, 1.0f}},
        {{0.5f, 0.5f, 0.5f}, {0.0f, 1.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {1.0f, 0.0f}},
        {{-0.5f, 0.5f, 0.5f}, {0.0f, 1.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f}},

        // Bottom
        {{-0.5f, -0.5f, -0.5f}, {0.0f, -1.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 1.0f}},
        {{0.5f, -0.5f, -0.5f}, {0.0f, -1.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {1.0f, 1.0f}},
        {{0.5f, -0.5f, 0.5f}, {0.0f, -1.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {1.0f, 0.0f}},
        {{-0.5f, -0.5f, 0.5f}, {0.0f, -1.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f}},

        // Back
        {{-0.5f, 0.5f, -0.5f}, {0.0f, 0.0f, -1.0f}, {0.0f, 0.0f, 0.0f}, {1.0f, 1.0f}},
        {{0.5f, 0.5f, -0.5f}, {0.0f, 0.0f, -1.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 1.0f}},
        {{0.5f, -0.5f, -0.5f}, {0.0f, 0.0f, -1.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f}},
        {{-0.5f, -0.5f, -0.5f}, {0.0f, 0.0f, -1.0f}, {0.0f, 0.0f, 0.0f}, {1.0f, 0.0f}},

        // Front
        {{-0.5f, 0.5f, 0.5f}, {0.0f, 0.0f, 1.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 1.0f}},
        {{0.5f, 0.5f, 0.5f}, {0.0f, 0.0f, 1.0f}, {0.0f, 0.0f, 0.0f}, {1.0f, 1.0f}},
        {{0.5f, -0.5f, 0.5f}, {0.0f, 0.0f, 1.0f}, {0.0f, 0.0f, 0.0f}, {1.0f, 0.0f}},
        {{-0.5f, -0.5f, 0.5f}, {0.0f, 0.0f, 1.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f}},

        // Left
        {{-0.5f, 0.5f, -0.5f}, {-1.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 1.0f}},
        {{-0.5f, -0.5f, -0.5f}, {-1.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f}},
        {{-0.5f, -0.5f, 0.5f}, {-1.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {1.0f, 0.0f}},
        {{-0.5f, 0.5f, 0.5f}, {-1.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {1.0f, 1.0f}},

        // Right
        {{0.5f, 0.5f, -0.5f}, {1.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {1.0f, 1.0f}},
        {{0.5f, -0.5f, -0.5f}, {1.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {1.0f, 0.0f}},
        {{0.5f, -0.5f, 0.5f}, {1.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f}},
        {{0.5f, 0.5f, 0.5f}, {1.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 1.0f}},
    };

    u32 cubeIndices[] = {
        2, 1, 0,
        0, 3, 2,

        4, 5, 6,
        6, 7, 4,

        8, 9, 10,
        10, 11, 8,

        14, 13, 12,
        12, 15, 14,

        16, 17, 18,
        18, 19, 16,

        22, 21, 20,
        20, 23, 22
    };
    // clang-format on

    MeshData result = {};

    result.vertices =
        AllocateArray(tempArena, Vertex, ArrayCount(cubeVertices));
    CopyMemory(result.vertices, cubeVertices,
        ArrayCount(cubeVertices) * sizeof(Vertex));
    result.indices = AllocateArray(tempArena, u32, ArrayCount(cubeIndices));
    CopyMemory(
        result.indices, cubeIndices, ArrayCount(cubeIndices) * sizeof(u32));
    result.vertexCount = ArrayCount(cubeVertices);
    result.indexCount = ArrayCount(cubeIndices);

    CalculateTangents(result);

    return result;
}

struct TriangleFace
{
    u32 i, j, k;
};

inline Vertex CreateUnitVertex(f32 x, f32 y, f32 z)
{
    Vertex result;
    result.position = Normalize(Vec3(x, y, z));
    result.normal = result.position;
    result.texCoord = Vec2(result.position.x, result.position.y);
    return result;
}

internal MeshData CreateIcosahedronMesh(u32 tesselationLevel, MemoryArena *arena)
{
    MeshData result = {};
    // FIXME: Actually calculate how many vertices and indices
    u32 maxVertices = 2048;
    u32 maxIndices = 4096;
    result.vertices = AllocateArray(arena, Vertex, maxVertices);
    result.indices = AllocateArray(arena, u32, maxIndices);

    f32 t = (1.0f + Sqrt(5.0f)) * 0.5f;

    result.vertices[0] = CreateUnitVertex(-1, t, 0);
    result.vertices[1] = CreateUnitVertex(1, t, 0);
    result.vertices[2] = CreateUnitVertex(-1, -t, 0);
    result.vertices[3] = CreateUnitVertex(1, -t, 0);

    result.vertices[4] = CreateUnitVertex(0, -1, t);
    result.vertices[5] = CreateUnitVertex(0, 1, t);
    result.vertices[6] = CreateUnitVertex(0, -1, -t);
    result.vertices[7] = CreateUnitVertex(0, 1, -t);

    result.vertices[8] = CreateUnitVertex(t, 0, -1);
    result.vertices[9] = CreateUnitVertex(t, 0, 1);
    result.vertices[10] = CreateUnitVertex(-t, 0, -1);
    result.vertices[11] = CreateUnitVertex(-t, 0, 1);

    u32 vertexCount = 12;

    u32 initialFaceCount = 20;
    TriangleFace *initialFaces =
        AllocateArray(arena, TriangleFace, initialFaceCount);

    initialFaces[0] = TriangleFace{0, 11, 5};
    initialFaces[1] = TriangleFace{0, 5, 1};
    initialFaces[2] = TriangleFace{0, 1, 7};
    initialFaces[3] = TriangleFace{0, 7, 10};
    initialFaces[4] = TriangleFace{0, 10, 11};

    initialFaces[5] = TriangleFace{1, 5, 9};
    initialFaces[6] = TriangleFace{5, 11, 4};
    initialFaces[7] = TriangleFace{11, 10, 2};
    initialFaces[8] = TriangleFace{10, 7, 6};
    initialFaces[9] = TriangleFace{7, 1, 8};

    initialFaces[10] = TriangleFace{3, 9, 4};
    initialFaces[11] = TriangleFace{3, 4, 2};
    initialFaces[12] = TriangleFace{3, 2, 6};
    initialFaces[13] = TriangleFace{3, 6, 8};
    initialFaces[14] = TriangleFace{3, 8, 9};

    initialFaces[15] = TriangleFace{4, 9, 5};
    initialFaces[16] = TriangleFace{2, 4, 11};
    initialFaces[17] = TriangleFace{6, 2, 10};
    initialFaces[18] = TriangleFace{8, 6, 7};
    initialFaces[19] = TriangleFace{9, 8, 1};

    u32 currentFaceCount = initialFaceCount;
    TriangleFace *currentFaces = initialFaces;
    for (u32 i = 0; i < tesselationLevel; ++i)
    {
        TriangleFace *newFaces =
            AllocateArray(arena, TriangleFace, currentFaceCount * 4);
        u32 newFaceCount = 0;

        for (size_t j = 0; j < currentFaceCount; ++j)
        {
            TriangleFace currentFace = currentFaces[j];

            // 3 edges in total comprised of 2 indices each
            u32 edges[6] = {currentFace.i, currentFace.j, currentFace.j,
                currentFace.k, currentFace.k, currentFace.i};

            u32 indicesAdded[3];

            for (u32 k = 0; k < 3; ++k)
            {
                // Retrieve vertices for each edge
                u32 idx0 = edges[k * 2];
                u32 idx1 = edges[k * 2 + 1];
                Vertex v0 = result.vertices[idx0];
                Vertex v1 = result.vertices[idx1];

                // Add mid point vertex
                vec3 u = v0.position + v1.position;

                Assert(vertexCount < maxVertices);
                result.vertices[vertexCount] = CreateUnitVertex(u.x, u.y, u.z);

                indicesAdded[k] = vertexCount++;
            }

            newFaces[newFaceCount++] =
                TriangleFace{currentFace.i, indicesAdded[0], indicesAdded[2]};
            newFaces[newFaceCount++] =
                TriangleFace{currentFace.j, indicesAdded[1], indicesAdded[0]};
            newFaces[newFaceCount++] =
                TriangleFace{currentFace.k, indicesAdded[2], indicesAdded[1]};
            newFaces[newFaceCount++] =
                TriangleFace{indicesAdded[0], indicesAdded[1], indicesAdded[2]};
        }

        currentFaces = newFaces;
        currentFaceCount = newFaceCount;
    }

    Assert(currentFaceCount * 3 < maxIndices);
    CopyMemory(result.indices, currentFaces, currentFaceCount * sizeof(TriangleFace));

    // Also frees the allocated faces for all tesselation levels
    //MemoryArenaFree(arena, initialFaces);

    result.vertexCount = vertexCount;
    result.indexCount = currentFaceCount * 3;

    CalculateTangents(result);

    return result;
}

internal MeshData GetQuadMeshData(MemoryArena *tempArena)
{
    // clang-format off
    Vertex quadVertices[] = {
        {{-0.5f, -0.5f, 0.0f}, {1.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f}},
        {{0.5f, -0.5f, 0.0f}, {0.0f, 1.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {1.0f, 0.0f}},
        {{0.5f, 0.5f, 0.0f}, {0.0f, 0.0f, 1.0f}, {0.0f, 0.0f, 0.0f}, {1.0f, 1.0f}},
        {{-0.5f, 0.5f, 0.0f}, {1.0f, 1.0f, 1.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 1.0f}}
    };

    u32 quadIndices[] = {
        0, 1, 2,
        2, 3, 0
    };
    // clang-format on

    MeshData result = {};

    result.vertices =
        AllocateArray(tempArena, Vertex, ArrayCount(quadVertices));
    CopyMemory(result.vertices, quadVertices,
        ArrayCount(quadVertices) * sizeof(Vertex));
    result.indices = AllocateArray(tempArena, u32, ArrayCount(quadIndices));
    CopyMemory(
        result.indices, quadIndices, ArrayCount(quadIndices) * sizeof(u32));
    result.vertexCount = ArrayCount(quadVertices);
    result.indexCount = ArrayCount(quadIndices);

    CalculateTangents(result);

    return result;
}

internal void LoadInternalMeshes(GameMemory *memory, MemoryArena *tempArena)
{
    TIMED_BLOCK();

    MeshData quadMeshData = GetQuadMeshData(tempArena);
    RenderCommand_CreateMesh *createQuadMesh =
        AllocateRenderCommand(memory, RenderCommand_CreateMesh);
    createQuadMesh->vertices = quadMeshData.vertices;
    createQuadMesh->vertexCount = quadMeshData.vertexCount;
    createQuadMesh->vertexLayout = VertexLayout_Standard;
    createQuadMesh->indices = quadMeshData.indices;
    createQuadMesh->indexCount = quadMeshData.indexCount;
    createQuadMesh->id = Mesh_Quad;

    MeshData cubeMeshData = GetCubeMeshData(tempArena);
    RenderCommand_CreateMesh *createCubeMesh =
        AllocateRenderCommand(memory, RenderCommand_CreateMesh);
    createCubeMesh->vertices = cubeMeshData.vertices;
    createCubeMesh->vertexCount = cubeMeshData.vertexCount;
    createCubeMesh->vertexLayout = VertexLayout_Standard;
    createCubeMesh->indices = cubeMeshData.indices;
    createCubeMesh->indexCount = cubeMeshData.indexCount;
    createCubeMesh->id = Mesh_Cube;

    MeshData sphereMeshData = CreateIcosahedronMesh(2, tempArena);
    RenderCommand_CreateMesh *createSphereMesh =
        AllocateRenderCommand(memory, RenderCommand_CreateMesh);
    createSphereMesh->vertices = sphereMeshData.vertices;
    createSphereMesh->vertexCount = sphereMeshData.vertexCount;
    createSphereMesh->vertexLayout = VertexLayout_Standard;
    createSphereMesh->indices = sphereMeshData.indices;
    createSphereMesh->indexCount = sphereMeshData.indexCount;
    createSphereMesh->id = Mesh_Sphere;

    //MeshData patchMeshData = GeneratePatchMesh(tempArena, TERRAIN_PATCH_GRID_DIM);
    //CreateMesh(Mesh_Patch, tempArena, patchMeshData.vertices, patchMeshData.vertexCount,
            //patchMeshData.indices, patchMeshData.indexCount, memory);

    MeshData terrainPatchMeshData = GeneratePatchMesh(tempArena, TERRAIN_VERTICES_PER_NODE);
    CreateMesh(Mesh_TerrainPatch, tempArena, terrainPatchMeshData.vertices,
        terrainPatchMeshData.vertexCount, terrainPatchMeshData.indices,
        terrainPatchMeshData.indexCount, 0, memory);
}

const char *colorOnlyShaderSource = R"FOO(
#ifdef VERTEX_SHADER
layout(location = 0) in vec3 vertexPosition;
#ifdef USE_TEXTURE
layout(location = 3) in vec2 vertexTexCoord;
#endif

uniform mat4 mvp;

#ifdef USE_TEXTURE
out vec2 fragTexCoords;
#endif

void main()
{
#ifdef USE_TEXTURE
  fragTexCoords = vertexTexCoord;
#endif
  gl_Position = mvp * vec4(vertexPosition, 1.0);
}
#endif

#ifdef FRAGMENT_SHADER

#ifdef USE_TEXTURE
uniform sampler2D albedoTexture;
in vec2 fragTexCoords;
#endif
uniform vec4 color = vec4(1.0);

out vec4 outputColor;
void main()
{
#ifdef USE_TEXTURE
#ifdef USE_TEXTURE_ALPHA
    outputColor = texture(albedoTexture, fragTexCoords).rgba;
#else
    outputColor = vec4(texture(albedoTexture, fragTexCoords).rgb, 1.0);
#endif
#ifdef MULTIPLY_TEXTURE_BY_COLOR
    outputColor *= color;
#endif
#else
    outputColor = color;
#endif
}
#endif
)FOO";

const char *alphaMappingShaderSource = R"FOO(
#ifdef VERTEX_SHADER
layout(location = 0) in vec3 vertexPosition;
layout(location = 3) in vec2 vertexTexCoord;

uniform mat4 mvp;

out vec2 fragTexCoords;

void main()
{
  fragTexCoords = vertexTexCoord;
  gl_Position = mvp * vec4(vertexPosition, 1.0);
}
#endif

#ifdef FRAGMENT_SHADER

uniform sampler2D alphaMap;
in vec2 fragTexCoords;
uniform vec4 color;

out vec4 outputColor;
void main()
{
    outputColor = color;
    outputColor.a = texture(alphaMap, fragTexCoords).r;
}
#endif
)FOO";

const char *tonemappingSource = R"FOO(
#ifdef VERTEX_SHADER
layout(location = 0) in vec3 vertexPosition;
layout(location = 3) in vec2 vertexTexCoord;

uniform mat4 mvp;

out vec2 fragTexCoords;

void main()
{
  fragTexCoords = vertexTexCoord;
  gl_Position = mvp * vec4(vertexPosition, 1.0);
}
#endif

#ifdef FRAGMENT_SHADER
uniform sampler2D hdrTexture;
out vec4 outputColor;
in vec2 fragTexCoords;

float A = 0.15;
float B = 0.50;
float C = 0.10;
float D = 0.20;
float E = 0.02;
float F = 0.30;
float W = 11.2;

vec3 Uncharted2Tonemap(vec3 x)
{
   return ((x*(A*x+C*B)+D*E)/(x*(A*x+B)+D*F))-E/F;
}

void main()
{
    vec3 texColor = texture(hdrTexture, fragTexCoords).rgb;
    float exposure = 0.9;
    texColor *= exposure;
    //texColor = texColor / (vec3(1) + texColor); // Reinhard
    float exposureBias = 2.0;
    vec3 curr = Uncharted2Tonemap(texColor * exposureBias);
    vec3 whiteScale = 1.0 / Uncharted2Tonemap(vec3(W));
    vec3 color = curr * whiteScale;
    texColor = pow(color, vec3(1.0 / 2.2));
    outputColor = vec4(texColor, 1);
}
#endif
)FOO";

const char *cubeMapSource = R"FOO(
#ifdef VERTEX_SHADER
layout(location = 0) in vec3 vertexPosition;

uniform mat4 mvp;

out vec3 fragTexCoords;

void main()
{
  fragTexCoords = vertexPosition;
  gl_Position = mvp * vec4(vertexPosition, 1.0);
}
#endif


#ifdef FRAGMENT_SHADER
uniform samplerCube cubeMap;
out vec4 outputColor;
in vec3 fragTexCoords;

void main()
{
   float gamma = 2.2;
   outputColor.rgb = pow(texture(cubeMap, fragTexCoords).rgb, vec3(gamma));
   outputColor.a = 1.0;
}
#endif
)FOO";

const char *fullscreenVignetteSource = R"FOO(
#ifdef VERTEX_SHADER
layout(location = 0) in vec3 vertexPosition;

uniform mat4 mvp;

out vec2 fragPosition;

void main()
{
  vec4 p = mvp * vec4(vertexPosition, 1.0);
  fragPosition = p.xy;
  gl_Position = p;
}
#endif


#ifdef FRAGMENT_SHADER
out vec4 outputColor;
in vec2 fragPosition;

void main()
{
   vec2 p = fragPosition * 0.5 + 0.5;
   vec2 center = vec2(0.5, 0.5);
   float len = length(p - center);

   float r = 0.85;
   float softness = 0.55;

   outputColor = vec4(0, 0, 0, 1.0 - smoothstep(r,r - softness, len));
}
#endif
)FOO";

const char *vertexColorSource = R"FOO(
#ifdef VERTEX_SHADER
layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec3 vertexColor;

uniform mat4 mvp;

out vec3 fragColor;

void main()
{
  fragColor = vertexColor;
  gl_Position = mvp * vec4(vertexPosition, 1.0);
}
#endif


#ifdef FRAGMENT_SHADER
out vec4 outputColor;
in vec3 fragColor;

#ifdef USE_DEPTH_FADE
uniform float maxDistance;
#endif

void main()
{
   float gamma = 2.2;
   float alpha = 1.0;
#ifdef USE_DEPTH_FADE
   float distance = gl_FragCoord.z / gl_FragCoord.w;
   alpha = 1.0 - clamp(distance / maxDistance, 0, 1);
#endif
   outputColor = vec4(pow(fragColor, vec3(gamma)), alpha);
}
#endif
)FOO";

const char *visualizeNormalsSource = R"FOO(
#ifdef VERTEX_SHADER
layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec3 vertexNormal;
layout(location = 3) in vec2 vertexTexCoord;

uniform mat4 mvp;
uniform mat4 modelMatrix;

out vec3 fragNormal;

void main()
{
  fragNormal = vec3(modelMatrix * vec4(vertexNormal, 0.0));
  gl_Position = mvp * vec4(vertexPosition, 1.0);
}
#endif


#ifdef FRAGMENT_SHADER
out vec4 outputColor;
in vec3 fragNormal;

void main()
{
   float gamma = 2.2;
   vec3 N = 0.5 * normalize(fragNormal) + 0.5;
   outputColor = vec4(N, 1.0);
}
#endif
)FOO";

inline void AddUniformDefinition(
    RenderCommand_CreateShader *createShader, u32 id, u32 type, const char *name)
{
    u32 idx = createShader->uniformDefinitionCount;
    Assert(idx < ArrayCount(createShader->uniformDefinitions));

    createShader->uniformDefinitions[idx].id = id;
    createShader->uniformDefinitions[idx].type = type;
    createShader->uniformDefinitions[idx].name = name;
    createShader->uniformDefinitionCount++;
}

internal void LoadInternalShaders(GameMemory *memory, MemoryArena *tempArena)
{
    TIMED_BLOCK();
    {
        RenderCommand_CreateShader *createColorShader =
            AllocateRenderCommand(memory, RenderCommand_CreateShader);
        createColorShader->id = Shader_Color;
        createColorShader->shaderSource = colorOnlyShaderSource;
        createColorShader->depthTestEnabled = true;
        createColorShader->alphaBlendingEnabled = true;
        AddUniformDefinition(
            createColorShader, UniformValue_Color, UniformType_Vec4, "color");
        AddUniformDefinition(
            createColorShader, UniformValue_MvpMatrix, UniformType_Mat4, "mvp");
    }

    {
        RenderCommand_CreateShader *createColorShader =
            AllocateRenderCommand(memory, RenderCommand_CreateShader);
        createColorShader->id = Shader_ColorUI;
        createColorShader->shaderSource = colorOnlyShaderSource;
        createColorShader->depthTestEnabled = false;
        createColorShader->alphaBlendingEnabled = true;
        AddUniformDefinition(
            createColorShader, UniformValue_Color, UniformType_Vec4, "color");
        AddUniformDefinition(
            createColorShader, UniformValue_MvpMatrix, UniformType_Mat4, "mvp");
    }

    {
        RenderCommand_CreateShader *createTextureShader =
            AllocateRenderCommand(memory, RenderCommand_CreateShader);
        createTextureShader->id = Shader_Texture;
        createTextureShader->shaderSource = colorOnlyShaderSource;
        createTextureShader->depthTestEnabled = true;
        createTextureShader->defines = "#define USE_TEXTURE\n";
        AddUniformDefinition(createTextureShader, UniformValue_AlbedoTexture,
            UniformType_Texture, "albedoTexture");
        AddUniformDefinition(createTextureShader, UniformValue_MvpMatrix,
            UniformType_Mat4, "mvp");
    }

    {
        RenderCommand_CreateShader *createTextureShader =
            AllocateRenderCommand(memory, RenderCommand_CreateShader);
        createTextureShader->id = Shader_TextureAlpha;
        createTextureShader->shaderSource = colorOnlyShaderSource;
        createTextureShader->depthTestEnabled = false;
        createTextureShader->alphaBlendingEnabled = true;
        createTextureShader->defines = "#define USE_TEXTURE\n#define USE_TEXTURE_ALPHA\n";
        AddUniformDefinition(createTextureShader, UniformValue_AlbedoTexture,
            UniformType_Texture, "albedoTexture");
        AddUniformDefinition(createTextureShader, UniformValue_MvpMatrix,
            UniformType_Mat4, "mvp");
    }

    {
        RenderCommand_CreateShader *createTextureShader =
            AllocateRenderCommand(memory, RenderCommand_CreateShader);
        createTextureShader->id = Shader_ParticleTexture;
        createTextureShader->shaderSource = colorOnlyShaderSource;
        createTextureShader->depthTestEnabled = false;
        createTextureShader->alphaBlendingEnabled = true;
        createTextureShader->defines =
            "#define USE_TEXTURE\n#define USE_TEXTURE_ALPHA\n#define "
            "MULTIPLY_TEXTURE_BY_COLOR\n";
        AddUniformDefinition(
            createTextureShader, UniformValue_Color, UniformType_Vec4, "color");
        AddUniformDefinition(createTextureShader, UniformValue_AlbedoTexture,
            UniformType_Texture, "albedoTexture");
        AddUniformDefinition(createTextureShader, UniformValue_MvpMatrix,
            UniformType_Mat4, "mvp");
    }

    {
        RenderCommand_CreateShader *createTonemappingShader =
            AllocateRenderCommand(memory, RenderCommand_CreateShader);
        createTonemappingShader->id = Shader_Tonemapping;
        createTonemappingShader->shaderSource = tonemappingSource;
        createTonemappingShader->depthTestEnabled = false;
        AddUniformDefinition(createTonemappingShader, UniformValue_MvpMatrix,
            UniformType_Mat4, "mvp");
        AddUniformDefinition(createTonemappingShader, UniformValue_HdrBuffer,
            UniformType_RenderTarget, "hdrTexture");
    }

    {
        RenderCommand_CreateShader *createCubeMapShader =
            AllocateRenderCommand(memory, RenderCommand_CreateShader);
        createCubeMapShader->id = Shader_CubeMap;
        createCubeMapShader->shaderSource = cubeMapSource;
        createCubeMapShader->depthTestEnabled = false;
        AddUniformDefinition(createCubeMapShader, UniformValue_MvpMatrix,
            UniformType_Mat4, "mvp");
        AddUniformDefinition(createCubeMapShader, UniformValue_CubeMap,
            UniformType_Texture, "cubeMap");
    }

    {
        RenderCommand_CreateShader *createAlphaMappingShader =
            AllocateRenderCommand(memory, RenderCommand_CreateShader);
        createAlphaMappingShader->id = Shader_AlphaMapping;
        createAlphaMappingShader->shaderSource = alphaMappingShaderSource;
        createAlphaMappingShader->depthTestEnabled = false;
        createAlphaMappingShader->alphaBlendingEnabled = true;
        AddUniformDefinition(createAlphaMappingShader, UniformValue_MvpMatrix,
            UniformType_Mat4, "mvp");
        AddUniformDefinition(createAlphaMappingShader, UniformValue_AlphaMap,
            UniformType_Texture, "alphaMap");
        AddUniformDefinition(createAlphaMappingShader, UniformValue_Color,
            UniformType_Vec4, "color");
    }

    {
        RenderCommand_CreateShader *createVertexColorShader =
            AllocateRenderCommand(memory, RenderCommand_CreateShader);
        createVertexColorShader->id = Shader_VertexColor;
        createVertexColorShader->shaderSource = vertexColorSource;
        createVertexColorShader->depthTestEnabled = false;
        createVertexColorShader->alphaBlendingEnabled = false;
        AddUniformDefinition(createVertexColorShader, UniformValue_MvpMatrix,
            UniformType_Mat4, "mvp");
    }

    {
        RenderCommand_CreateShader *createVertexColorShader =
            AllocateRenderCommand(memory, RenderCommand_CreateShader);
        createVertexColorShader->id = Shader_VertexColorWithDepth;
        createVertexColorShader->shaderSource = vertexColorSource;
        createVertexColorShader->defines = "#define USE_DEPTH_FADE\n";
        createVertexColorShader->depthTestEnabled = true;
        createVertexColorShader->alphaBlendingEnabled = true;
        AddUniformDefinition(createVertexColorShader, UniformValue_MvpMatrix,
            UniformType_Mat4, "mvp");
        AddUniformDefinition(createVertexColorShader, UniformValue_MaxDistance,
                UniformType_F32, "maxDistance");
    }

    {
        RenderCommand_CreateShader *createVisualizeNormalsShader =
            AllocateRenderCommand(memory, RenderCommand_CreateShader);
        createVisualizeNormalsShader->id = Shader_VisualizeNormals;
        createVisualizeNormalsShader->shaderSource = visualizeNormalsSource;
        createVisualizeNormalsShader->depthTestEnabled = true;
        createVisualizeNormalsShader->alphaBlendingEnabled = false;
        AddUniformDefinition(createVisualizeNormalsShader, UniformValue_MvpMatrix,
            UniformType_Mat4, "mvp");
        AddUniformDefinition(createVisualizeNormalsShader, UniformValue_ModelMatrix,
            UniformType_Mat4, "modelMatrix");
    }
}

#define GLYPH_COUNT 128
struct Font
{
    stbtt_packedchar charData[GLYPH_COUNT];
    u32 texture;
    u32 textureWidth;
    u32 textureHeight;
    float spaceAdvance;
    float lineSpacing;
    float height;
};

bool CreateFont(Font *font, const void *assetData, u32 assetDataLength,
    GameMemory *memory, MemoryArena *tempArena, u32 bitmapWidth,
    u32 bitmapHeight, f32 fontSize, u32 id)
{
    stbtt_pack_context packingContext;

    u8 *bitmap =
        (u8 *)MemoryArena_Allocate(tempArena, bitmapWidth * bitmapHeight);

    // TODO: Manage memory allocation
    stbtt_PackBegin(&packingContext, bitmap, bitmapWidth, bitmapHeight, 0, 1, NULL);
    stbtt_PackSetOversampling(&packingContext, 2, 2);
    stbtt_PackFontRange(&packingContext, (const u8 *)assetData, 0, fontSize, 32,
        95, font->charData + 32);
    stbtt_PackEnd(&packingContext);

    // TODO: Don't use bilinear filtering for glyph sheet texture
    RenderCommand_CreateTexture *createTexture =
        AllocateRenderCommand(memory, RenderCommand_CreateTexture);
    createTexture->pixels = bitmap;
    createTexture->width = bitmapWidth;
    createTexture->height = bitmapHeight;
    createTexture->id = id;
    createTexture->colorFormat = ColorFormat_R8;

    stbtt_fontinfo fontInfo;
    if (!stbtt_InitFont(&fontInfo, (const u8 *)assetData, 0))
    {
        Assert(!"stbtt_InitFont failed");
    }
    i32 ascent, descent, lineGap;
    stbtt_GetFontVMetrics(&fontInfo, &ascent, &descent, &lineGap);

    i32 advance, leftSideBearing;
    stbtt_GetCodepointHMetrics(&fontInfo, ' ', &advance,
            &leftSideBearing);
    float scale = stbtt_ScaleForMappingEmToPixels(&fontInfo, fontSize);

    font->texture = id;
    font->textureWidth = bitmapWidth;
    font->textureHeight = bitmapHeight;
    font->spaceAdvance = advance * scale;
    font->lineSpacing = (ascent + descent + lineGap) * scale;
    font->height = (ascent + descent) * scale;

    // Memory is never freed because this should use the temp allocator
    // NOTE: This frees all of the other bitmaps as well.
    //MemoryArenaFree(arena, bitmap);

    return true;
}

internal float AddCharacterToBuffer(
    VertexBuffer *buffer, Font *font, i32 c, float x, float y)
{
    stbtt_aligned_quad q;
    stbtt_GetPackedQuad(font->charData, font->textureWidth, font->textureHeight,
        c, &x, &y, &q, 0);

    Vertex topLeft = {}; 
    Vertex bottomLeft = {};
    Vertex bottomRight = {};
    Vertex topRight = {};

    float minX = q.x0;
    float minY = -q.y1;
    float maxX = q.x1;
    float maxY = -q.y0;

    topLeft.position = Vec3(minX, maxY, 0.0f);
    topLeft.texCoord = Vec2(q.s0, q.t0);

    bottomLeft.position = Vec3(minX, minY, 0.0f);
    bottomLeft.texCoord = Vec2(q.s0, q.t1);

    bottomRight.position = Vec3(maxX, minY, 0.0f);
    bottomRight.texCoord = Vec2(q.s1, q.t1);

    topRight.position = Vec3(maxX, maxY, 0.0f);
    topRight.texCoord = Vec2(q.s1, q.t0);

    auto vertex = buffer->vertices + buffer->count;
    *vertex++ = topRight;
    *vertex++ = topLeft;
    *vertex++ = bottomLeft;

    *vertex++ = bottomLeft;
    *vertex++ = bottomRight;
    *vertex++ = topRight;
    buffer->count += 6;

    return x;
}

internal float CalculateTextLength(Font *font, const char *str, u32 length = 0)
{
    float result = 0.0f;
    float current = 0.0f;
    u32 count = 0;
    while (*str)
    {
        if (length > 0 && count >= length)
        {
            break;
        }
        count++;

        if (*str >= 32)
        {
            float x = 0.0f;
            float y = 0.0f;
            stbtt_aligned_quad q;
            stbtt_GetPackedQuad(font->charData, font->textureWidth, font->textureHeight,
                    (i32)(*str), &x, &y, &q, 0);

            current += x;
        }
        else if (*str == ' ')
        {
            current += font->spaceAdvance;
        }
        else if (*str == '\n')
        {
            if (current > result)
            {
                result = current;
            }
            current = 0.0f;
        }
        //else if (*str == '\t')
        //{
            //current += (i32)font->glyphs[32].advance * 2;
        //}
        str++;
    }
    if (current > result)
    {
        result = current;
    }
    return result;
}

struct AddTextToBufferResult
{
    u32 firstVertex;
    u32 vertexCount;
};

internal AddTextToBufferResult AddTextToBuffer(VertexBuffer *textBuffer,
    const char *text, Font *font, u32 length = 0)
{
    AddTextToBufferResult result = {};
    result.firstVertex = textBuffer->count;

    if (length == 0)
    {
        length = (u32)StringLength(text);
    }

    float shiftWidth = 4.0f;

    float xOffset = 0.0f;
    float yOffset = 0.0f;
    const char *cursor = text;
    for (u32 i = 0; i < length; ++i)
    {
        if (*cursor > 32)
        {
            AddCharacterToBuffer(
                textBuffer, font, *cursor, xOffset, yOffset);
            // FIXME: This assumes we're using a monospaced font, which is not always true
            xOffset += font->spaceAdvance;
        }
        else if (*cursor == ' ')
        {
            xOffset += font->spaceAdvance;
        }
        else if (*cursor == '\t')
        {
            xOffset += font->spaceAdvance * shiftWidth;
        }
        else if (*cursor == '\n')
        {
            yOffset += font->lineSpacing;
            xOffset = 0.0f;
        }
        cursor++;
    }
    result.vertexCount = textBuffer->count - result.firstVertex;

    return result;
}

struct WaveHeader
{
  u32 riffId;
  u32 size;
  u32 waveId;
};

#define RIFF_CODE( a, b, c, d )                                                \
  ( ( ( u32 )( a ) << 0 ) | ( ( u32 )( b ) << 8 ) |                  \
  ( ( u32 )( c ) << 16 ) | ( ( u32 )( d ) << 24 ) )

enum
{
  WaveChunkIdFormat = RIFF_CODE( 'f', 'm', 't', ' ' ),
  WaveChunkIdData   = RIFF_CODE( 'd', 'a', 't', 'a' ),
  WaveChunkIdRiff   = RIFF_CODE( 'R', 'I', 'F', 'F' ),
  WaveChunkIdWave   = RIFF_CODE( 'W', 'A', 'V', 'E' ),
};

struct WaveChunk
{
  u32 id;
  u32 size;
};

#pragma pack(push)
struct WaveFormat
{
  u16 formatTag;
  u16 channelCount;
  u32 samplesPerSecond;
  u32 averageBytesPerSecond;
  u16 blockAlign;
  u16 bitsPerSample;
  u16 cbSize;
  u16 validBitsPerSample;
  u32 channelMask;
  u8 subFormat[16];
};
#pragma pack(pop)

struct ChunkIterator
{
    u8 *at;
    u8 *stop;
};

inline ChunkIterator ParseChunkAt(void *at, void *stop)
{
    ChunkIterator iter;

    iter.at = (u8 *)at;
    iter.stop = (u8 *)stop;

    return iter;
}

inline ChunkIterator NextChunk(ChunkIterator iter)
{
    WaveChunk *chunk = (WaveChunk *)iter.at;
    u32 size = (chunk->size + 1) & ~1;
    iter.at += sizeof(WaveChunk) + size;

    return iter;
}

inline bool IsValid(ChunkIterator iter)
{
    bool result = (iter.at < iter.stop);
    return result;
}

inline void *GetChunkData(ChunkIterator iter)
{
    void *result = (iter.at + sizeof(WaveChunk));
    return result;
}

inline u32 GetType(ChunkIterator iter)
{
    WaveChunk *chunk = (WaveChunk *)iter.at;
    return chunk->id;
}

inline u32 GetChunkDataSize(ChunkIterator iter)
{
    WaveChunk *chunk = (WaveChunk *)iter.at;
    return chunk->size;
}

internal AudioData LoadAudioData(
    const void *assetData, u32 assetDataLength, MemoryArena *audioDataArena)
{
    WaveHeader *header = (WaveHeader *)assetData;

    Assert(assetDataLength != 0);
    Assert(header->riffId == WaveChunkIdRiff);
    Assert(header->waveId == WaveChunkIdWave);

    u32 channelCount = 0;
    u32 sampleDataSize = 0;
    i16 *sampleData = NULL;
    u32 samplesPerSecond = 0;
    for (ChunkIterator iter =
             ParseChunkAt(header + 1, (u8 *)(header + 1) + header->size - 4);
         IsValid(iter); iter = NextChunk(iter))
    {
        switch (GetType(iter))
        {
        case WaveChunkIdFormat:
        {
            WaveFormat *format = (WaveFormat *)GetChunkData(iter);
            Assert(format->formatTag == 1);      // Must be PCM data
            Assert(format->bitsPerSample == 16); // 16-bit audio
            Assert(format->blockAlign == (sizeof(i16) * format->channelCount));
            Assert(format->samplesPerSecond == 48000); // Temporary
            Assert(format->channelCount == 2); // Temporary
            channelCount = format->channelCount;
            samplesPerSecond = format->samplesPerSecond;
            break;
        }
        case WaveChunkIdData:
            sampleData = (i16 *)GetChunkData(iter);
            sampleDataSize = GetChunkDataSize(iter);
            break;
        }
    }

    AudioData result = {};
    result.channelCount = channelCount;
    result.samplesPerSecond = samplesPerSecond;
    result.samples = AllocateArray(audioDataArena, i16, sampleDataSize);
    CopyMemory(result.samples, sampleData, sampleDataSize);
    u32 bytesPerSample = sizeof(i16) * channelCount;
    Assert(sampleDataSize % bytesPerSample == 0);
    result.sampleCount = sampleDataSize / bytesPerSample;

    return result;
}
