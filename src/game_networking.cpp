struct CreateEntityEvent
{
    u32 type;

    union
    {
        struct
        {
            vec3 position;
            b32 isActivePlayer;
        } player;

        struct
        {
            vec3 position;
        } enemy;

        struct
        {
            u32 itemId;
            u32 itemQuantity;
            u32 slotIdx;
            NetEntityId owner;
        } item;

        struct
        {
            vec3 position;
        } storageBox;

        struct
        {
            vec3 position;
        } foundation;

        struct
        {
            vec3 position;
        } ceiling;

        struct
        {
            vec3 position;
            quat rotation;
        } wall;

        struct
        {
            vec3 position;
            quat rotation;
        } doorway;

        struct
        {
            vec3 position;
        } healthTest;
    };
};

struct UpdateEntityEvent
{
    u32 type;

    union
    {
        struct
        {
            vec3 position;
            vec3 rotation;
            vec3 velocity;
            NetEntityId inventorySubject;
            f32 health;
            u32 activeActionBarSlot;
        } player;

        struct
        {
            vec3 position;
            vec3 rotation;
            vec3 velocity;
            f32 health;
        } enemy;

        struct
        {
            u32 slotIdx;
            u32 itemQuantity;
            NetEntityId owner;
        } item;
    };
};

struct DamageReceivedEvent
{
    NetEntityId netEntityId;
};

struct BulletImpactEvent
{
    vec3 position;
    vec3 normal;
};

struct GunShotEvent
{
    vec3 position;
};

struct EntityEventHeader
{
    NetEntityId netEntityId;
    u8 type;
    u8 length;
};

enum
{
    EntityEventType_Create,
    EntityEventType_Update,
    EntityEventType_Delete,
};

struct GameUpdatePacket
{
    u8 eventData[512];
    u32 eventDataLength;
    u32 eventCount;

    DamageReceivedEvent damageReceivedEvents[4];
    u32 damageReceivedEventCount;
    GunShotEvent gunShotEvents[4];
    u32 gunShotEventCount;
    BulletImpactEvent bulletImpactEvents[4];
    u32 bulletImpactEventCount;
};

inline b32 GenericWriteEntityEvent(GameUpdatePacket *packetData,
        NetEntityId netEntityId, u8 type, void *event, u32 length)
{
    b32 result = false;
    u32 totalSize = sizeof(EntityEventHeader) + length;
    if (packetData->eventDataLength + totalSize <=
        ArrayCount(packetData->eventData))
    {
        EntityEventHeader *header =
            (EntityEventHeader *)(packetData->eventData +
                                  packetData->eventDataLength);

        header->netEntityId = netEntityId;
        header->type = type;
        header->length = SafeTruncateU32ToU8(length);
        CopyMemory(header + 1, event, length);
        packetData->eventDataLength += totalSize;

        result = true;
    }
    return result;
}

inline b32 WriteEntityCreationEvent(GameUpdatePacket *packetData,
    NetEntityId netEntityId, CreateEntityEvent *event)
{
    b32 result = GenericWriteEntityEvent(
        packetData, netEntityId, EntityEventType_Create, event, sizeof(*event));
    return result;
}

inline b32 WriteEntityUpdateEvent(GameUpdatePacket *packetData,
    NetEntityId netEntityId, UpdateEntityEvent *event)
{
    b32 result = GenericWriteEntityEvent(
        packetData, netEntityId, EntityEventType_Update, event, sizeof(*event));
    return result;
}

inline b32 WriteEntityDeletionEvent(GameUpdatePacket *packetData,
    NetEntityId netEntityId)
{
    b32 result = GenericWriteEntityEvent(
        packetData, netEntityId, EntityEventType_Delete, NULL, 0);
    return result;
}

struct NetEntityUpdateQueueEntry
{
    u32 entityUpdateIdx; // Index into entitiesToUpdate
    u32 entityIdx; // Index into client->entities
    f32 priority;
};

// FIXME: Should probably put a bit more effort and testing into sorting
inline void SortUpdateQueue(NetEntityUpdateQueueEntry *queue, u32 count)
{
    if (count > 1)
    {
        for (u32 i = 0; i < count - 1; ++i)
        {
            for (u32 j = 0; j < count - 1; ++j)
            {
                NetEntityUpdateQueueEntry *a = queue + j;
                NetEntityUpdateQueueEntry *b = queue + j + 1;
                if (a->priority < b->priority)
                {
                    SwapValues(*a, *b, NetEntityUpdateQueueEntry);
                }
            }
        }
    }
}

// FIXME: Update logging to only log for entity network events that are
// actually going over the network not just the ones that were serialized
internal void PopulateGamePacketForClient(NetEntityClient *client,
    EntityId entityToControl, ecs_EntityWorld *world,
    GameEventQueue *eventQueue, GameUpdatePacket *packetData)
{
    // Work out which entities need to be created for this client
    EntityId entitiesToCreate[ArrayCount(client->entities)];
    u32 entitiesToCreateCount = GetEntitiesToCreate(
        world, client, entitiesToCreate, ArrayCount(entitiesToCreate));

    // Work out which entities we need to send updates for
    EntityId entitiesToUpdate[ArrayCount(client->entities)];
    u32 entitiesToUpdateCount = 0;
    NetEntityId netEntityIdsToDelete[ArrayCount(client->entities)];
    NetEntityId netEntityIdsToUpdate[ArrayCount(client->entities)];
    u32 netEntityIdsToDeleteCount = 0;
    NetEntityUpdateQueueEntry updateQueue[ArrayCount(client->entities)];
    for (u32 entityIdx = 0; entityIdx < client->entityCount; ++entityIdx)
    {
        EntityId entity = client->entities[entityIdx];
        NetEntityId netEntityId = client->netEntityIds[entityIdx];
        f32 priority = client->priorities[entityIdx];

        if (ecs_HasComponent(world, NetEntityIdComponentId, entity))
        {
            u32 type = GetEntityType(world, entity);

            // TODO: Make this a flag of some sorts that can be set on an entity
            // prefab
            if (type != EntityType_StorageBox &&
                type != EntityType_Foundation && type != EntityType_Wall &&
                type != EntityType_Doorway && type != EntityType_Ceiling)
            {
                entitiesToUpdate[entitiesToUpdateCount] = entity;
                netEntityIdsToUpdate[entitiesToUpdateCount] = netEntityId;
                updateQueue[entitiesToUpdateCount] = {
                    entitiesToUpdateCount, entityIdx, priority};
                entitiesToUpdateCount++;
            }
        }
        else
        {
            netEntityIdsToDelete[netEntityIdsToDeleteCount++] = netEntityId;
#ifdef LOG_NETWORK_EVENTS
            LogMessage(
                "Send deletion event for entity %u(%u)", entity, netEntityId);
#endif
        }
    }

    // TODO: This could be quite wasteful, serialize all potential entities
    // even if they won't fit into packet
    // Send updates events
    UpdateEntityEvent updateEvents[ArrayCount(client->entities)];
    ClearToZero(updateEvents, sizeof(updateEvents));
    u32 updateEventCount = 0;
    for (u32 entityIdx = 0; entityIdx < entitiesToUpdateCount; ++entityIdx)
    {
        Assert(updateEventCount < ArrayCount(updateEvents));

        UpdateEntityEvent *event = updateEvents + updateEventCount++;
        EntityId entity = entitiesToUpdate[entityIdx];
        NetEntityId netEntityId = netEntityIdsToUpdate[entityIdx];

        event->type = GetEntityType(world, entity);

        switch (event->type)
        {
        case EntityType_Player:
        {
#ifdef LOG_NETWORK_EVENTS
            LogMessage("Send Player update event for entity %u(%u)", entity,
                netEntityId);
#endif
            ecs_GetComponent(world, EulerAnglesComponentId, &entity, 1,
                &event->player.rotation, sizeof(event->player.rotation));

            ecs_GetComponent(world, PositionComponentId, &entity, 1,
                &event->player.position, sizeof(event->player.position));

            ecs_GetComponent(world, VelocityComponentId, &entity, 1,
                &event->player.velocity, sizeof(event->player.velocity));

            ecs_GetComponent(world, HealthComponentId, &entity, 1,
                &event->player.health, sizeof(event->player.health));

            ecs_GetComponent(world, ActionBarActiveSlotComponentId, &entity, 1,
                &event->player.activeActionBarSlot,
                sizeof(event->player.activeActionBarSlot));

            EntityId inventorySubject = NULL_ENTITY;
            ecs_GetComponent(world, OpenContainerComponentId, &entity, 1,
                &inventorySubject, sizeof(inventorySubject));

            if (inventorySubject != NULL_ENTITY)
            {
                MapEntityIdToNetEntityId(world, &inventorySubject, 1,
                    &event->player.inventorySubject);
            }
        }
        break;
        case EntityType_Item:
        {
#ifdef LOG_NETWORK_EVENTS
            LogMessage("Send Item update event for entity %u(%u)", entity,
                netEntityId);
#endif
            EntityId owner = NULL_ENTITY;
            ecs_GetComponent(world, InventorySlotOwnerComponentId, &entity, 1,
                &owner, sizeof(owner));
            MapEntityIdToNetEntityId(world, &owner, 1, &event->item.owner);

            ecs_GetComponent(world, InventorySlotIndexComponentId, &entity, 1,
                &event->item.slotIdx, sizeof(event->item.slotIdx));

            ecs_GetComponent(world, ItemQuantityComponentId, &entity, 1,
                &event->item.itemQuantity, sizeof(event->item.itemQuantity));
        }
        break;
        case EntityType_Enemy:
        {
#ifdef LOG_NETWORK_EVENTS
            LogMessage("Send Enemy update event for entity %u(%u)", entity,
                netEntityId);
#endif
            ecs_GetComponent(world, EulerAnglesComponentId, &entity, 1,
                &event->enemy.rotation, sizeof(event->enemy.rotation));

            ecs_GetComponent(world, PositionComponentId, &entity, 1,
                &event->enemy.position, sizeof(event->enemy.position));

            ecs_GetComponent(world, VelocityComponentId, &entity, 1,
                &event->enemy.velocity, sizeof(event->enemy.velocity));

            ecs_GetComponent(world, HealthComponentId, &entity, 1,
                &event->enemy.health, sizeof(event->enemy.health));
        }
        break;
        default:
        {
            InvalidCodePath();
        }
        break;
        }
    }

    CreateEntityEvent events[ArrayCount(client->entities)];
    u32 eventCount = 0;
    EntityId entitiesCreated[ArrayCount(client->entities)]; // Probably not needed anymore
    NetEntityId newNetEntityIds[ArrayCount(client->entities)];
    u32 entitiesCreatedCount = 0;
    for (u32 entityIdx = 0; entityIdx < entitiesToCreateCount; ++entityIdx)
    {
        Assert(eventCount < ArrayCount(events));
        CreateEntityEvent *event = events + eventCount++;

        EntityId entity = entitiesToCreate[entityIdx];
        entitiesCreated[entitiesCreatedCount] = entity;

        NetEntityId netEntityId = 0;
        MapEntityIdToNetEntityId(world, &entity, 1, &netEntityId);
        newNetEntityIds[entitiesCreatedCount] = netEntityId;
        entitiesCreatedCount++;

        event->type = GetEntityType(world, entity);
        switch (event->type)
        {
        case EntityType_Item:
        {
#ifdef LOG_NETWORK_EVENTS
            LogMessage("Send Item create event for entity %u(%u)", entity,
                netEntityId);
#endif
            ecs_GetComponent(world, ItemIdComponentId, &entity, 1,
                &event->item.itemId, sizeof(event->item.itemId));
            ecs_GetComponent(world, ItemQuantityComponentId, &entity, 1,
                &event->item.itemQuantity, sizeof(event->item.itemQuantity));

            EntityId owner = NULL_ENTITY;
            ecs_GetComponent(world, InventorySlotOwnerComponentId, &entity, 1,
                &owner, sizeof(owner));
            MapEntityIdToNetEntityId(world, &owner, 1, &event->item.owner);

            ecs_GetComponent(world, InventorySlotIndexComponentId, &entity, 1,
                &event->item.slotIdx, sizeof(event->item.slotIdx));
        }
        break;
        case EntityType_Player:
        {
#ifdef LOG_NETWORK_EVENTS
            LogMessage("Send Player create event for entity %u(%u)", entity,
                netEntityId);
#endif
            ecs_GetComponent(world, PositionComponentId, &entity, 1,
                &event->player.position, sizeof(event->player.position));

            event->player.isActivePlayer = (entity == entityToControl);
        }
        break;
        case EntityType_StorageBox:
            ecs_GetComponent(world, PositionComponentId, &entity, 1,
                &event->storageBox.position,
                sizeof(event->storageBox.position));
            break;
        case EntityType_Foundation:
            ecs_GetComponent(world, PositionComponentId, &entity, 1,
                &event->foundation.position,
                sizeof(event->foundation.position));
            break;
        case EntityType_Wall:
            ecs_GetComponent(world, PositionComponentId, &entity, 1,
                &event->wall.position, sizeof(event->wall.position));
            ecs_GetComponent(world, RotationComponentId, &entity, 1,
                &event->wall.rotation, sizeof(event->wall.rotation));
            break;
        case EntityType_Doorway:
            ecs_GetComponent(world, PositionComponentId, &entity, 1,
                &event->doorway.position, sizeof(event->doorway.position));
            ecs_GetComponent(world, RotationComponentId, &entity, 1,
                &event->doorway.rotation, sizeof(event->doorway.rotation));
            break;
        case EntityType_Ceiling:
            ecs_GetComponent(world, PositionComponentId, &entity, 1,
                &event->ceiling.position, sizeof(event->ceiling.position));
            break;
        case EntityType_Enemy:
        {
#ifdef LOG_NETWORK_EVENTS
            LogMessage("Send Enemy create event for entity %u(%u)", entity,
                netEntityId);
#endif
            ecs_GetComponent(world, PositionComponentId, &entity, 1,
                &event->enemy.position, sizeof(event->enemy.position));
        }
        break;
        default:
        {
            InvalidCodePath();
        }
        break;
        }
    }

    // TODO: Common event queue that we can use to prioritize events
    // TODO: Don't broadcast all events to all clients, need system for working
    // out which events are of which client
    for (u32 i = 0; i < eventQueue->length; ++i)
    {
        GameEvent *event = eventQueue->events + i;
        switch (event->type)
        {
        case GameEvent_ReceivedDamage:
        {
            // Broadcast received damage event to all clients
            if (packetData->damageReceivedEventCount <
                ArrayCount(packetData->damageReceivedEvents))
            {
                DamageReceivedEvent *damageReceivedEvent =
                    packetData->damageReceivedEvents +
                    packetData->damageReceivedEventCount++;

                MapEntityIdToNetEntityId(world, &event->receivedDamage.entity,
                    1, &damageReceivedEvent->netEntityId);
            }
            // TODO: What is our policy for events if we don't have capacity
            // for them in the packet, do we just drop them?
            break;
        }
        case GameEvent_BulletImpact:
        {
            if (packetData->bulletImpactEventCount <
                ArrayCount(packetData->bulletImpactEvents))
            {
                BulletImpactEvent *bulletImpactEvent =
                    packetData->bulletImpactEvents +
                    packetData->bulletImpactEventCount++;

                bulletImpactEvent->position = event->bulletImpact.position;
                bulletImpactEvent->normal = event->bulletImpact.normal;
            }
            break;
        }
        case GameEvent_GunShot:
        {
            // Broadcast gunshot event to all clients
            if (packetData->gunShotEventCount <
                ArrayCount(packetData->gunShotEvents))
            {
                GunShotEvent *gunShotEvent =
                    packetData->gunShotEvents + packetData->gunShotEventCount++;

                gunShotEvent->position = event->gunShot.position;
            }
            break;
        }
        default:
            break;
        }
    }


    Assert(entitiesToUpdateCount == updateEventCount);

    SortUpdateQueue(updateQueue, updateEventCount);

    u32 creationEventCursor = 0;
    u32 deletionEventCursor = 0;
    u32 updateEventCursor = 0;
    while (packetData->eventDataLength < ArrayCount(packetData->eventData))
    {
        // Write creation events first
        b32 eventWritten = false;
        if (creationEventCursor < eventCount)
        {
            if (WriteEntityCreationEvent(packetData,
                newNetEntityIds[creationEventCursor],
                events + creationEventCursor))
            {
                eventWritten = true;
                creationEventCursor++;
            }
        }

        // Then deletion events
        // TODO: Rename netEntityIdsToDeleteCount to deletionEventCount or something else
        if (!eventWritten && deletionEventCursor < netEntityIdsToDeleteCount)
        {
            if (WriteEntityDeletionEvent(
                    packetData, netEntityIdsToDelete[deletionEventCursor]))
            {
                eventWritten = true;
                deletionEventCursor++;
            }
        }

        // Then finally update events
        if (!eventWritten && updateEventCursor < updateEventCount)
        {
            u32 entityUpdateIdx =
                updateQueue[updateEventCursor].entityUpdateIdx;
            Assert(entityUpdateIdx < updateEventCount);
            if (WriteEntityUpdateEvent(packetData,
                    netEntityIdsToUpdate[entityUpdateIdx],
                    updateEvents + entityUpdateIdx))
            {
                eventWritten = true;
                updateEventCursor++;
            }
        }

        if (!eventWritten)
        {
            break; // Cannot write anymore data to packet
        }
    }

    OnEntitiesCreated(
        client, entitiesCreated, newNetEntityIds, creationEventCursor);

    // TODO: Bit messy and exposes details of NetEntityClient
    // Update net entity priorities for client based on which were able to be written to the packet
    for (u32 i = 0; i < updateEventCursor; ++i)
    {
        u32 entityIdx = updateQueue[i].entityIdx;
        Assert(entityIdx < client->entityCount);
        client->priorities[entityIdx] = DEFAULT_NET_ENTITY_PRIORITY;
    }

    // TODO: We'll probably want some debug statistics around how priority of
    // entities is changing
    for (u32 i = updateEventCursor; i < updateEventCount; ++i)
    {
        Assert(i < ArrayCount(updateQueue));
        u32 entityIdx = updateQueue[i].entityIdx;
        Assert(entityIdx < client->entityCount);
        client->priorities[entityIdx] = Min(client->priorities[entityIdx] + 0.1f, F32_MAX);
    }

    RemoveNetEntityIdsFromClient(
        client, netEntityIdsToDelete, deletionEventCursor);
}

internal void ProcessCreateEntityEvent(
    ecs_EntityWorld *world, CreateEntityEvent *event, NetEntityId netEntityId)
{
    EntityId entity = NULL_ENTITY;
    switch (event->type)
    {
    case EntityType_Player:
    {
        entity = SpawnEntityAtPosition(
            world, EntityPrefab_Player, event->player.position);
#ifdef LOG_NETWORK_EVENTS
        LogMessage(
            "Recv Player create event for entity %u(%u)", entity, netEntityId);
#endif

        // Set active camera
        if (event->player.isActivePlayer)
        {
            ecs_AddComponent(world, ActivePlayerComponentId, &entity, 1);

            // Always make the new active player the active camera
            // TODO: Make this configurable through a cvar for debugging
            b32 overrideActiveCamera = true;
            if (overrideActiveCamera)
            {
                EntityId camera = NULL_ENTITY;
                if (GetFirstEntity(world, ActiveCameraComponentId, &camera))
                {
                    ecs_RemoveComponent(
                        world, ActiveCameraComponentId, &camera, 1);
                }

                ecs_AddComponent(world, ActiveCameraComponentId, &entity, 1);

                b32 isVisible = false;
                ecs_SetComponent(world, IsVisibleComponentId, &entity, 1,
                    &isVisible, sizeof(isVisible));
            }
        }
    }
    break;
    case EntityType_Item:
    {
        // FIXME: This will assert if owner entity has not been
        // created yet on client or has not had its netEntityId set
        // yet.
        EntityId owner = NULL_ENTITY;
        MapNetEntityIdToEntityId(world, &event->item.owner, 1, &owner);

        entity = CreateItemEntity(world, event->item.itemId,
            event->item.itemQuantity, event->item.slotIdx, owner);

#ifdef LOG_NETWORK_EVENTS
        LogMessage(
            "Recv Item create event for entity %u(%u)", entity, netEntityId);
#endif
    }
    break;
    case EntityType_StorageBox:
        entity = SpawnEntityAtPosition(
            world, EntityPrefab_StorageBox, event->storageBox.position);
        break;
    case EntityType_Foundation:
        entity = SpawnEntityAtPosition(
            world, EntityPrefab_Foundation, event->foundation.position);
        break;
    case EntityType_Wall:
        entity = SpawnEntityAtPositionWithRotation(world, EntityPrefab_Wall,
            event->wall.position, event->wall.rotation);
        break;
    case EntityType_Doorway:
        entity = SpawnEntityAtPositionWithRotation(world, EntityPrefab_Doorway,
            event->doorway.position, event->doorway.rotation);
        break;
    case EntityType_Ceiling:
        entity = SpawnEntityAtPosition(
            world, EntityPrefab_Ceiling, event->ceiling.position);
        break;
    case EntityType_Enemy:
    {
        entity = SpawnEntityAtPosition(
            world, EntityPrefab_Enemy, event->enemy.position);
#ifdef LOG_NETWORK_EVENTS
        LogMessage(
            "Recv Enemy create event for entity %u(%u)", entity, netEntityId);
#endif
    }
    break;
    default:
        InvalidCodePath();
        break;
    }

    Assert(netEntityId != 0);
    ecs_SetComponent(world, NetEntityIdComponentId, &entity, 1, &netEntityId,
        sizeof(netEntityId));
}

internal void ProcessUpdateEntityEvent(
    ecs_EntityWorld *world, UpdateEntityEvent *event, NetEntityId netEntityId)
{
    EntityId entity = NULL_ENTITY;
    MapNetEntityIdToEntityId(world, &netEntityId, 1, &entity);

    switch (event->type)
    {
    case EntityType_Player:
    {
#ifdef LOG_NETWORK_EVENTS
        LogMessage(
            "Recv Player update event for entity %u(%u)", entity, netEntityId);
#endif
        ecs_SetComponent(world, EulerAnglesComponentId, &entity, 1,
            &event->player.rotation, sizeof(event->player.rotation));

        quat rotation = FromEulerAngles(event->player.rotation);
        ecs_SetComponent(world, RotationComponentId, &entity, 1, &rotation,
            sizeof(rotation));

        ecs_SetComponent(world, PositionComponentId, &entity, 1,
            &event->player.position, sizeof(event->player.position));
        ecs_SetComponent(world, VelocityComponentId, &entity, 1,
            &event->player.velocity, sizeof(event->player.velocity));

        ecs_SetComponent(world, HealthComponentId, &entity, 1,
            &event->player.health, sizeof(event->player.health));

        ecs_SetComponent(world, ActionBarActiveSlotComponentId, &entity, 1,
            &event->player.activeActionBarSlot,
            sizeof(event->player.activeActionBarSlot));

        EntityId subject = NULL_ENTITY;
        if (event->player.inventorySubject != 0)
        {
            MapNetEntityIdToEntityId(
                world, &event->player.inventorySubject, 1, &subject);
        }
        ecs_SetComponent(world, OpenContainerComponentId, &entity, 1, &subject,
            sizeof(subject));

        b32 value = true;
        ecs_SetComponent(world, IsWorldTransformDirtyComponentId, &entity, 1,
            &value, sizeof(value));
    }
    break;
    case EntityType_Item:
    {
#ifdef LOG_NETWORK_EVENTS
        LogMessage(
            "Recv Item update event for entity %u(%u)", entity, netEntityId);
#endif
        EntityId owner = NULL_ENTITY;
        MapNetEntityIdToEntityId(world, &event->item.owner, 1, &owner);
        ecs_SetComponent(world, InventorySlotOwnerComponentId, &entity, 1,
            &owner, sizeof(owner));

        ecs_SetComponent(world, InventorySlotIndexComponentId, &entity, 1,
            &event->item.slotIdx, sizeof(event->item.slotIdx));

        ecs_SetComponent(world, ItemQuantityComponentId, &entity, 1,
            &event->item.itemQuantity, sizeof(event->item.itemQuantity));
    }
    break;
    case EntityType_Enemy:
    {
#ifdef LOG_NETWORK_EVENTS
        LogMessage(
            "Recv Enemy update event for entity %u(%u)", entity, netEntityId);
#endif
        ecs_SetComponent(world, EulerAnglesComponentId, &entity, 1,
            &event->enemy.rotation, sizeof(event->enemy.rotation));

        quat rotation =
            FromEulerAngles(Vec3(0.0f, event->enemy.rotation.y, 0.0f));
        ecs_SetComponent(world, RotationComponentId, &entity, 1, &rotation,
            sizeof(rotation));

        ecs_SetComponent(world, PositionComponentId, &entity, 1,
            &event->enemy.position, sizeof(event->enemy.position));
        ecs_SetComponent(world, VelocityComponentId, &entity, 1,
            &event->enemy.velocity, sizeof(event->enemy.velocity));

        ecs_SetComponent(world, HealthComponentId, &entity, 1,
            &event->enemy.health, sizeof(event->enemy.health));

        b32 value = true;
        ecs_SetComponent(world, IsWorldTransformDirtyComponentId, &entity, 1,
            &value, sizeof(value));
    }
    break;
    default:
        InvalidCodePath();
        break;
    }
}

internal void ProcessGameUpdatePacketFromServer(
    ecs_EntityWorld *world, GameUpdatePacket *packetData)
{
    u32 cursor = 0;
    while (cursor < packetData->eventDataLength)
    {
        EntityEventHeader *header =
            (EntityEventHeader *)(packetData->eventData + cursor);


        // Check that we won't read passed eventData buffer
        Assert(cursor + sizeof(*header) + header->length <=
               packetData->eventDataLength);

        switch (header->type)
        {
        case EntityEventType_Create:
        {
            Assert(header->length == sizeof(CreateEntityEvent));
            cursor += sizeof(*header) + header->length;

            CreateEntityEvent *event = (CreateEntityEvent*)(header + 1);
            ProcessCreateEntityEvent(
                world, event, header->netEntityId);
        }
        break;
        case EntityEventType_Update:
        {
            Assert(header->length == sizeof(UpdateEntityEvent));
            cursor += sizeof(*header) + header->length;

            UpdateEntityEvent *event = (UpdateEntityEvent*)(header + 1);
            ProcessUpdateEntityEvent(world, event, header->netEntityId);
        }
        break;
        case EntityEventType_Delete:
        {
            cursor += sizeof(*header);

            EntityId entityId = NULL_ENTITY;
            MapNetEntityIdToEntityId(world, &header->netEntityId, 1, &entityId);
#ifdef LOG_NETWORK_EVENTS
            LogMessage("Recv deletion event for entity %u(%u)",
                entityId, header->netEntityId);
#endif
            ecs_QueueEntitiesForDeletion(world, &entityId, 1);
        }
        break;
        default:
            InvalidCodePath();
            break;
        }
    }
}
