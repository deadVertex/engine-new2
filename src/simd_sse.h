#pragma once

#ifdef PLATFORM_WINDOWS
#include <intrin.h>
#elif PLATFORM_LINUX
#include <x86intrin.h>
#endif

struct simd_u32
{
    __m128i v;
};

struct simd_f32
{
    __m128 v;
};

union simd_vec3
{
    struct
    {
        simd_f32 x;
        simd_f32 y;
        simd_f32 z;
    };

    simd_f32 data[3];
};

inline simd_f32 LengthSq(simd_vec3 a);
inline simd_vec3 Normalize(simd_vec3 v);

inline simd_f32 SIMD_F32(f32 a)
{
    simd_f32 result = {_mm_set_ps1(a)};
    return result;
}

inline simd_f32 SIMD_F32(simd_u32 a)
{
    simd_f32 result = {_mm_cvtepi32_ps(a.v)};
    return result;
}

inline simd_u32 SIMD_U32(u32 a)
{
    simd_u32 result = {_mm_set1_epi32(a)};
    return result;
}

inline simd_u32 SIMD_U32(u32 a, u32 b, u32 c, u32 d, u32 e, u32 f, u32 g, u32 h)
{
    simd_u32 result = {_mm_set_epi32(a, b, c, d)};
    return result;
}

inline simd_f32 operator+(simd_f32 a, simd_f32 b)
{
    simd_f32 result = {_mm_add_ps(a.v, b.v)};
    return result;
}

inline simd_f32 operator-(simd_f32 a, simd_f32 b)
{
    simd_f32 result = {_mm_sub_ps(a.v, b.v)};
    return result;
}

inline simd_f32 operator-(simd_f32 a)
{
    simd_f32 result = {_mm_sub_ps(_mm_setzero_ps(), a.v)};
    return result;
}


inline simd_f32 operator*(simd_f32 a, simd_f32 b)
{
    simd_f32 result = {_mm_mul_ps(a.v, b.v)};
    return result;
}

inline simd_f32 operator/(simd_f32 a, simd_f32 b)
{
    simd_f32 result = {_mm_div_ps(a.v, b.v)};
    return result;
}

inline simd_u32 operator<<(simd_u32 a, u32 b)
{
    simd_u32 result = {_mm_slli_epi32(a.v, b)};
    return result;
}

inline simd_u32 operator>>(simd_u32 a, u32 b)
{
    simd_u32 result = {_mm_srli_epi32(a.v, b)};
    return result;
}

inline simd_u32& operator^=(simd_u32 &a, simd_u32 b)
{
    a.v = _mm_xor_si128(a.v, b.v);
    return a;
}

inline simd_f32 Min(simd_f32 a, simd_f32 b)
{
    simd_f32 result = {_mm_min_ps(a.v, b.v)};
    return result;
}

inline simd_f32 Max(simd_f32 a, simd_f32 b)
{
    simd_f32 result = {_mm_max_ps(a.v, b.v)};
    return result;
}

inline simd_f32 Sqrt(simd_f32 a)
{
    simd_f32 result = {_mm_sqrt_ps(a.v)};
    return result;
}

inline simd_u32 operator>(simd_f32 a, simd_f32 b)
{
    simd_u32 result = {_mm_castps_si128(_mm_cmpgt_ps(a.v, b.v))};
    return result;
}

inline simd_u32 operator<(simd_f32 a, simd_f32 b)
{
    simd_u32 result = {_mm_castps_si128(_mm_cmplt_ps(a.v, b.v))};
    return result;
}

inline simd_vec3 SafeNormalize(simd_vec3 a)
{
    simd_u32 mask = (LengthSq(a) > SIMD_F32(EPSILON));
    simd_vec3 result = Normalize(a);
    result.x.v = _mm_and_ps(result.x.v, _mm_castsi128_ps(mask.v));
    result.y.v = _mm_and_ps(result.y.v, _mm_castsi128_ps(mask.v));
    result.z.v = _mm_and_ps(result.z.v, _mm_castsi128_ps(mask.v));

    return result;
}

inline simd_u32 operator&(simd_u32 a, simd_u32 b)
{
    simd_u32 result = {_mm_and_si128(a.v, b.v)};
    return result;
}

inline u32 HorizontalAdd(simd_u32 a)
{
    // Not great
    u32 result =
        *((u32 *)&a + 0) + *((u32 *)&a + 1) + *((u32 *)&a + 2) + *((u32 *)&a + 3);
    return result;
}

inline simd_f32 Abs(simd_f32 a)
{
    // Create mask which only has the 31st bit set
    simd_f32 signMask = {_mm_castsi128_ps(_mm_set1_epi32(1<<31))};

    // Negate mask and AND it with the input value to clear the sign bit
    simd_f32 result = {_mm_andnot_ps(signMask.v, a.v)};
    return result;
}

inline void ConditionalAssign(simd_u32 *a, simd_u32 b, simd_u32 mask)
{
    // *a = (mask & b) | (~mask & a);
    a->v = _mm_or_si128(_mm_and_si128(mask.v, b.v), _mm_andnot_si128(mask.v, a->v));
}

inline void ConditionalAssign(simd_f32 *a, simd_f32 b, simd_u32 mask)
{
    // *a = (mask & b) | (~mask & a);
    a->v = _mm_or_ps(_mm_and_ps(_mm_castsi128_ps(mask.v), b.v),
        _mm_andnot_ps(_mm_castsi128_ps(mask.v), a->v));
}

inline simd_u32 operator>(simd_u32 a, simd_u32 b)
{
    simd_u32 result = {_mm_cmpgt_epi32(a.v, b.v)};
    return result;
}

inline simd_vec3 GatherVec3_(void *base, size_t offset, size_t stride, simd_u32 index)
{
    simd_vec3 result;
    vec3 a = *(vec3*)((u8 *)base + offset + stride * *((u32 *)&index.v + 0));
    vec3 b = *(vec3*)((u8 *)base + offset + stride * *((u32 *)&index.v + 1));
    vec3 c = *(vec3*)((u8 *)base + offset + stride * *((u32 *)&index.v + 2));
    vec3 d = *(vec3*)((u8 *)base + offset + stride * *((u32 *)&index.v + 3));

    result.x.v = _mm_set_ps(d.x, c.x, b.x, a.x);
    result.y.v = _mm_set_ps(d.y, c.y, b.y, a.y);
    result.z.v = _mm_set_ps(d.z, c.z, b.z, a.z);

    return result;
}

inline simd_f32 GatherF32_(void *base, size_t offset, size_t stride, simd_u32 index)
{
    f32 a = *(f32*)((u8 *)base + offset + stride * *((u32 *)&index.v + 0));
    f32 b = *(f32*)((u8 *)base + offset + stride * *((u32 *)&index.v + 1));
    f32 c = *(f32*)((u8 *)base + offset + stride * *((u32 *)&index.v + 2));
    f32 d = *(f32*)((u8 *)base + offset + stride * *((u32 *)&index.v + 3));

    simd_f32 result = {_mm_set_ps(d, c, b, a)};
    return result;
}

inline vec3 HorizontalAdd(simd_vec3 a)
{
    // Not great
    f32 *x = (f32 *)&a + 0;
    f32 *y = (f32 *)&a + 4;
    f32 *z = (f32 *)&a + 8;
    vec3 result;
    result.x = x[0] + x[1] + x[2] + x[3];
    result.y = y[0] + y[1] + y[2] + y[3];
    result.z = z[0] + z[1] + z[2] + z[3];
    return result;
}

inline simd_u32 operator==(simd_u32 a, simd_u32 b)
{
    simd_u32 result = {_mm_cmpeq_epi32(a.v, b.v)};
    return result;
}
