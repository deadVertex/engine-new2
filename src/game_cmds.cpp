struct RpcParameter
{
    const char *name;
    u32 type;
    union
    {
        u32 u;
        f32 f;
        vec3 v3;
        quat q;
    } value;
};

struct Rpc
{
    const char *functionName;
    u32 parameterCount;
    RpcParameter parameters[8];
};

struct RpcParameterSpec
{
    const char *name;
    u32 type;
    b32 isOptional;
};

struct RpcSpec
{
    const char *name;
    u32 parameterCount;
    RpcParameterSpec parameters[8];
};

inline void SendRpcToServer(Rpc *rpc);

internal i32 Cmd_Echo(u32 argc, const char **argv)
{
    for (u32 i = 1; i < argc; ++i)
    {
        LogConsole(argv[i]);
    }

    return 0;
}

internal i32 Cmd_ListCommands(Cmd_System *cmdSystem)
{
    for (u32 i = 0; i < cmdSystem->count; ++i)
    {
        LogConsole(cmdSystem->commands[i].name);
    }

    return 0;
}

// FIXME: Replace atof with our own implementation
internal void Cmd_Set(CVarTable *table, u32 argc, const char **argv)
{
    if (argc == 1)
    {
        LogConsole("Must provide the name of the cvar to set");
        return;
    }

    if (argc == 2)
    {
        LogConsole("Must provide a value to set cvar to");
        return;
    }

    if (argc > 3)
    {
        LogConsole("Too many arguments given");
        return;
    }

    const char *name = argv[1];
    const char *value = argv[2];

    u32 index = 0;
    if (FindCVarByName(table, name, &index))
    {
        CVarValue *cvar = table->values + index;
        if (cvar->dataType == DataType_F32)
        {
            cvar->f = (f32)atof(value);
        }
        else if (cvar->dataType == DataType_U32)
        {
            cvar->u = (u32)atol(value);
        }
        else
        {
            LogConsole("Don't support cvars which are not of data type f32 or u32");
        }
    }
    else
    {
        LogConsole("Could not find cvar with name \"%s\"", name);
    }
}

internal void Cmd_Print(CVarTable *table, u32 argc, const char **argv)
{
    if (argc == 1)
    {
        LogConsole("Must provide the name of the cvar to set");
        return;
    }

    if (argc > 2)
    {
        LogConsole("Too many arguments given");
        return;
    }

    const char *name = argv[1];

    u32 index = 0;
    if (FindCVarByName(table, name, &index))
    {
        CVarValue *cvar = table->values + index;
        switch (cvar->dataType)
        {
        case DataType_F32:
            LogConsole("%g", cvar->f);
            break;
        case DataType_U32:
            LogConsole("%u", cvar->u);
            break;
        default:
            LogConsole("Unsupported cvar data type.");
            break;
        }
    }
    else
    {
        LogConsole("Could not find cvar with name \"%s\", name");
    }
}

internal i32 Cmd_ListCvars(CVarTable *table)
{
    for (u32 i = 0; i < table->count; ++i)
    {
        LogConsole(table->names[i]);
    }
    return 0;
}

enum
{
    CollisionDebugFilter_Aabb = 0x1,
    CollisionDebugFilter_Triangle = 0x2,
    CollisionDebugFilter_Sphere = 0x4,
    CollisionDebugFilter_Capsule = 0x8,
    CollisionDebugFilter_All = 0x15,
};

internal i32 Cmd_CollisionDebugInfo(
    CollisionDebugState *state, i32 argc, const char **argv)
{
    u32 filter = CollisionDebugFilter_All;
    if (argc > 1)
    {
        if (StringCompare(argv[1], "triangle"))
        {
            filter = CollisionDebugFilter_Triangle;
        }
        else if (StringCompare(argv[1], "aabb"))
        {
            filter = CollisionDebugFilter_Aabb;
        }
        else if (StringCompare(argv[1], "sphere"))
        {
            filter = CollisionDebugFilter_Sphere;
        }
        else if (StringCompare(argv[1], "capsule"))
        {
            filter = CollisionDebugFilter_Capsule;
        }
        else
        {
            LogConsole("Unknown filter %s", argv[1]);
        }
    }

    LogConsole("INDEX\tTYPE\tSTART\tEND");
    for (u32 i = 0; i < state->count; ++i)
    {
        CollisionDebugCall *call = state->calls + i;
        const char *type = "Unknown";
        vec3 start = call->start;
        vec3 end = call->end;
        b32 printLine = false;
        switch (call->type)
        {
            case CollisionDebugCall_RayIntersectAabb:
                type = "Ray Intersect AABB";
                printLine = filter & CollisionDebugFilter_Aabb;
                break;
            case CollisionDebugCall_RayIntersectTriangle:
                type = "Ray Intersect Triangle";
                printLine = filter & CollisionDebugFilter_Triangle;
                break;
            case CollisionDebugCall_RayIntersectSphere:
                type = "Ray Intersect Sphere";
                printLine = filter & CollisionDebugFilter_Sphere;
                break;
            case CollisionDebugCall_RayIntersectCapsule:
                type = "Ray Intersect Capsule";
                printLine = filter & CollisionDebugFilter_Capsule;
                break;
            case CollisionDebugCall_SphereSweepSpheres:
                type = "Sphere Sweep Spheres";
                printLine = filter & CollisionDebugFilter_Sphere;
                break;
            case CollisionDebugCall_SphereSweepCapsules:
                type = "Sphere Sweep Capsules";
                printLine = filter & CollisionDebugFilter_Capsule;
                break;
            case CollisionDebugCall_SphereSweepTriangles:
                type = "Sphere Sweep Triangles";
                printLine = filter & CollisionDebugFilter_Triangle;
                break;
            default:
                InvalidCodePath();
                break;
        }

        if (printLine)
        {
            LogConsole("%u\t%s\t(%g, %g, %g)\t(%g, %g, %g)", i, type,
                    start.x, start.y, start.z, end.x, end.y, end.z);
        }
    }
    LogConsole("Recorded function calls: %u/%u",
            state->count, ArrayCount(state->calls));
    MemoryArena *arena = &state->memoryArena;
    LogConsole("Memory usage: %.2f%%",
        (arena->capacity > 0.0f)
            ? ((f32)arena->size / (f32)arena->capacity) * 100.0f
            : 0.0f);

    return 0;
}

internal i32 Cmd_GetPlatformData(GameMemory *memory)
{
    PlatformData data;
    memory->getPlatformData(&data);

    LogConsole("client_running: %s\nserver_running: %s\nwindow_mode: %u\n"
               "vsync_enabled: %s\nframerate_cap: %u\nframebuffer_width: "
               "%u\nframebuffer_height: %u\n",
        BoolToString(data.isClientRunning), BoolToString(data.isServerRunning),
        data.windowMode, BoolToString(data.vSyncEnabled), data.frameRateCap,
        data.frameBufferWidth, data.frameBufferHeight);

    return 0;
}

internal i32 Cmd_Quit(GameMemory *memory)
{
    PlatformData data;
    memory->getPlatformData(&data);
    data.isRunning = false;
    memory->setPlatformData(&data);

    return 0;
}

internal i32 Cmd_ServerExec(GameMemory *memory, u32 argc, const char **argv)
{
    if (argc == 1)
    {
        LogConsole("Must provide a command to send to server");
        return 1;
    }

    for (u32 i = 1; i < argc; ++i)
    {
        char *dst = (char *)memory->reliableOutgoingData +
                    memory->reliableOutgoingDataLength;
        u32 capacity = sizeof(memory->reliableOutgoingData) -
                       memory->reliableOutgoingDataLength;
        u32 length = StringCopy(dst, capacity, argv[i]);
        dst[length] = (i + 1 == argc) ? ';' : ' ';
        memory->reliableOutgoingDataLength += length + 1;
    }

    return 0;
}

internal i32 Cmd_EcsGet(ecs_EntityWorld *world, u32 argc, const char **argv)
{
    if (argc < 3)
    {
        LogConsole("Must provide component name and entity");
        return 1;
    }

    const char *component = argv[1];
    EntityId entity = (u32)atol(argv[2]);

    ComponentMetaData *metaData = FindComponentMetaDataByName(world, component);
    if (metaData != NULL)
    {
        ecs_ComponentTable *table = world->tables + metaData->id;
        u32 valueIndex = 0;
        if (BinarySearchTable(table, entity, &valueIndex))
        {
            u8 *value = (u8 *)table->values + valueIndex * table->objectSize;

            u32 dataType = metaData->dataType;
            switch (dataType)
            {
            case DataType_U32:
            {
                u32 v = *(u32 *)value;
                LogConsole("%u", v);
            }
            break;
            case DataType_F32:
            {
                f32 v = *(f32 *)value;
                LogConsole("%g", v);
            }
            break;
            case DataType_Vec3:
            {
                vec3 v = *(vec3 *)value;
                LogConsole("(%g, %g, %g)", v.x, v.y, v.z);
            }
            break;
            case DataType_Quat:
            {
                quat v = *(quat *)value;
                LogConsole("(%g, %g, %g, %g)", v.x, v.y, v.z, v.w);
            }
            break;
            default:
            {
                LogConsole("NULL");
            }
            break;
            }
        }
        else
        {
            LogConsole(
                "Entity %u does not have an entry in the %s component table",
                entity, component);
            return 1;
        }
    }
    else
    {
        LogConsole("Invalid component name");
        return 1;
    }

    return 0;
}

internal i32 Cmd_EcsSet(ecs_EntityWorld *world, u32 argc, const char **argv)
{
    if (argc < 4)
    {
        LogConsole("Must provide component name, entity and new component value");
        return 1;
    }

    const char *component = argv[1];
    EntityId entity = (u32)atol(argv[2]);

    ComponentMetaData *metaData = FindComponentMetaDataByName(world, component);
    if (metaData != NULL)
    {
        ecs_ComponentTable *table = world->tables + metaData->id;
        u32 valueIndex = 0;
        if (BinarySearchTable(table, entity, &valueIndex))
        {
            u8 *value = (u8 *)table->values + valueIndex * table->objectSize;

            u32 dataType = metaData->dataType;
            switch (dataType)
            {
            case DataType_U32:
            {
                *(u32 *)value = (u32)atol(argv[3]);
            }
            break;
            case DataType_F32:
            {
                *(f32 *)value = (f32)atof(argv[3]);
            }
            break;
            case DataType_Vec3:
            {
                if (argc < 6)
                {
                    LogConsole("Must provide all 3 axis values for a vec3");
                    return 1;
                }
                vec3 *v = (vec3 *)value;
                v->x = (f32)atof(argv[3]);
                v->y = (f32)atof(argv[4]);
                v->z = (f32)atof(argv[5]);
            }
            break;
            case DataType_Quat:
            {
                if (argc < 7)
                {
                    LogConsole("Must provide all 4 axis values for a quat");
                    return 1;
                }
                quat *v = (quat *)value;
                v->x = (f32)atof(argv[3]);
                v->y = (f32)atof(argv[4]);
                v->z = (f32)atof(argv[5]);
                v->w = (f32)atof(argv[6]);
                *v = Normalize(*v);
            }
            break;
            default:
            {
                LogConsole("Component does not store values");
                return 1;
            }
            break;
            }
        }
        else
        {
            LogConsole(
                "Entity %u does not have an entry in the %s component table",
                entity, component);
            return 1;
        }
    }
    else
    {
        LogConsole("Invalid component name");
        return 1;
    }

    return 0;
}

internal i32 Cmd_EcsDumpTable(ecs_EntityWorld *world, u32 argc, const char **argv)
{
    if (argc == 1)
    {
        LogConsole("Must provide component name");
        return 1;
    }

    ComponentMetaData *metaData = FindComponentMetaDataByName(world, argv[1]);
    if (metaData != NULL)
    {
        LogConsole("Entity Component Table: %s", metaData->name);
        ecs_ComponentTable *table = world->tables + metaData->id;
        for (u32 i = 0; i < table->count; ++i)
        {
            EntityId entityId = table->keys[i];
            u8 *value = (u8 *)table->values + i * table->objectSize;

            u32 dataType = metaData->dataType;
            switch (dataType)
            {
            case DataType_U32:
            {
                u32 v = *(u32 *)value;
                LogConsole("%u\t%u", entityId, v);
            }
            break;
            case DataType_F32:
            {
                f32 v = *(f32 *)value;
                LogConsole("%u\t%g", entityId, v);
            }
            break;
            case DataType_Vec3:
            {
                vec3 v = *(vec3 *)value;
                LogConsole("%u\t(%g, %g, %g)", entityId, v.x, v.y, v.z);
            }
            break;
            case DataType_Quat:
            {
                quat v = *(quat *)value;
                LogConsole(
                    "%u\t(%g, %g, %g, %g)", entityId, v.x, v.y, v.z, v.w);
            }
            break;
            case DataType_Aabb:
            {
                BoundingBox aabb = *(BoundingBox *)value;
                LogConsole("%u\t(%g, %g, %g) - (%g, %g, %g)", entityId,
                    aabb.min.x, aabb.min.y, aabb.min.z, aabb.max.x, aabb.max.y,
                    aabb.max.z);
            }
            break;
            default:
            {
                LogConsole("%u", entityId);
            }
            break;
            }
        }
    }
    else
    {
        LogConsole("Invalid component id");
    }

    return 0;
}

internal i32 Cmd_AddItem(ecs_EntityWorld *world, u32 argc, const char **argv)
{
    if (argc < 3)
    {
        LogConsole("Must provide item id and owner entity id");
        return 1;
    }
    u32 itemId = (u32)atol(argv[1]);
    EntityId owner = (EntityId)atol(argv[2]);

    AddItemToInventory(world, owner, itemId);

    return 0;
}

internal i32 Cmd_SaveMap(ecs_EntityWorld *world, GameMemory *memory, MemoryArena *tempArena)
{
    u8 *buffer = AllocateArray(tempArena, u8, Megabytes(4));
    u32 length = ecs_SerializeWorld(world, buffer, Megabytes(4));

    memory->writeEntireFile("./test_map", buffer, length);

    LogConsole("Map saved to ./test_map");

    return 0;
}

internal i32 Cmd_ListComponents(ecs_EntityWorld *world)
{
    LogConsole("INDEX\tCOMPONENT ID\tCOMPONENT NAME\tDATA TYPE");
    for (u32 i = 0; i < ArrayCount(world->componentMetaData); ++i)
    {
        ComponentMetaData metaData = world->componentMetaData[i];
        LogConsole("%u\t0x%X\t%s\t%s", i, metaData.id, metaData.name,
            GetDataTypeName(metaData.dataType));
    }

    return 0;
}

internal i32 Cmd_ListPrefabs(ecs_EntityWorld *world)
{
    LogConsole("Name\tComponentCount");
    for (u32 i = 0; i < ArrayCount(world->prefabs); ++i)
    {
        EntityPrefab *prefab = world->prefabs + i;
        LogConsole("%s\t%u", prefab->name, prefab->count);
    }
    return 0;
}

inline RpcParameter *GetParmeter(Rpc *rpc, const char *name)
{
    RpcParameter *result = NULL;
    for (u32 i = 0; i < rpc->parameterCount; ++i)
    {
        if (StringCompare(rpc->parameters[i].name, name))
        {
            result = rpc->parameters + i;
            break;
        }
    }

    return result;
}

internal b32 ValidateRpcSpec(RpcSpec *spec, Rpc *rpc)
{
    b32 result = true;
    for (u32 i = 0; i < spec->parameterCount; ++i)
    {
        RpcParameterSpec *expected = spec->parameters + i;
        RpcParameter *found = GetParmeter(rpc, expected->name);
        if (found != NULL)
        {
            if (found->type != expected->type)
            {
                result = false;
                break;
            }
        }
        else if (!expected->isOptional)
        {
            result = false;
        }
    }

    return result;
}

internal void SpawnEntityAtPositionRpc(ecs_EntityWorld *world, Rpc *rpc)
{
    RpcSpec spec = {};
    spec.name = "SpawnEntityAtPosition";
    spec.parameterCount = 2;
    spec.parameters[0].name = "prefabId";
    spec.parameters[0].type = DataType_U32;
    spec.parameters[1].name = "position";
    spec.parameters[1].type = DataType_Vec3;

    if (ValidateRpcSpec(&spec, rpc))
    {
        u32 prefabId = GetParmeter(rpc, "prefabId")->value.u;
        vec3 position = GetParmeter(rpc, "position")->value.v3;

        Assert(prefabId < ArrayCount(world->prefabs));
        EntityPrefab *prefab = world->prefabs + prefabId;

        EntityId entityId = SpawnEntityAtPosition(world, prefab, position);
    }
    else
    {
        LogMessage("ValidateRpcSpec failed");
    }
}

internal void Cmd_SpawnEntityAtPosition(
    ecs_EntityWorld *world, u32 argc, const char **argv)
{
    // TODO: Argument parser object
    if (argc == 5)
    {
        EntityPrefab *prefab = GetEntityPrefabByName(world, argv[1]);
        if (prefab != NULL)
        {
            vec3 position = Vec3(
                (f32)atof(argv[2]), (f32)atof(argv[3]), (f32)atof(argv[4]));
            Rpc rpc;
            rpc.functionName = "SpawnEntityAtPosition";
            rpc.parameterCount = 2;
            rpc.parameters[0].name = "prefabId";
            rpc.parameters[0].type = DataType_U32;
            rpc.parameters[0].value.u = GetEntityPrefabId(world, argv[1]);
            rpc.parameters[1].name = "position";
            rpc.parameters[1].type = DataType_Vec3;
            rpc.parameters[1].value.v3 = position;
            SpawnEntityAtPositionRpc(world, &rpc);
            SendRpcToServer(&rpc);
            //EntityId entityId = SpawnEntityAtPosition(world, prefab, position);
        }
        else
        {
            LogConsole("Invalid prefab \"%s\"", argv[1]);
        }
    }
    else
    {
        LogConsole("Invalid number of arguments, expected 5");
    }
}

internal void com_HandleCommand(CommandExecutionEvent *event,
    Cmd_System *commandSystem, CVarTable *cvarTable, GameMemory *memory,
    ecs_EntityWorld *world, MemoryArena *tempArena,
    CollisionDebugState *collisionDebugState)
{
    switch (event->commandId)
    {
        case CommandID_Echo:
            Cmd_Echo(event->argc, event->argv);
            break;
        case CommandID_ListCommands:
            Cmd_ListCommands(commandSystem);
            break;
        case CommandID_Set:
            Cmd_Set(cvarTable, event->argc, event->argv);
            break;
        case CommandID_Print:
            Cmd_Print(cvarTable, event->argc, event->argv);
            break;
        case CommandID_ListCvars:
            Cmd_ListCvars(cvarTable);
            break;
        case CommandID_GetPlatformData:
            Cmd_GetPlatformData(memory);
            break;
        case CommandID_Quit:
            Cmd_Quit(memory);
            break;
        case CommandID_EcsDumpTable:
            Cmd_EcsDumpTable(world, event->argc, event->argv);
            break;
        case CommandID_EcsGet:
            Cmd_EcsGet(world, event->argc, event->argv);
            break;
        case CommandID_EcsSet:
            Cmd_EcsSet(world, event->argc, event->argv);
            break;
        case CommandID_SaveMap:
            Cmd_SaveMap(world, memory, tempArena);
            break;
        case CommandID_CollisionDebugInfo:
            Cmd_CollisionDebugInfo(collisionDebugState, event->argc, event->argv);
            break;
        case CommandID_ListComponents:
            Cmd_ListComponents(world);
            break;
        case CommandID_ListPrefabs:
            Cmd_ListPrefabs(world);
            break;
        case CommandID_SpawnEntityAtPosition:
            Cmd_SpawnEntityAtPosition(world, event->argc, event->argv);
            break;
        default:
            LogConsole("Unknown command '%s'", event->argv[0]);
            break;
    }
}

