#include "opengl.h"

global OpenGL_Mesh g_Meshes[MAX_MESHES];
global OpenGL_Shader g_Shaders[MAX_SHADERS];
global OpenGL_Texture g_Textures[MAX_TEXTURES];
global OpenGL_VertexBuffer g_VertexBuffers[MAX_VERTEX_BUFFERS];

global OpenGL_UniformBuffer g_UniformBuffers[MAX_UNIFORM_BUFFERS];
global OpenGL_RenderTarget g_RenderTargets[MAX_RENDER_TARGETS];

internal OpenGL_Info OpenGL_GetInfo()
{
    OpenGL_Info result = {};

    result.vendor = (const char *)glGetString(GL_VENDOR);
    result.renderer = (const char *)glGetString(GL_RENDERER);
    result.version = (const char *)glGetString(GL_VERSION);
    result.shadingLanguageVersion = (const char *)glGetString(GL_SHADING_LANGUAGE_VERSION);
    result.extensions = (const char *)glGetString(GL_EXTENSIONS);

    return result;
}

internal b32 OpenGL_CompileShader(
    GLuint shader, const char *src, b32 isVertex, const char *defines = "",
    const char **includes = NULL, u32 includeCount = 0)
{
    const char *sources[256] = {0};
    Assert(includeCount < ArrayCount(sources) - 5);

    // TODO: Expose GLSL shader version as a platform define
    const char *version = "#version 330\n";
    const char *typeDefine =
        isVertex ? "#define VERTEX_SHADER\n" : "#define FRAGMENT_SHADER\n";
    const char *line = "#line 1\n";

    sources[0] = version;
    sources[1] = typeDefine;
    sources[2] = defines;
    u32 sourceCount = 3;
    for (u32 i = 0; i < includeCount; ++i)
    {
        sources[sourceCount++] = includes[i];
    }
    sources[sourceCount++] = line;
    sources[sourceCount++] = src;

    glShaderSource(shader, sourceCount, sources, NULL);
    glCompileShader(shader);

    i32 success;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
    if (!success) // Check if the shader was compiled successfully.
    {
        i32 logLength;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLength);

        char log[8192];
        Assert((u32)logLength < ArrayCount(log));

        i32 len = 0;
        glGetShaderInfoLog(shader, logLength, &len, log);

        if (isVertex)
        {
            LogMessage("[VERTEX]\n%s", log);
        }
        else
        {
            LogMessage("[FRAGMENT]\n%s", log);
        }
        return false;
    }
    return true;
}

internal b32 OpenGL_LinkShader(GLuint vertex, GLuint fragment, GLuint program)
{
    glAttachShader(program, vertex);
    glAttachShader(program, fragment);
    glLinkProgram(program);

    i32 success;
    glGetProgramiv(program, GL_LINK_STATUS, &success);
    if (!success) // Check if the shader was linked successfully.
    {
        i32 logLength;
        glGetShaderiv(program, GL_INFO_LOG_LENGTH, &logLength);

        char log[4096];
        i32 len = 0;
        glGetProgramInfoLog(program, logLength, &len, log);
        LogMessage("[LINKER]\n%s", log);
        return false;
    }
    return true;
}

internal void OpenGL_DeleteShader(GLuint program)
{
    Assert(program != 0);
    glUseProgram(0);
    GLsizei count = 0;
    GLuint shaders[2];
    // Need to retrieve the vertex and fragment shaders and delete them
    // explicitly, deleting the shader program only detaches the two
    // shaders, it does not free the resources for them.
    glGetAttachedShaders(program, 2, &count, shaders);
    glDeleteProgram(program);
    glDeleteShader(shaders[0]);
    glDeleteShader(shaders[1]);
}

internal GLuint OpenGL_CreateShader(const char *shaderSource,
    const char *defines = "", const char **includes = NULL, u32 includeCount = 0)
{
    GLuint vertex = glCreateShader(GL_VERTEX_SHADER);
    GLuint fragment = glCreateShader(GL_FRAGMENT_SHADER);
    GLuint program = glCreateProgram();

    b32 vertexSuccess = OpenGL_CompileShader(
        vertex, shaderSource, true, defines, includes, includeCount);
    b32 fragmentSuccess = OpenGL_CompileShader(
        fragment, shaderSource, false, defines, includes, includeCount);

    if (vertexSuccess && fragmentSuccess)
    {
        if (OpenGL_LinkShader(vertex, fragment, program))
        {
            return program;
        }
    }

    if (program != 0)
    {
        LogMessage("Failed to compile shader");
        glUseProgram(0);
        glDeleteProgram(program);
        glDeleteShader(vertex);
        glDeleteShader(fragment);
    }

    return 0;
}

struct OpenGL_ColorFormat
{
    u32 format;
    u32 internalFormat;
    u32 dataType;
};

OpenGL_ColorFormat OpenGL_GetColorFormat(u32 colorFormat)
{
    OpenGL_ColorFormat result = {};
    switch (colorFormat)
    {
        case ColorFormat_RGBA8:
            result.format = GL_RGBA;
            result.internalFormat = GL_RGBA8;
            result.dataType = GL_UNSIGNED_BYTE;
            break;
        case ColorFormat_R16:
            result.format = GL_RED;
            result.internalFormat = GL_R16;
            result.dataType = GL_UNSIGNED_SHORT;
            break;
        case ColorFormat_R8:
            result.format = GL_RED;
            result.internalFormat = GL_R8;
            result.dataType = GL_UNSIGNED_BYTE;
            break;
        default:
            InvalidCodePath();
    }

    return result;
}

internal u32 OpenGL_CreateTexture(
    void *pixels, u32 width, u32 height, u32 colorFormat, u32 samplerMode)
{
    u32 result = 0;
    glGenTextures(1, &result);
    glBindTexture(GL_TEXTURE_2D, result);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    OpenGL_ColorFormat textureFormat = OpenGL_GetColorFormat(colorFormat);

    glTexImage2D(GL_TEXTURE_2D, 0, textureFormat.internalFormat, width, height,
        0, textureFormat.format, textureFormat.dataType, pixels);

    if (samplerMode == TextureSamplerMode_Nearest)
    {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    }
    else if (samplerMode == TextureSamplerMode_Linear)
    {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    }
    else if (samplerMode == TextureSamplerMode_LinearMipMapping)
    {
        glTexParameteri(
            GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        glGenerateMipmap(GL_TEXTURE_2D);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 16);
    }
    else
    {
        InvalidCodePath();
    }

    glBindTexture(GL_TEXTURE_2D, 0);

    return result;
}

internal u32 OpenGL_CreateCubeMap(
    void *pixels[CUBE_MAP_MAX_FACES], u32 width, u32 colorFormat)
{
    u32 texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_CUBE_MAP, texture);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    OpenGL_ColorFormat textureFormat = OpenGL_GetColorFormat(colorFormat);

    // +Z
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0,
        textureFormat.internalFormat, width, width, 0, textureFormat.format,
        textureFormat.dataType, pixels[CUBE_MAP_FACE_Z_POS]);

    // -Z
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0,
        textureFormat.internalFormat, width, width, 0, textureFormat.format,
        textureFormat.dataType, pixels[CUBE_MAP_FACE_Z_NEG]);

    // +Y
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0,
        textureFormat.internalFormat, width, width, 0, textureFormat.format,
        textureFormat.dataType, pixels[CUBE_MAP_FACE_Y_POS]);

    // -Y
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0,
        textureFormat.internalFormat, width, width, 0, textureFormat.format,
        textureFormat.dataType, pixels[CUBE_MAP_FACE_Y_NEG]);

    // +X
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0,
        textureFormat.internalFormat, width, width, 0, textureFormat.format,
        textureFormat.dataType, pixels[CUBE_MAP_FACE_X_POS]);

    // -X
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0,
        textureFormat.internalFormat, width, width, 0, textureFormat.format,
        textureFormat.dataType, pixels[CUBE_MAP_FACE_X_NEG]);

    glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

    return texture;
}

internal OpenGL_UniformBuffer OpenGL_CreateUniformBuffer(u32 capacity)
{
    OpenGL_UniformBuffer result = {};
    result.capacity = capacity;
    glGenBuffers(1, &result.ubo);
    glBindBuffer(GL_UNIFORM_BUFFER, result.ubo);
    glBufferData(GL_UNIFORM_BUFFER, capacity, NULL, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);

    return result;
}

internal void OpenGL_DeleteUniformBuffer(OpenGL_UniformBuffer *buffer)
{
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
    glDeleteBuffers(1, &buffer->ubo);
}

void OpenGLReportErrorMessage(GLenum source, GLenum type, GLuint id,
                              GLenum severity, GLsizei length,
                              const GLchar *message, const void *userParam)
{
    const char *typeStr = nullptr;
    switch (type)
    {
    case GL_DEBUG_TYPE_ERROR:
        typeStr = "ERROR";
        break;
    case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
        typeStr = "DEPRECATED_BEHAVIOR";
        break;
    case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
        typeStr = "UNDEFINED_BEHAVIOR";
        break;
    case GL_DEBUG_TYPE_PORTABILITY:
        typeStr = "PORTABILITY";
        break;
    case GL_DEBUG_TYPE_PERFORMANCE:
        typeStr = "PERFORMANCE";
        break;
    case GL_DEBUG_TYPE_OTHER:
        typeStr = "OTHER";
        break;
    default:
        typeStr = "";
        break;
    }

    const char *severityStr = nullptr;
    switch (severity)
    {
    case GL_DEBUG_SEVERITY_LOW:
        severityStr = "LOW";
        break;
    case GL_DEBUG_SEVERITY_MEDIUM:
        severityStr = "MEDIUM";
        break;
    case GL_DEBUG_SEVERITY_HIGH:
        severityStr = "HIGH";
        break;
    default:
        severityStr = "";
        break;
    }

    LogMessage("OPENGL|%s:%s:%s", typeStr, severityStr, message);
}

internal void OpenGL_DeleteRenderTarget(OpenGL_RenderTarget renderTarget)
{
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    glDeleteFramebuffers(1, &renderTarget.fbo);
    glDeleteTextures(1, &renderTarget.texture);
}

internal OpenGL_RenderTarget OpenGL_CreateRenderTarget(
    u32 width, u32 height, b32 isShadowBuffer)
{
    OpenGL_RenderTarget result = {};
    result.width = width;
    result.height = height;
    glGenFramebuffers(1, &result.fbo);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, result.fbo);

    glGenTextures(1, &result.texture);
    glBindTexture(GL_TEXTURE_2D, result.texture);

    if (isShadowBuffer)
    {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT16, result.width,
                result.height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
        float borderColor[] = { 1.0f, 1.0f, 1.0f, 1.0f };
        glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor); 

        // Not very clear in OpenGL docs what this actually does, in theory it
        // should enable hardware PCF shadows which in theory are fetching 4 texels
        // per sample. Probably still need to do normal PCF in shader as this is
        // intended to be mainly used as an optimization rather than a replacement
        // for a software implementation.
        glTexParameteri(
                GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);

        glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                GL_TEXTURE_2D, result.texture, 0);

        glDrawBuffer(GL_NONE);
    }
    else
    {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, width, height, 0, GL_RGB,
            GL_FLOAT, NULL);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

        glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
            GL_TEXTURE_2D, result.texture, 0);

        glGenTextures(1, &result.depthTexture);
        glBindTexture(GL_TEXTURE_2D, result.depthTexture);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32, width, height, 0,
            GL_DEPTH_COMPONENT, GL_FLOAT, NULL);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

        glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
            GL_TEXTURE_2D, result.depthTexture, 0);

        GLenum buffer = GL_COLOR_ATTACHMENT0;
        glDrawBuffers(1, &buffer);
    }

    GLenum status = glCheckFramebufferStatus(GL_DRAW_FRAMEBUFFER);
    if (status != GL_FRAMEBUFFER_COMPLETE)
    {
        LogMessage("Framebuffer error, status: 0x%x", status);
        OpenGL_DeleteRenderTarget(result);
        ClearToZero(&result, sizeof(result));
    }

    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

    return result;
}

internal bool OpenGL_Init()
{
    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
    glDebugMessageCallback(OpenGLReportErrorMessage, NULL);
    GLuint unusedIds = 0;
    glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0,
                          &unusedIds, GL_TRUE);

    return true;
}

internal bool OpenGL_BindShader(
    OpenGL_Shader *shader, ShaderUniformValue *values, u32 valueCount)
{
    if (shader->program == 0)
    {
        return false;
    }

    if (shader->depthTestEnabled)
    {
        glEnable(GL_DEPTH_TEST);
    }
    else
    {
        glDisable(GL_DEPTH_TEST);
    }

    if (shader->alphaBlendingEnabled)
    {
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    }
    else
    {
        glDisable(GL_BLEND);
    }

    if (shader->faceCullingMode != FaceCulling_None)
    {
        glEnable(GL_CULL_FACE);
        switch (shader->faceCullingMode)
        {
            case FaceCulling_BackFace:
                glCullFace(GL_BACK);
                break;
            case FaceCulling_FrontFace:
                glCullFace(GL_FRONT);
                break;
            default:
                InvalidCodePath();
                break;
        }
    }
    else
    {
        glDisable(GL_CULL_FACE);
    }

    if (shader->lineModeEnabled)
    {
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    }
    else
    {
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    }

    glUseProgram(shader->program);

    u32 textureBindingCount = 0;
    for (u32 valueIdx = 0; valueIdx < valueCount; ++valueIdx)
    {
        ShaderUniformValue *value = values + valueIdx;

        // TODO: Use shader->uniformLocationCount
        Assert(value->id < ArrayCount(shader->uniformLocations));
        i32 location = shader->uniformLocations[value->id];

        switch (value->type)
        {
        case UniformType_Vec3:
            glUniform3fv(location, 1, value->v3.data);
            break;
        case UniformType_Vec4:
            glUniform4fv(location, 1, value->v4.data);
            break;
        case UniformType_Mat4:
            glUniformMatrix4fv(location, 1, GL_FALSE, value->m4.data);
            break;
        case UniformType_Texture:
        {
            Assert(value->texture < ArrayCount(g_Textures));
            OpenGL_Texture *texture = g_Textures + value->texture;
            glActiveTexture(GL_TEXTURE0 + textureBindingCount);
            glBindTexture(texture->type, texture->handle);
            glUniform1i(location, textureBindingCount);
            textureBindingCount++;
        } break;
        case UniformType_Buffer:
        {
            Assert(value->buffer < ArrayCount(g_UniformBuffers));
            u32 ubo = g_UniformBuffers[value->buffer].ubo;
            glBindBufferBase(GL_UNIFORM_BUFFER, location, ubo);
        } break;
        case UniformType_RenderTarget:
        {
            Assert(value->renderTarget < ArrayCount(g_RenderTargets));
            u32 renderTargetTexture = g_RenderTargets[value->renderTarget].texture;
            glActiveTexture(GL_TEXTURE0 + textureBindingCount);
            glBindTexture(GL_TEXTURE_2D, renderTargetTexture);
            glUniform1i(location, textureBindingCount);
            textureBindingCount++;
        } break;
        case UniformType_F32:
            glUniform1f(location, value->f);
            break;
        default:
            InvalidCodePath();
            break;
        }
    }

    return true;
}

inline u32 GetVertexSize(u32 layout)
{
    switch (layout)
    {
        case VertexLayout_Standard:
            return sizeof(Vertex);
        case VertexLayout_PositionAndColor:
            return sizeof(VertexPC);
        case VertexLayout_Skinning:
            return sizeof(VertexSkinning);
            break;
        default:
            InvalidCodePath();
            return 0;
    }
}

internal void ConfigureVertexLayout(u32 vertexLayout)
{
    switch (vertexLayout)
    {
    case VertexLayout_Standard:
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex),
            (void *)OffsetOf(Vertex, position));

        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex),
            (void *)OffsetOf(Vertex, normal));

        glEnableVertexAttribArray(2);
        glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex),
            (void *)OffsetOf(Vertex, tangent));

        glEnableVertexAttribArray(3);
        glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex),
            (void *)OffsetOf(Vertex, texCoord));
        break;
    case VertexLayout_PositionAndColor:
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(VertexPC),
            (void *)OffsetOf(VertexPC, position));

        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(VertexPC),
            (void *)OffsetOf(VertexPC, color));
        break;
    case VertexLayout_Skinning:
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(VertexSkinning),
            (void *)OffsetOf(VertexSkinning, position));

        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(VertexSkinning),
            (void *)OffsetOf(VertexSkinning, normal));

        glEnableVertexAttribArray(2);
        glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(VertexSkinning),
            (void *)OffsetOf(VertexSkinning, tangent));

        glEnableVertexAttribArray(3);
        glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, sizeof(VertexSkinning),
            (void *)OffsetOf(VertexSkinning, texCoord));

        glEnableVertexAttribArray(4);
        glVertexAttribIPointer(4, 1, GL_UNSIGNED_INT, sizeof(VertexSkinning),
            (void *)OffsetOf(VertexSkinning, boneId));
        break;
    default:
        InvalidCodePath();
        break;
    }
}

internal OpenGL_VertexBuffer OpenGL_CreateVertexBuffer(
    u32 vertexLayout, u32 vertexCount)
{
    OpenGL_VertexBuffer result = {};
    result.layout = vertexLayout;

    u32 capacity = GetVertexSize(vertexLayout) * vertexCount;
    result.capacity = capacity;

    glGenVertexArrays(1, &result.vao);
    glBindVertexArray(result.vao);

    glGenBuffers(1, &result.vbo);
    glBindBuffer(GL_ARRAY_BUFFER, result.vbo);
    glBufferData(GL_ARRAY_BUFFER, capacity, NULL, GL_DYNAMIC_DRAW);

    ConfigureVertexLayout(vertexLayout);

    glBindVertexArray(0);

    return result;
}

internal void OpenGL_DeleteVertexBuffer(OpenGL_VertexBuffer vertexBuffer)
{
    glBindVertexArray(0);
    glDeleteVertexArrays(1, &vertexBuffer.vao);
    glDeleteBuffers(1, &vertexBuffer.vbo);
}

// TODO: Why are we binding the vertex array here??!?!?
internal void OpenGL_UpdateVertexBuffer(
    OpenGL_VertexBuffer vertexBuffer, u32 layout, void *vertices, u32 count)
{
    Assert(layout == vertexBuffer.layout);
    u32 length = GetVertexSize(layout) * count;
    Assert(length < vertexBuffer.capacity);
    glBindVertexArray(vertexBuffer.vao);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer.vbo);

    glBufferSubData(GL_ARRAY_BUFFER, 0, length, vertices);
    glBindVertexArray(0);
}

internal void OpenGL_DrawFrame(
    RenderCommand *commands, u32 count, u32 windowWidth, u32 windowHeight)
{
    TIMED_BLOCK();

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glViewport(0, 0, windowWidth, windowHeight);
    glEnable(GL_DEPTH_TEST);

    // Handle all update and create commands before draw commands
    for (u32 i = 0; i < count; ++i)
    {
        RenderCommand *command = commands + i;
        switch (command->type)
        {
        case RenderCommand_CreateMeshTypeId:
        {
            Assert(command->createMesh.id < ArrayCount(g_Meshes));
            OpenGL_Mesh *mesh = g_Meshes + command->createMesh.id;

            if (mesh->vertexArray != 0)
            {
                glBindVertexArray(0);
                glDeleteVertexArrays(1, &mesh->vertexArray);
                glDeleteBuffers(1, &mesh->vertexBuffer);
                glDeleteBuffers(1, &mesh->indexBuffer);
                ClearToZero(mesh, sizeof(*mesh));
            }

            glGenVertexArrays(1, &mesh->vertexArray);
            glBindVertexArray(mesh->vertexArray);

            glGenBuffers(1, &mesh->vertexBuffer);
            glGenBuffers(1, &mesh->indexBuffer);

            u32 capacity = GetVertexSize(command->createMesh.vertexLayout) *
                           command->createMesh.vertexCount;
            glBindBuffer(GL_ARRAY_BUFFER, mesh->vertexBuffer);
            glBufferData(GL_ARRAY_BUFFER, capacity,
                command->createMesh.vertices, GL_STATIC_DRAW);

            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh->indexBuffer);
            glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                command->createMesh.indexCount * sizeof(u32),
                command->createMesh.indices, GL_STATIC_DRAW);
            mesh->indexCount = command->createMesh.indexCount;

            glBindBuffer(GL_ARRAY_BUFFER, mesh->vertexBuffer);
            ConfigureVertexLayout(command->createMesh.vertexLayout);

            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh->indexBuffer);

            glBindVertexArray(0);
        }
        break;
        case RenderCommand_CreateInstancedMeshTypeId:
        {
            Assert(command->createInstancedMesh.id < ArrayCount(g_Meshes));
            OpenGL_Mesh *mesh = g_Meshes + command->createInstancedMesh.id;

            if (mesh->vertexArray != 0)
            {
                glBindVertexArray(0);
                glDeleteVertexArrays(1, &mesh->vertexArray);
                glDeleteBuffers(1, &mesh->vertexBuffer);
                glDeleteBuffers(1, &mesh->indexBuffer);
                glDeleteBuffers(1, &mesh->modelMatrixBuffer);
                ClearToZero(mesh, sizeof(*mesh));
            }

            glGenVertexArrays(1, &mesh->vertexArray);
            glBindVertexArray(mesh->vertexArray);

            glGenBuffers(1, &mesh->vertexBuffer);
            glGenBuffers(1, &mesh->indexBuffer);
            glGenBuffers(1, &mesh->modelMatrixBuffer);

            glBindBuffer(GL_ARRAY_BUFFER, mesh->vertexBuffer);
            glBufferData(GL_ARRAY_BUFFER,
                command->createInstancedMesh.vertexCount * sizeof(Vertex),
                command->createInstancedMesh.vertices, GL_STATIC_DRAW);

            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh->indexBuffer);
            glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                command->createInstancedMesh.indexCount * sizeof(u32),
                command->createInstancedMesh.indices, GL_STATIC_DRAW);
            mesh->indexCount = command->createInstancedMesh.indexCount;

            glBindBuffer(GL_ARRAY_BUFFER, mesh->vertexBuffer);
            ConfigureVertexLayout(VertexLayout_Standard);

            glBindBuffer(GL_ARRAY_BUFFER, mesh->modelMatrixBuffer);
            glBufferData(GL_ARRAY_BUFFER,
                command->createInstancedMesh.maxInstanceCount * sizeof(mat4),
                NULL, GL_DYNAMIC_DRAW);
            mesh->maxInstanceCount = command->createInstancedMesh.maxInstanceCount;

            // NOTE: Assuming we only support VertexLayout_Standard
            glEnableVertexAttribArray(4);
            glVertexAttribPointer(
                4, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(vec4), (void *)0);
            glEnableVertexAttribArray(5);
            glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(vec4),
                (void *)(sizeof(vec4)));
            glEnableVertexAttribArray(6);
            glVertexAttribPointer(6, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(vec4),
                (void *)(2 * sizeof(vec4)));
            glEnableVertexAttribArray(7);
            glVertexAttribPointer(7, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(vec4),
                (void *)(3 * sizeof(vec4)));

            glVertexAttribDivisor(4, 1);
            glVertexAttribDivisor(5, 1);
            glVertexAttribDivisor(6, 1);
            glVertexAttribDivisor(7, 1);

            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh->indexBuffer);

            glBindVertexArray(0);
        }
        break;
        case RenderCommand_CreateShaderTypeId:
        {
            Assert(command->createShader.id < ArrayCount(g_Shaders));

            OpenGL_Shader *shader = g_Shaders + command->createShader.id;

            if (shader->program != 0)
            {
                OpenGL_DeleteShader(shader->program);
                ClearToZero(shader, sizeof(*shader));
            }

            const char *shaderSource = command->createShader.shaderSource;
            const char *defines = "";
            if (command->createShader.defines != NULL)
            {
                defines = command->createShader.defines;
            }
            shader->program = OpenGL_CreateShader(shaderSource, defines,
                command->createShader.includes,
                command->createShader.includeCount);

            if (shader->program != 0)
            {
                shader->depthTestEnabled =
                    command->createShader.depthTestEnabled;
                shader->alphaBlendingEnabled =
                    command->createShader.alphaBlendingEnabled;
                shader->lineModeEnabled = command->createShader.lineModeEnabled;
                shader->faceCullingMode = command->createShader.faceCullingMode;

                u32 uboCount = 0;

                for (u32 uniformIdx = 0;
                     uniformIdx < command->createShader.uniformDefinitionCount;
                     ++uniformIdx)
                {
                    ShaderUniformDefinitions *def =
                        command->createShader.uniformDefinitions + uniformIdx;

                    Assert(def->id < ArrayCount(shader->uniformLocations));
                    if (def->type == UniformType_Buffer)
                    {
                        i32 uniformLocation = uboCount++;
                        i32 uboIdx =
                            glGetUniformBlockIndex(shader->program, def->name);
                        Assert(uboIdx >= 0);
                        glUniformBlockBinding(
                            shader->program, uboIdx, uniformLocation);
                        shader->uniformLocations[def->id] = uniformLocation;
                    }
                    else
                    {
                        shader->uniformLocations[def->id] =
                            glGetUniformLocation(shader->program, def->name);
                    }
                }
            }
        }
        break;
        case RenderCommand_CreateTextureTypeId:
        {
            Assert(command->createTexture.id < ArrayCount(g_Textures));
            OpenGL_Texture *texture = g_Textures + command->createTexture.id;

            if (texture->handle != 0)
            {
                glDeleteTextures(1, &texture->handle);
            }

            texture->handle =
                OpenGL_CreateTexture(command->createTexture.pixels,
                    command->createTexture.width, command->createTexture.height,
                    command->createTexture.colorFormat,
                    command->createTexture.samplerMode);
            texture->type = GL_TEXTURE_2D;
        }
        break;
        case RenderCommand_CreateCubeMapTypeId:
        {
            Assert(command->createTexture.id < ArrayCount(g_Textures));
            OpenGL_Texture *texture = g_Textures + command->createTexture.id;

            if (texture->handle != 0)
            {
                glDeleteTextures(1, &texture->handle);
            }

            texture->handle = OpenGL_CreateCubeMap(
                command->createCubeMap.pixels, command->createCubeMap.width,
                command->createCubeMap.colorFormat);
            texture->type = GL_TEXTURE_CUBE_MAP;
        }
        break;
        case RenderCommand_CreateUniformBufferTypeId:
        {
            Assert(
                command->createUniformBuffer.id < ArrayCount(g_UniformBuffers));

            OpenGL_UniformBuffer *buffer =
                g_UniformBuffers + command->createUniformBuffer.id;

            if (buffer->ubo != 0)
            {
                OpenGL_DeleteUniformBuffer(buffer);
            }

            *buffer = OpenGL_CreateUniformBuffer(
                command->createUniformBuffer.capacity);
        }
        break;
        case RenderCommand_UpdateUniformBufferTypeId:
        {
            Assert(
                command->updateUniformBuffer.id < ArrayCount(g_UniformBuffers));

            OpenGL_UniformBuffer *buffer =
                g_UniformBuffers + command->updateUniformBuffer.id;

            Assert(command->updateUniformBuffer.length +
                       command->updateUniformBuffer.offset <=
                   buffer->capacity);

            glBindBuffer(GL_UNIFORM_BUFFER, buffer->ubo);
            glBufferSubData(GL_UNIFORM_BUFFER,
                command->updateUniformBuffer.offset,
                command->updateUniformBuffer.length,
                command->updateUniformBuffer.data);
            glBindBuffer(GL_UNIFORM_BUFFER, 0);
        }
        break;
        case RenderCommand_CreateRenderTargetTypeId:
        {
            Assert(
                command->createRenderTarget.id < ArrayCount(g_RenderTargets));
            Assert(command->createRenderTarget.id != RenderTarget_BackBuffer);

            OpenGL_RenderTarget *renderTarget =
                g_RenderTargets + command->createRenderTarget.id;

            if (renderTarget->fbo != 0)
            {
                OpenGL_DeleteRenderTarget(*renderTarget);
            }

            *renderTarget =
                OpenGL_CreateRenderTarget(command->createRenderTarget.width,
                    command->createRenderTarget.height,
                    command->createRenderTarget.isShadowBuffer);
        }
        break;
        case RenderCommand_CreateVertexBufferTypeId:
        {
            Assert(
                command->createVertexBuffer.id < ArrayCount(g_VertexBuffers));

            OpenGL_VertexBuffer *vertexBuffer =
                g_VertexBuffers + command->createVertexBuffer.id;

            if (vertexBuffer->vao != 0)
            {
                OpenGL_DeleteVertexBuffer(*vertexBuffer);
            }

            *vertexBuffer = OpenGL_CreateVertexBuffer(
                command->createVertexBuffer.vertexLayout,
                command->createVertexBuffer.vertexCount);
        }
        break;
        case RenderCommand_UpdateVertexBufferTypeId:
        {
            Assert(
                command->updateVertexBuffer.id < ArrayCount(g_VertexBuffers));

            OpenGL_VertexBuffer vertexBuffer =
                g_VertexBuffers[command->updateVertexBuffer.id];

            OpenGL_UpdateVertexBuffer(vertexBuffer,
                command->updateVertexBuffer.vertexLayout,
                command->updateVertexBuffer.vertices,
                command->updateVertexBuffer.vertexCount);
        }
        break;
        default:
            break;
        }
    }

    for (u32 i = 0; i < count; ++i)
    {
        RenderCommand *command = commands + i;

        switch (command->type)
        {
        case RenderCommand_DrawMeshTypeId:
        {
            Assert(command->drawMesh.shader < ArrayCount(g_Shaders));
            OpenGL_Shader *shader = g_Shaders + command->drawMesh.shader;
            if (OpenGL_BindShader(shader, command->drawMesh.uniformValues,
                    command->drawMesh.uniformValueCount))
            {
                Assert(command->drawMesh.mesh < ArrayCount(g_Meshes));
                OpenGL_Mesh *mesh = g_Meshes + command->drawMesh.mesh;
                glBindVertexArray(mesh->vertexArray);
                glDrawElements(
                    GL_TRIANGLES, mesh->indexCount, GL_UNSIGNED_INT, NULL);
                glBindVertexArray(0);
            }
        }
        break;
        case RenderCommand_DrawInstancedMeshTypeId:
        {
            Assert(command->drawInstancedMesh.shader < ArrayCount(g_Shaders));
            OpenGL_Shader *shader = g_Shaders + command->drawInstancedMesh.shader;
            if (OpenGL_BindShader(shader, command->drawInstancedMesh.uniformValues,
                    command->drawInstancedMesh.uniformValueCount))
            {
                Assert(command->drawInstancedMesh.mesh < ArrayCount(g_Meshes));
                OpenGL_Mesh *mesh = g_Meshes + command->drawInstancedMesh.mesh;
                Assert(command->drawInstancedMesh.instanceCount <
                       mesh->maxInstanceCount);
                Assert(mesh->modelMatrixBuffer != 0);
                glBindBuffer(GL_ARRAY_BUFFER, mesh->modelMatrixBuffer);
                glBufferSubData(GL_ARRAY_BUFFER, 0,
                    command->drawInstancedMesh.instanceCount * sizeof(mat4),
                    command->drawInstancedMesh.modelMatrices);

                glBindVertexArray(mesh->vertexArray);
                glDrawElementsInstanced(GL_TRIANGLES, mesh->indexCount,
                    GL_UNSIGNED_INT, NULL,
                    command->drawInstancedMesh.instanceCount);
                glBindVertexArray(0);
            }
        }
        break;
        case RenderCommand_BindRenderTargetTypeId:
        {
            Assert(command->bindRenderTarget.id < ArrayCount(g_RenderTargets));

            if (command->bindRenderTarget.id == RenderTarget_BackBuffer)
            {
                glBindFramebuffer(GL_FRAMEBUFFER, 0);
                glViewport(0, 0, windowWidth, windowHeight);
            }
            else
            {
                OpenGL_RenderTarget *renderTarget =
                    g_RenderTargets + command->bindRenderTarget.id;

                glBindFramebuffer(GL_FRAMEBUFFER, renderTarget->fbo);
                glViewport(0, 0, renderTarget->width, renderTarget->height);
            }
        }
        break;
        case RenderCommand_ClearTypeId:
        {
            vec4 clearColor = command->clear.color;
            glClearColor(
                clearColor.r, clearColor.g, clearColor.b, clearColor.a);
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        }
        break;
        case RenderCommand_DrawVertexBufferTypeId:
        {
            Assert(command->drawVertexBuffer.shader < ArrayCount(g_Shaders));
            OpenGL_Shader *shader =
                g_Shaders + command->drawVertexBuffer.shader;
            if (OpenGL_BindShader(shader,
                    command->drawVertexBuffer.uniformValues,
                    command->drawVertexBuffer.uniformValueCount))
            {
                Assert(
                    command->drawVertexBuffer.id < ArrayCount(g_VertexBuffers));
                OpenGL_VertexBuffer vertexBuffer =
                    g_VertexBuffers[command->drawVertexBuffer.id];

                u32 primitive = 0;
                if (command->drawVertexBuffer.primitive == Primitive_Triangles)
                {
                    primitive = GL_TRIANGLES;
                }
                else if (command->drawVertexBuffer.primitive == Primitive_Lines)
                {
                    primitive = GL_LINES;
                }
                else
                {
                    InvalidCodePath();
                }

                glBindVertexArray(vertexBuffer.vao);
                glDrawArrays(primitive, command->drawVertexBuffer.firstVertex,
                    command->drawVertexBuffer.vertexCount);
                glBindVertexArray(0);
            }
        }
        break;
        default:
            break;
        }
    }
}
