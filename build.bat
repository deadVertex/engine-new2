@echo off

set Defines=-DDEBUG_FIXED_ADDRESSES -DPLATFORM_WINDOWS
set Libraries=..\windows-dependencies\lib\SDL2.lib ..\windows-dependencies\lib\glew32.lib ..\windows-dependencies\lib\glfw3dll.lib user32.lib winmm.lib gdi32.lib opengl32.lib Ws2_32.lib

set CompilerFlags=-Od -MT -F16777216 -nologo -Gm- -GR- -EHa -W4 -WX -wd4305 -wd4127 -wd4201 -wd4189 -wd4100 -wd4996 -wd4505 -FC -Z7 -I..\src -I..\thirdparty -I..\windows-dependencies\include %Defines%
set LinkerFlags=-opt:ref -incremental:no %Libraries%

REM All files are created in the build directory
IF NOT EXIST build mkdir build
pushd build

REM cl %CompilerFlags% ..\src\raytracer\main.cpp -I..\windows-dependencies\include -Fe:raytracer.exe -link %LinkerFlags%
REM set LastError=%ERRORLEVEL%
REM raytracer.exe
REM start output.bmp

ctime -begin ..\game.ctm
REM Build game library
echo WATING FOR PDB > lock.tmp
del game_*.pdb > NUL 2> NUL
cl %CompilerFlags%  ..\src\game.cpp -LD -link -PDB:game_%random%.pdb -DLL -EXPORT:GameUpdate -EXPORT:GameGetSoundSamples -EXPORT:GameServerUpdate %LinkerFlags%
del lock.tmp
ctime -end ..\game.ctm

REM cl %CompilerFlags%  -d2cgsummary ..\src\game.cpp -LD -link -PDB:game_%random%.pdb -DLL -EXPORT:GameUpdate -EXPORT:GameGetSoundSamples -EXPORT:GameServerUpdate %LinkerFlags% > game.d2
REM cl %CompilerFlags%  -Bt ..\src\game.cpp -LD -link -PDB:game_%random%.pdb -DLL -EXPORT:GameUpdate -EXPORT:GameGetSoundSamples -EXPORT:GameServerUpdate %LinkerFlags% > game.bt
REM cl %CompilerFlags%  -d1reportTime ..\src\game.cpp -LD -link -PDB:game_%random%.pdb -DLL -EXPORT:GameUpdate -EXPORT:GameGetSoundSamples -EXPORT:GameServerUpdate %LinkerFlags% > game.d1
REM cl %CompilerFlags% -P ..\src\game.cpp
REM cl %CompilerFlags% -P ..\src\platform_main.cpp

ctime -begin ..\exe.ctm
REM Build platform executable
cl %CompilerFlags% ..\src\platform_main.cpp -Fe:windows_main.exe -link %LinkerFlags%
ctime -end ..\exe.ctm
popd
