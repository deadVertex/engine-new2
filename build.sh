#! /bin/bash

BUILD_GAME=1
BUILD_UNIT_TESTS=0
RUN_UNIT_TESTS=0
BUILD_RAY_TRACER=0
RUN_RAY_TRACER=0
DEBUG_COMPILE_TIMES=0
CALCULATE_BUILD_TIMES=0

# Need to add -lm to platform command line with clang on arch to link against math library
# Need -Wno-unused-but-set-variable when compiling with GCC
COMPILER="clang++"
COMMON_CFLAGS="-g -ggdb -Wall -Wextra -Werror -std=c++14 -Isrc -Ithirdparty \
-Wno-unused-variable  \
-Wno-missing-braces \
-Wno-unused-parameter \
-Wno-unused-function \
-DPLATFORM_LINUX \
"

if [ "$COMPILER" == "clang++" ] && [ $DEBUG_COMPILE_TIMES -ne 0 ]; then
    COMMON_CFLAGS="$COMMON_CFLAGS -ftime-trace"
fi

if [ "$COMPILER" == "g++" ]; then
    COMMON_CFLAGS="$COMMON_CFLAGS -Wno-unused-but-set-variable"
fi

if [ $CALCULATE_BUILD_TIMES -ne 0 ]; then
    COMPILER="time $COMPILER"
fi

mkdir -p build

if [ $BUILD_RAY_TRACER -ne 0 ]; then
    CFLAGS="-O2 -mavx2 $COMMON_CFLAGS -DSIMD_LANE_WIDTH=8"

    echo "Building ray tracer..."
    $COMPILER src/raytracer/main.cpp -o build/raytracer $CFLAGS -lm -lpthread

    if [ $RUN_RAY_TRACER -ne 0 ]; then
        if [ $? -eq 0 ]; then
            echo "Running ray tracer..."
            pushd build
            ./raytracer
            popd
        fi
    fi
fi

if [ $BUILD_GAME -ne 0 ]; then
    CFLAGS="-O0 $COMMON_CFLAGS"

    echo "Building game..."
    # Build game runtime
    eval $COMPILER src/game.cpp -o build/game_temp.so $CFLAGS -shared -rdynamic -fPIC

    mv build/game_temp.so build/game.so

    echo "Building platform executable..."
    # Build platform executable
    eval $COMPILER src/platform_main.cpp -o build/linux_main $CFLAGS -lGLEW -lglfw -lSDL2 -lGL -ldl -lm
fi

if [ $BUILD_UNIT_TESTS -ne 0 ]; then
    CFLAGS="-O0 $COMMON_CFLAGS"

    echo "Building unit tests..."

    $COMPILER src/collision_detection_tests.cpp -o build/unit_tests $CFLAGS -lm

    if [ $RUN_UNIT_TESTS -ne 0 ]; then
        if [ $? -eq 0 ]; then
            echo "Running unit tests..."
            pushd build >/dev/null
            ./unit_tests
            popd >/dev/null
        fi
    fi
fi

